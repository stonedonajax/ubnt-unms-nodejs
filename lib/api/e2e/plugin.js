'use strict';

/*
 * Hapijs Plugin definition
 */

const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../../util/hapi');
const { registerRoutes } = require('./routes');
const { getAesKeys, getDiscoveryResults, reset, setup } = require('./service');

function register(server, options) {
  const {
    DB, dal, discovery, settings, deviceStore, macAesKeyStore, messageHub, firmwareDal, scheduler, store, nginx,
  } = server.plugins;

  /**
   * @name E2EService
   * @type {{
   *    getAesKeys: function():Promise.<CorrespondenceMacAesKey[]>,
   *    getDiscoveryResults: function():Promise.<CorrespondenceDiscoveryResult[]>,
   *    reset: function():Promise.<void>,
   *    setup: function():Promise.<void>,
   * }}
   */
  const service = {
    getAesKeys: weave(getAesKeys, { dal }),
    getDiscoveryResults: weave(getDiscoveryResults, { DB, dal, discovery, settings, deviceStore, firmwareDal }),
    reset: weave(reset, { dal, DB, deviceStore, macAesKeyStore, messageHub, firmwareDal, scheduler, settings, store }),
    setup: weave(setup, { dal, DB, messageHub, settings, nginx }),
  };

  server.expose(service);

  registerRoutes(server, options, service);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'e2e',
  version: '1.0.0',
  dependencies: [
    'DB', 'dal', 'discovery', 'settings', 'deviceStore', 'firmwareDal', 'macAesKeyStore', 'messageHub', 'scheduler',
    'store', 'nginx',
  ],
};
