'use strict';

const joi = require('joi');
const validation = require('../../validation');
const enums = require('../../enums');

/*
 * Route definitions
 */

function registerRoutes(server, options, service) {
  server.route({
    method: 'GET',
    path: '/e2e/aes-keys',
    config: {
      auth: false,
    },
    handler(request, reply) {
      reply(
        service.getAesKeys()
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/e2e/discovery-results',
    config: {
      auth: false,
    },
    handler(request, reply) {
      reply(
        service.getDiscoveryResults()
      );
    },
  });

  server.route({
    method: 'POST',
    path: '/e2e/reset',
    config: {
      auth: false,
    },
    handler(request, reply) {
      reply(
        service.reset()
      );
    },
  });

  server.route({
    method: 'POST',
    path: '/e2e/setup',
    config: {
      auth: false,
      validate: {
        payload: joi.object({
          user: joi.object().keys({
            username: validation.username,
            password: validation.password,
            email: joi.string().email().required(),
          }),
          settings: joi.object().keys({
            hostname: validation.hostname.optional(),
            locale: joi.object({
              longDateFormat: joi.object({
                LT: joi.string().valid(enums.TimeFormatEnum).required(),
                LL: joi.string().valid(enums.DateFormatEnum).required(),
              }).required(),
            }).optional(),
            timezone: validation.timezone.optional(),
            smtp: validation.smtp.optional(),
          }).optional(),
        }),
      },
    },
    handler(request, reply) {
      const { user, settings } = request.payload;
      reply(
        service.setup(user, settings)
      );
    },
  });
}

module.exports = {
  registerRoutes,
};
