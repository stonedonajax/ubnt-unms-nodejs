'use strict';

const { Reader: reader, Either } = require('monet');
const { weave } = require('ramda-adjunct');
const { evolve, map } = require('ramda');
const uuid = require('uuid');
const { Observable } = require('rxjs/Rx');

const { allP } = require('../../util');
const { FirmwareOriginEnum } = require('../../enums');
const { generatePasswordHash, createToken, signUserToken } = require('../../auth');
const { merge, mergeRight } = require('../../transformers');
const { fromDbNms, toDbNms } = require('../../transformers/nms');
const { toDbUser, toDbUserProfile } = require('../../transformers/user');
const { fromDbList: fromDbDeviceList } = require('../../transformers/device');
const { fromDbList: fromDbDiscoveryDeviceList } = require('../../transformers/discovery/device');
const { fromDbList: fromDbDiscoveryResultList } = require('../../transformers/discovery/result');
const { mergeWithDiscoveryDeviceList } = require('../../transformers/discovery/result/mergers');
const { mergeCorrespondenceDeviceList } = require('../../transformers/discovery/device/mergers');
const { fromDbList: fromDbMacAesKeyList } = require('../../transformers/mac-aes-key');

/**
 * @param {CorrespondenceDiscoveryResult} cmDiscoveryResult
 * @return {loadDiscoveryDeviceList~callback}
 */
const loadDiscoveryDeviceList = cmDiscoveryResult => reader(
  /**
   * @function loadDiscoveryDeviceList~callback
   * @param {DbDal} dal
   * @param {DB} DB
   * @param {DeviceStore} deviceStore
   * @param {Discovery} discovery
   * @param {FirmwareDal} firmwareDal
   * @return {Observable.<CorrespondenceDiscoveryResult>}
   */
  ({ dal, DB, deviceStore, discovery, firmwareDal }) => Observable.forkJoin(
    dal.discoveryDeviceRepository.findAll({ where: { resultId: cmDiscoveryResult.id } }),
    DB.device.list()
  ).mergeEither(([dbDiscoveryDeviceList, dbDeviceList]) =>
    fromDbDiscoveryDeviceList({ firmwareDal, discovery }, dbDiscoveryDeviceList)
      .chain(mergeRight(mergeCorrespondenceDeviceList, fromDbDeviceList({ deviceStore, firmwareDal }, dbDeviceList)))
      .chain(merge(mergeWithDiscoveryDeviceList, Either.of(cmDiscoveryResult))))
);

/**
 * @return {Reader.<getAesKeys~callback>}
 */
const getAesKeys = () => reader(
  /**
   * @function getAesKeys~callback
   * @param {DbDal} dal
   * @return {Promise.<CorrespondenceMacAesKey[]>}
   */
  ({ dal }) => Observable.from(dal.macAesKeyRepository.findAll())
    .mergeEither(fromDbMacAesKeyList({}))
    .map(map(evolve({
      key: key => key.toString('base64'),
    })))
    .toPromise()
);

/**
 * @return {Reader.<getDiscoveryResults~callback>}
 */
const getDiscoveryResults = () => reader(
  /**
   * @function getDiscoveryResults~callback
   * @param {DbDal} dal
   * @param {DB} DB
   * @param {DeviceStore} deviceStore
   * @param {Discovery} discovery
   * @param {FirmwareDal} firmwareDal
   * @return {Promise.<CorrespondenceDiscoveryResult[]>}
   */
  ({ dal, DB, deviceStore, discovery, firmwareDal }) => Observable.from(dal.discoveryResultRepository.findAll())
    .mergeEither(fromDbDiscoveryResultList({}))
    .mergeAll()
    .mergeMap(weave(loadDiscoveryDeviceList, { dal, DB, deviceStore, discovery, firmwareDal }))
    .toArray()
    .toPromise()
);

const reset = () => reader(
  ({ dal, DB, deviceStore, macAesKeyStore, messageHub, firmwareDal, scheduler, settings, store }) => {
    // stop periodic tasks
    scheduler.pause();

    return Observable.concat(
      // remove AES keys and settings
      macAesKeyStore.removeAll(),
      DB.nms.flushRedis().then(() => settings.loadSettings()), // reload default settings
      // disconnect devices
      ...deviceStore.findAll().map(commDevice => commDevice.connection.close()),
      Observable.timer(1000), // delay for actions to complete
      Observable.defer(() => {
        store.clear(); // will clear outages etc...

        return allP([
          DB.nms.flushRedis(),
          firmwareDal.removeAll(FirmwareOriginEnum.Manual),
          dal.deviceMetadataRepository.removeAll(),
          dal.dataLinkRepository.removeAll(),
          dal.discoveryResultRepository.removeAll(),
          dal.outageRepository.removeAll(),
          dal.logRepository.removeAll(),
          dal.taskRepository.removeAll(),
          dal.userRepository.removeAll(),
          dal.siteRepository.removeAll(),
          dal.deviceSiteRepository.clearAllRelations(),
        ]);
      })
    ).finally(() => {
      // resume periodic tasks
      scheduler.resume();
      messageHub.publish(messageHub.messages.settingsChanged());
    })
      .last()
      .mapTo({ result: true, message: 'Application reset.' })
      .toPromise();
  }
);

const setup = (user, nms) => reader(
  ({ dal, DB, messageHub, settings, nginx }) => {
    const cmUser = {
      id: uuid.v4(),
      username: user.username,
      password: generatePasswordHash(user.password),
      email: user.email,
      totpAuthEnabled: false,
      totpAuthSecret: null,
      role: null,
      profile: {
        alerts: false,
        presentationMode: false,
        forceChangePassword: false,
        lastLogItemId: null,
        tableConfig: null,
      },
    };

    const settings$ = Observable.defer(DB.nms.get)
      .mergeEither(fromDbNms({}))
      .map(cmNms => Object.assign(cmNms, nms, { isConfigured: true }))
      .tapO(cmNms => Observable.fromEither(toDbNms(cmNms)).mergeMap(dbNms => DB.nms.update(dbNms)))
      .tapO(() => settings.loadSettings())
      .mergeMap(cmNms => nginx.updateSslCertificate(cmNms.useLetsEncrypt))
      .do(() => messageHub.publish(messageHub.messages.settingsChanged()));

    const user$ = Observable.fromEither(toDbUser(cmUser))
      .mergeMap(dbUser => dal.userRepository.create(dbUser))
      .tapO(() => Observable.fromEither(toDbUserProfile(cmUser))
        .mergeMap(dbUserProfile => dal.userProfileRepository.upsert(dbUserProfile))
      )
      .mapTo(createToken(1000000, cmUser))
      .tapO(token => DB.token.insert(token))
      .mergeMap(token => signUserToken(token).promise())
      .map(signedToken => ({ user: cmUser, token: signedToken }));

    return Observable.merge(
      settings$.ignoreElements(),
      user$
    ).toPromise();
  }
);

module.exports = {
  getAesKeys,
  getDiscoveryResults,
  reset,
  setup,
};
