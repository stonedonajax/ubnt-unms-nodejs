'use strict';

const validation = require('../../../validation');


/*
 * Hapijs routes definition
 */

function registerRoutes(server, options, service) {
  server.route({
    method: 'GET',
    path: '/v2.0/devices/aircubes/{id}',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const { id: deviceId } = request.params;

      reply(
        service.airCubeDetail(deviceId)
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.0/devices/aircubes/{id}/stations',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      reply(
        service.airCubeStations(request.params.id)
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.0/devices/aircubes/{id}/frequency-lists',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      reply(
        service.getFrequencyList(request.params.id)
      );
    },
  });

  server.route({
    method: 'PUT',
    path: '/v2.0/devices/aircubes/{id}/config/wireless',
    config: {
      description: 'Update AirCube wireless config',
      tags: ['api', 'aircubes'],
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      reply({ result: true, message: 'Update wireless config success.' });
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.0/devices/aircubes/{id}/config/network',
    config: {
      description: 'Get AirCube network config',
      tags: ['api', 'aircubes'],
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      reply(
        service.getNetworkConfig(request.params.id)
      );
    },
  });

  server.route({
    method: 'PUT',
    path: '/v2.0/devices/aircubes/{id}/config/network',
    config: {
      description: 'Update AirCube network config',
      tags: ['api', 'aircubes'],
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      reply(
        ({ result: true, message: 'Update network config success.' })
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.0/devices/aircubes/{id}/config/system',
    config: {
      description: 'Get AirCube system config',
      tags: ['api', 'aircubes'],
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      reply(
        service.getSystemConfig(request.params.id)
      );
    },
  });

  server.route({
    method: 'PUT',
    path: '/v2.0/devices/aircubes/{id}/config/system',
    config: {
      description: 'Update AirCube system settings',
      tags: ['api', 'aircubes'],
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      reply(
        ({ result: true, message: 'Update system config success.' })
      );
    },
  });
}

module.exports = {
  registerRoutes,
};
