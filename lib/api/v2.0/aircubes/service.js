'use strict';

const { Reader: reader } = require('monet');
const { pathOr, pipeP } = require('ramda');
const { EntityEnum } = require('../../../enums');
const { entityExistsCheck, resolveP } = require('../../../util');
const { tap } = require('lodash/fp');


/**
 * AirCube detail.
 */

const airCubeDetail = deviceId => reader(
  ({ fixtures }) => pipeP(
    resolveP,
    fixtures.devices.getDeviceWithStations,
    tap(entityExistsCheck(EntityEnum.Device))
  )(deviceId)
);

const airCubeStations = deviceId => reader(
  ({ fixtures }) => pipeP(
    resolveP,
    fixtures.devices.getDeviceWithStations,
    pathOr(null, ['aircube', 'stations'])
  )(deviceId)
);

const getFrequencyList = () => reader(
  ({ fixtures }) => fixtures.devices.getDeviceFrequencyList()
);

const getNetworkConfig = deviceId => reader(
  ({ fixtures }) => fixtures.devices.getDeviceNetwork(deviceId)
);

const getSystemConfig = deviceId => reader(
  ({ fixtures }) => fixtures.devices.getDeviceSystem(deviceId)
);

module.exports = {
  airCubeDetail,
  airCubeStations,

  getFrequencyList,
  getNetworkConfig,
  getSystemConfig,
};

