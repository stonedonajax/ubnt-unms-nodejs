'use strict';

const { registerPlugin } = require('../../util/hapi');

/*
 * Route definitions
 */
function register(server) {
  server.route({
    method: 'GET',
    path: '/v2.0/dashboard',
    handler(request, reply) {
      setTimeout(() => {
        reply({
          network: {
            status: true,
            version: '1.3.11',
          },
          notifications: [
            {
              author: { userName: 'Petr' },
              createdAt: '12.2.2017',
              message: 'Hale has been disconected for over 30 seconds.',
            },
            {
              author: { userName: 'Nada' },
              createdAt: '20.2.2017',
              message: 'Helium has been disconected for over 40 seconds.',
            },
          ],
          sites: {
            active: 75,
            all: 96,
          },
          clients: {
            active: 374,
            all: 435,
          },
        });
      }, 5000);
    },
  });
}

/*
 * Hapijs Plugin definition
 */
exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'dashboard_v2.0',
  version: '1.0.0',
  dependencies: ['fixtures'],
};
