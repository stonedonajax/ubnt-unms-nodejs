'use strict';

/*
 * Hapijs Plugin definition
 */

const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../../../util/hapi');
const { DB } = require('../../../db');
const { registerRoutes } = require('./routes');
const {
  airCubeDetail, listStations, getFrequencyLists, updateWirelessConfig, getNetworkConfig, updateNetworkConfig,
  getSystemConfig, updateSystemConfig,
} = require('./service');
const {
  deviceDetail, stations, getFrequencyLists: getFrequencyListsView,
  updateWirelessConfig: updateWirelessConfigView, getNetworkConfig: getNetworkConfigView,
  updateNetworkConfig: updateNetworkConfigView, getSystemConfig: getSystemConfigView,
  updateSystemConfig: updateSystemConfigView,
} = require('./view');


function register(server, options) {
  const { deviceStore, firmwareDal, dal } = server.plugins;
  const airCubeDetailBound = weave(airCubeDetail, { DB, deviceStore, firmwareDal, dal });
  const listStationsBound = weave(listStations, { deviceStore });
  const getFrequencyListsBound = weave(getFrequencyLists, { deviceStore });
  const updateWirelessConfigBound = weave(updateWirelessConfig, { deviceStore });
  const getNetworkConfigBound = weave(getNetworkConfig, { deviceStore });
  const updateNetworkConfigBound = weave(updateNetworkConfig, { deviceStore });
  const getSystemConfigBound = weave(getSystemConfig, { deviceStore });
  const updateSystemConfigBound = weave(updateSystemConfig, { deviceStore });
  const service = {
    airCubeDetail: airCubeDetailBound,
    listStations: listStationsBound,
    getFrequencyLists: getFrequencyListsBound,
    updateWirelessConfig: updateWirelessConfigBound,
    getNetworkConfig: getNetworkConfigBound,
    updateNetworkConfig: updateNetworkConfigBound,
    getSystemConfig: getSystemConfigBound,
    updateSystemConfig: updateSystemConfigBound,
  };

  const view = {
    deviceDetail: weave(deviceDetail, { service }),
    stations: weave(stations, { service }),
    getFrequencyLists: weave(getFrequencyListsView, { service }),
    updateWirelessConfig: weave(updateWirelessConfigView, { service }),
    getNetworkConfig: weave(getNetworkConfigView, { service }),
    updateNetworkConfig: weave(updateNetworkConfigView, { service }),
    getSystemConfig: weave(getSystemConfigView, { service }),
    updateSystemConfig: weave(updateSystemConfigView, { service }),
  };

  server.expose(service);

  registerRoutes(server, options, view);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiAirCubesV2.1',
  version: '2.1.0',
  dependencies: ['deviceStore', 'firmwareDal'],
};
