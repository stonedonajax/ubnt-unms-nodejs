'use strict';

const validation = require('../../../validation');
const { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } = require('http-status');
const {
  ErrorSchema, AuthHeaderSchema, AirCubeWifiConfigSchema, StatusSchema, FrequencyListSchema, AirCubeNetworkConfigSchema,
  AirCubeSystemConfigSchema,
} = require('../osm');

/*
 * Hapijs routes definition
 */

function registerRoutes(server, options, view) {
  server.route({
    method: 'GET',
    path: '/v2.1/devices/aircubes/{id}',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler: view.deviceDetail,
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/aircubes/{id}/stations',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler: view.stations,
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/aircubes/{id}/frequency-lists',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
      response: {
        status: {
          [OK]: FrequencyListSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.getFrequencyLists,
  });

  // TODO(jan.beseda@ubnt.com): add mock api route
  server.route({
    method: 'PUT',
    path: '/v2.1/devices/aircubes/{id}/config/wireless',
    config: {
      description: 'Update AirCube wireless config',
      tags: ['api', 'aircubes'],
      auth: {
        scope: 'admin',
      },
      validate: {
        headers: AuthHeaderSchema,
        params: {
          id: validation.deviceId,
        },
        payload: AirCubeWifiConfigSchema,
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.updateWirelessConfig,
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/aircubes/{id}/config/network',
    config: {
      description: 'Get AirCube network config',
      tags: ['api', 'aircubes'],
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
      response: {
        status: {
          [OK]: AirCubeNetworkConfigSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.getNetworkConfig,
  });

  server.route({
    method: 'PUT',
    path: '/v2.1/devices/aircubes/{id}/config/network',
    config: {
      description: 'Update AirCube network config',
      tags: ['api', 'aircubes'],
      auth: {
        scope: 'admin',
      },
      validate: {
        headers: AuthHeaderSchema,
        params: {
          id: validation.deviceId,
        },
        payload: AirCubeNetworkConfigSchema,
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.updateNetworkConfig,
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/aircubes/{id}/config/system',
    config: {
      description: 'Get AirCube system config',
      tags: ['api', 'aircubes'],
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
      response: {
        status: {
          [OK]: AirCubeSystemConfigSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.getSystemConfig,
  });

  server.route({
    method: 'PUT',
    path: '/v2.1/devices/aircubes/{id}/config/system',
    config: {
      description: 'Update AirCube system settings',
      tags: ['api', 'aircubes'],
      auth: {
        scope: 'admin',
      },
      validate: {
        headers: AuthHeaderSchema,
        params: {
          id: validation.deviceId,
        },
        payload: AirCubeSystemConfigSchema,
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.updateSystemConfig,
  });
}

module.exports = {
  registerRoutes,
};
