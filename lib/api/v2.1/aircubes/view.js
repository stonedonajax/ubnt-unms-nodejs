'use strict';

const { Reader: reader } = require('monet');
const { cata } = require('ramda-adjunct');
const { Observable } = require('rxjs');

const { resolveP, rejectP } = require('../../../util');
const { toApiAirCubeStatusDetail } = require('../../../transformers/device');
const { toApiStationsList } = require('../../../transformers/device/aircube');
const {
  parseApiWirelessConfig, parseApiNetworkConfig, parseApiSystemConfig,
} = require('../../../transformers/device/aircube/parsers');
const { toApiFrequencyLists, toApiNetworkConfig, toApiSystemConfig,
} = require('../../../transformers/device/aircube/mappers');

const deviceDetail = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      service
        .airCubeDetail(deviceId)
        .then(toApiAirCubeStatusDetail)
        .then(cata(rejectP, resolveP))
    );
  }
);

const stations = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      service.listStations(deviceId)
        .mergeEither(toApiStationsList)
        .toPromise()
    );
  }
);

const getFrequencyLists = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      service.getFrequencyLists(deviceId)
        .map(toApiFrequencyLists)
        .toPromise()
    );
  }
);

const updateWirelessConfig = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      Observable.of(parseApiWirelessConfig(request.payload))
        .mergeMap(service.updateWirelessConfig(deviceId))
        .mapTo({ result: true, message: 'Wireless config updated.' })
        .toPromise()
    );
  }
);

const getNetworkConfig = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      service.getNetworkConfig(deviceId)
        .map(toApiNetworkConfig)
        .toPromise()
    );
  }
);

const updateNetworkConfig = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      Observable.of(parseApiNetworkConfig(request.payload))
        .mergeMap(service.updateNetworkConfig(deviceId))
        .mapTo({ result: true, message: 'Network config updated.' })
        .toPromise()
    );
  }
);

const getSystemConfig = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      service.getSystemConfig(deviceId)
        .map(toApiSystemConfig)
        .toPromise()
    );
  }
);

const updateSystemConfig = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      Observable.of(parseApiSystemConfig(request.payload))
        .mergeMap(service.updateSystemConfig(deviceId))
        .mapTo({ result: true, message: 'System settings updated.' })
        .toPromise()
    );
  }
);

module.exports = {
  deviceDetail,
  stations,
  getFrequencyLists,
  updateWirelessConfig,
  getNetworkConfig,
  updateNetworkConfig,
  getSystemConfig,
  updateSystemConfig,
};
