'use strict';

const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../../../util/hapi');
const { DB } = require('../../../db');
const serviceModule = require('./service');
const viewModule = require('./view');
const { registerRoutes } = require('./routes');


function register(server, options) {
  const { deviceStore, firmwareDal, dal } = server.plugins;


  const service = {
    airFiberDetail: weave(serviceModule.airFiberDetail, { DB, deviceStore, firmwareDal, dal }),
    listStations: weave(serviceModule.listStations, { deviceStore }),
  };

  const view = {
    deviceDetail: weave(viewModule.deviceDetail, { service }),
    stations: weave(viewModule.stations, { service }),
  };

  server.expose(service);

  registerRoutes(server, options, view);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiAirFibersV2.1',
  version: '2.1.0',
  dependencies: ['deviceStore', 'firmwareDal'],
};
