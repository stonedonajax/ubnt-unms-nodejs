'use strict';

const { Reader: reader } = require('monet');
const { when } = require('ramda');
const { isNotNull } = require('ramda-adjunct');
const { tap } = require('lodash/fp');

const { resolveP, rejectP, allP, entityExistsCheck } = require('../../../util');
const { EntityEnum } = require('../../../enums');

const { parseDbDeviceSiteId } = require('../../../transformers/device/parsers');
const { fromDb: fromDbDevice } = require('../../../transformers/device');
const { merge: mergeM } = require('../../../transformers');
const { mergeMetadata } = require('../../../transformers/device/mergers');
const { fromDb: fromDbDeviceMetadata } = require('../../../transformers/device/metadata');

/*
 * AirFiber detail.
 */

const airFiberDetail = airFiberId => reader(
  ({ DB, deviceStore, firmwareDal, dal }) => {
    const dbAirFiberPromise = DB.airFiber.findById(airFiberId)
      .then(tap(entityExistsCheck(EntityEnum.AirFiber)));
    const dbSitePromise = dbAirFiberPromise
      .then(parseDbDeviceSiteId)
      .then(when(isNotNull, dal.siteRepository.findOneById));
    const dbDeviceMetadataPromise = dal.deviceMetadataRepository.findById(airFiberId);

    return allP([dbSitePromise, dbAirFiberPromise, dbDeviceMetadataPromise])
      .then(([dbSite, dbAirFiber, dbDeviceMetadata]) =>
        fromDbDevice({ firmwareDal, deviceStore, dbSite }, dbAirFiber)
          .chain(mergeM(mergeMetadata, fromDbDeviceMetadata({}, dbDeviceMetadata)))
          .cata(rejectP, resolveP));
  }
);

const listStations = deviceId => reader(
  ({ deviceStore }) => deviceStore.getOrThrow(deviceId)
    .mergeMap(commDevice => commDevice.getStations())
);

module.exports = {
  airFiberDetail,
  listStations,
};
