'use strict';

const { map } = require('lodash/fp');

/**
 * @param {CorrespondenceStation} cmStation
 * @return {Object}
 */
const toApiStation = cmStation => ({
  timestamp: cmStation.timestamp,
  ipAddress: cmStation.ipAddress,
  name: cmStation.name,
  vendor: cmStation.vendor,
  radio: cmStation.radio,
  mac: cmStation.mac,
  upTime: cmStation.upTime,
  latency: cmStation.latency,
  distance: cmStation.distance,
  rxBytes: cmStation.rxBytes,
  txBytes: cmStation.txBytes,
  rxRate: cmStation.rxRate,
  txRate: cmStation.txRate,
  rxSignal: cmStation.rxSignal,
  txSignal: cmStation.txSignal,
  downlinkCapacity: cmStation.downlinkCapacity,
  uplinkCapacity: cmStation.uplinkCapacity,
  deviceIdentification: cmStation.deviceIdentification,
});

/**
 * @function toApiStationsList
 * @param {CorrespondenceStation[]} cmStationList
 * @return {Object}
 */
const toApiStationsList = map(toApiStation);

module.exports = {
  toApiStationsList,
};
