'use strict';

const { Reader: reader } = require('monet');
const { cata } = require('ramda-adjunct');

const { resolveP, rejectP } = require('../../../util');
const { toApiAirFiberStatusDetail } = require('../../../transformers/device');
const { toApiStationsList } = require('./transformers/mappers');

const deviceDetail = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      service
        .airFiberDetail(deviceId)
        .then(toApiAirFiberStatusDetail)
        .then(cata(rejectP, resolveP))
    );
  }
);

const stations = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      service.listStations(deviceId)
        .map(toApiStationsList)
        .toPromise()
    );
  }
);

module.exports = {
  deviceDetail,
  stations,
};
