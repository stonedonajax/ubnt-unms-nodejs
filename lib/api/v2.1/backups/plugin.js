'use strict';

const { weave } = require('ramda-adjunct');

const viewModule = require('./view');
const { registerPlugin } = require('../../../util/hapi');
const { registerRoutes } = require('./routes');

function register(server, options) {
  const { DB, deviceBackups: service } = server.plugins;
  const config = server.settings.app;

  const view = {
    listBackups: weave(viewModule.listBackups, { service }),
    createBackup: weave(viewModule.createBackup, { service }),
    createMultiBackup: weave(viewModule.createMultiBackup, { service }),
    uploadBackup: weave(viewModule.uploadBackup, { DB, service }),
    downloadBackup: weave(viewModule.downloadBackup, { service }),
    deleteBackup: weave(viewModule.deleteBackup, { service }),
    applyBackup: weave(viewModule.applyBackup, { service }),
  };

  registerRoutes(server, options, view, config);
}

/*
 * Hapijs Plugin definition
 */
exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiDeviceBackupsV2.1',
  version: '1.0.0',
  dependencies: ['deviceBackups'],
};
