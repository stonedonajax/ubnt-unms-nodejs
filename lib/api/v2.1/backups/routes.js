'use strict';

const joi = require('joi');
const { OK, UNAUTHORIZED, NOT_FOUND, NOT_ACCEPTABLE, INTERNAL_SERVER_ERROR } = require('http-status');

const { ErrorSchema, StatusSchema, AuthHeaderSchema } = require('../osm');
const { BackupListSchema, BackupSchema } = require('./schemas');
const validation = require('../../../validation');

function registerRoutes(server, options, view, config) {
  server.route({
    method: 'GET',
    path: '/v2.1/devices/{id}/backups',
    config: {
      description: 'Get device backups',
      tags: ['api', 'backups'],
      validate: {
        params: {
          id: validation.deviceId,
        },
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [OK]: BackupListSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_FOUND]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.listBackups,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/devices/{id}/backups',
    config: {
      description: 'Create device backup',
      tags: ['api', 'backups'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          id: validation.deviceId,
        },
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [OK]: BackupSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_FOUND]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.createBackup,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/devices/backups',
    config: {
      description: 'Create or download the latest backup of multiple devices',
      tags: ['api', 'backups'],
      auth: {
        scope: 'admin',
      },
      validate: {
        payload: {
          deviceIds: joi.array().items(validation.deviceId).min(1),
        },
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.createMultiBackup,
  });

  server.route({
    method: 'PUT',
    path: '/v2.1/devices/{id}/backups',
    config: {
      description: 'Upload device backup',
      tags: ['api', 'backups'],
      auth: {
        scope: 'admin',
      },
      payload: {
        output: 'data',
        maxBytes: config.deviceConfigBackup.fileMaxBytes,
        parse: true,
      },
      validate: {
        params: {
          id: validation.deviceId,
        },
        payload: joi.object().keys({
          file: joi.binary().required(),
        }),
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [OK]: BackupSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_FOUND]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.uploadBackup,
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/{deviceId}/backups/{backupId}',
    config: {
      description: 'Download device backup',
      tags: ['api', 'backups'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          deviceId: validation.deviceId,
          backupId: validation.backupId,
        },
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_FOUND]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.downloadBackup,
  });

  server.route({
    method: 'DELETE',
    path: '/v2.1/devices/{deviceId}/backups/{backupId}',
    config: {
      description: 'Delete device backup',
      tags: ['api', 'backups'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          deviceId: validation.deviceId,
          backupId: validation.backupId,
        },
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_FOUND]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.deleteBackup,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/devices/{deviceId}/backups/{backupId}/apply',
    config: {
      description: 'Apply device backup',
      tags: ['api', 'backups'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          deviceId: validation.deviceId,
          backupId: validation.backupId,
        },
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_FOUND]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.applyBackup,
  });
}

module.exports = {
  registerRoutes,
};
