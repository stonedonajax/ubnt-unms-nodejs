'use strict';

const joi = require('joi');
const { values } = require('ramda');

const { BackupTypeEnum } = require('../../../enums');

const BackupSchema = joi.object({
  id: joi.string().guid().required(),
  type: joi.string().valid(values(BackupTypeEnum)).required(),
  extension: joi.string().required(),
  timestamp: joi.number().required(),
}).label('DeviceBackup');

const BackupListSchema = joi.array()
  .items(BackupSchema)
  .label('DeviceBackupList');

module.exports = {
  BackupSchema,
  BackupListSchema,
};
