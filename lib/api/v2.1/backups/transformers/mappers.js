'use strict';

const { map } = require('lodash/fp');

/**
 * @param {CorrespondenceDeviceBackup} cmDeviceBackup
 * @return {Object}
 */
const toApiDeviceBackup = cmDeviceBackup => ({
  id: cmDeviceBackup.id,
  type: cmDeviceBackup.type,
  extension: cmDeviceBackup.extension,
  timestamp: cmDeviceBackup.timestamp,
});

/**
 * @param {CorrespondenceDeviceBackup[]} cmDeviceBackupList
 * @return {Object[]}
 */
const toApiDeviceBackupList = map(toApiDeviceBackup);

module.exports = {
  toApiDeviceBackup,
  toApiDeviceBackupList,
};
