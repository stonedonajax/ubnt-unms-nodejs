'use strict';

const { flow, constant } = require('lodash/fp');
const { evolve } = require('ramda');

const { DeviceTypeEnum } = require('../../../../enums');
const { parseTarBackup, parseCfgBackup } = require('../../../../device-backups/transformers/parsers');

const parseAirCubeBackup = flow(
  parseTarBackup,
  evolve({ extension: constant('cfg') })
);

const parseApiBackup = ({ cmDevice }, buffer) => {
  const type = cmDevice.identification.type;

  switch (type) {
    case DeviceTypeEnum.AirFiber:
    case DeviceTypeEnum.AirMax:
    case DeviceTypeEnum.ToughSwitch:
      return parseCfgBackup(buffer);
    case DeviceTypeEnum.AirCube:
      return parseAirCubeBackup(buffer);
    default:
      return parseTarBackup(buffer);
  }
};

module.exports = {
  parseApiBackup,
};
