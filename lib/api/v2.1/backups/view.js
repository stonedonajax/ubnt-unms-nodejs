'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader } = require('monet');

const { toApiDeviceBackup, toApiDeviceBackupList } = require('./transformers/mappers');
const { parseApiBackup } = require('./transformers/parsers');
const { fromDb: fromDbDevice } = require('../../../transformers/device');

function listBackups(request, reply) {
  return reader(({ service }) => {
    reply(
      service.listBackups(request.params.id)
        .map(toApiDeviceBackupList)
        .toPromise()
    );
  });
}

function createBackup(request, reply) {
  return reader(({ service }) => {
    reply(
      service.create(request.params.id)
        .map(toApiDeviceBackup)
        .toPromise()
    );
  });
}

function createMultiBackup(request, reply) {
  return reader(({ service }) => {
    const { deviceIds } = request.payload;

    reply(
      service.multi.create(deviceIds)
        .toPromise()
    ).type('application/tar+gzip');
  });
}

function uploadBackup(request, reply) {
  return reader(({ DB, service }) => {
    const buffer = request.payload.file;
    const deviceId = request.params.id;

    reply(
      Observable.from(DB.device.findById(deviceId))
        .mergeEither(fromDbDevice({}))
        .map(cmDevice => parseApiBackup({ cmDevice }, buffer))
        .mergeMap(cmDeviceBackup => service.save(deviceId, cmDeviceBackup))
        .map(toApiDeviceBackup)
        .toPromise()
    );
  });
}

function downloadBackup(request, reply) {
  return reader(({ service }) => {
    const { deviceId, backupId } = request.params;

    reply(
      service
        .findById(deviceId, backupId, 'stream')
        .map(cmDeviceBackup => cmDeviceBackup.source)
        .toPromise()
    ).type('application/octet-stream');
  });
}

function deleteBackup(request, reply) {
  return reader(({ service }) => {
    const { deviceId, backupId } = request.params;

    reply(
      service
        .remove(deviceId, backupId)
        .mapTo({ result: true, message: 'Backup deleted' })
        .toPromise()
    );
  });
}

function applyBackup(request, reply) {
  return reader(({ service }) => {
    const { deviceId, backupId } = request.params;

    reply(
      service
        .apply(deviceId, backupId)
        .mapTo({ result: true, message: 'Backup applied' })
        .toPromise()
    );
  });
}

module.exports = {
  listBackups,
  createBackup,
  createMultiBackup,
  uploadBackup,
  downloadBackup,
  deleteBackup,
  applyBackup,
};
