'use strict';

/*
 * Hapijs Plugin definition
 */

const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../../../util/hapi');
const { registerRoutes } = require('./routes');
const {
  getDataLinkById, getDataLinkListBySiteId, createDataLink, updateDataLink, deleteDataLink, getDataLinkList,
} = require('./service');
const viewModule = require('./view');
const { DB } = require('../../../db');


function register(server, options) {
  const { dataLinkStore, dal, site: siteService } = server.plugins;

  /**
   * @name ApiDataLink
   * @type {{
   *  getDataLinkList
   *  getDataLinkById,
   *  getDataLinkListBySiteId,
   *  createDataLink,
   *  updateDataLink,
   *  deleteDataLink
   * }}
   */
  const service = {
    getDataLinkList: weave(getDataLinkList, { dataLinkStore }),
    getDataLinkById: weave(getDataLinkById, { dataLinkStore }),
    getDataLinkListBySiteId: weave(getDataLinkListBySiteId, { dataLinkStore }),
    createDataLink: weave(createDataLink, { DB, dal, dataLinkStore }),
    updateDataLink: weave(updateDataLink, { dataLinkStore }),
    deleteDataLink: weave(deleteDataLink, { dataLinkStore }),
  };

  const view = {
    getDataLinkList: weave(viewModule.getDataLinkList, { service }),
    getDataLinkById: weave(viewModule.getDataLinkById, { service }),
    getDataLinkListBySiteId: weave(viewModule.getDataLinkListBySiteId, { service, siteService }),
    createDataLink: weave(viewModule.createDataLink, { service, dataLinkStore }),
    updateDataLink: weave(viewModule.updateDataLink, { service, dataLinkStore }),
    deleteDataLink: weave(viewModule.deleteDataLink, { service }),
  };

  server.expose(service);

  registerRoutes(server, options, view);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiDataLinkV2.1',
  dependencies: ['dataLinkStore', 'dal'],
};
