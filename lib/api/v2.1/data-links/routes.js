'use strict';

const joi = require('joi');
const { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR, CONFLICT, NOT_FOUND } = require('http-status');

const { ErrorSchema, AuthHeaderSchema, DataLinkSchema, DataLinkNewSchema } = require('../osm');

/*
 * Hapijs routes definition
 */

function registerRoutes(server, options, view) {
  server.route({
    method: 'GET',
    path: '/v2.1/data-links',
    config: {
      description: 'List of data links',
      tags: ['api', 'dataLink'],
      validate: {
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [OK]: joi.array().items(DataLinkSchema),
          [UNAUTHORIZED]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
      handler: view.getDataLinkList,
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.1/data-links/sites/{siteId}',
    config: {
      description: 'List of data links based siteId',
      tags: ['api', 'dataLink'],
      validate: {
        headers: AuthHeaderSchema,
        params: {
          siteId: joi.string().guid().required(),
        },
      },
      response: {
        status: {
          [OK]: joi.array().items(DataLinkSchema),
          [UNAUTHORIZED]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
      handler: view.getDataLinkListBySiteId,
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.1/data-links/{id}',
    config: {
      description: 'Get data link based on its ID',
      tags: ['api', 'dataLink'],
      validate: {
        headers: AuthHeaderSchema,
        params: {
          id: joi.string().guid().required(),
        },
      },
      response: {
        status: {
          [OK]: DataLinkSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_FOUND]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.getDataLinkById,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/data-links',
    config: {
      description: 'Create data link',
      tags: ['api', 'dataLink'],
      validate: {
        headers: AuthHeaderSchema,
        payload: DataLinkNewSchema,
      },
      response: {
        status: {
          [OK]: DataLinkSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [CONFLICT]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.createDataLink,
  });

  server.route({
    method: 'PUT',
    path: '/v2.1/data-links',
    config: {
      description: 'Update data link',
      tags: ['api', 'dataLink'],
      validate: {
        headers: AuthHeaderSchema,
        payload: DataLinkSchema,
      },
      response: {
        status: {
          [OK]: DataLinkSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [CONFLICT]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.updateDataLink,
  });

  server.route({
    method: 'DELETE',
    path: '/v2.1/data-links/{id}',
    config: {
      description: 'Delete data link based on its ID',
      tags: ['api', 'dataLink'],
      validate: {
        headers: AuthHeaderSchema,
        params: {
          id: joi.string().guid().required(),
        },
      },
      response: {
        status: {
          [OK]: joi.string().guid().required(),
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_FOUND]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.deleteDataLink,
  });
}

module.exports = {
  registerRoutes,
};
