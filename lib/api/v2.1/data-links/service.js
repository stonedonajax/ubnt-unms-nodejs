'use strict';

const { Reader: reader } = require('monet');
const { view } = require('ramda');
const { Observable } = require('rxjs/Rx');

require('../../../util/observable');
const { parseDbDevice } = require('../../../transformers/device/parsers');
const { mergeDeviceBoth } = require('../../../data-link/transformers/mergers');
const {
  dataLinkListBySiteIdSelector, dataLinkByIdSelector, dataLinkListSelector,
} = require('../../../data-link/selectors');
const { requestSave, requestRemoveById, requestUpdate } = require('../../../data-link/actions');
const {
  deviceIdFromLens, deviceIdToLens, siteIdFromLens, siteIdToLens,
} = require('../../../data-link/transformers/lenses');
const { EntityEnum } = require('../../../enums');
const { entityExistsCheck } = require('../../../util');


const getDataLinkList = () => reader(
  ({ dataLinkStore }) => Observable.of(dataLinkListSelector(dataLinkStore.getState()))
);

const getDataLinkListBySiteId = siteId => reader(
  ({ dataLinkStore }) => Observable.of(dataLinkListBySiteIdSelector(dataLinkStore.getState(), siteId))
);

const getDataLinkById = id => reader(
  ({ dataLinkStore }) => Observable.of(dataLinkByIdSelector(dataLinkStore.getState(), id))
);

const createDataLink = cmDataLink => reader(
  ({ DB, dal, dataLinkStore }) => Observable.forkJoin(
    Observable.from(dal.siteRepository.findOneById(view(siteIdFromLens, cmDataLink))),
    Observable.from(DB.device.findById(view(deviceIdFromLens, cmDataLink))),
    Observable.from(dal.siteRepository.findOneById(view(siteIdToLens, cmDataLink))),
    Observable.from(DB.device.findById(view(deviceIdToLens, cmDataLink)))
  )
    .do(([dbSiteFrom, dbDeviceFrom, dbSiteTo, dbDeviceTo]) => {
      entityExistsCheck(EntityEnum.Site, dbSiteFrom);
      entityExistsCheck(EntityEnum.Device, dbDeviceFrom);
      entityExistsCheck(EntityEnum.Site, dbSiteTo);
      entityExistsCheck(EntityEnum.Device, dbDeviceTo);
    })
    .map(([dbSiteFrom, dbDeviceFrom, dbSiteTo, dbDeviceTo]) => {
      const fromCmDevice = parseDbDevice({ dbSite: dbSiteFrom }, dbDeviceFrom);
      const toCmDevice = parseDbDevice({ dbSite: dbSiteTo }, dbDeviceTo);
      const cmDataLinkMerged = mergeDeviceBoth(cmDataLink, fromCmDevice, toCmDevice);
      dataLinkStore.dispatch(requestSave(cmDataLinkMerged));
      return dataLinkByIdSelector(dataLinkStore.getState(), cmDataLinkMerged.id);
    })
);

const updateDataLink = cmDataLink => reader(
  ({ dataLinkStore }) => Observable.of(dataLinkStore.dispatch(requestUpdate(cmDataLink)))
    .mapTo(cmDataLink)
);

const deleteDataLink = id => reader(
  ({ dataLinkStore }) => Observable.of(dataLinkStore.dispatch(requestRemoveById(id)))
);


module.exports = {
  getDataLinkList,
  getDataLinkListBySiteId,
  getDataLinkById,
  createDataLink,
  updateDataLink,
  deleteDataLink,
};
