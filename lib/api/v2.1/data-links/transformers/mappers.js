'use strict';

const { map, path, pathOr, assocPath } = require('ramda');
const { isNilOrEmpty } = require('ramda-adjunct');

const {
  InterfaceIdentificationTypeEnum, DataLinkTypeEnum, DataLinkStatusEnum, StatusEnum,
} = require('../../../../enums');
const { toApiInterfaceStatus, toApiInterfaceConfig } = require('../../../../transformers/interfaces/mappers');
const { toApiDeviceDisplayName } = require('../../../../transformers/device/mappers');
const { isOnuDeviceType } = require('../../../../feature-detection/common');

/**
 * API model of dataLink
 * @typedef {Object} APIDataLink
 * @property {string} id UUID
 * @property {DataLinkEdge} from
 * @property {DataLinkEdge} to
 * @property {string} origin
 * @property {string} state
 * @property {string} type of dataLink
 */

const isDisconnected = status => status === StatusEnum.Disconnected;

const isDisabled = (deviceModel, status) => !isOnuDeviceType(deviceModel) && status === StatusEnum.Disabled;

const toApiDataLinkStatus = (dataLink) => {
  const deviceFromModel = pathOr(null, ['from', 'device', 'identification', 'model'], dataLink);
  const deviceToModel = pathOr(null, ['to', 'device', 'identification', 'model'], dataLink);

  const deviceFromStatus = path(['from', 'device', 'status'], dataLink);
  const deviceToStatus = path(['to', 'device', 'status'], dataLink);

  const interfaceFromStatus = toApiInterfaceStatus(path(['from', 'interface'], dataLink));
  const interfaceToStatus = toApiInterfaceStatus(path(['to', 'interface'], dataLink));

  if (isNilOrEmpty(path(['to'], dataLink))) {
    return DataLinkStatusEnum.Proposed;
  }

  if (isDisconnected(deviceFromStatus) || isDisconnected(deviceToStatus)
     || isDisconnected(interfaceFromStatus) || isDisconnected(interfaceToStatus)) {
    return DataLinkStatusEnum.Disconnected;
  }

  if (isDisabled(deviceFromModel, interfaceFromStatus) || isDisabled(deviceToModel, interfaceToStatus)) {
    return DataLinkStatusEnum.Disabled;
  }

  return DataLinkStatusEnum.Active;
};

const toApiDataLinkType = (dataLink) => {
  switch (path(['from', 'interface', 'identification', 'type'], dataLink)) {
    case InterfaceIdentificationTypeEnum.Wifi:
    case InterfaceIdentificationTypeEnum.Logical:
      return DataLinkTypeEnum.Wireless;
    case InterfaceIdentificationTypeEnum.Pon:
      return DataLinkTypeEnum.Pon;
    case InterfaceIdentificationTypeEnum.Ethernet:
    case InterfaceIdentificationTypeEnum.Sfp:
    case InterfaceIdentificationTypeEnum.Switch:
    case InterfaceIdentificationTypeEnum.Bridge:
    case InterfaceIdentificationTypeEnum.Vlan:
    case InterfaceIdentificationTypeEnum.PPPoE:
      return DataLinkTypeEnum.Ethernet;
    default:
      return null;
  }
};

const toApiDevice = correspondenceDataLinkDevice => assocPath(
  ['identification', 'displayName'],
  toApiDeviceDisplayName(correspondenceDataLinkDevice),
  correspondenceDataLinkDevice
);

/**
 * @param {DataLink} dataLink
 * @return {APIDataLink}
 */
const toApiDataLink = dataLink => ({
  id: dataLink.id,
  from: {
    site: dataLink.from.site,
    device: toApiDevice(dataLink.from.device),
    interface: toApiInterfaceConfig(dataLink.from.interface),
  },
  to: {
    site: dataLink.to.site,
    device: toApiDevice(dataLink.to.device),
    interface: toApiInterfaceConfig(dataLink.to.interface),
  },
  type: toApiDataLinkType(dataLink),
  state: toApiDataLinkStatus(dataLink),
  origin: dataLink.origin,
  ssid: dataLink.from.device.ssid,
  frequency: dataLink.from.device.frequency,
});

const toApiDataLinkList = map(toApiDataLink);


module.exports = {
  toApiDataLinkStatus,
  toApiDataLinkType,
  toApiDataLink,
  toApiDataLinkList,
};
