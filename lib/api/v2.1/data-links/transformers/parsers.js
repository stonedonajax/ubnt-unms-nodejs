'use strict';

const aguid = require('aguid');
const { curry, map, pathOr } = require('ramda');

const { DataLinkOriginEnum } = require('../../../../enums');
const { parseDataLinkEdge } = require('../../../../data-link/transformers/parsers');
const { parseApiInterfaceConfig } = require('../../../../transformers/interfaces/parsers');


// parseApiDataLink :: Object -> Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseApiDataLink = curry((auxiliaries, apiDataLink) => ({
  id: pathOr(aguid(), ['id'], apiDataLink),
  from: {
    site: apiDataLink.from.site,
    device: apiDataLink.from.device,
    interface: parseApiInterfaceConfig({}, apiDataLink.from.interface),
  },
  to: {
    site: apiDataLink.to.site,
    device: apiDataLink.to.device,
    interface: parseApiInterfaceConfig({}, apiDataLink.to.interface),
  },
  origin: pathOr(DataLinkOriginEnum.Manual, ['origin'], apiDataLink),
}));

const parseApiDataLinkList = curry((auxiliaries, apiDataLinkList) =>
  map(parseApiDataLink(auxiliaries), apiDataLinkList)
);

// parseNewApiDataLink :: Object -> Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseNewApiDataLink = curry((auxiliaries, newApiDataLink) => ({
  id: pathOr(aguid(), ['id'], newApiDataLink),
  from: parseDataLinkEdge({}, {
    siteId: newApiDataLink.siteIdFrom,
    deviceId: newApiDataLink.deviceIdFrom,
    interfaceName: newApiDataLink.interfaceNameFrom,
  }),
  to: parseDataLinkEdge({}, {
    siteId: newApiDataLink.siteIdTo,
    deviceId: newApiDataLink.deviceIdTo,
    interfaceName: newApiDataLink.interfaceNameTo,
  }),
  origin: pathOr(DataLinkOriginEnum.Manual, ['origin'], newApiDataLink),
}));


module.exports = {
  parseApiDataLink,
  parseApiDataLinkList,

  parseNewApiDataLink,
};
