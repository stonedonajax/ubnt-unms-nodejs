'use strict';

const { Reader: reader } = require('monet');
const { curry, when, pipe, path, concat, map, eqBy, uniqWith, flatten } = require('ramda');
const { isNull } = require('ramda-adjunct');
const { Observable } = require('rxjs');
const boom = require('boom');

const { toApiDataLinkList, toApiDataLink } = require('./transformers/mappers');
const { parseNewApiDataLink, parseApiDataLink } = require('./transformers/parsers');
const { isDataLinkConnectionExistsSelector, isDataLinkExistsSelector } = require('../../../data-link/selectors');


const checkCanAddNewDataLink = curry((dataLinkStore, cmDataLink) => {
  if (isDataLinkConnectionExistsSelector(dataLinkStore.getState(), cmDataLink)) {
    return Observable.throw(boom.conflict('DataLink already exists', toApiDataLink(cmDataLink)));
  }
  return Observable.of(cmDataLink);
});

const notFoundWhenNotExists = curry((dataLinkStore, cmDataLink) => {
  if (isDataLinkExistsSelector(dataLinkStore.getState(), cmDataLink.id)) {
    return Observable.of(cmDataLink);
  }
  return Observable.throw(boom.notFound('DataLink not found', toApiDataLink(cmDataLink)));
});

function getDataLinkList(request, reply) {
  return reader(({ service }) => {
    reply(
      service.getDataLinkList()
        .map(toApiDataLinkList)
        .toPromise()
    );
  });
}

function getDataLinkListBySiteId(request, reply) {
  return reader(({ service, siteService }) => {
    reply(
      Observable.from(siteService.getSite(request.params.siteId))
        .map(pipe(
          path(['description', 'endpoints']),
          map(path(['id'])),
          concat([request.params.siteId])
        ))
        .mergeMap(siteIds => Observable.forkJoin(map(service.getDataLinkListBySiteId, siteIds)))
        .map(flatten)
        .map(uniqWith(eqBy(path(['id']))))
        .map(toApiDataLinkList)
        .toPromise()
    );
  });
}

function getDataLinkById(request, reply) {
  return reader(({ service }) => {
    reply(
      service
        .getDataLinkById(request.params.id)
        .do(when(isNull, () => Observable.throw(boom.notFound('DataLink not found', request.params.id))))
        .map(toApiDataLink)
        .toPromise()
    );
  });
}

function createDataLink(request, reply) {
  return reader(({ service, dataLinkStore }) => {
    reply(
      Observable.of(parseNewApiDataLink({}, request.payload))
      .mergeMap(checkCanAddNewDataLink(dataLinkStore))
      .mergeMap(service.createDataLink)
      .map(toApiDataLink)
      .toPromise()
    );
  });
}

function updateDataLink(request, reply) {
  return reader(({ service, dataLinkStore }) => {
    reply(
      Observable.of(parseApiDataLink({}, request.payload))
      .mergeMap(notFoundWhenNotExists(dataLinkStore))
      .mergeMap(service.updateDataLink)
      .map(toApiDataLink)
      .toPromise()
    );
  });
}

function deleteDataLink(request, reply) {
  return reader(({ service }) => {
    reply(
      service
        .getDataLinkById(request.params.id)
        .do(when(isNull, () => Observable.throw(boom.notFound('DataLink not found', request.params.id))))
        .mergeMap(() => service.deleteDataLink(request.params.id))
        .mapTo(request.params.id)
        .toPromise()
    );
  });
}


module.exports = {
  getDataLinkList,
  getDataLinkListBySiteId,
  getDataLinkById,
  createDataLink,
  updateDataLink,
  deleteDataLink,
};
