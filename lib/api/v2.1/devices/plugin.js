'use strict';

const joi = require('joi');
const {
  map, spread, invokeArgs, getOr, equals, forEach, mapValues, fromPairs, get, values, head, flatten, nth,
  sortBy, curry, flow, toPairs, isPlainObject, isUndefined, omitBy, isNull, tap, isArray, defaultTo, round, isFinite,
} = require('lodash/fp');
const { when, pathSatisfies, always, ifElse, length, comparator, omit, isNil } = require('ramda');
const { cata, isNotUndefined } = require('ramda-adjunct');
const { ErrorSchema, AuthHeaderSchema, DeviceSsidListSchema, DeviceApsProfileListSchema } = require('../osm');
const { OK, UNAUTHORIZED, NOT_ACCEPTABLE, INTERNAL_SERVER_ERROR } = require('http-status');

const { registerPlugin } = require('../../../util/hapi');
const { tapP, entityExistsCheck, allP, resolveP, rejectP } = require('../../../util');
const { DB } = require('../../../db');
const validation = require('../../../validation');
const { toDisplayName: toInterfaceDisplayName } = require('../../../transformers/interfaces/utils');
const { EntityEnum, DeviceTypeEnum } = require('../../../enums');
const config = require('../../../../config');
const { supportedStatistics, StatisticsSupportEnum } = require('../../../feature-detection/statistics');
const service = require('./service');
const { fromDbList } = require('../../../transformers/device');

/*
 * Business logic
 */

function aggregateStatisticsIntervals(statisticsIntervals) {
  const entries = {};

  // iterate over all entries in all intervals in statistics
  // and keep only the last entry for each unique timestamp
  statisticsIntervals.forEach(interval =>
    interval.forEach((entry) => {
      entries[entry.timestamp] = entry;
    })
  );
  return values(entries);
}

const extractStatisticValue = (support, accumulator, timestamp, value, key) => {
  if (isNotUndefined(support) && support !== StatisticsSupportEnum.Never) {
    if (isFinite(value)) {
      accumulator[key].push({ x: timestamp, y: value });
    } else if (support === StatisticsSupportEnum.Always) {
      accumulator[key].push({ x: timestamp, y: null });
    }
  }
};

const extractInterfaceStatistics = (support, accumulator, timestamp, key, rx, tx) => {
  if (isNotUndefined(support) && support !== StatisticsSupportEnum.Never) {
    if (isFinite(rx) && isFinite(tx)) {
      accumulator.receive.push({ x: timestamp, y: rx });
      accumulator.transmit.push({ x: timestamp, y: tx });
    } else if (support === StatisticsSupportEnum.Always) {
      accumulator.receive.push({ x: timestamp, y: null });
      accumulator.transmit.push({ x: timestamp, y: null });
    }
  }
};

const extractInterfaces = (accumulator, entry, timestamp, interfacesSupport) => {
  for (const [name, { stats }] of Object.entries(entry.interfaces)) {
    const statisticsSupport = defaultTo(interfacesSupport['*'], interfacesSupport[name]);

    if (statisticsSupport === StatisticsSupportEnum.Never || isNil(stats)) {
      continue; // eslint-disable-line no-continue
    }

    let interfaceAccumulator = accumulator.interfaces[name];
    if (isUndefined(interfaceAccumulator)) {
      interfaceAccumulator = {
        receive: [],
        transmit: [],
      };
      // eslint-disable-next-line no-param-reassign
      accumulator.interfaces[name] = interfaceAccumulator;
    }
    extractInterfaceStatistics(
      statisticsSupport, interfaceAccumulator, timestamp, name, round(stats.rx_bps), round(stats.tx_bps)
    );
  }
};

const extractPsu = (accumulator, entry, timestamp, psuSupport) => {
  for (const [name, { stats }] of Object.entries(entry.psu)) {
    const statisticsSupport = defaultTo(psuSupport['*'], psuSupport[name]);

    if (statisticsSupport === StatisticsSupportEnum.Never || isNil(stats)) {
      continue; // eslint-disable-line no-continue
    }

    let psuAccumulator = accumulator.psu[name];
    if (isUndefined(psuAccumulator)) {
      psuAccumulator = {
        voltage: [],
      };
      // eslint-disable-next-line no-param-reassign
      accumulator.psu[name] = psuAccumulator;
    }

    if (isNotUndefined(statisticsSupport) && statisticsSupport !== StatisticsSupportEnum.Never) {
      const value = round(stats.voltage);
      if (isFinite(value)) {
        psuAccumulator.voltage.push({ x: timestamp, y: value });
      } else if (statisticsSupport === StatisticsSupportEnum.Always) {
        psuAccumulator.voltage.push({ x: timestamp, y: null });
      }
    }
    extractInterfaceStatistics(
      statisticsSupport, psuAccumulator, timestamp, name, round(stats.rx_bps), round(stats.tx_bps)
    );
  }
};

const extractStatistics = support => (accumulator, entry) => {
  const timestamp = entry.timestamp;
  if (isPlainObject(entry.stats)) {
    for (const [name, value] of Object.entries(entry.stats)) {
      extractStatisticValue(support[name], accumulator, timestamp, round(value), name);
    }
  }

  const interfacesSupport = support.interfaces;
  if (isPlainObject(entry.interfaces) && interfacesSupport !== StatisticsSupportEnum.Never) {
    extractInterfaces(accumulator, entry, timestamp, interfacesSupport);
  }

  const psuSupport = support.psu;
  if (isPlainObject(entry.psu) && psuSupport !== StatisticsSupportEnum.Never) {
    extractPsu(accumulator, entry, timestamp, psuSupport);
  }

  return accumulator;
};

const buildEmptyStatistics = mapValues((flag) => {
  if (isPlainObject(flag)) {
    return {};
  }

  if (flag !== StatisticsSupportEnum.Never) {
    return [];
  }

  return null;
});

const removeEmptyInterface = (support, statisticsObject) => {
  for (const [name, value] of Object.entries(statisticsObject)) {
    const statisticsSupport = isPlainObject(support) ? defaultTo(support['*'], support[name]) : support;
    const isEmptyStatistics = value.receive.length === 0 || value.transmit.length === 0;
    if (statisticsSupport === StatisticsSupportEnum.NonEmpty && isEmptyStatistics) {
      // eslint-disable-next-line no-param-reassign
      delete statisticsObject[name];
    }
  }
};

const removeEmptyPsu = (support, statisticsObject) => {
  for (const [name, value] of Object.entries(statisticsObject)) {
    const statisticsSupport = isPlainObject(support) ? defaultTo(support['*'], support[name]) : support;
    const isEmptyStatistics = value.voltage.length === 0;
    if (statisticsSupport === StatisticsSupportEnum.NonEmpty && isEmptyStatistics) {
      // eslint-disable-next-line no-param-reassign
      delete statisticsObject[name];
    }
  }
};

const removeEmpty = (support, statisticsObject) => {
  for (const [name, value] of Object.entries(statisticsObject)) {
    const statisticsSupport = support[name];
    if (isPlainObject(statisticsSupport) && isPlainObject(value)) {
      if (name === 'interfaces') {
        removeEmptyInterface(statisticsSupport, value);
      } else if (name === 'psu') {
        removeEmptyPsu(statisticsSupport, value);
      }
    } else if (statisticsSupport === StatisticsSupportEnum.NonEmpty && (!isArray(value) || value.length === 0)) {
      // eslint-disable-next-line no-param-reassign
      statisticsObject[name] = null;
    }
  }
};

const xAxisComparator = comparator((a, b) => a.x < b.x);

const sortStatistics = when(Array.isArray, invokeArgs('sort', [xAxisComparator]));

const transformInterfaceStatisticsObject = nameToDisplayNameMap => flow(
  omitBy(isNull),
  toPairs,
  sortBy(nth(0)), // sort by key
  map(([name, stats]) => Object.assign(
    mapValues(sortStatistics, stats),
    { name: getOr(name, [name], nameToDisplayNameMap) }
  ))
);

function transformStatistics(support, dbInterfaceNameDisplayNameMap, statistics) {
  const statisticsObject = statistics.reduce(extractStatistics(support), buildEmptyStatistics(support));
  forEach(sortStatistics, statisticsObject);
  const transformStatisticsObject = transformInterfaceStatisticsObject(dbInterfaceNameDisplayNameMap);

  removeEmpty(support, statisticsObject);

  if (support.interfaces !== StatisticsSupportEnum.Never) {
    statisticsObject.interfaces = transformStatisticsObject(statisticsObject.interfaces);
  }

  if (support.psu !== StatisticsSupportEnum.Never) {
    statisticsObject.psu = transformStatisticsObject(statisticsObject.psu);
  }

  return statisticsObject;
}

const extractInterfaceNameDisplayNamePair = (dbInterface) => {
  const name = get(['identification', 'name'], dbInterface);
  const description = get(['identification', 'description'], dbInterface);
  const type = get(['identification', 'type'], dbInterface);

  return [name, toInterfaceDisplayName({ name, description, type })];
};

const createInterfaceIdToNameMap = flow(
  map(extractInterfaceNameDisplayNamePair),
  fromPairs
);

const insertPeriodAndInterval = curry((interval, statistics) =>
  Object.assign({}, statistics, {
    period: config.statisticsIntervalPeriodMapping[interval],
    interval: {
      start: Date.now() - config.statisticsIntervalLengthMapping[interval],
      end: Date.now(),
    },
  })
);

/*
 * Route definitions
 */

function register(server) {
  server.route({
    method: 'GET',
    path: '/v2.1/devices/aps/profiles',
    config: {
      description: 'Get devices profiles',
      tags: ['api', 'device'],
      validate: {
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [OK]: DeviceApsProfileListSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler(request, reply) {
      const { dal } = server.plugins;

      reply(
        service.apsProfiles().run({ DB, dal })
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/ssids',
    config: {
      description: 'Get devices wireless configuration',
      tags: ['api', 'device'],
      validate: {
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [OK]: DeviceSsidListSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler(request, reply) {
      reply(
        service.ssidList().run({ DB })
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices',
    config: {
      validate: {
        query: {
          siteId: validation.siteId.optional(),
        },
      },
    },
    handler(request, reply) {
      const { firmwareDal, deviceStore, dal } = server.plugins;

      reply(
        service.deviceList(request.query.siteId).run({ DB, deviceStore, firmwareDal, dal })
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/{id}',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const { dal } = server.plugins;

      reply(
        service.deviceDetail(request.params.id).run({ DB, dal })
      );
    },
  });

  server.route({
    method: 'POST',
    path: '/v2.1/devices/{id}/authorize',
    config: {
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const { user, site, dal, eventLog, messageHub } = server.plugins;
      const deviceId = request.params.id;
      const siteId = request.payload.siteId;
      const userId = request.token.userId;

      reply(
        service.authorizeDevice(deviceId, siteId, userId).run({ user, site, dal, eventLog, DB, messageHub })
      );
    },
  });

  server.route({
    method: 'POST',
    path: '/v2.1/devices/{id}/restart',
    config: {
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const { deviceStore, 'apiOnusV2.1': apiOnus, messageHub, user, dal } = server.plugins;
      const { id: deviceId } = request.params;
      const { userId } = request.token;

      reply(
        service.restartDevice(deviceId).run({ DB, deviceStore, apiOnus, userId, messageHub, user, dal })
      );
    },
  });

  server.route({
    method: 'POST',
    path: '/v2.1/devices/{id}/upgrade-to-latest',
    config: {
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const { firmwareDal, 'apiTasksV2.1': apiTasks } = server.plugins;
      const { id: deviceId } = request.params;
      const { userId } = request.token;

      reply(
        service.upgradeFirmwareToLatest(userId, deviceId).run({ DB, apiTasks, firmwareDal })
      );
    },
  });


  server.route({
    method: 'DELETE',
    path: '/v2.1/devices/{id}',
    config: {
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const deviceId = request.params.id;
      const { messageHub } = server.plugins;
      const { deviceRemoved } = messageHub.messages;

      const devicePromise = DB.device.findById(deviceId)
        .then(tapP(entityExistsCheck(EntityEnum.Device)));

      const onusPromise = devicePromise
        .then(ifElse(
          pathSatisfies(equals(DeviceTypeEnum.Olt), ['identification', 'type']),
          () => DB.onu.findAllIdsByOltId(deviceId).map(id => DB.onu.findById(id)),
          always([])
        ));

      reply(
        Promise.all([devicePromise, onusPromise])
          .then(flatten)
          .then(tapP(dbDeviceList => allP(dbDeviceList.map(DB.device.remove))))
          .then(fromDbList({}))
          .then(cata(rejectP, resolveP))
          .then(tap(forEach(cmDevice => messageHub.publish(deviceRemoved(cmDevice)))))
          .then(length)
          .then(count => ({ result: true, message: `${count} device(s) deleted` }))
      );
    },
  });

  server.route({
    method: 'POST',
    path: '/v2.1/devices/{id}/refresh',
    config: {
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const deviceId = request.params.id;
      const { macAesKeyStore } = server.plugins;

      reply(
        service.refreshDevice(deviceId).run({ macAesKeyStore })
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/{id}/statistics',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
        query: {
          interval: validation.interval,
        },
      },
    },
    handler(request, reply) {
      const deviceId = request.params.id;

      const devicePromise = DB.device.findById(deviceId)
        .then(tapP(entityExistsCheck(EntityEnum.Device)));

      const supportedStatisticsPromise = devicePromise
        .then(getOr(null, ['identification', 'model']))
        .then(supportedStatistics);

      const idToNameMapPromise = devicePromise
        .then(getOr([], 'interfaces'))
        .then(createInterfaceIdToNameMap);

      const statisticsPromise = DB.statistics.findByIdAndInterval(deviceId, request.query.interval, Date.now())
        .then(aggregateStatisticsIntervals);

      reply(
        Promise.all([supportedStatisticsPromise, idToNameMapPromise, statisticsPromise])
          .then(spread(transformStatistics))
          .then(insertPeriodAndInterval(request.query.interval))
      );
    },
  });


  server.route({
    method: 'GET',
    path: '/v2.1/devices/{id}/system',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const { deviceStore } = server.plugins;
      const deviceId = request.params.id;

      // TODO(michal.sedlak@ubnt.com): Erouter and OLT specific method
      reply(
        service.getSystem(deviceId).run({ deviceStore })
      );
    },
  });

  server.route({
    method: 'PUT',
    path: '/v2.1/devices/{id}/system',
    config: {
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          id: validation.deviceId,
        },
        payload: {
          name: joi.string().min(2).max(63).required(),
          domainName: joi.string().min(2).max(63).allow(null),
          timezone: validation.timezone,
          admin: joi.object().allow(null),
          dns1: joi.string().ip({ version: ['ipv4'] }).allow(null),
          dns2: joi.string().ip({ version: ['ipv4'] }).allow(null),
          gateway: joi.string().ip({ version: ['ipv4'] }).allow(null),
          readOnlyAccount: joi.object().allow(null),
        },
      },
    },
    handler(request, reply) {
      const { deviceStore } = server.plugins;
      const deviceId = request.params.id;

      // TODO(michal.sedlak@ubnt.com): Erouter and OLT specific method
      reply(
        service.setSystem(deviceId, request.payload).run({ deviceStore })
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/{id}/system/unms',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const { deviceSettings } = server.plugins;
      reply(
        service.getDeviceUnmsSettings(request.params.id).run({ deviceSettings })
      );
    },
  });

  server.route({
    method: 'PUT',
    path: '/v2.1/devices/{id}/system/unms',
    config: {
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          id: validation.deviceId,
        },
        payload: {
          overrideGlobal: joi.boolean().required(),
          devicePingAddress: validation.devicePingAddress,
          devicePingIntervalNormal: validation.devicePingIntervalNormal,
          devicePingIntervalOutage: validation.devicePingIntervalOutage,
          deviceTransmissionProfile: validation.DeviceTransmissionProfile,
          meta: {
            alias: validation.deviceAlias,
            note: validation.deviceNote,
          },
        },
      },
    },
    handler(request, reply) {
      const { id } = request.params;
      const { deviceStore, deviceSettings, dal } = server.plugins;
      const unmsSettings = omit(['meta'], request.payload);
      const unmsSettingsPromise = service.updateDeviceUnmsSettings(id, unmsSettings)
        .run({ DB, deviceSettings, deviceStore });
      const deviceMetadataPromise = service.updateDeviceSettingsMetadata(id, request.payload.meta).run({ dal });
      reply(
        allP([unmsSettingsPromise, deviceMetadataPromise])
          .then(head)
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/{id}/services',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const { deviceStore } = server.plugins;
      const deviceId = request.params.id;

      // TODO(michal.sedlak@ubnt.com): Erouter and OLT specific method
      reply(
        service.getServices(deviceId).run({ deviceStore })
      );
    },
  });

  server.route({
    method: 'PUT',
    path: '/v2.1/devices/{id}/services',
    config: {
      auth: {
        scope: 'admin',
      },
      validate: {
        // TODO(michal.sedlak@ubnt.com): Missing payload validation
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler(request, reply) {
      const { deviceStore } = server.plugins;
      const deviceId = request.params.id;

      // TODO(michal.sedlak@ubnt.com): Erouter and OLT specific method
      reply(
        service.setServices(deviceId, request.payload).run({ deviceStore })
      );
    },
  });

  server.route({
    method: 'GET',
    path: '/v2.1/devices/macs',
    handler(request, reply) {
      reply(
        service.macAddressList().run({ DB })
      );
    },
  });

  server.route({
    method: 'POST',
    path: '/v2.1/devices/{deviceId}/interfaces/{interfaceName}/resetstats',
    config: {
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          deviceId: validation.deviceId,
          interfaceName: validation.interfaceName,
        },
      },
    },
    handler(request, reply) {
      const { deviceId, interfaceName } = request.params;

      reply(
        service.resetInterfaceStatistics(deviceId, interfaceName).run({ DB })
      );
    },
  });

  server.route({
    method: 'POST',
    path: '/v2.1/devices/{deviceId}/interfaces/enablelag',
    config: {
      auth: {
        scope: 'admin',
      },
    },
    handler(request, reply) {
      const { deviceId } = request.params;
      const { deviceStore } = server.plugins;

      reply(
        service.enableLag(deviceId).run({ deviceStore })
      );
    },
  });

  server.route({
    method: 'POST',
    path: '/v2.1/devices/{deviceId}/interfaces/disablelag',
    config: {
      auth: {
        scope: 'admin',
      },
    },
    handler(request, reply) {
      const { deviceId } = request.params;
      const { deviceStore } = server.plugins;

      reply(
        service.disableLag(deviceId).run({ deviceStore })
      );
    },
  });

  server.expose(service);
}


  /*
   * Hapijs Plugin definition
   */
exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'devicesV2.1',
  version: '1.0.0',
  dependencies: [
    'deviceStore', 'firmwareDal', 'apiOnusV2.1', 'apiTasksV2.1', 'messageHub', 'dal', 'user', 'eventLog',
  ],
};
