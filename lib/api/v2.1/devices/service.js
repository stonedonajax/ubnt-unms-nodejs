'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader } = require('monet');
const boom = require('boom');
const {
  ifElse, chain, pathEq, map, when, pick, assocPath, reject, pathSatisfies, concat, assoc, filter, propSatisfies,
} = require('ramda');
const { cata, isNotNull } = require('ramda-adjunct');
const { Either } = require('monet');
const {
  get, eq, tap, constant, head, stubArray, flatten, find, isUndefined, isNull, spread, keyBy, has, __,
  getOr, compact, flow, curry, noop, countBy, assign, includes,
} = require('lodash/fp');
const { Future } = require('fluture');
const FutureTEither = require('monad-t/lib/FlutureTMonetEither');

const {
  EntityEnum,
  DeviceTypeEnum,
  TaskTypeEnum,
  StatusEnum,
  WirelessModeEnum,
  DeviceModelEnum,
} = require('../../../enums');
const { entityExistsCheck, allP, rejectP, resolveP, tapP } = require('../../../util');
const { compareSemver } = require('../../../util/semver');
const { hasCustomScriptsSupport } = require('../../../feature-detection/firmware');
const {
  fromDbList: fromDbDeviceList, toApiDeviceStatusOverviewList, fromDb: fromDbDevice, toDb: toDbDevice,
  toApiDeviceStatusOverview,
} = require('../../../transformers/device');
const { toApiSystem, fromApiSystem, fromApiServices, toApiServices } = require('../../../transformers/device/erouter');
const { toApiUser, fromDbUser } = require('../../../transformers/user');
const {
  toApiMacAddressList, fromDbDeviceList: macAddressListFromDbDeviceList,
} = require('../../../transformers/device/mac-address');
const {
  fromDbDevice: fromDbDeviceUnmsSettings, fromApiUnmsSettings, toApiUnmsSettings, mergeDbDeviceUnmsSettings,
} = require('../../../transformers/device/unms-settings');
const { fromDbList: fromDbInterfaceList, toDbInterfaceList } = require('../../../transformers/interfaces');
const { fromDb: fromDbSite } = require('../../../transformers/site');
const { replaceInterfaceInList } = require('../../../transformers/interfaces/mergers');
const { resetInterfaceStatistics: resetInterfaceStatisticsAp } = require('../../../transformers/interfaces/ap');
const {
  mergeInterfaces, mergeMetadataList, mergeMetadata, mergeSite,
} = require('../../../transformers/device/mergers');
const {
  fromDbList: fromDbDeviceMetadataList, toDb: toDbDeviceMetadata, fromDb: fromDbDeviceMetadata,
} = require('../../../transformers/device/metadata');
const { mergeRight, merge } = require('../../../transformers');
const { castMacAesKeyToDevice } = require('../../../transformers/mac-aes-key');
const { toApiSsidList: toApiSsidAirCubeList } = require('../../../transformers/device/aircube');
const { toApiApsProfileList: toApiApsProfileAirFiberList } = require('../../../transformers/device/airfiber');
const {
  toApiSsidList: toApiSsidAirMaxList,
  toApiApsProfileList: toApiApsProfileAirMaxList,
} = require('../../../transformers/device/airmax');


const filterUnknownDevices = (devices, unknownDevices) => {
  const deviceIdsMap = keyBy(get(['identification', 'id']), devices);
  return reject(pathSatisfies(has(__, deviceIdsMap), ['identification', 'id']), unknownDevices);
};

/**
 * @param {?string} [siteId]
 * @return {Reader.<deviceList~callback>}
 */
const deviceList = (siteId = null) => reader(
  /**
   * @function deviceList~callback
   * @param {DB} DB
   * @param {FirmwareDal} firmwareDal
   * @param {DeviceStore} deviceStore
   * @param {DbDal} dal
   * @return {Promise.<ApiDeviceStatusOverview[]>}
   */
  ({ DB, firmwareDal, deviceStore, dal }) => {
    const dbSiteListP = dal.siteRepository.findAllSites();
    const dbDeviceMetadataListP = dal.deviceMetadataRepository.findAll();
    const dbUnknownDeviceListP = resolveP(siteId)
      .then(ifElse(isNull, () => dal.macAesKeyRepository.findAll({}), stubArray))
      .then(map(castMacAesKeyToDevice({})));
    const dbDeviceListP = DB.device.findAll({ siteId });
    const dbLatestBackupListP = dbDeviceListP
      .then(dbDeviceList => allP(dbDeviceList.map(dbDevice =>
        DB.device.findLatestBackup(dbDevice.id)
          .then(latestBackup => ({ deviceId: dbDevice.id, latestBackup }))
      )));

    return allP([dbSiteListP, dbDeviceListP, dbUnknownDeviceListP, dbDeviceMetadataListP, dbLatestBackupListP])
      .then(([dbSiteList, dbDeviceList, dbUnknownDeviceList, dbDeviceMetadataList, dbLatestBackupList]) => {
        const deviceIdsMap = keyBy(get(['identification', 'id']), dbDeviceList);
        // TODO(michal.sedlak@ubnt.com): Remove when moving devices to PG
        const metadataIdsMap = keyBy(get(['id']), dbDeviceMetadataList);
        const missingMetadata = dbDeviceList.filter(dbDevice => !has(dbDevice.identification.id, metadataIdsMap));
        const devices = dbDeviceList.concat(filterUnknownDevices(deviceIdsMap, dbUnknownDeviceList));
        const data = { dbSiteList, devices, dbDeviceMetadataList, dbLatestBackupList };

        if (missingMetadata.length > 0) {
          return allP(missingMetadata.map(dbDevice => dal.deviceMetadataRepository.save({
            id: dbDevice.identification.id,
            failedMessageDecryption: false,
            restartTimestamp: null,
            alias: null,
            note: null,
          })))
            .then(concat(dbDeviceMetadataList))
            .then(assoc('dbDeviceMetadataList', __, data));
        }

        return data;
      })
      .then(({ dbSiteList, devices, dbDeviceMetadataList, dbLatestBackupList }) =>
        fromDbDeviceList({ dbSiteList, firmwareDal, deviceStore, dbLatestBackupList }, devices)
          .chain(mergeRight(mergeMetadataList, fromDbDeviceMetadataList({}, dbDeviceMetadataList)))
          .chain(toApiDeviceStatusOverviewList)
          .cata(rejectP, resolveP)
      );
  }
);


/**
 * Device detail.
 */

const deviceDetail = deviceId => reader(
  ({ DB, dal }) => FutureTEither
    .do(function* async() {
      const dbDevice = yield FutureTEither.encaseP(DB.device.findById, deviceId)
        .mapRej(() => boom.notFound('Device not found'));
      const dbDeviceMetadata = yield FutureTEither.encaseP(dal.deviceMetadataRepository.findById, deviceId);
      const cmDevice = fromDbDevice({}, dbDevice);
      const cmDeviceMetadata = fromDbDeviceMetadata({}, dbDeviceMetadata);
      let apiDeviceStatusOverview = cmDevice
        .chain(merge(mergeMetadata, cmDeviceMetadata));

      const siteId = yield FutureTEither.fromEither(cmDevice).map(get(['identification', 'siteId']));

      if (isNotNull(siteId)) {
        const dbSite = yield FutureTEither.encaseP(dal.siteRepository.findOneById, siteId);
        if (isNotNull(dbSite)) {
          apiDeviceStatusOverview = apiDeviceStatusOverview.chain(merge(mergeSite, fromDbSite({}, dbSite)));
        }
      }

      return yield FutureTEither.fromEither(apiDeviceStatusOverview)
        .chainEither(toApiDeviceStatusOverview);
    })
    .promise()
);

/**
 * Restart device.
 */

const restartDevice = deviceId => reader(
  ({ DB, deviceStore, apiOnus, userId, messageHub, user, dal }) => {
    // thunkify non-primitive calls
    const restartOnuThunk = () => apiOnus.restartOnu(deviceId);
    const restartDeviceThunk = () => Observable.of(deviceStore.get(deviceId))
      .do(entityExistsCheck(EntityEnum.Device))
      .mergeMap(commDevice => commDevice.restartDevice())
      .toPromise();
    const { deviceRestarted } = messageHub.messages;

    const dbUserPromise = user
      .getUser(userId)
      .chainEither(toApiUser)
      .promise();
    const dbDevicePromise = DB.device.findById(deviceId);
    const dbDeviceMetadataPromise = dal.deviceMetadataRepository.findById(deviceId)
      .then(tapP(entityExistsCheck(EntityEnum.DeviceMetadata)));
    const dataPromise = allP([dbDevicePromise, dbUserPromise, dbDeviceMetadataPromise]);

    return dataPromise
      .then(head)
      .then(tap(entityExistsCheck(EntityEnum.Device)))
      .then(get(['identification', 'type']))
      .then(ifElse(
        eq(DeviceTypeEnum.Onu),
        restartOnuThunk,
        restartDeviceThunk
      ))
      .then(constant(dataPromise))
      .then(([dbDevice, dbUser, dbDeviceMetadata]) => allP([
        fromDbDevice({}, dbDevice)
          .chain(merge(mergeMetadata, fromDbDeviceMetadata({}, dbDeviceMetadata)))
          .cata(rejectP, resolveP),
        fromDbUser({}, dbUser).cata(rejectP, resolveP),
      ]))
      .then(([cmDevice, cmUser]) =>
        messageHub.publish(deviceRestarted(cmDevice, cmUser))
      )
      .then(constant({ result: true, message: 'Device restarted' }));
  }
);


/**
 * Upgrade firmware on device.
 */

/**
 * @param {string} userId
 * @param {string} deviceId
 * @return {Reader.<upgradeFirmwareToLatest~callback>}
 */
const upgradeFirmwareToLatest = (userId, deviceId) => reader(
  /**
   * @function upgradeFirmwareToLatest~callback
   * @param {ApiTasks} apiTasks
   * @param {FirmwareDal} firmwareDal
   * @param {DB} DB
   * @return {!Promise.<DbTask[]>}
   */
  ({ apiTasks, firmwareDal, DB }) => Observable.from(DB.device.findById(deviceId))
    .mergeEither(fromDbDevice({}))
    .mergeMap((cmDevice) => {
      const { platformId, firmwareVersion } = cmDevice.identification;
      const airMaxCustomScripts = hasCustomScriptsSupport(platformId, firmwareVersion);
      const currentFirmware = firmwareDal.findFirmwareDetails(platformId, firmwareVersion);
      const latestFirmware = firmwareDal.findLatestFirmware(platformId, { supports: { airMaxCustomScripts } });
      if (latestFirmware !== null && compareSemver(currentFirmware.semver.current, latestFirmware.semver) >= 0) {
        return rejectP(boom.badData('Already upgraded to latest version'));
      }

      return apiTasks.startTasks(userId, TaskTypeEnum.FirmwareUpgrade, [{
        deviceId,
        firmwareId: latestFirmware.identification.id,
      }]);
    })
    .toPromise()
);

const getSystem = deviceId => reader(
  ({ deviceStore }) => Observable.of(deviceStore.get(deviceId))
    .do(entityExistsCheck(EntityEnum.Device))
    .mergeMap(commDevice => commDevice.getSystem())
    .mergeEither(toApiSystem)
    .toPromise()
);

const setSystem = (deviceId, apiSystem) => reader(
  ({ deviceStore }) => Observable.of(deviceStore.get(deviceId))
    .do(entityExistsCheck(EntityEnum.Device))
    .mergeMap(commDevice => Observable.fromEither(fromApiSystem(apiSystem))
      .mergeMap(cmSystem => commDevice.setSystem(cmSystem))
      .mergeEither(toApiSystem) // this is here because of timezone hack in getSystem
    )
    .toPromise()
);


const getServices = deviceId => reader(
  ({ deviceStore }) => Observable.of(deviceStore.get(deviceId))
    .do(entityExistsCheck(EntityEnum.Device))
    .mergeMap(commDevice => commDevice.getServices())
    .mergeEither(toApiServices)
    .toPromise()
);

const setServices = (deviceId, apiServices) => reader(
  ({ deviceStore }) => Observable.of(deviceStore.get(deviceId))
    .do(entityExistsCheck(EntityEnum.Device))
    .mergeMap(commDevice => Observable.fromEither(fromApiServices(apiServices))
      .mergeMap(cmServices => commDevice.setServices(cmServices))
    )
    .mapTo(apiServices)
    .toPromise()
);


/**
 * Get list of a mac addresses of all devices registered in UNMS.
 */

const macAddressList = () => reader(
  ({ DB }) => allP([DB.olt.list(), DB.erouter.list(), DB.airCube.list(), DB.airMax.list()])
    .then(flatten)
    .then(macAddressListFromDbDeviceList)
    .then(chain(toApiMacAddressList))
    .then(cata(rejectP, resolveP))
);

/**
 * Reset an interface's statistics
 */

const resetInterfaceStatistics = (deviceId, interfaceName) => reader(
  ({ DB }) => FutureTEither
    .do(function* async() {
      const dbDevice = yield FutureTEither
        .encaseP(DB.device.findById, deviceId)
        .filter(isNotNull, boom.notFound(`Device(${deviceId}) not found`));

      const cmDeviceE = fromDbDevice({}, dbDevice);

      const cmInterfaceListE = cmDeviceE
        .map(get(['interfaces']))
        .chain(fromDbInterfaceList({ dbDevice }));

      const cmInterfaceE = cmInterfaceListE
        .map(find(pathEq(['identification', 'name'], interfaceName)))
        .chain(cmInterface => (
          isUndefined(cmInterface)
            ? Either.Left(boom.notFound(`Interface(${interfaceName}) on Device(${deviceId}) not found`))
            : Either.Right(cmInterface)
        ));

      return [cmDeviceE, cmInterfaceListE, cmInterfaceE];
    })
    .chainEither(([cmDeviceE, cmInterfaceListE, cmInterfaceE]) => cmDeviceE
      .chain(merge(mergeInterfaces, cmInterfaceListE
        .chain(merge(replaceInterfaceInList, cmInterfaceE
          .map(resetInterfaceStatisticsAp)
        ))
        .chain(toDbInterfaceList)
      ))
    )
    .chainEither(toDbDevice)
    .chain(FutureTEither.encaseP(DB.device.update))
    .map(constant({ result: true, message: 'Interface statistics reset' }))
    .promise()
);

/**
 * Get device specific UNMS settings
 */

/**
 * @param {string} deviceId
 * @return {Reader.<getDeviceUnmsSettings~callback>}
 */
const getDeviceUnmsSettings = deviceId => reader(
  /**
   * @param {DB} DB
   * @return {Promise.<ApiUnmsSettings>}
   */
  ({ deviceSettings }) => deviceSettings.loadSettings(deviceId)
    .mergeEither(toApiUnmsSettings)
    .toPromise()
);

/**
 * Update device specific UNMS settings
 */

/**
 * @param {string} deviceId
 * @param {ApiUnmsSettings} apiUnmsSettings
 * @return {Reader.<updateDeviceUnmsSettings~callback>}
 */
const updateDeviceUnmsSettings = (deviceId, apiUnmsSettings) => reader(
  /**
   * @param {DB} DB
   * @param {DeviceStore} deviceStore
   * @param {DeviceSettings} deviceSettings
   * @return {Promise.<ApiUnmsSettings>}
   */
  ({ DB, deviceStore, deviceSettings }) => {
    const cmDeviceUnmsSettings = fromApiUnmsSettings({}, apiUnmsSettings);

    return Observable.from(DB.device.findById(deviceId))
      .do(entityExistsCheck(EntityEnum.Device))
      .mergeEither(fromDbDevice({}))
      .mergeEither(mergeRight(mergeDbDeviceUnmsSettings, cmDeviceUnmsSettings))
      .mergeEither(toDbDevice)
      .tapO(dbDevice => DB.device.update(dbDevice))
      .mergeEither(fromDbDeviceUnmsSettings)
      .tapO(() => Observable.of(deviceStore.get(deviceId))
        .filter(isNotNull)
        .mergeMap(commDevice => deviceSettings.loadSettings(deviceId)
          .mergeMap(unmsSettings => commDevice.setSetup(unmsSettings)))
      )
      .mergeEither(toApiUnmsSettings)
      .toPromise();
  });

/**
 * Update device metadata
 */

/**
 * @param {string} deviceId
 * @param {ApiDeviceMetadata} apiDeviceMetadata
 * @return {Reader.<updateDeviceSettingsMetadata~callback>}
 */
const updateDeviceSettingsMetadata = (deviceId, apiDeviceMetadata) => reader(
  /**
   * @param {dal} dal
   * @return {Promise.<ApiDeviceMetadata>}
   */
  ({ dal }) => {
    const { note, alias } = apiDeviceMetadata;

    return FutureTEither
      .fromEither(toDbDeviceMetadata({ id: deviceId, note, alias })
        .map(pick(['id', 'note', 'alias']))
        .chain(Future.encaseP(dal.deviceMetadataRepository.update))
        .promise());
  });

/**
 * Refresh device communication by deleting mac aes key from store and db
 * @param {guid} deviceId
 * @return {Status}
 */
const refreshDevice = deviceId => reader(
  ({ macAesKeyStore }) => macAesKeyStore.remove(deviceId)
    .then(constant({ result: true, message: 'Device refreshed' }))
);

/**
 * @return {Reader.<countDevicesByStatus~callback>}
 */
const countDevicesByStatus = () => reader(
  /**
   * @function countDevicesByStatus~callback
   * @param {DB} DB
   * @param {DeviceStore} deviceStore
   * @return {Promise.<number>}
   */
  ({ DB, deviceStore }) => allP([
    DB.olt.list(), DB.erouter.list(), DB.airCube.list(), DB.airMax.list(), DB.eswitch.list(), DB.epower.list(),
    DB.airFiber.list(), DB.toughSwitch.list(),
  ])
    .then(flatten)
    .then(fromDbDeviceList({ deviceStore }))
    .then(map(countBy(get(['overview', 'status']))))
    .then(map(assign({ // add default values for missing statuses
      [StatusEnum.Active]: 0,
      [StatusEnum.Disconnected]: 0,
      [StatusEnum.Unauthorized]: 0,
    })))
    .then(cata(rejectP, resolveP))
);

const markDeviceAuthorized = curry((siteId, dbDevice) => {
  const status = pathEq(['overview', 'status'], StatusEnum.Disconnected, dbDevice)
    ? StatusEnum.Disconnected
    : StatusEnum.Active;

  return flow(
    assocPath(['overview', 'status'], status),
    assocPath(['identification', 'authorized'], true),
    assocPath(['identification', 'site'], { id: siteId })
  )(dbDevice);
});

const canLogDeviceAuthorizeEvent = cmDevice => (
  getOr(null, ['identification', 'siteId'], cmDevice) === null &&
  getOr(null, ['identification', 'type'], cmDevice) !== DeviceTypeEnum.Onu
);

/**
 * @function authorizeDevice
 * @param {String} deviceId
 * @param {String} siteId
 * @param {String} userId
 * @return {Reader.<authorizeDevice~callback>}
 */
const authorizeDevice = (deviceId, siteId, userId = null) => reader(
   /**
   * @function authorizeDevice~callback
   * @param {DB} DB
   * @param {dal} dal
   * @param {messageHub} messageHub
   * @param {site} site
   * @param {user} user
   * @param {eventLog} eventLog
   * @return {Promise.<{result,message}>}
   */
  ({ user, site, dal, eventLog, DB, messageHub }) => {
    const dbDeviceP = DB.device.findById(deviceId)
      .then(tapP(entityExistsCheck(EntityEnum.Device)));

    const dbDeviceMetadataP = dal.deviceMetadataRepository.findById(deviceId)
      .then(tapP(entityExistsCheck(EntityEnum.DeviceMetadata)));

    const newCmSiteP = dal.siteRepository.findOneById(siteId)
      .then(fromDbSite({}))
      .then(cata(rejectP, resolveP));

    const cmUserP = isNull(userId) ? null : user.getUser(userId).promise();

    const oldCmSiteP = dbDeviceP
      .then(getOr(null, ['identification', 'site', 'id']))
      .then(when(
        isNotNull,
        oldSiteId => dal.siteRepository.findOneById(oldSiteId)
          .then(fromDbSite({}))
          .then(cata(rejectP, resolveP))
      ));

    const updateDeviceP = dbDeviceP
      .then(markDeviceAuthorized(siteId))
      .then(tapP(DB.device.update));

    const cmDeviceP = allP([dbDeviceP, dbDeviceMetadataP, newCmSiteP])
      .then(([dbDevice, dbDeviceMetadata, correspondenceSite]) => fromDbDevice({ correspondenceSite }, dbDevice)
        .chain(merge(mergeMetadata, fromDbDeviceMetadata({}, dbDeviceMetadata)))
        .cata(rejectP, resolveP)
      );

    return Promise.all([cmDeviceP, cmUserP, newCmSiteP, updateDeviceP])
      .then(when(
        spread(canLogDeviceAuthorizeEvent),
        spread(eventLog.logDeviceAuthorizedEvent)
      ))
      .then(() => allP([oldCmSiteP, newCmSiteP])
        .then(map(getOr(null, ['identification', 'id'])))
        .then(compact)
        .then(map(site.synchronizeSiteStatus))
        .then(allP)
      )
      .then(constant(Promise.all([cmDeviceP, newCmSiteP, cmUserP])))
      .then(spread(eventLog.logDeviceMovedEvent))
      .then(resolveP(cmDeviceP).then(cmDevice => messageHub.publish(messageHub.messages.deviceAuthorized(cmDevice))))
      .then(constant({ result: true, message: 'Authorized' }))
      .catch(noop);
  }
);

const automaticAuthorizeDevice = (deviceId, siteId) => reader(({ user, site, dal, eventLog, DB }) => {
  const dbDeviceP = Either.of(DB.device.findById(deviceId)).cata(constant(null), resolveP);
  const dbSiteP = Either.of(dal.siteRepository.findOneById(siteId)).cata(constant(null), resolveP);
  return allP([dbDeviceP, dbSiteP])
    .then(([dbDevice, dbSite]) => {
      if (isNull(dbDevice) && isNull(dbSite)) { return resolveP() }

      return authorizeDevice(deviceId, siteId).run({ user, site, dal, eventLog, DB });
    });
});

const ssidList = () => reader(
  ({ DB }) => {
    const cmAirMaxSsidList$ = Observable.fromPromise(DB.airMax.list())
      .mergeEither(fromDbDeviceList({}))
      .mergeEither(toApiSsidAirMaxList);

    const cmAirCubeSsidList$ = Observable.fromPromise(DB.airCube.list())
      .mergeEither(fromDbDeviceList({}))
      .mergeEither(toApiSsidAirCubeList);

    return Observable.forkJoin([cmAirCubeSsidList$, cmAirMaxSsidList$])
      .map(flatten)
      .toPromise();
  }
);

const apDeviceModeValues = [
  WirelessModeEnum.Ap,
  WirelessModeEnum.ApPtp,
  WirelessModeEnum.ApPtmp,
  WirelessModeEnum.ApPtmpAirmaxMixed,
  WirelessModeEnum.ApPtmpAirmaxAc,
];

const isInApMode = curry((deviceType, device) =>
  pathSatisfies(includes(__, apDeviceModeValues), [deviceType, 'wirelessMode'], device)
);
const isAirMaxInApMode = isInApMode('airmax');
const isAirFiberInApMode = isInApMode('airfiber');
const ltuAirFiberValues = [DeviceModelEnum.AFLTU, DeviceModelEnum.AFLTULITE, DeviceModelEnum.AF5XHD];
const isAirFiberLtuType = pathSatisfies(includes(__, ltuAirFiberValues), ['identification', 'model']);
const hasMacAddress = propSatisfies(isNotNull, 'mac');

const apsProfiles = () => reader(
  ({ DB, dal }) => {
    const dbSiteList$ = Observable.fromPromise(dal.siteRepository.findAllSites());

    const cmAirMaxApsProfileList$ = dbSiteList$
      .mergeMap(dbSiteList => Observable.fromPromise(DB.airMax.list())
        .mergeEither(fromDbDeviceList({ dbSiteList })))
      .map(filter(isAirMaxInApMode))
      .mergeEither(toApiApsProfileAirMaxList);

    const cmAirFiberApsProfileList$ = dbSiteList$
      .mergeMap(dbSiteList => Observable.fromPromise(DB.airFiber.list())
        .mergeEither(fromDbDeviceList({ dbSiteList }))
      )
      .map(filter(isAirFiberLtuType))
      .map(filter(isAirFiberInApMode))
      .mergeEither(toApiApsProfileAirFiberList);

    return Observable.forkJoin([cmAirMaxApsProfileList$, cmAirFiberApsProfileList$])
      .map(flatten)
      .map(filter(hasMacAddress))
      .toPromise();
  }
);

const setLag = curry((enable, deviceId) => reader(
  ({ deviceStore }) => Observable.of(deviceStore.get(deviceId))
    .do(entityExistsCheck(EntityEnum.Device))
    .mergeMap(commDevice => commDevice.updateLayer2({ lagEnabled: enable }))
    .toPromise()
));

const enableLag = setLag(true);
const disableLag = setLag(false);

module.exports = {
  deviceList,
  deviceDetail,
  restartDevice,
  upgradeFirmwareToLatest,
  macAddressList,
  resetInterfaceStatistics,
  getDeviceUnmsSettings,
  updateDeviceUnmsSettings,
  refreshDevice,
  countDevicesByStatus,
  updateDeviceSettingsMetadata,
  getSystem,
  setSystem,
  getServices,
  setServices,
  authorizeDevice,
  automaticAuthorizeDevice,
  ssidList,
  enableLag,
  disableLag,
  apsProfiles,
};
