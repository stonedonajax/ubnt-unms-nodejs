'use strict';

const validation = require('../../../validation');


/*
 * Hapijs routes definition
 */

function registerRoutes(server, options, view) {
  server.route({
    method: 'GET',
    path: '/v2.1/devices/epowers/{id}',
    config: {
      validate: {
        params: {
          id: validation.deviceId,
        },
      },
    },
    handler: view.deviceDetail,
  });
}

module.exports = {
  registerRoutes,
};
