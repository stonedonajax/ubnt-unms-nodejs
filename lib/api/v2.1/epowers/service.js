'use strict';

const { Reader: reader } = require('monet');
const { when } = require('ramda');
const { isNotNull } = require('ramda-adjunct');
const { tap } = require('lodash/fp');

const { resolveP, rejectP, allP, entityExistsCheck } = require('../../../util');
const { EntityEnum } = require('../../../enums');

const { parseDbDeviceSiteId } = require('../../../transformers/device/parsers');
const { fromDb: fromDbDevice } = require('../../../transformers/device');
const { merge: mergeM } = require('../../../transformers');
const { mergeMetadata } = require('../../../transformers/device/mergers');
const { fromDb: fromDbDeviceMetadata } = require('../../../transformers/device/metadata');

/*
 * EdgePower detail.
 */

const epowerDetail = epowerId => reader(
  ({ DB, deviceStore, firmwareDal, dal }) => {
    const dbEpowerPromise = DB.epower.findById(epowerId)
      .then(tap(entityExistsCheck(EntityEnum.Epower)));
    const dbSitePromise = dbEpowerPromise
      .then(parseDbDeviceSiteId)
      .then(when(isNotNull, dal.siteRepository.findOneById));
    const dbDeviceMetadataPromise = dal.deviceMetadataRepository.findById(epowerId);

    return allP([dbSitePromise, dbEpowerPromise, dbDeviceMetadataPromise])
      .then(([dbSite, dbEpower, dbDeviceMetadata]) =>
        fromDbDevice({ firmwareDal, deviceStore, dbSite }, dbEpower)
          .chain(mergeM(mergeMetadata, fromDbDeviceMetadata({}, dbDeviceMetadata)))
          .cata(rejectP, resolveP));
  }
);

module.exports = {
  epowerDetail,
};
