'use strict';

const { Reader: reader } = require('monet');
const { cata } = require('ramda-adjunct');

const { resolveP, rejectP } = require('../../../util');
const { toApiEpowerStatusDetail } = require('../../../transformers/device');

const deviceDetail = (request, reply) => reader(
  ({ service }) => {
    const { id: deviceId } = request.params;

    reply(
      service
        .epowerDetail(deviceId)
        .then(toApiEpowerStatusDetail)
        .then(cata(rejectP, resolveP))
    );
  }
);

module.exports = {
  deviceDetail,
};
