'use strict';

/*
 * Hapijs Plugin definition
 */

const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../../../util/hapi');
const { registerRoutes } = require('./routes');
const viewModule = require('./view');


function register(server, options) {
  const { firmwareDal: service } = server.plugins;

  const view = {
    getFirmwares: weave(viewModule.getFirmwares, { service }),
    uploadFirmwareImage: weave(viewModule.uploadFirmwareImage, { service }),
    removeFirmwares: weave(viewModule.removeFirmwares, { service }),
    downloadFirmwares: weave(viewModule.downloadFirmwares, { service }),
  };

  registerRoutes(server, options, view);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiFirmwaresV2.1',
  version: '1.0.0',
  dependencies: 'firmwareDal',
};
