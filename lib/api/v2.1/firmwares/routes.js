'use strict';

const validation = require('../../../validation');

/*
 * Route definitions
 */

function registerRoutes(server, options, view) {
  server.route({
    method: 'GET',
    path: '/v2.1/firmwares',
    handler: view.getFirmwares,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/firmwares',
    config: {
      auth: {
        scope: 'admin',
      },
      payload: {
        output: 'stream',
        maxBytes: 500e6,
        parse: false,
        allow: 'multipart/form-data',
      },
    },
    handler: view.uploadFirmwareImage,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/firmwares/delete',
    config: {
      auth: {
        scope: 'admin',
      },
      validate: {
        payload: validation.firmwareDeleteList,
      },
    },
    handler: view.removeFirmwares,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/firmwares/download',
    handler: view.downloadFirmwares,
  });
}

module.exports = {
  registerRoutes,
};
