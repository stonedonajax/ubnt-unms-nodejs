'use strict';

const { Reader: reader } = require('monet');
const { Observable } = require('rxjs/Rx');
const { Form } = require('multiparty');
const { isString } = require('lodash/fp');

const { FirmwareOriginEnum } = require('../../../enums');
const { toApiFirmwareList, toApiFirmware } = require('./transformers/mappers');

/**
 * @param {Hapi.Request} request
 * @param {Function} reply
 * @return {Reader.<getFirmwares~callback>}
 */
const getFirmwares = (request, reply) => reader(
  /**
   * @function getFirmwares~callback
   * @param {FirmwareDal} service
   * @return {ApiFirmware[]}
   */
  ({ service }) => reply(toApiFirmwareList(service.findAll()))
);

/**
 * @param {Hapi.Request} request
 * @param {Function} reply
 * @return {Reader.<uploadFirmwareImage~callback>}
 */
const uploadFirmwareImage = (request, reply) => reader(
  /**
   * @function uploadFirmwareImage~callback
   * @param {FirmwareDal} service
   * @return {Promise.<ApiFirmware>}
   */
  ({ service }) => {
    const rawRequest = request.raw.req;
    const form = new Form();

    const error$ = Observable.fromEvent(form, 'error').mergeMap(Observable.throw);
    const close$ = Observable.fromEvent(form, 'close');
    reply(
      Observable.fromEvent(form, 'part')
        .merge(error$)
        .takeUntil(close$)
        .mergeMap((part) => {
          if (!isString(part.filename)) {
            part.resume();
            return Observable.empty();
          }

          return service.save(FirmwareOriginEnum.Manual, part)
            .then(toApiFirmware);
        })
        .first()
        .toPromise()
    );

    form.parse(rawRequest);
  }
);

/**
 * @param {Hapi.Request} request
 * @param {Function} reply
 * @return {Reader.<removeFirmwares~callback>}
 */
const removeFirmwares = (request, reply) => reader(
  /**
   * @function removeFirmwares~callback
   * @param {FirmwareDal} service
   * @return {Promise.<ApiFirmware[]>}
   */
  ({ service }) => reply(
    Observable.from(request.payload)
      .map(service.findById)
      .mergeMap(firmware => service.remove(firmware))
      .toArray()
      .map(toApiFirmwareList)
      .toPromise()
  )
);

/**
 * @param {Hapi.Request} request
 * @param {Function} reply
 * @return {Reader.<downloadFirmwares~callback>}
 */
const downloadFirmwares = (request, reply) => reader(
    /**
   * @function downloadFirmwares~callback
   * @param {FirmwareDal} service
   * @return {Promise.<ApiFirmware[]>}
   */
  ({ service }) => reply(
      service.api.updateFirmwares()
        .map(toApiFirmwareList)
        .toPromise()
    )
);


module.exports = {
  uploadFirmwareImage,
  getFirmwares,
  removeFirmwares,
  downloadFirmwares,
};
