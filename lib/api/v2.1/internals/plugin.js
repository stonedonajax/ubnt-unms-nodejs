'use strict';

const { registerPlugin } = require('../../../util/hapi');
const { registerRoutes } = require('./routes');


function register(server, options) {
  registerRoutes(server, options);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiInternalsV2.1',
  version: '1.0.0',
  dependencies: ['socketShell'],
};
