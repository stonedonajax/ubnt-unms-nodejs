'use strict';

const { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } = require('http-status');

const { ErrorSchema, StatusSchema, AuthHeaderSchema } = require('../osm');


function registerRoutes(server) {
  server.route({
    method: 'POST',
    path: '/v2.1/ws/restricted',
    config: {
      description: 'Verify websocket client',
      tags: ['api', 'websocket'],
      isInternal: true,
      validate: {
        headers: AuthHeaderSchema,
      },
      auth: {
        scope: 'admin',
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler(request, reply) {
      reply({ result: true, message: 'ok' });
    },
  });

  server.route({
    method: 'POST',
    path: '/v2.1/ws/unrestricted',
    config: {
      description: 'Verify websocket client',
      tags: ['api', 'websocket'],
      isInternal: true,
      validate: {
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler(request, reply) {
      reply({ result: true, message: 'ok' });
    },
  });
}


module.exports = {
  registerRoutes,
};
