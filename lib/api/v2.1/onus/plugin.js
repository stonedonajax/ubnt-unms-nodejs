'use strict';

/*
 * Hapijs Plugin definition
 */

const { weave, weaveLazy } = require('ramda-adjunct');
const { curry, pick } = require('lodash/fp');

const { registerPlugin } = require('../../../util/hapi');
const { registerRoutes } = require('./routes');
const {
  onuList, onuList: { loadData: loadOnuList }, onuDetail, restartOnu, getOnuClients,
  removeOnu, resetInterfaceListStatistics, blockOnu, unblockOnu, updateOnu,
} = require('./service');


const lazyConfig = curry((pluginNames, server) => () => pick(pluginNames, server.plugins));


function register(server, options) {
  server.expose('loadOnuList', loadOnuList);

  const { firmwareDal, deviceStore, DB, eventLog, dal } = server.plugins;

  const service = {
    onuList: weaveLazy(onuList, lazyConfig(['apiOltsV2.1', 'deviceStore', 'firmwareDal', 'DB', 'dal'], server)),
    onuDetail: weave(onuDetail, { deviceStore, firmwareDal, DB, dal }),
    restartOnu: weave(restartOnu, { deviceStore, DB }),
    removeOnu: weave(removeOnu, { deviceStore, DB, eventLog }),
    resetInterfaceListStatistics: weave(resetInterfaceListStatistics, { DB }),
    blockOnu: weave(blockOnu, { deviceStore, DB, firmwareDal, dal }),
    unblockOnu: weave(unblockOnu, { deviceStore, DB, firmwareDal, dal }),
    updateOnu: weave(updateOnu, { deviceStore, DB, firmwareDal, dal }),
    loadOnuList: weave(onuList.loadData, { deviceStore, firmwareDal, DB, dal }),
    getOnuClients: weave(getOnuClients, { DB, deviceStore }),
  };

  server.expose(service);

  return registerRoutes(server, options, service);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiOnusV2.1',
  version: '2.1.0',
  dependencies: ['deviceStore', 'firmwareDal', 'DB', 'eventLog'],
};
