'use strict';

const joi = require('joi');

const onuClientSchema = joi.object({
  mac: joi.string().mac().required(),
  vendor: joi.string().required(),
}).label('onuClientSchema');

const onuClientListSchema = joi.array().items(onuClientSchema)
  .label('onuClientListSchema');

module.exports = {
  onuClientListSchema,
};
