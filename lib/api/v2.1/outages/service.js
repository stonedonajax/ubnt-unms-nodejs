'use strict';

const { Reader: reader } = require('monet');
const { keyBy, map, get, compact } = require('lodash/fp');
const { cata } = require('ramda-adjunct');

const { allP, resolveP, rejectP } = require('../../../util');
const { toApiOutageView } = require('../../../transformers/outage');
const { safeParseOutageView } = require('../../../transformers/outage/parsers');
const { fromDbList } = require('../../../transformers/site');

const outageItemList = (outagesParams, aggsParams) => reader(
  ({ dal, outages }) => {
    const outageIdsInProgress = map(get('id'), outages.getOutages());
    const params = outagesParams;
    if (outagesParams.inProgress) {
      params.outageIds = outageIdsInProgress;
    }
    const dbOutageItemListP = dal.outageRepository.findAllByRequestParams(params);
    const dbOutageAggregationItemListP = dal.outageRepository.findAggsByRequestParams(aggsParams);
    const siteMapP = dal.siteRepository.findAllSites()
      .then(fromDbList({}))
      .then(cata(rejectP, keyBy('id')));
    const dbDeviceMetadataListP = dbOutageItemListP
      .then(map(get(['device', 'id'])))
      .then(compact)
      .then(deviceIds => dal.deviceMetadataRepository.findAll({ where: { id: { $in: deviceIds } } }));

    return allP([dbOutageItemListP, dbOutageAggregationItemListP, siteMapP, dbDeviceMetadataListP])
      .then(([dbOutageItemList, dbOutageAggregationItemList, siteMap, dbDeviceMetadataList]) =>
        safeParseOutageView(
          {
            outageIdsInProgress,
            siteMap,
            dbOutageAggregationItemList,
            dbDeviceMetadataList,
            type: outagesParams.type,
            limit: outagesParams.limit,
            currentPage: outagesParams.page,
            inProgress: outagesParams.inProgress,
          },
          dbOutageItemList
        )
          .chain(toApiOutageView)
          .cata(rejectP, resolveP)
      );
  }
);

const countUnread = ({ timestamp }) => reader(
  ({ dal }) => dal.outageRepository.countUnread(timestamp)
);

module.exports = {
  outageItemList,
  countUnread,
};
