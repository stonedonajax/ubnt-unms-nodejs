'use strict';

const { weave } = require('ramda-adjunct');

const viewModule = require('./view');
const { registerPlugin } = require('../../../util/hapi');
const { registerRoutes } = require('./routes');

function register(server, options) {
  const { site: service } = server.plugins;

  const view = {
    importSites: weave(viewModule.importSites, { service }),
    clearAllDeviceSiteRelations: weave(viewModule.clearAllDeviceSiteRelations, { service }),
    createSite: weave(viewModule.createSite, { service }),
    updateSite: weave(viewModule.updateSite, { service }),
    getSites: weave(viewModule.getSites, { service }),
    getSite: weave(viewModule.getSite, { service }),
    deleteSite: weave(viewModule.deleteSite, { service }),

    getImages: weave(viewModule.getImages, { service }),
    rotateImageLeft: weave(viewModule.rotateImageLeft, { service }),
    rotateImageRight: weave(viewModule.rotateImageRight, { service }),
    deleteImage: weave(viewModule.deleteImage, { service }),
    reorderImage: weave(viewModule.reorderImage, { service }),
    getImage: weave(viewModule.getImage, { service }),
    saveImage: weave(viewModule.saveImage, { service }),
    updateImage: weave(viewModule.updateImage, { service }),
  };

  registerRoutes(server, options, view);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiSiteV2.1',
  version: '1.0.0',
  dependencies: ['site'],
};
