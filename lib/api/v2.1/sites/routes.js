'use strict';

const joi = require('joi');
const { OK, UNAUTHORIZED, NOT_ACCEPTABLE, INTERNAL_SERVER_ERROR } = require('http-status');
const { test } = require('ramda');

const { ErrorSchema, StatusSchema } = require('../osm');
const { SitesSchema, SiteSchema, CreateSiteSchema, GallerySchema, ImportSiteListSchema } = require('./schemas');
const validation = require('../../../validation');
const config = require('../../../../config');
const { bytesFormatter } = require('../../../util');


const isPixelLimitError = test(/Input image exceeds pixel limit/);

const isMaxSizeExceededResponse = test(/Payload content length greater than maximum allowed/);

const customizeUploadErrorMessage = (request, reply) => {
  const response = reply.request.response;

  if (response.isBoom) {
    if (isMaxSizeExceededResponse(response.message)) {
      const maxSize = bytesFormatter(config.siteImages.maxBytes);
      response.output.payload.message = `Maximum upload size of ${maxSize} exceeded.`;
    } else if (isPixelLimitError(response.message)) {
      response.output.payload.message = 'Maximum image size exceeded.';
    }
  }

  return reply.continue();
};

function registerRoutes(server, options, view) {
  server.route({
    method: 'DELETE',
    path: '/v2.1/sites/{id}',
    config: {
      description: 'Delete a site',
      tags: ['api', 'site'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          id: validation.siteId,
        },
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.deleteSite,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/sites',
    config: {
      description: 'Create a site',
      tags: ['api', 'site'],
      auth: {
        scope: 'admin',
      },
      validate: {
        payload: CreateSiteSchema,
      },
      response: {
        status: {
          [OK]: SiteSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.createSite,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/sites/devicesiterelations',
    config: {
      description: 'Import sites and/or endpoints',
      tags: ['api', 'site'],
      auth: {
        scope: 'admin',
      },
      validate: {
        payload: ImportSiteListSchema,
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.importSites,
  });

  server.route({
    method: 'DELETE',
    path: '/v2.1/sites/devicesiterelations',
    config: {
      description: 'Clear all device sites/endpoints relations',
      tags: ['api', 'site'],
      auth: {
        scope: 'admin',
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.clearAllDeviceSiteRelations,
  });

  server.route({
    method: 'PUT',
    path: '/v2.1/sites/{id}',
    config: {
      description: 'Update site',
      tags: ['api', 'site'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          id: validation.siteId,
        },
        payload: validation.site,
      },
      response: {
        status: {
          [OK]: SiteSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.updateSite,
  });

  server.route({
    method: 'GET',
    path: '/v2.1/sites',
    config: {
      description: 'Get all sites',
      tags: ['api', 'site'],
      validate: {
        query: {
          id: joi.array().items(validation.siteId.optional()).single(true),
        },
      },
      response: {
        status: {
          [OK]: SitesSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.getSites,
  });

  server.route({
    method: 'GET',
    path: '/v2.1/sites/{id}',
    config: {
      description: 'Get a single site detail',
      tags: ['api', 'site'],
      validate: {
        params: {
          id: validation.siteId,
        },
      },
      response: {
        status: {
          [OK]: SiteSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.getSite,
  });

  server.route({
    method: 'GET',
    path: '/v2.1/sites/{id}/images',
    config: {
      description: 'Get image list',
      tags: ['api', 'site', 'gallery'],
      validate: {
        params: {
          id: validation.siteId,
        },
      },
      response: {
        status: {
          [OK]: GallerySchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.getImages,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/sites/{siteId}/images/{imageId}/rotateleft',
    config: {
      description: 'Rotate image left',
      tags: ['api', 'site', 'gallery'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          siteId: validation.siteId,
          imageId: validation.imageId,
        },
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.rotateImageLeft,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/sites/{siteId}/images/{imageId}/rotateright',
    config: {
      description: 'Rotate image right',
      tags: ['api', 'site', 'gallery'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          siteId: validation.siteId,
          imageId: validation.imageId,
        },
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.rotateImageRight,
  });

  server.route({
    method: 'DELETE',
    path: '/v2.1/sites/{siteId}/images/{imageId}',
    config: {
      description: 'Delete image',
      tags: ['api', 'site', 'gallery'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          siteId: validation.siteId,
          imageId: validation.imageId,
        },
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.deleteImage,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/sites/{siteId}/images/{imageId}/reorder',
    config: {
      description: 'Reorder images',
      tags: ['api', 'site', 'gallery'],
      auth: {
        scope: 'admin',
      },
      validate: {
        payload: validation.imageReorder,
        params: {
          siteId: validation.siteId,
          imageId: validation.imageId,
        },
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.reorderImage,
  });

  server.route({
    method: 'GET',
    path: '/v2.1/sites/{siteId}/images/{imageId}',
    config: {
      description: 'Get site image',
      tags: ['api', 'site', 'gallery'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          siteId: validation.siteId,
          imageId: validation.imageId,
        },
      },
      response: {
        status: {
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.getImage,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/sites/{id}/images',
    config: {
      description: 'Insert site image',
      tags: ['api', 'site', 'gallery'],
      auth: {
        scope: 'admin',
      },
      payload: {
        output: 'stream',
        parse: false,
        maxBytes: config.siteImages.maxBytes,
        allow: 'multipart/form-data',
      },
      validate: {
        params: {
          id: validation.siteId,
        },
      },
      ext: {
        onPreResponse: {
          method: customizeUploadErrorMessage,
        },
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.saveImage,
  });

  server.route({
    method: 'PATCH',
    path: '/v2.1/sites/{siteId}/images/{imageId}',
    config: {
      description: 'Update site image',
      tags: ['api', 'site', 'gallery'],
      auth: {
        scope: 'admin',
      },
      validate: {
        params: {
          siteId: validation.siteId,
          imageId: validation.imageId,
        },
        payload: validation.imageData,
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.updateImage,
  });
}

module.exports = {
  registerRoutes,
};
