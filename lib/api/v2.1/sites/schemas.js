'use strict';

const joi = require('joi');
const validation = require('../../../validation');
const { SiteTypeEnum } = require('../../../enums');

const ImportSiteSchema = joi.object().keys({
  parentSiteName: joi.string().allow(null).required(),
  name: joi.string().allow(null).required(),
  type: joi.string().valid(SiteTypeEnum.Site, SiteTypeEnum.Client).required(),
  longitude: joi.number().min(-180).max(180).required().allow(null),
  latitude: joi.number().min(-90).max(90).required().allow(null),
  height: joi.number().allow(null).required(),
  elevation: joi.number().allow(null).required(),
  address: joi.string().allow(null).required(),
  contactName: joi.string().allow(null).required(),
  contactPhone: joi.string().allow(null).required(),
  contactEmail: joi.string().allow(null).required(),
  note: joi.string().allow(null).required(),
  macAddresses: joi.array().items(joi.string()).required().allow(null),
  ipAddresses: joi.array().items(joi.string().ip()).required().allow(null),
});

const ImportSiteListSchema = joi.array().min(1).items(ImportSiteSchema);

const CreateSiteSchema = joi.object().keys({
  parentSiteId: validation.siteId.required().allow(null),
  name: joi.string().allow(null).required(),
  location: joi.object().keys({
    longitude: joi.number().min(-180).max(180).required(),
    latitude: joi.number().min(-90).max(90).required(),
  }).allow(null).required(),
  height: joi.number().allow(null).required(),
  elevation: joi.number().allow(null).required(),
  address: joi.string().allow(null).required(),
  contactName: joi.string().allow(null),
  contactPhone: joi.string().allow(null),
  contactEmail: joi.string().allow(null),
  note: joi.string().allow(null).required(),
}).label('CreateSite');

const SiteSchema = joi.object().keys({
  id: validation.siteId,
  identification: joi.object().required(),
  description: joi.object().required(),
  notifications: joi.object().required(),
}).label('Site');

const SitesSchema = joi.array().items(SiteSchema);

const ImageSchema = joi.object({
  id: validation.imageId,
  identification: joi.object({ id: validation.imageId }).required(),
  name: joi.string().allow(''),
  height: joi.number().required(),
  width: joi.number().required(),
  size: joi.number().required(),
  order: joi.number().required(),
  date: joi.object().required(),
  description: joi.string().required().allow(''),
  fileName: joi.string().required(),
  fileType: joi.string().required(),
  fullUrl: joi.string().required(),
  thumbUrl: joi.string().required(),
}).label('Image');

const GallerySchema = joi.array().items(ImageSchema);

module.exports = {
  CreateSiteSchema,
  SitesSchema,
  SiteSchema,
  ImageSchema,
  GallerySchema,
  ImportSiteListSchema,
};
