'use strict';

const aguid = require('aguid');
const { getOr, has, escape, flow } = require('lodash/fp');
const { when, propSatisfies, merge, always, ifElse, isNil } = require('ramda');
const { isNotNull } = require('ramda-adjunct');

const { SiteTypeEnum, AlertTypeEnum, StatusEnum } = require('../../../enums');


const parseLatitude = (payload) => {
  if (has(['latitude'], payload)) { return payload.latitude }
  return flow(getOr(null, ['location', 'latitude']), when(isNotNull, Number))(payload);
};

const parseLongitude = (payload) => {
  if (has(['longitude'], payload)) { return payload.longitude }
  return flow(getOr(null, ['location', 'longitude']), when(isNotNull, Number))(payload);
};

// calculateNewSitePropsByType :: Object -> Object
const calculateNewSitePropsByType = payload => ifElse(
  propSatisfies(isNil, 'parentSiteId'),
  always({ type: SiteTypeEnum.Site, notification_type: AlertTypeEnum.System }),
  always({ type: SiteTypeEnum.Endpoint, notification_type: AlertTypeEnum.None })
)(payload);

const createNewSite = payload => merge(calculateNewSitePropsByType(payload), {
  id: aguid(),
  status: StatusEnum.Inactive,
  name: payload.name,
  address: getOr(null, ['address'], payload),
  note: flow(getOr(null, ['note']), when(isNotNull, escape))(payload),
  parent_id: getOr(null, ['parentSiteId'], payload),
  contact_name: getOr(null, ['contactName'], payload),
  contact_email: getOr(null, ['contactEmail'], payload),
  contact_phone: getOr(null, ['contactPhone'], payload),
  latitude: parseLatitude(payload),
  longitude: parseLongitude(payload),
  height: getOr(null, ['height'], payload),
  elevation: getOr(null, ['elevation'], payload),
});


module.exports = {
  createNewSite,
};
