'use strict';

const { Reader: reader } = require('monet');
const moment = require('moment-timezone');
const { getOr, constant, spread, size, isString, noop, curry, __, merge } = require('lodash/fp');
const { head, always } = require('ramda');
const { isNotNil } = require('ramda-adjunct');
const sharp = require('sharp');
const bluebird = require('bluebird');
const fs = bluebird.promisifyAll(require('fs-extra'));
const boom = require('boom');
const { Observable } = require('rxjs/Rx');
const aguid = require('aguid');
const { Form } = require('multiparty');
const cloneable = require('cloneable-readable');

const config = require('../../../../config');
require('../../../util/observable');
const {
  toApiOverviewList, toApiOverview, toApiImageList, toDb: toDbSite, fromApiSite, toDbImage,
} = require('../../../transformers/site');
const { getSiteImagesFilePath, getSiteImagesDir } = require('../../../util/siteImage');
const { readFile } = require('../../../util');
const { createNewSite } = require('./util');
const { removeImageFiles } = require('../../../util/siteImage');

// improve memory consumption
sharp.cache(false);
sharp.concurrency(1);

const { readFileAsync, ensureDirAsync, removeAsync } = fs;

function createSite(request, reply) {
  return reader(({ service }) => {
    const newSite$ = Observable.of(createNewSite(request.payload));
    const parentSite$ = request.payload.parentSiteId
      ? service.getSite(request.payload.parentSiteId)
      : Observable.of(null);

    return reply(
      Observable.merge(newSite$, parentSite$.ignoreElements())
        .mergeMap(service.createSite)
        .mergeEither(toApiOverview)
        .toPromise()
    );
  });
}

function updateSite(request, reply) {
  return reader(({ service }) => {
    const siteId = request.params.id;

    const notifications$ = Observable.from(service.clearNotification(siteId))
      .switchMap(() => Observable.from(getOr([], ['payload', 'notifications', 'users'], request)))
      .mergeMap(service.addSiteNotification(siteId))
      .ignoreElements();

    const site$ = Observable.of(request.payload)
      .mergeEither(fromApiSite({}))
      .mergeEither(toDbSite)
      .mergeMap(service.updateSite)
      .mergeEither(toApiOverview);

    return reply(
      notifications$.concat(site$).toPromise()
    );
  });
}

function getSites(request, reply) {
  const siteIds = getOr([], ['query', 'id'], request);

  return reader(({ service }) => reply(
    service
      .getSites(siteIds)
      .mergeEither(toApiOverviewList)
      .toPromise()
  ));
}

function getSite(request, reply) {
  return reader(({ service }) => reply(
    service
      .getSite(request.params.id)
      .mergeEither(toApiOverview)
      .toPromise()
  ));
}

const checkCanDeleteSite = (devicesList, siteEndpointsCount) => {
  if (siteEndpointsCount > 0) { throw boom.badData('The site contains clients.') }
  if (size(devicesList) > 0) { throw boom.badData('The site contains devices.') }
};


function deleteSite(request, reply) {
  return reader(({ service }) => {
    // TODO(karel.kristal@ubnt.com): read devices count from PG in one SQL query when devices are in PG too
    const siteId = request.params.id;
    const deviceList$ = service.getDevicesBySiteId(siteId);
    const site$ = service.getSiteEndpointsCount(siteId);

    return reply(
      Observable.forkJoin(deviceList$, site$)
        .map(spread(checkCanDeleteSite))
        .tapO(() => removeAsync(getSiteImagesDir(siteId)))
        .mergeMap(() => service.deleteSite(siteId))
        .mapTo({ result: true, message: 'Site deleted.' })
        .toPromise()
    );
  });
}

function getImages(request, reply) {
  const siteId = request.params.id;
  const hostname = `https://${request.info.host}/`;

  return reader(({ service }) => reply(
    service
      .getImages(siteId, hostname)
      .mergeEither(toApiImageList)
      .toPromise()
  ));
}

const handleImageRotation = (request, reply, direction) => reader(({ service }) => {
  const angle = direction === 'left' ? 270 : 90;
  const { siteId, imageId } = request.params;
  const fileName = getSiteImagesFilePath(siteId, imageId);
  const thumbName = getSiteImagesFilePath(siteId, imageId, true);

  return reply(
    service
      .getImage(imageId)
      .tapO(() => {
        const thumbPromise = readFileAsync(thumbName)
          .then(thumbData => sharp(thumbData).rotate(angle).toFile(thumbName));
        const filePromise = readFileAsync(fileName)
          .then(fileData => sharp(fileData).rotate(angle).toFile(fileName));

        return Promise.all([thumbPromise, filePromise]);
      })
      .mergeMap(cmImage => service.increaseImageVersion(cmImage)
        .mapTo({ result: true, message: `Image rotated ${direction}` })
      )
      .toPromise()
  );
});

function rotateImageLeft(request, reply) {
  return reader(({ service }) => handleImageRotation(request, reply, 'left').run({ service }));
}

function rotateImageRight(request, reply) {
  return reader(({ service }) => handleImageRotation(request, reply, 'right').run({ service }));
}

function deleteImage(request, reply) {
  return reader(({ service }) => {
    const imageId = request.params.imageId;
    const siteId = request.params.siteId;

    return reply(
      service
        .deleteImage(imageId)
        .tapO(() => removeImageFiles(siteId, imageId))
        .mapTo({ result: true, message: 'Image deleted' })
        .toPromise()
    );
  });
}

function reorderImage(request, reply) {
  return reader(({ service }) => {
    const siteId = request.params.siteId;
    const { currentOrder, nextOrder } = request.payload;

    return reply(
      service
        .reorderImage(siteId, currentOrder, nextOrder)
        .map(constant({ result: true, message: 'Images reordered' }))
        .toPromise()
    );
  });
}

function getImage(request, reply) {
  return reader(({ service }) => {
    const { siteId, imageId } = request.params;
    const site$ = service.getSite(siteId);
    const image$ = service.getImage(imageId);

    return reply(
      Observable.forkJoin(site$, image$)
        .map(constant(getSiteImagesFilePath(siteId, imageId)))
        .map(readFile)
        .toPromise()
    );
  });
}

const createNewImagesData = curry((siteId, newImages) => newImages.map((data) => {
  const { id, name, imageInfo } = data;
  const { format, height, width, size: imageSize } = imageInfo;
  return {
    id,
    name,
    height,
    width,
    siteId,
    fileName: name,
    description: '',
    date: moment().format(),
    size: Math.round(imageSize / 1024),
    fileType: format,
  };
}));

const saveImageFile = (fileStream, siteId) => {
  const { width, height } = config.siteImages.thumb;
  const maxResolution = config.siteImages.maxResolution;

  const id = aguid();
  const name = fileStream.filename;

  return ensureDirAsync(getSiteImagesDir(siteId))
    .then(() => new Promise((resolve, reject) => {
      const stream = cloneable(fileStream);

      const imageFile = getSiteImagesFilePath(siteId, id);
      const thumbFile = getSiteImagesFilePath(siteId, id, true);

      let callCounter = 2; // await two callbacks call before resolve
      let imageInfo = null;

      // for some reason it's not possible to clone pipelines
      const imagePipeline = sharp()
        .sequentialRead()
        .limitInputPixels(maxResolution)
        .jpeg()
        .toFile(imageFile, (err, data) => {
          callCounter -= 1;
          if (isNotNil(err)) {
            callCounter = 0; // prevent resolve
            reject(err);
          } else if (callCounter === 0) {
            resolve({ imageInfo: data, name, id, siteId });
          } else {
            imageInfo = data;
          }
        });

      const thumbPipeline = sharp()
        .sequentialRead()
        .limitInputPixels(maxResolution)
        .resize(width, height)
        .jpeg()
        .toFile(thumbFile, (err) => {
          callCounter -= 1;
          if (isNotNil(err)) {
            callCounter = 0; // prevent resolve
            reject(err);
          } else if (callCounter === 0) {
            resolve({ imageInfo, name, id, siteId });
          }
        });

      stream.clone().pipe(imagePipeline);
      stream.pipe(thumbPipeline);
    }));
};

function saveImage(request, reply) {
  return reader(({ service }) => {
    const siteId = request.params.id;
    const rawRequest = request.raw.req;
    const form = new Form();

    const error$ = Observable.fromEvent(form, 'error').mergeMap(Observable.throw);
    const close$ = Observable.fromEvent(form, 'close');
    const parts$ = Observable.fromEvent(form, 'part')
      .merge(error$)
      .takeUntil(close$)
      .mergeMap((part) => {
        if (!isString(part.filename)) {
          part.resume();
          return Observable.empty();
        }

        return saveImageFile(part, siteId);
      })
      .toArray(); // buffer to single array

    Observable.from(parts$)
      .map(createNewImagesData(siteId))
      .mergeAll()
      .mergeMap(service.saveImage)
      .subscribe(
        noop,
        reply,
        () => reply({ result: true, message: 'Images uploaded' })
      );

    form.parse(rawRequest);
  });
}

function updateImage(request, reply) {
  return reader(({ service }) => {
    const { siteId, imageId } = request.params;
    const site$ = service.getSite(siteId);
    const image$ = service.getImage(imageId);

    return reply(
      Observable.forkJoin(image$, site$)
        .map(head)
        .map(merge(__, request.payload))
        .mergeEither(toDbImage)
        .mergeMap(service.updateImage)
        .map(constant({ result: true, message: 'Image updated' }))
        .toPromise()
    );
  });
}

function importSites(request, reply) {
  return reader(({ service }) => reply(
    service.handleSitesImport(request.payload)
      .then(always({ result: true, message: 'Import file(s) uploaded.' }))
  ));
}

function clearAllDeviceSiteRelations(request, reply) {
  return reader(({ service }) => service.clearAllDeviceSiteRelations()
    .subscribe(
      noop,
      reply,
      () => reply({ result: true, message: 'Failed to clear device-site relations' })
    )
  );
}


module.exports = {
  deleteSite,
  importSites,
  clearAllDeviceSiteRelations,
  createSite,
  updateSite,
  getSites,
  getSite,

  getImage,
  getImages,
  rotateImageLeft,
  rotateImageRight,
  deleteImage,
  reorderImage,
  saveImage,
  updateImage,
};
