'use strict';

const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../../../util/hapi');
const registerRoutes = require('./routes');
const { start, stop, getAll } = require('./view');

function register(server) {
  const { speedTest } = server.plugins;

  const view = {
    start: weave(start, { speedTest }),
    stop: weave(stop, { speedTest }),
    getAll: weave(getAll, { speedTest }),
  };

  registerRoutes(server, view);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiSpeedTestV2.1',
  dependencies: ['speedTest'],
};
