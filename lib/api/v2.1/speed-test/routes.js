'use strict';

const joi = require('joi');
const { values } = require('ramda');

const { SpeedTestDirectionEnum } = require('../../../enums');

function registerRoutes(server, view) {
  server.route({
    method: 'POST',
    path: '/v2.1/speed-tests/start',
    config: {
      validate: {
        payload: joi.object({
          source: joi.string().uuid().required(),
          target: joi.string().uuid().required(),
          duration: joi.number().allow(null).required(),
          direction: joi.string().valid(...values(SpeedTestDirectionEnum)).required(),
        }),
      },
    },
    handler: view.start,
  });

  server.route({
    method: 'GET',
    path: '/v2.1/speed-tests',
    handler: view.getAll,
  });

  server.route({
    method: 'DELETE',
    path: '/v2.1/speed-tests/{id}',
    config: {
      validate: {
        params: {
          id: joi.string().uuid().required(),
        },
      },
    },
    handler: view.stop,
  });
}

module.exports = registerRoutes;
