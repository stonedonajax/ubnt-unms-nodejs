'use strict';

const { values, map, applySpec, prop, pipe, converge, merge, invoker } = require('ramda');


const toApiSpeedTestStarted = speedTestId => ({
  id: speedTestId,
  result: true,
  message: 'Speed test started.',
});

const toApiSpeedTestMeta = applySpec({
  masterId: prop('source'),
  slaveId: prop('target'),
  timeLimit: prop('duration'),
});

const toApiSpeedTestDetail = converge(merge, [
  applySpec({
    speedTestId: prop('id'),
    timestamp: pipe(prop('timestamp'), invoker(0, 'valueOf')),
    data: prop('data'),
  }),
  pipe(prop('meta'), toApiSpeedTestMeta),
]);

const toApiSpeedTestList = pipe(
  values,
  map(toApiSpeedTestDetail)
);

module.exports = {
  toApiSpeedTestStarted,
  toApiSpeedTestList,
  toApiSpeedTestDetail,
};
