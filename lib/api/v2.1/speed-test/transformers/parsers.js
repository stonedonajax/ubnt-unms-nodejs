'use strict';

const fromApiOptions = options => ({
  source: options.source,
  target: options.target,
  direction: options.direction,
  duration: options.duration,
});

module.exports = {
  fromApiOptions,
};
