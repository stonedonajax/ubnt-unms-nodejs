'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader } = require('monet');

const { fromApiOptions } = require('./transformers/parsers');
const { toApiSpeedTestStarted, toApiSpeedTestList } = require('./transformers/mappers');

const start = (request, reply) => reader(
  ({ speedTest }) => reply(
    Observable.of(request.payload)
      .map(fromApiOptions)
      .mergeMap(speedTest.start)
      .map(toApiSpeedTestStarted)
      .toPromise()
  )
);

const stop = (request, reply) => reader(
  ({ speedTest }) => reply(
    Observable.of(request.params.id)
      .mergeMap(speedTest.stop)
      .mapTo({ result: true, message: 'Speed test stopped' })
      .toPromise()
  )
);

const getAll = (request, reply) => reader(
  ({ speedTest }) => reply(
    speedTest.getSpeedTests()
      .map(toApiSpeedTestList)
      .toPromise()
  )
);

module.exports = {
  start,
  stop,
  getAll,
};
