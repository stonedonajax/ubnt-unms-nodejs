'use strict';

const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../../../util/hapi');
const { DB } = require('../../../db');
const { toughSwitchDetail } = require('./service');
const { deviceDetail } = require('./view');
const { registerRoutes } = require('./routes');


function register(server, options) {
  const { deviceStore, firmwareDal, dal } = server.plugins;

  const toughSwitchDetailBound = weave(toughSwitchDetail, { DB, deviceStore, firmwareDal, dal });

  const service = {
    toughSwitchDetail: toughSwitchDetailBound,
  };

  const view = {
    deviceDetail: weave(deviceDetail, { service }),
  };

  server.expose(service);

  registerRoutes(server, options, view);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiToughSwitchesV2.1',
  version: '2.1.0',
  dependencies: ['deviceStore', 'firmwareDal'],
};
