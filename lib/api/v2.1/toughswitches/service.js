'use strict';

const { Reader: reader } = require('monet');
const { when } = require('ramda');
const { isNotNull } = require('ramda-adjunct');
const { tap } = require('lodash/fp');

const { resolveP, rejectP, allP, entityExistsCheck } = require('../../../util');
const { EntityEnum } = require('../../../enums');

const { parseDbDeviceSiteId } = require('../../../transformers/device/parsers');
const { fromDb: fromDbDevice } = require('../../../transformers/device');
const { merge: mergeM } = require('../../../transformers');
const { mergeMetadata } = require('../../../transformers/device/mergers');
const { fromDb: fromDbDeviceMetadata } = require('../../../transformers/device/metadata');

/*
 * ToughSwitch detail.
 */

const toughSwitchDetail = toughSwitchId => reader(
  ({ DB, deviceStore, firmwareDal, dal }) => {
    const dbToughSwitchPromise = DB.toughSwitch.findById(toughSwitchId)
      .then(tap(entityExistsCheck(EntityEnum.ToughSwitch)));
    const dbSitePromise = dbToughSwitchPromise
      .then(parseDbDeviceSiteId)
      .then(when(isNotNull, dal.siteRepository.findOneById));
    const dbDeviceMetadataPromise = dal.deviceMetadataRepository.findById(toughSwitchId);

    return allP([dbSitePromise, dbToughSwitchPromise, dbDeviceMetadataPromise])
      .then(([dbSite, dbToughSwitch, dbDeviceMetadata]) =>
        fromDbDevice({ firmwareDal, deviceStore, dbSite }, dbToughSwitch)
          .chain(mergeM(mergeMetadata, fromDbDeviceMetadata({}, dbDeviceMetadata)))
          .cata(rejectP, resolveP));
  }
);

module.exports = {
  toughSwitchDetail,
};
