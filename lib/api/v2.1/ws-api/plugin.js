'use strict';

const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../../../util/hapi');
const { createConnection, editSubscriptions, restrictedAccess } = require('./view');
const registerRoutes = require('./routes');

function register(server) {
  const service = server.plugins.socketApi;

  const view = {
    createConnection: weave(createConnection, { service }),
    editSubscriptions: weave(editSubscriptions, { service }),
    restrictedAccess: weave(restrictedAccess, { }),
  };

  registerRoutes(server, view);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'apiWsApiV2.1',
  version: '1.0.0',
  dependencies: ['socketApi'],
};
