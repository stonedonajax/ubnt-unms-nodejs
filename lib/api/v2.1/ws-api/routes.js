'use strict';

const { OK, CREATED, UNAUTHORIZED, NOT_ACCEPTABLE, INTERNAL_SERVER_ERROR } = require('http-status');
const joi = require('joi');

const { ErrorSchema, StatusSchema, AuthHeaderSchema } = require('../osm');

function registerRoutes(server, view) {
  server.route({
    method: 'POST',
    path: '/v2.1/ws-api/connection',
    config: {
      description: 'Create websocket connection session',
      tags: ['api', 'ws-api'],
      validate: {
        headers: AuthHeaderSchema,
      },
      response: {
        status: {
          [CREATED]: joi.object({
            result: joi.bool().required(),
            message: joi.string().required(),
            connectionId: joi.string().uuid().required(),
          }),
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.createConnection,
  });

  server.route({
    method: 'POST',
    path: '/v2.1/ws-api/subscription',
    config: {
      description: 'Update websocket subscriptions',
      tags: ['api', 'ws-api'],
      validate: {
        headers: AuthHeaderSchema,
        payload: joi.object({
          connectionId: joi.string().uuid().required(),
          action: joi.string().required(),
          type: joi.string().required(),
          entity: joi.string().required(),
          payload: joi.any().optional(),
        }),
      },
      response: {
        status: {
          [OK]: StatusSchema,
          [UNAUTHORIZED]: ErrorSchema,
          [NOT_ACCEPTABLE]: ErrorSchema,
          [INTERNAL_SERVER_ERROR]: ErrorSchema,
        },
      },
    },
    handler: view.editSubscriptions,
  });
}

module.exports = registerRoutes;
