'use strict';

const fromApiSubscription = apiSubscription => ({
  connectionId: apiSubscription.connectionId,
  type: apiSubscription.type,
  entity: apiSubscription.entity,
  action: apiSubscription.action,
  payload: apiSubscription.payload,
});

module.exports = {
  fromApiSubscription,
};
