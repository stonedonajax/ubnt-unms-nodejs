'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader } = require('monet');

const { fromApiSubscription } = require('./transformers/parser');

const createConnection = (request, reply) => reader(
  ({ service }) => reply({
    result: true,
    message: 'Connection created.',
    connectionId: service.createWsApiConnection(request.token.userId),
  }).code(201)
);

const editSubscriptions = (request, reply) => reader(
  ({ service }) => reply(
    Observable.of(request.payload)
      .map(fromApiSubscription)
      .mergeMap(service.handleSubscription)
      .mapTo({ result: true, message: 'Subscription updated' })
      .take(1) // very ugly hack
      .toPromise()
  )
);

const restrictedAccess = (request, reply) => reader(
  () => reply({ result: true, message: 'ok' })
);


module.exports = {
  createConnection,
  editSubscriptions,
  restrictedAccess,
};
