'use strict';

const boom = require('boom');
const { weave } = require('ramda-adjunct');

const { isAppLocked, backupToFile, restoreFromDir, cleanUnmsBackupDir, cleanUnmsRestoreDir } = require('./index');
const { registerPlugin } = require('../util/hapi');

function register(server) {
  server.ext('onRequest', (request, reply) => (
    isAppLocked() ? reply(boom.serverUnavailable('Application is locked for maintenance')) : reply.continue())
  );

  const config = server.settings.app;
  const { firmwareDal, messageHub, DB, scheduler } = server.plugins;


  const pluginApi = {
    backupToFile: weave(backupToFile, { config, firmwareDal }),
    restoreFromDir: weave(restoreFromDir, { config, firmwareDal, messageHub, DB }),
  };

  server.expose(pluginApi);

  // register scheduled tasks
  if (!config.demo) {
    scheduler.registerDailyTask(() => cleanUnmsBackupDir(config.nmsBackup), 'cleanUnmsBackupDir');
    scheduler.registerDailyTask(() => cleanUnmsRestoreDir(config.nmsBackup), 'cleanUnmsRestoreDir');
  }
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'backups',
  version: '1.0.0',
  dependencies: ['DB', 'scheduler', 'firmwareDal', 'messageHub'],
};
