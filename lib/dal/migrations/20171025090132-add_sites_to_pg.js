'use strict';

const redis = require('redis');
const bluebird = require('bluebird');
const {
  partial, flow, get, values, curry, getOr, isNull, isEmpty, size, spread, eq, omit, isUndefined, capitalize, cloneDeep,
} = require('lodash/fp');
const { map, when, pathEq } = require('ramda');
const { isNotNull } = require('ramda-adjunct');

const { allP, tapP, resolveP } = require('../../util');
const { SiteTypeEnum, StatusEnum } = require('../../enums');

const host = process.env.UNMS_REDISDB_HOST || '127.0.0.1';
const port = parseInt(process.env.UNMS_REDISDB_PORT, 10) || 6379;
const redisClient = redis.createClient({ host, port });
bluebird.promisifyAll(redis.RedisClient.prototype);


/**
 *  DB definition
 */


/* eslint-disable global-require */
const DB = (function iife() {
  const mhgetall = (keys, callback) => {
    const batch = redisClient.batch();
    keys.forEach(key => batch.get(key));
    batch.exec((err, results) => callback(err, results));
  };

  const getAllTypes = (db, type, fmap = JSON.parse) => {
    let result = [];
    let cursor = '0';
    const scanTypes = (callback) => {
      db.scan(cursor, 'MATCH', `${type}:*`, 'COUNT', '100', (err, res) => {
        if (err) { return callback(err, null) }
        cursor = res[0];
        result = result.concat(res[1]);
        if (cursor !== '0') return scanTypes(callback);
        return callback(null, result);
      });
    };

    return new Promise((resolve, reject) => {
      scanTypes((err1, res1) => {
        if (err1) { return reject(err1) }

        return mhgetall(res1, (err2, res2) => {
          if (err2) {
            return reject(err2);
          }
          return resolve(res2.map(fmap));
        });
      });
    });
  };

  const insertType = (db, type, obj, fmap = JSON.stringify) =>
    db.setAsync(`${type}:${obj.id}`, fmap(obj))
      .then(tapP(() => db.hsetAsync('id2type', obj.id, type)))
      .then(eq('OK'));

  const removeTypeById = (db, type, id) =>
    db.delAsync(`${type}:${id}`)
      .then(tapP(() => db.hdelAsync('id2type', id)));

  // removeSiteById :: Number -> Promise(Number)
  const removeSiteById = partial(removeTypeById, [redisClient, SiteTypeEnum.Site]);

  // removeSite :: Object -> Promise(Number)
  const removeSite = flow(get('id'), removeSiteById);

  // listImages :: String -> Promise(Number)
  const listImages = siteId => redisClient.hgetallAsync(`site_images:${siteId}`)
    .then(when(isNotNull, flow(values, map(JSON.parse))));

  // getAllSites :: () => Promise([Object])
  const getAllSites = partial(getAllTypes, [redisClient, SiteTypeEnum.Site]);

  // removeImage :: String, Object -> Promise(Number)
  const removeImage = curry(
    (siteId, image) => redisClient.hdelAsync(`site_images:${siteId}`, get('id', image))
  );

  // insertSite :: Object -> Promise(Number)
  const insertSite = partial(insertType, [redisClient, SiteTypeEnum.Site]);

  // insertImage :: String -> Object -> Promise(String)
  const insertImage = curry(
    (siteId, image) => redisClient.hsetAsync(`site_images:${siteId}`, image.id, JSON.stringify(image))
  );

  return {
    site: {
      insertImage,
      listImages,
      removeImage,
      list: getAllSites,
      remove: removeSite,
      insert: insertSite,
    },
  };
}());
/* eslint-disable global-require */


/**
 *  SQL queries definitions
 */


const upSqlQueries = [
  `
  DROP TABLE IF EXISTS site_notification;
  `,
  `
  DROP TABLE IF EXISTS site_image;
  `,
  `
  DROP TABLE IF EXISTS site;
  `,
  `
  DROP TYPE IF EXISTS siteTypeEnum;
  `,
  `
  DROP TYPE IF EXISTS notificationTypeEnum;
  `,
  `
  DROP TYPE IF EXISTS siteStatusEnum;
  `,
  `
  CREATE TYPE siteTypeEnum AS ENUM ('site', 'endpoint');
  `,
  `
  CREATE TYPE notificationTypeEnum AS ENUM ('system', 'none', 'custom');
  `,
  `
  CREATE TYPE siteStatusEnum AS ENUM ('active', 'inactive', 'disconnected');
  `,
  `
  CREATE TABLE site (
    id UUID PRIMARY KEY,
    "name" VARCHAR,
    "type" siteTypeEnum NOT NULL DEFAULT 'site',
    status siteStatusEnum NOT NULL DEFAULT 'inactive',
    parent_id UUID,
    address VARCHAR,
    note TEXT,
    contact_name VARCHAR,
    contact_phone VARCHAR,
    contact_email VARCHAR,
    longitude VARCHAR,
    latitude VARCHAR,
    height SMALLINT,
    elevation REAL,
    notification_type notificationTypeEnum NOT NULL DEFAULT 'system'
  );
  `,
  `
  CREATE INDEX ON site(parent_id);
  `,
  `
  CREATE TABLE site_notification (
    site_id UUID,
    user_id UUID
  );
  `,
  `
  CREATE TABLE site_image (
    id UUID PRIMARY KEY,
    "name" VARCHAR,
    height SMALLINT NOT NULL,
    width SMALLINT NOT NULL,
    file_name VARCHAR NOT NULL,
    "order" SMALLINT NOT NULL,
    description TEXT,
    date TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    size SMALLINT NOT NULL,
    file_type VARCHAR NOT NULL,
    site_id UUID,
    version INT NOT NULL DEFAULT 1
  );
  `,
  `
   CREATE INDEX ON site_image(site_id);
  `,
];

const finalizeUpSqlQueries = [
  `
  ALTER TABLE site ADD FOREIGN KEY (parent_id) REFERENCES site(id);
  `,
  `
  ALTER TABLE site ADD CONSTRAINT client_has_parent_id_constraint CHECK(
    (type='site' AND parent_id IS NULL) OR (type='endpoint' AND parent_id IS NOT NULL)
  );
  `,
  `
  ALTER TABLE site_image ADD FOREIGN KEY (site_id) REFERENCES site(id)
  `,
  `
  ALTER TABLE site_notification ADD FOREIGN KEY (site_id) REFERENCES site(id) ON DELETE CASCADE
  `,
  `
  ALTER TABLE site_notification ADD FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE
  `,
];

const downSqlQueries = [
  `
  DROP TABLE IF EXISTS site_notification;
  `,
  `
  DROP TABLE IF EXISTS site_image;
  `,
  `
  DROP TABLE IF EXISTS site;
  `,
  `
  DROP TYPE IF EXISTS siteTypeEnum;
  `,
  `
  DROP TYPE IF EXISTS notificationTypeEnum;
  `,
  `
  DROP TYPE IF EXISTS siteStatusEnum;
  `,
];


/**
 *  UP migration definition
 */


const createNewSqlStructure = queryInterface => upSqlQueries.reduce((acc, query) =>
  acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());

const imposeConstrainsToPg = queryInterface => finalizeUpSqlQueries.reduce((acc, query) =>
  acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());


const saveSites = (queryInterface, data) => allP(data.map(({ site }) =>
  queryInterface.sequelize.query(
    `
    INSERT INTO site(id, name, type, status, parent_id, address, note, contact_name, contact_phone, contact_email,
    longitude, latitude, height, elevation, notification_type) VALUES ($id, $name, $type, $status, $parent, $address,
    $note, $contactName, $phone, $email, $longitude, $latitude, $height, $elevation, $notificationsType);
  `,
    {
      type: queryInterface.sequelize.QueryTypes.SELECT,
      bind: {
        id: site.identification.id,
        name: getOr(null, ['identification', 'name'], site),
        type: getOr(SiteTypeEnum.Site, ['identification', 'type'], site).toLowerCase(),
        status: getOr(StatusEnum.Inactive, ['identification', 'status'], site).toLowerCase(),
        parent: getOr(null, ['identification', 'parent'], site),
        address: getOr(null, ['description', 'address'], site),
        note: getOr(null, ['description', 'note'], site),
        contactName: getOr(null, ['description', 'contact', 'name'], site),
        phone: getOr(null, ['description', 'contact', 'phone'], site),
        email: getOr(null, ['description', 'contact', 'email'], site),
        longitude: getOr(null, ['description', 'location', 'longitude'], site),
        latitude: getOr(null, ['description', 'location', 'latitude'], site),
        height: Math.round(getOr(0, ['description', 'height'], site)),
        elevation: getOr(null, ['description', 'elevation'], site),
        notificationsType: getOr('system', ['notifications', 'type'], site),
      },
    }
)));

const saveImages = (queryInterface, data) => allP(data.map(({ site, images }) => {
  if (isNull(images)) { return resolveP() }

  return allP(images.map((image) => {
    const { id, fileName, height, width, order, date, size: imageSize, fileType } = image;
    let name = getOr(null, ['name'], image);
    let description = getOr(null, ['description'], image);
    name = isEmpty(name) ? null : name;
    description = isEmpty(description) ? null : description;
    const siteId = site.identification.id;

    return queryInterface.sequelize.query(
      `
      INSERT INTO site_image(id, name, height, width, "order", file_name, description, date, size, file_type, site_id)
      VALUES ($id, $name, $height, $width, $order, $fileName, $description, $date, $imageSize, $fileType, $siteId);
    `,
      {
        type: queryInterface.sequelize.QueryTypes.SELECT,
        bind: { id, name, height, width, order, fileName, description, date, imageSize, fileType, siteId },
      }
    );
  }));
}));

const saveNotifications = (queryInterface, data) => allP(data.map(({ site }) => {
  if (size(get(['notifications', 'users'], site)) < 1) { return resolveP() }
  if (pathEq(['notifications', 'type'], 'system', site) || pathEq(['notifications', 'type'], 'none', site)) {
    return resolveP();
  }
  const siteId = site.identification.id;

  return allP(site.notifications.users.map(userId => queryInterface.sequelize.query(
    `
      INSERT INTO site_notification(user_id, site_id) VALUES ('${userId}', '${siteId}');
    `
  )));
}));

const loadDataFromRedis = () => DB.site.list()
  .then(sites => allP(sites.map(site =>
    DB.site.listImages(site.identification.id).then(images => ({ site, images })))
  ));

const saveDataToPg = curry((queryInterface, data) => allP([
  saveSites(queryInterface, data),
  saveImages(queryInterface, data),
  saveNotifications(queryInterface, data),
]));

const removeDataFromRedis = data => allP(data.map(({ site, images }) => {
  const removeSiteP = DB.site.remove(site);
  const removeImagesP = resolveP(images)
    .then(when(
      isNotNull,
      imageList => allP(imageList.map(image => DB.site.removeImage(site.identification.id, image)))
    ));

  return allP([removeSiteP, removeImagesP]);
}));


/**
 *  DOWN migration definition
 */


const removeNewSqlStructure = queryInterface => downSqlQueries.reduce((acc, query) =>
  acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());

const loadDataFromPg = queryInterface => allP([
  queryInterface.sequelize.query(
    'SELECT s.*, sn.user_id notification_user_id FROM site s LEFT JOIN site_notification sn ON s.id = sn.site_id',
    { type: queryInterface.sequelize.QueryTypes.SELECT }),
  queryInterface.sequelize.query('SELECT * FROM site_image', { type: queryInterface.sequelize.QueryTypes.SELECT }),
]);

const addParsedSite = (acc, site) => {
  const { id, notification_user_id } = site;
  const accumulator = cloneDeep(acc);

  if (isUndefined(accumulator[id])) {
    const {
      name, type, status, address, note, contact_name, contact_phone, contact_email, latitude, longitude, height,
      elevation, notification_type, parent_id: parent,
    } = site;
    const contact = { name: contact_name, phone: contact_phone, email: contact_email };
    const location = isNull(longitude) || isNull(latitude)
      ? null
      : { longitude: Number(longitude), latitude: Number(latitude) };

    accumulator[id] = {
      id,
      identification: { id, name, type, status: capitalize(status), parent },
      description: { address, note, contact, location, height, elevation, endpoints: [] },
      notifications: { type: notification_type, users: [] },
    };
  }
  if (isNotNull(notification_user_id)) {
    accumulator[id].notifications.users.push(notification_user_id);
  }

  return accumulator;
};

const addParsedImage = (acc, image) => {
  const accumulator = cloneDeep(acc);
  const {
    id, name, height, width, file_name: fileName, order, description, date, size: fileSize, file_type: fileType,
    site_id: siteId,
  } = image;
  const identification = { id };

  accumulator.push({
    id, name, height, width, order, fileName, identification, description, date, size: fileSize, fileType, siteId,
  });
  return accumulator;
};

const saveDataToRedis = (sites, images) => {
  const parentSites = sites.filter(pathEq(['type'], SiteTypeEnum.Site));
  const endpoints = sites.filter(pathEq(['type'], SiteTypeEnum.Endpoint));

  // parse sites
  let redisSites = parentSites.reduce(addParsedSite, {});

  // parse endpoints
  redisSites = endpoints.reduce((acc, endpoint) => {
    const newAcc = addParsedSite(acc, endpoint);
    if (Array.isArray(get([`${endpoint.parent_id}`, 'description', 'endpoints'], newAcc))) {
      newAcc[endpoint.parent_id].description.endpoints.push(endpoint.id);
    }
    return newAcc;
  }, redisSites);

  // parse images
  const redisImages = images.reduce(addParsedImage, []);

  const sitesP = allP(values(redisSites).map(site => DB.site.insert(site)));
  const imagesP = allP(redisImages.map(image => DB.site.insertImage(image.siteId, omit(['siteId'], image))));

  return allP([sitesP, imagesP]);
};


/**
 *  RUN migration
 */


module.exports = {
  up(queryInterface) {
    return createNewSqlStructure(queryInterface)
      .then(() => loadDataFromRedis())
      .then(tapP(saveDataToPg(queryInterface)))
      .then(removeDataFromRedis)
      .then(() => imposeConstrainsToPg(queryInterface));
  },
  down(queryInterface) {
    return loadDataFromPg(queryInterface)
      .then(spread(saveDataToRedis))
      .then(() => removeNewSqlStructure(queryInterface));
  },
};
