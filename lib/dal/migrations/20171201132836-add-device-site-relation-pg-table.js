'use strict';

const upQueries = [
  `
  DROP TABLE IF EXISTS device_site;
  `,
  `
  CREATE TABLE device_site (
    id UUID PRIMARY KEY NOT NULL,
    mac MACADDR DEFAULT NULL,
    ip VARCHAR DEFAULT NULL,
    site_id UUID NOT NULL REFERENCES site (id) ON DELETE CASCADE,
    CONSTRAINT check_device_site_has_mac_or_ip CHECK (mac IS NOT NULL OR ip IS NOT NULL)
  );
  `,
];

const downQueries = [
  `
  DROP TABLE IF EXISTS device_site;
  `,
];

module.exports = {
  up(queryInterface) {
    return upQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());
  },
  down(queryInterface) {
    return downQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());
  },
};
