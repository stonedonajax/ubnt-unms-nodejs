'use strict';

const upQueries = [
  `
  ALTER TABLE discovery_device ADD COLUMN IF NOT EXISTS all_ips VARCHAR[] NOT NULL DEFAULT ARRAY[]::VARCHAR[];
  `,
  `
  CREATE INDEX ON discovery_device USING gin (all_ips);
  `,
  `
  ALTER TABLE discovery_device ADD COLUMN IF NOT EXISTS all_macs MACADDR[] NOT NULL DEFAULT ARRAY[]::MACADDR[];
  `,
  `
  CREATE INDEX ON discovery_device USING gin (all_macs);
  `,
];

const downQueries = [
  `
  ALTER TABLE discovery_device DROP COLUMN IF EXISTS all_ips;
  `,
  `
  ALTER TABLE discovery_device DROP COLUMN IF EXISTS all_macs;
  `,
];

module.exports = {
  up(queryInterface) {
    return upQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());
  },
  down(queryInterface) {
    return downQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());
  },
};
