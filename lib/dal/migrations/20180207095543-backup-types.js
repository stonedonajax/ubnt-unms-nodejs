'use strict';

const redis = require('redis');
const bluebird = require('bluebird');
const { flow, values, flatten, constant, map, zip, partial, endsWith, parseInt } = require('lodash/fp');
const { assoc, dissoc, evolve, when } = require('ramda');
const fse = require('fs-extra');
const path = require('path');
const { isNotPlainObj, isNotNumber, reduceP } = require('ramda-adjunct');

const { DeviceTypeEnum } = require('../../enums');
const config = require('../../../config');

const host = process.env.UNMS_REDISDB_HOST || '127.0.0.1';
const port = parseInt(10, process.env.UNMS_REDISDB_PORT) || 6379;
const redisClient = redis.createClient({ host, port });
bluebird.promisifyAll(redis.RedisClient.prototype);

/**
 * @readonly
 * @enum {string}
 */
const BackupTypeEnum = Object.freeze({
  Cfg: 'cfg',
  Tar: 'tar',
});

/**
 *  DB definition
 */

const DB = (function iife() {
  const id2Type = id => redisClient.hgetAsync('id2type', id);

  const findTypeById = (db, type, id, fmap = JSON.parse) => db.getAsync(`${type}:${id}`).then(fmap);

  // findDeviceById :: String -> Promise(Object)
  const findDeviceById = deviceId =>
    id2Type(deviceId)
      .then(type => findTypeById(redisClient, type, deviceId));

  const mhgetall = (keys, callback) => {
    const batch = redisClient.batch();
    keys.forEach(key => batch.hgetall(key));
    batch.exec((err, results) => callback(err, results));
  };

  const getAllTypes = (db, type) => {
    let result = [];
    let cursor = '0';
    const scanTypes = (callback) => {
      db.scan(cursor, 'MATCH', `${type}:*`, 'COUNT', '100', (err, res) => {
        if (err) { return callback(err, null) }
        cursor = res[0];
        result = result.concat(res[1]);
        if (cursor !== '0') {
          return scanTypes(callback);
        }
        return callback(null, result);
      });
    };

    return new Promise((resolve, reject) => {
      scanTypes((err1, res1) => {
        if (err1) { return reject(err1) }

        return mhgetall(res1, (err2, res2) => {
          if (err2) {
            return reject(err2);
          }
          return resolve(zip(res1, res2.map(flow(values, map(JSON.parse)))));
        });
      });
    });
  };

  const getAllBackups = partial(getAllTypes, [redisClient, 'config_backup']);

// insertBackup :: String -> Object -> Promise(String)
  const insertBackups = (deviceId, backups) => {
    const data = backups.map(backup => [backup.id, JSON.stringify(backup)]);
    return redisClient.hmsetAsync(`config_backup:${deviceId}`, flatten(data));
  };

  const removeAllBackups = deviceId => redisClient.delAsync(`config_backup:${deviceId}`);

  return {
    insertBackups,
    findDeviceById,
    removeAllBackups,
    getAllBackups,
  };
}());

const toAirOsBackup = flow(
  assoc('type', BackupTypeEnum.Cfg),
  assoc('extension', 'cfg')
);

const toEdgeOsBackup = flow(
  assoc('type', BackupTypeEnum.Tar),
  assoc('extension', 'tar.gz')
);

const toAirCubeBackup = flow(
  assoc('type', BackupTypeEnum.Tar),
  assoc('extension', 'cfg')
);

const removeTypeAndExtension = flow(
  dissoc('type'),
  dissoc('extension')
);

const ensureValidTimestamp = evolve({
  timestamp: when(isNotNumber, parseInt(10)),
});

const backupDir = config.deviceConfigBackup.dir;

const readAllBackupFiles = () => {
  const backups = fse.readdirSync(config.deviceConfigBackup.dir)
    .map(file => path.join(backupDir, file)) // compose full path
    .filter(file => fse.statSync(file).isDirectory())
    .map(dir => fse.readdirSync(dir).map(file => path.join(dir, file)));

  return flatten(backups);
};

const stripTarGzFromFiles = () => {
  fse.removeSync(path.join(backupDir, config.deviceConfigBackup.multiBackup.dir)); // remove multibackup
  readAllBackupFiles()
    .filter(file => endsWith('.tar.gz', file))
    .forEach(backupFile => fse.renameSync(backupFile, backupFile.substr(0, backupFile.length - 7))); // strip '.tar.gz'
};

const addTarGzToFiles = () => {
  fse.removeSync(path.join(backupDir, config.deviceConfigBackup.multiBackup.dir)); // remove multibackup
  readAllBackupFiles()
    .filter(file => !endsWith('.tar.gz', file))
    .forEach(backupFile => fse.renameSync(backupFile, `${backupFile}.tar.gz`));
};

const removeBackups = (deviceId) => {
  const dir = path.join(backupDir, deviceId);
  fse.removeSync(dir);
};

module.exports = {
  up() {
    stripTarGzFromFiles();
    return DB.getAllBackups()
      .then(reduceP((_, [id, backups]) => {
        const deviceId = id.replace('config_backup:', '');
        return DB.findDeviceById(deviceId)
          .catch(constant(null))
          .then((dbDevice) => {
            if (isNotPlainObj(dbDevice)) {
              removeBackups(deviceId);
              return DB.removeAllBackups(deviceId);
            }

            const type = dbDevice.identification.type;

            switch (type) {
              case DeviceTypeEnum.AirFiber:
              case DeviceTypeEnum.AirMax:
              case DeviceTypeEnum.ToughSwitch:
                return DB.insertBackups(deviceId, backups.map(toAirOsBackup).map(ensureValidTimestamp));
              case DeviceTypeEnum.AirCube:
                return DB.insertBackups(deviceId, backups.map(toAirCubeBackup).map(ensureValidTimestamp));
              default:
                return DB.insertBackups(deviceId, backups.map(toEdgeOsBackup).map(ensureValidTimestamp));
            }
          });
      }, null));
  },
  down() {
    addTarGzToFiles();
    return DB.getAllBackups()
      .then(reduceP((_, [id, backups]) => {
        const deviceId = id.replace('config_backup:', '');
        return DB.insertBackups(deviceId, backups.map(removeTypeAndExtension));
      }, null));
  },
};
