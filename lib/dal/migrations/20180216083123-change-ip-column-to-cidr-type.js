'use strict';

const upQueries = [
  `
  TRUNCATE device_site;
  `,
  `
  ALTER TABLE device_site DROP COLUMN IF EXISTS ip;
  `,
  `
  ALTER TABLE device_site ADD COLUMN ip INET DEFAULT NULL;
  `,
];

const downQueries = [
  `
  TRUNCATE device_site;
  `,
  `
  ALTER TABLE device_site DROP COLUMN IF EXISTS ip;
  `,
  `
  ALTER TABLE device_site ADD COLUMN ip VARCHAR DEFAULT NULL;
  `,
];

module.exports = {
  up(queryInterface) {
    return upQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());
  },
  down(queryInterface) {
    return downQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());
  },
};
