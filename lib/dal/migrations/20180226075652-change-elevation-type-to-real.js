'use strict';

const upQueries = [
  `
  ALTER TABLE site ALTER COLUMN elevation TYPE REAL;
  `,
];

module.exports = {
  up(queryInterface) {
    return upQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());
  },
  down() {
    // can not be done without effecting data
  },
};
