'use strict';

const upQueries = [
  `
  ALTER TABLE site_image
    DROP CONSTRAINT site_image_site_id_fkey,
    ADD CONSTRAINT site_image_site_id_fkey FOREIGN KEY (site_id) REFERENCES site(id) ON DELETE CASCADE;
  `,
];

const downQueries = [
  `
  ALTER TABLE site_image
    DROP CONSTRAINT site_image_site_id_fkey,
    ADD CONSTRAINT site_image_site_id_fkey FOREIGN KEY (site_id) REFERENCES site(id) ON DELETE NO ACTION;
  `,
];

module.exports = {
  up(queryInterface) {
    return upQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());
  },
  down(queryInterface) {
    return downQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve());
  },
};
