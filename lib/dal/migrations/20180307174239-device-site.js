'use strict';

const aguid = require('aguid');
const { uniqBy, isNil, isUndefined, zip, toPairs, defaultTo } = require('lodash/fp');

const upQueries = [
  `
  DROP TABLE IF EXISTS device_site_ip;
  `,
  `
  DROP TABLE IF EXISTS device_site_mac;
  `,
  `
  CREATE TABLE public.device_site_ip
  (
    site_id UUID NOT NULL,
    ip INET NOT NULL,
    CONSTRAINT device_site_ip_site_id_ip_pk PRIMARY KEY (site_id, ip),
    CONSTRAINT device_site_ip_site_id_fk FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE
  );
  `,
  `
  CREATE TABLE public.device_site_mac
  (
    site_id UUID NOT NULL,
    mac MACADDR NOT NULL,
    CONSTRAINT device_site_mac_site_id_mac_pk PRIMARY KEY (site_id, mac),
    CONSTRAINT device_site_mac_site_id_fk FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE
  );
  `,
];

const downQueries = [
  `
  DROP TABLE IF EXISTS device_site_ip;
  `,
  `
  DROP TABLE IF EXISTS device_site_mac;
  `,
];

const createDeviceSite = [
  `
  DROP TABLE IF EXISTS device_site;
  `,
  `
  CREATE TABLE device_site (
    id UUID PRIMARY KEY NOT NULL,
    mac MACADDR DEFAULT NULL,
    ip INET DEFAULT NULL,
    site_id UUID NOT NULL REFERENCES site (id) ON DELETE CASCADE,
    CONSTRAINT check_device_site_has_mac_or_ip CHECK (mac IS NOT NULL OR ip IS NOT NULL)
  );
  `,
];

module.exports = {
  up(queryInterface) {
    return upQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve())
      .then(() => queryInterface.sequelize.query('SELECT site_id, "mac", "ip" FROM device_site'))
      .then((records) => {
        let ips = [];
        let macs = [];
        records[0].forEach(({ site_id, mac, ip }) => {
          if (!isNil(mac)) {
            macs.push({ site_id, mac });
          }
          if (!isNil(ip)) {
            ips.push({ site_id, ip });
          }
        });

        macs = uniqBy(({ site_id: siteId, mac }) => `${siteId}_${mac}`, macs);
        ips = uniqBy(({ site_id: siteId, ip }) => `${siteId}_${ip}`, ips);
        return Promise.all([
          ips.length > 0 ? queryInterface.bulkInsert('device_site_ip', ips) : null,
          macs.length > 0 ? queryInterface.bulkInsert('device_site_mac', macs) : null,
        ]);
      })
      .then(() => queryInterface.dropTable('device_site'));
  },
  down(queryInterface) {
    return createDeviceSite
      .reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve())
      .then(() => Promise.all([
        queryInterface.sequelize.query('SELECT site_id, "ip" FROM device_site_ip'),
        queryInterface.sequelize.query('SELECT site_id, "mac" FROM device_site_mac'),
      ]))
      .then(([ipRecords, macRecords]) => {
        const deviceSites = {};

        ipRecords[0].forEach(({ site_id, ip }) => {
          if (isNil(ip)) {
            return;
          }

          if (isUndefined(deviceSites[site_id])) {
            deviceSites[site_id] = { macs: [], ips: [] };
          }

          deviceSites[site_id].ips.push(ip);
        });

        macRecords[0].forEach(({ site_id, mac }) => {
          if (isNil(mac)) {
            return;
          }

          if (isUndefined(deviceSites[site_id])) {
            deviceSites[site_id] = { macs: [], ips: [] };
          }

          deviceSites[site_id].macs.push(mac);
        });

        const records = toPairs(deviceSites).reduce((accumulator, [siteId, { ips, macs }]) => {
          accumulator.push(...zip(ips, macs).map(([ip, mac]) => ({
            id: aguid(),
            site_id: siteId,
            ip: defaultTo(null, ip),
            mac: defaultTo(null, mac),
          })));
          return accumulator;
        }, []);

        return records.length > 0 ? queryInterface.bulkInsert('device_site', records) : null;
      })
      .then(() =>
        downQueries.reduce((acc, query) => acc.then(() => queryInterface.sequelize.query(query)), Promise.resolve())
      );
  },
};
