'use strict';

const { functorTrait } = require('../utils');


/* eslint-disable new-cap */
module.exports = (sequelize, DataTypes) => {
  const deviceSiteIpModel = {
    siteId: {
      type: DataTypes.UUID,
      primaryKey: true,
      field: 'site_id',
    },
    ip: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
  };

  const config = {
    instanceMethods: Object.assign({}, functorTrait(() => sequelize.models.deviceSiteIpModel)),
    tableName: 'device_site_ip',
  };

  return sequelize.define('deviceSiteIpModel', deviceSiteIpModel, config);
};
/* eslint-enable new cap */
