'use strict';

const { functorTrait } = require('../utils');


/* eslint-disable new-cap */
module.exports = (sequelize, DataTypes) => {
  const deviceSiteMacModel = {
    siteId: {
      type: DataTypes.UUID,
      primaryKey: true,
      field: 'site_id',
    },
    mac: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
  };

  const config = {
    instanceMethods: Object.assign({}, functorTrait(() => sequelize.models.deviceSiteMacModel)),
    tableName: 'device_site_mac',
  };

  return sequelize.define('deviceSiteMacModel', deviceSiteMacModel, config);
};
/* eslint-enable new cap */
