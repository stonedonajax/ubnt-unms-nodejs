'use strict';

const { values } = require('lodash/fp');

const { SiteTypeEnum, StatusEnum, AlertTypeEnum } = require('../../enums');
const { functorTrait } = require('../utils');


/* eslint-disable new-cap */
module.exports = (sequelize, DataTypes) => {
  const siteModel = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
    },
    type: {
      type: DataTypes.ENUM,
      values: values(SiteTypeEnum),
      defaultValue: SiteTypeEnum.Site,
      allowNull: false,
    },
    status: {
      type: DataTypes.ENUM,
      values: [StatusEnum.Inactive, StatusEnum.Active, StatusEnum.Disconnected],
      defaultValue: StatusEnum.Inactive,
      allowNull: false,
    },
    parentId: {
      type: DataTypes.UUID,
      field: 'parent_id',
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    note: {
      type: DataTypes.TEXT,
      field: 'text',
    },
    contactName: {
      type: DataTypes.STRING,
      field: 'contact_name',
    },
    contactPhone: {
      type: DataTypes.STRING,
      field: 'contact_phone',
    },
    contactEmail: {
      type: DataTypes.STRING,
      field: 'contact_email',
    },
    longitude: {
      type: DataTypes.STRING,
    },
    latitude: {
      type: DataTypes.STRING,
    },
    height: {
      type: DataTypes.INTEGER,
    },
    elevation: {
      type: DataTypes.REAL,
    },
    notificationType: {
      type: DataTypes.ENUM,
      values: values(AlertTypeEnum),
      defaultValue: AlertTypeEnum.System,
      allowNull: false,
      field: 'notification_type',
    },
  };

  const config = {
    instanceMethods: Object.assign({}, functorTrait(() => sequelize.models.siteModel)),
    tableName: 'task',
  };

  return sequelize.define('siteModel', siteModel, config);
};
/* eslint-enable new cap */
