'use strict';

const { functorTrait } = require('../utils');


/* eslint-disable new-cap */
module.exports = (sequelize, DataTypes) => {
  const siteImageModel = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    height: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    width: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    fileName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'file_name',
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      field: 'text',
    },
    date: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    size: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    fileType: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'file_type',
    },
    siteId: {
      type: DataTypes.UUID,
      allowNull: false,
      field: 'site_id',
    },
    version: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
    },
  };

  const config = {
    instanceMethods: Object.assign({}, functorTrait(() => sequelize.models.siteImageModel)),
    tableName: 'task',
  };

  return sequelize.define('siteImageModel', siteImageModel, config);
};
/* eslint-enable new cap */
