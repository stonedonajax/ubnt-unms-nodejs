'use strict';

const { functorTrait } = require('../utils');


/* eslint-disable new-cap */
module.exports = (sequelize, DataTypes) => {
  const siteNotificationModel = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
    },
    siteId: {
      type: DataTypes.UUID,
      field: 'site_id',
    },
    userId: {
      type: DataTypes.UUID,
      field: 'user_id',
    },
  };

  const config = {
    instanceMethods: Object.assign({}, functorTrait(() => sequelize.models.siteNotificationModel)),
    tableName: 'task',
  };

  return sequelize.define('siteNotificationModel', siteNotificationModel, config);
};
/* eslint-enable new cap */
