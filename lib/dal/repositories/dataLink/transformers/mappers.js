'use strict';

const toDbDataLink = dataLink => ({
  id: dataLink.id,
  deviceIdFrom: dataLink.from.device.identification.id,
  interfaceNameFrom: dataLink.from.interface.identification.name,
  deviceIdTo: dataLink.to.device.identification.id,
  interfaceNameTo: dataLink.to.interface.identification.name,
  origin: dataLink.origin,
});


module.exports = {
  toDbDataLink,
};
