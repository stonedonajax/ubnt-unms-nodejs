'use strict';

const { curry, map } = require('ramda');

const { parseDataLinkEdge } = require('../../../../data-link/transformers/parsers');


// parseDbDataLink :: Object -> Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseDbDataLink = curry((auxiliaries, dbDataLink) => ({
  id: dbDataLink.id,
  from: parseDataLinkEdge({}, { deviceId: dbDataLink.deviceIdFrom, interfaceName: dbDataLink.interfaceNameFrom }),
  to: parseDataLinkEdge({}, { deviceId: dbDataLink.deviceIdTo, interfaceName: dbDataLink.interfaceNameTo }),
  origin: dbDataLink.origin,
}));

// parseDbDataLinkList :: (Object, Array.<DbDataLink>) -> Array.<Correspondence>
//     DbDataLink = Object
//     Correspondence = Object
const parseDbDataLinkList = curry((auxiliaries, dbDataLinkList) => map(parseDbDataLink(auxiliaries), dbDataLinkList));


module.exports = {
  parseDbDataLink,
  parseDbDataLinkList,
};
