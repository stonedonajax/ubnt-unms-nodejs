'use strict';

const { flow, split, first } = require('lodash/fp');
const { QueryTypes } = require('sequelize');
const { Reader: reader } = require('monet');

const { singleOrDefault, knex } = require('../utils');
const { allP, resolveP } = require('../.././util');

const stripSubnetLength = flow(
  split('/'),
  first
);

const findOneAuthorizationSiteId = (ips, macs) => reader(
  (config) => {
    let query = null;
    if (macs.length > 0) {
      query = knex.column({ siteId: 'site_id' })
        .select()
        .from('device_site_mac')
        .whereIn('mac', macs);
    }

    if (ips.length > 0) {
      if (query !== null) {
        query = query.unionAll(function unionAll() {
          this.column({ siteId: 'site_id' })
            .select()
            .from('device_site_ip')
            .whereIn('ip', ips.concat(ips.map(stripSubnetLength)));
        });
      } else {
        query = knex.column({ siteId: 'site_id' })
          .select()
          .from('device_site_ip')
          .whereIn('ip', ips.concat(ips.map(stripSubnetLength)));
      }
    }

    if (query !== null) {
      return config.query(query.limit(1).toString(), {
        type: QueryTypes.SELECT,
      }).then(singleOrDefault(null));
    }

    return resolveP(null);
  }
);

const clearAllRelations = () => reader(
  config => config.query(
    `
      TRUNCATE TABLE device_site_ip;
      TRUNCATE TABLE device_site_mac;
    `
  )
);

const addRelations = (siteId, ips = null, macs = null) => reader(
  (config) => {
    const queries = [];
    if (Array.isArray(ips) && ips.length > 0) {
      queries.push(
        `
        ${knex.table('device_site_ip').insert(ips.map(ip => ({ site_id: siteId, ip }))).toString()}
        ON CONFLICT DO NOTHING
        `.trim()
      );
    }

    if (Array.isArray(macs) && macs.length > 0) {
      queries.push(
        `
        ${knex.table('device_site_mac').insert(macs.map(mac => ({ site_id: siteId, mac }))).toString()}
        ON CONFLICT DO NOTHING
        `.trim()
      );
    }

    return allP(queries.map(query => config.query(query, {
      type: QueryTypes.INSERT,
    })));
  }
);

module.exports = {
  clearAllRelations,
  addRelations,
  findOneAuthorizationSiteId,
};
