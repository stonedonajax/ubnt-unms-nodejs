'use strict';

const { QueryTypes } = require('sequelize');
const { Reader: reader } = require('monet');
const { isEmpty, getOr } = require('lodash/fp');

const { buildResponse, singleOrDefault } = require('../utils');
const { thenP } = require('../../util');

const saveNewSite = site => reader(
  config => config.query(
    `
      INSERT INTO site (id, name, type, status, parent_id, address, note, contact_name, contact_phone, 
        contact_email, longitude, latitude, height, elevation, notification_type)        
      VALUES ($id, $name, $type, $status, $parentId, $address, $note, $contactName, $contactPhone, 
        $contactEmail, $longitude, $latitude, $height, $elevation, $notificationType)
      RETURNING *;
    `,
    {
      type: QueryTypes.INSERT,
      bind: {
        id: site.id,
        name: site.name,
        type: site.type,
        status: site.status,
        parentId: site.parent_id,
        address: site.address,
        note: site.note,
        contactName: site.contact_name,
        contactPhone: site.contact_phone,
        contactEmail: site.contact_email,
        longitude: site.longitude,
        latitude: site.latitude,
        height: site.height,
        elevation: site.elevation,
        notificationType: site.notification_type,
      },
    }
  )
    .then(singleOrDefault(null))
);

const saveSite = cmSite => reader(
  config => config.query(
    `
      UPDATE site
      SET name=$name, type=$type, status=$status, parent_id=$parentId, address=$address, note=$note, 
        contact_name=$contactName, contact_phone=$contactPhone, contact_email=$contactEmail, longitude=$longitude, 
        latitude=$latitude, height=$height, elevation=$elevation, notification_type=$notificationType
      WHERE id=$id
      RETURNING *
    `,
    {
      type: QueryTypes.UPDATE,
      bind: {
        id: cmSite.identification.id,
        name: cmSite.identification.name,
        type: cmSite.identification.type,
        status: cmSite.identification.status,
        parentId: getOr(null, ['identification', 'parent', 'id'], cmSite),
        address: cmSite.description.address,
        note: cmSite.description.note,
        contactName: cmSite.description.contact.name,
        contactPhone: cmSite.description.contact.phone,
        contactEmail: cmSite.description.contact.email,
        longitude: getOr(null, ['description', 'location', 'longitude'], cmSite),
        latitude: getOr(null, ['description', 'location', 'latitude'], cmSite),
        height: cmSite.description.height,
        elevation: cmSite.description.elevation,
        notificationType: cmSite.notifications.type,
      },
    }
  )
    .then(singleOrDefault(null))
);

const findAllSites = () => reader(
  config => config.query(
    'SELECT * FROM site',
    {
      type: QueryTypes.SELECT,
      model: config.models.siteModel,
      mapToModel: true,
    }
  )
);

const findOneById = siteId => reader(
  config => config.query(
    'SELECT * FROM site WHERE id=$siteId',
    {
      type: QueryTypes.SELECT,
      model: config.models.siteModel,
      mapToModel: true,
      bind: { siteId },
    }
  )
    .then(singleOrDefault(null))
);

const findById = siteIds => reader(
  config => config.query(
    `SELECT * FROM site WHERE id=ANY('{${siteIds.join(',')}}')`,
    {
      type: QueryTypes.SELECT,
    }
  )
);

const findWithEndpointsAndUsers = siteIds => reader(
  config => config.query(
    `
      SELECT
        s.id, s.name, s.type, s.status, s.parent_id "parentId", s.address, s.note, s.contact_name "contactName", 
          s.contact_phone "contactPhone", s.contact_email "contactEmail", s.longitude, s.latitude, s.height, 
          s.elevation, s.notification_type "notificationType", e.id "endpoints.id", e.name "endpoints.name", 
          e.status "endpoints.status", e.type "endpoints.type", e.parent_id "endpoints.parentId",
        p.id "parent.id", p.name "parent.name", p.status "parent.status", p.type "parent.type",
          p.parent_id "parent.parentId",
        u.id "users.id", u.username "users.username", u.email "users.email",
          u.totp_auth_enabled "users.totpAuthEnabled"
      FROM site s
        LEFT JOIN site e ON e.parent_id = s.id
        LEFT JOIN site p ON s.parent_id = p.id
        LEFT JOIN site_notification sn ON s.id = sn.site_id
        LEFT JOIN "user" u ON sn.user_id = u.id
      ${isEmpty(siteIds) ? '' : ` WHERE s.id = ANY('{${siteIds.join(',')}}')`}
    `,
    {
      type: QueryTypes.SELECT,
    }
  )
    .then(buildResponse({ specialKeyArray: ['users', 'endpoints'] }))
);

const findWithUsers = siteIds => reader(
  config => config.query(
    `
      SELECT
        s.id, s.name, s.type, s.status, s.parent_id "parentId", s.address, s.note, s.contact_name "contactName",
          s.contact_phone "contactPhone", s.contact_email "contactEmail", s.longitude, s.latitude, s.height,
          s.elevation, s.notification_type "notificationType",
        p.id "parent.id", p.name "parent.name", p.status "parent.status", p.type "parent.type",
          p.parent_id "parent.parentId",
        u.id "users.id", u.username "users.username", u.email "users.email",
          u.totp_auth_enabled "users.totpAuthEnabled"
      FROM site s
        LEFT JOIN site p ON s.parent_id = p.id
        LEFT JOIN site_notification sn ON s.id = sn.site_id
        LEFT JOIN "user" u ON sn.user_id = u.id
      ${isEmpty(siteIds) ? '' : ` WHERE s.id = ANY('{${siteIds.join(',')}}')`}
    `,
    {
      type: QueryTypes.SELECT,
    }
  )
    .then(buildResponse({ specialKeyArray: ['users'] }))
);

const findOneWithEndpointsCount = siteId => reader(
  config => config.query(
    `
      SELECT s.id, COUNT(e.id) "endpointsCount"
      FROM site s
      LEFT JOIN site e ON e.parent_id = s.id
      WHERE s.id = $siteId
      GROUP BY s.id;
    `,
    {
      type: QueryTypes.SELECT,
      bind: { siteId },
    }
  )
    .then(singleOrDefault(null))
);

const findOneWithEndpointsAndUsers = siteId => reader(
  config => findWithEndpointsAndUsers([siteId])
    .map(thenP(singleOrDefault(null)))
    .run(config)
);

const remove = siteId => reader(
  config => config.query(
    `
      DELETE FROM site WHERE id=$id;
    `,
    {
      type: QueryTypes.DELETE,
      bind: { id: siteId },
    }
  )
);

const getSiteAndEndpointCountsByStatus = () => reader(
  config => config.query(
    `
      SELECT type, status, COUNT(*) FROM site GROUP BY type, status;
    `,
    {
      type: QueryTypes.SELECT,
    }
  )
);

const removeAll = () => reader(
  config => config.query('TRUNCATE site CASCADE')
);


module.exports = {
  findAllSites,
  findById,
  findOneById,
  saveNewSite,
  saveSite,
  remove,
  findWithEndpointsAndUsers,
  findWithUsers,
  findOneWithEndpointsAndUsers,
  findOneWithEndpointsCount,
  getSiteAndEndpointCountsByStatus,
  removeAll,
};
