'use strict';

const { QueryTypes } = require('sequelize');
const { Reader: reader } = require('monet');

const { singleOrDefault, buildWhereQuery } = require('../utils');
const { thenP } = require('../../util');


const saveImage = image => reader(
  config => config.query(
    `
      INSERT INTO site_image (id, name, height, width, file_name, "order", description, date, size, file_type, site_id, 
        version)
      VALUES(:id, :name, :height, :width, :fileName,  
        (SELECT COALESCE((SELECT MAX(si.order) + 1 FROM site_image si GROUP BY site_id HAVING site_id=:siteId), 1)),
        :description, :date, :size, :fileType, :siteId, :version)
    `,
    {
      type: QueryTypes.INSERT,
      replacements: {
        id: image.id,
        name: image.name,
        height: image.height,
        width: image.width,
        fileName: image.fileName,
        description: image.description,
        date: image.date,
        size: image.size,
        fileType: image.fileType,
        siteId: image.siteId,
        version: 1,
      },
    }
  )
);

const findImages = (where = {}) => reader(
  config => config.query(
    `SELECT * FROM site_image ${buildWhereQuery(config, where, { model: config.models.siteImageModel })};`,
    {
      type: QueryTypes.SELECT,
      model: config.models.siteImageModel,
      mapToModel: true,
    }
  )
);

const findImage = id => reader(
  config => findImages({ id })
    .map(thenP(singleOrDefault(null)))
    .run(config)
);

const updateImage = dbImage => reader(
  config => config.query(
    `
      UPDATE site_image 
        SET name=$name, height=$height, width=$width, file_name=$fileName, "order"=$order,
          description=$description, date=$date, size=$size, file_type=$fileType, site_id=$siteId, version=$version
        WHERE id=$id;
    `,
    {
      type: QueryTypes.UPDATE,
      bind: {
        id: dbImage.id,
        name: dbImage.name,
        height: dbImage.height,
        width: dbImage.width,
        fileName: dbImage.fileName,
        order: dbImage.order,
        description: dbImage.description,
        date: dbImage.date,
        size: dbImage.size,
        fileType: dbImage.fileType,
        siteId: dbImage.siteId,
        version: dbImage.version,
      },
    }
  )
);

const deleteImage = imageId => reader(
  config => config.query(
    `
      DELETE FROM site_image WHERE id=$id;
    `,
    {
      type: QueryTypes.DELETE,
      bind: {
        id: imageId,
      },
    }
  )
);

const reorderImage = (siteId, currentOrder, nextOrder) => reader(
  config => config.query(
    `
      UPDATE site_image
        SET "order"=
          CASE
            WHEN "order" = :currentOrder THEN :nextOrder
            WHEN :currentOrder < :nextOrder AND "order" > :currentOrder AND "order" <= :nextOrder THEN "order" - 1
            WHEN "order" < :currentOrder AND "order" >= :nextOrder THEN "order" + 1
            ELSE "order"
          END
      WHERE site_id=:siteId;
    `,
    {
      type: QueryTypes.UPDATE,
      replacements: {
        currentOrder,
        nextOrder,
        siteId,
      },
    }
  )
);

module.exports = {
  saveImage,
  findImages,
  findImage,
  updateImage,
  deleteImage,
  reorderImage,
};
