'use strict';

const { QueryTypes } = require('sequelize');
const { Reader: reader } = require('monet');

const clearNotification = siteId => reader(
  config => config.query(
    `
      DELETE FROM site_notification WHERE site_id=$siteId;
    `,
    {
      type: QueryTypes.DELETE,
      bind: {
        siteId,
      },
    }
  )
);

const addSiteNotification = (siteId, userId) => reader(
  config => config.query(
    `
      INSERT INTO site_notification VALUES ($siteId, $userId);
    `,
    {
      type: QueryTypes.INSERT,
      bind: {
        siteId,
        userId,
      },
    }
  )
);


module.exports = {
  clearNotification,
  addSiteNotification,
};
