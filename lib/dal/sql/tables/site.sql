DROP TABLE IF EXISTS site_notification;

DROP TABLE IF EXISTS site;

DROP TABLE IF EXISTS site_image;

DROP TYPE IF EXISTS siteTypeEnum;

DROP TYPE IF EXISTS notificationTypeEnum;

DROP TYPE IF EXISTS siteStatusEnum;

CREATE TYPE siteTypeEnum AS ENUM (
  'site', 'endpoint'
);

CREATE TYPE notificationTypeEnum AS ENUM (
  'system', 'none', 'custom'
);

CREATE TYPE siteStatusEnum AS ENUM (
  'active', 'inactive', 'disconnected',
);

CREATE TABLE site (
  id UUID PRIMARY KEY,
  "name" VARCHAR,
  "type" siteTypeEnum NOT NULL DEFAULT 'site',
  status siteStatusEnum NOT NULL DEFAULT 'inactive',
  parent_id UUID REFERENCES site(id),
  address VARCHAR,
  note TEXT,
  contact_name VARCHAR,
  contact_phone VARCHAR,
  contact_email VARCHAR,
  longitude VARCHAR,
  latitude VARCHAR,
  height SMALLINT,
  elevation REAL,
  notification_type notificationTypeEnum NOT NULL DEFAULT 'system',
  CONSTRAINT client_has_parent_id_constraint CHECK(
    (type='site' AND parent_id IS NULL) OR (type='endpoint' AND parent_id IS NOT NULL)
  )
);

CREATE INDEX ON site(parent_id);


CREATE TABLE site_notification (
  id UUID PRIMARY KEY,
  site_id UUID REFERENCES site(id) ON DELETE CASCADE,
  user_id UUID REFERENCES "user"(id) ON DELETE CASCADE
);


CREATE TABLE site_image (
  id UUID PRIMARY KEY,
  "name" VARCHAR,
  height SMALLINT NOT NULL,
  width SMALLINT NOT NULL,
  file_name VARCHAR NOT NULL,
  "order" SMALLINT NOT NULL,
  description TEXT,
  date TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  size SMALLINT NOT NULL,
  file_type VARCHAR NOT NULL,
  site_id UUID REFERENCES site(id) ON DELETE CASCADE,
  version INT NOT NULL DEFAULT 1,
);

CREATE INDEX ON site_image(site_id);
