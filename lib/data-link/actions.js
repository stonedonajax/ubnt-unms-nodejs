'use strict';

const { createAction } = require('redux-actions');
const { Observable } = require('rxjs/Rx');
const {
  map, assocPath, pipe, head, last, filter, pathSatisfies, forEach, pathOr, equals, set, find,
} = require('ramda');
const { isNotEmpty, isNotNil } = require('ramda-adjunct');

const { parseDbDataLinkList } = require('../dal/repositories/dataLink/transformers/parsers');
const { parseDbDeviceList } = require('../transformers/device/parsers');
const { parseDbInterfaceList } = require('../transformers/interfaces/parsers');
const { mergeDeviceList, mergeDeviceBoth } = require('./transformers/mergers');
const { isOnuDeviceType } = require('../feature-detection/common');
const { dataLinkIdListBetweenDevicesSelector } = require('./selectors');
const { parseDummyDataLink } = require('./transformers/parsers');
const { DataLinkOriginEnum } = require('../enums');
const { interfaceNameFromLens, interfaceNameToLens } = require('./transformers/lenses');


const STORE_INIT = 'STORE_INIT';

const INIT_SUCCESS = 'INIT_SUCCESS';
const INIT_FAILURE = 'INIT_FAILURE';

const REMOVE_BY_ID_REQUEST = 'REMOVE_BY_ID_REQUEST';
const REMOVE_BY_ID_SUCCESS = 'REMOVE_BY_ID_SUCCESS';
const REMOVE_BY_ID_FAILURE = 'REMOVE_BY_ID_FAILURE';

const REMOVE_BY_DEVICE_ID_REQUEST = 'REMOVE_BY_DEVICE_ID_REQUEST';
const REMOVE_BY_DEVICE_ID_SUCCESS = 'REMOVE_BY_DEVICE_ID_SUCCESS';
const REMOVE_BY_DEVICE_ID_FAILURE = 'REMOVE_BY_DEVICE_ID_FAILURE';

const SAVE_REQUEST = 'SAVE_REQUEST';
const SAVE_SUCCESS = 'SAVE_SUCCESS';
const SAVE_FAILURE = 'SAVE_FAILURE';

const UPDATE_REQUEST = 'UPDATE_REQUEST';
const UPDATE_SUCCESS = 'UPDATE_SUCCESS';
const UPDATE_FAILURE = 'UPDATE_FAILURE';

const UPDATE_STATUS_BY_FROM_DEVICE_ID_TO_DEVICE_ID = 'UPDATE_STATUS_BY_FROM_DEVICE_ID_TO_DEVICE_ID';

const SYNCHRONIZE_DATA_REQUEST = 'SYNCHRONIZE_DATA_REQUEST';
const SYNCHRONIZE_DATA_SUCCESS = 'SYNCHRONIZE_DATA_SUCCESS';
const SYNCHRONIZE_DATA_FAILURE = 'SYNCHRONIZE_DATA_FAILURE';

const storeInit = createAction(STORE_INIT);

const requestRemoveById = createAction(REMOVE_BY_ID_REQUEST);
const removeByIdSuccess = createAction(REMOVE_BY_ID_SUCCESS);
const removeByIdFailure = createAction(REMOVE_BY_ID_FAILURE);

const requestRemoveByDeviceId = createAction(REMOVE_BY_DEVICE_ID_REQUEST);
const removeByDeviceIdSuccess = createAction(REMOVE_BY_DEVICE_ID_SUCCESS);
const removeByDeviceIdFailure = createAction(REMOVE_BY_DEVICE_ID_FAILURE);

const requestSave = createAction(SAVE_REQUEST);
const saveSuccess = createAction(SAVE_SUCCESS);
const saveFailure = createAction(SAVE_FAILURE);

const requestUpdate = createAction(UPDATE_REQUEST);
const updateSuccess = createAction(UPDATE_SUCCESS);
const updateFailure = createAction(UPDATE_FAILURE);

const requestSynchronizeData = createAction(SYNCHRONIZE_DATA_REQUEST);
const synchronizeDataSuccess = createAction(SYNCHRONIZE_DATA_SUCCESS);
const synchronizeDataFailure = createAction(SYNCHRONIZE_DATA_FAILURE);

const updateStatusByFromDeviceIdToDeviceId = createAction(UPDATE_STATUS_BY_FROM_DEVICE_ID_TO_DEVICE_ID);

const initializeSuccess = createAction(INIT_SUCCESS);
const initializeFailure = createAction(INIT_FAILURE);
const initialize = () => (dispatch, getState, { dal, DB }) => Observable.from(dal.dataLinkRepository.findAll())
    .map(parseDbDataLinkList({}))
    .mergeMap(cmDataLinkList => Observable.forkJoin(
      Observable.from(dal.siteRepository.findAllSites()),
      Observable.from(DB.device.list()))
        .map(([dbSiteList, dbDeviceList]) => pipe(
            parseDbDeviceList({ dbSiteList }),
            map(cmDevice => assocPath(['interfaces'], parseDbInterfaceList({}, cmDevice.interfaces), cmDevice))
        )(dbDeviceList))
        .map(cmDeviceList => [mergeDeviceList(cmDataLinkList, cmDeviceList), cmDeviceList])
      )
    .do(pipe(head, cmDataLinkList => dispatch(initializeSuccess(cmDataLinkList))))
    .map(last)
    .do(cmDeviceList => pipe(
      filter(pathSatisfies(isOnuDeviceType, ['identification', 'model'])),
      forEach((cmDeviceOnu) => {
        const oltId = cmDeviceOnu.onu.id;
        const onuId = cmDeviceOnu.id;
        const existingDataLinkIds = dataLinkIdListBetweenDevicesSelector(
          getState(),
          { deviceIdFrom: oltId, deviceIdTo: onuId }
        );

        if (isNotEmpty(existingDataLinkIds)) {
          dispatch(
            updateStatusByFromDeviceIdToDeviceId({ oltId, onuId, status: cmDeviceOnu.overview.status })
          );
          return;
        }

        const cmDeviceOlt = find(pathSatisfies(equals(oltId), ['identification', 'id']), cmDeviceList);

        if (isNotNil(cmDeviceOlt)) {
          const dummyDataLink = pipe(
            () => parseDummyDataLink({ origin: DataLinkOriginEnum.Unms }),
            set(interfaceNameFromLens, `pon${pathOr('1', ['onu', 'port'], cmDeviceOnu)}`),
            set(interfaceNameToLens, pathOr('pon0', ['interfaces', 0, 'identification', 'name'], cmDeviceOnu))
          )();

          dispatch(requestSave(mergeDeviceBoth(dummyDataLink, cmDeviceOlt, cmDeviceOnu)));
        }
      })
    )(cmDeviceList))
    .catch((error) => {
      dispatch(initializeFailure(error));
      return Observable.throw(error);
    })
    .toPromise();

module.exports = {
  STORE_INIT,
  INIT_SUCCESS,
  INIT_FAILURE,
  REMOVE_BY_ID_REQUEST,
  REMOVE_BY_ID_SUCCESS,
  REMOVE_BY_ID_FAILURE,
  REMOVE_BY_DEVICE_ID_REQUEST,
  REMOVE_BY_DEVICE_ID_SUCCESS,
  REMOVE_BY_DEVICE_ID_FAILURE,
  SAVE_REQUEST,
  SAVE_SUCCESS,
  SAVE_FAILURE,
  UPDATE_REQUEST,
  UPDATE_SUCCESS,
  UPDATE_FAILURE,
  UPDATE_STATUS_BY_FROM_DEVICE_ID_TO_DEVICE_ID,
  SYNCHRONIZE_DATA_REQUEST,
  SYNCHRONIZE_DATA_SUCCESS,
  SYNCHRONIZE_DATA_FAILURE,

  storeInit,
  initializeFailure,
  initializeSuccess,
  initialize,
  requestRemoveById,
  removeByIdSuccess,
  removeByIdFailure,
  requestRemoveByDeviceId,
  removeByDeviceIdSuccess,
  removeByDeviceIdFailure,
  requestSave,
  saveSuccess,
  saveFailure,
  requestUpdate,
  updateSuccess,
  updateFailure,
  updateStatusByFromDeviceIdToDeviceId,
  requestSynchronizeData,
  synchronizeDataSuccess,
  synchronizeDataFailure,
};
