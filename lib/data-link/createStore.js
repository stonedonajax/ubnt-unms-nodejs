'use strict';

const thunk = require('redux-thunk').default;
const { applyMiddleware, createStore } = require('redux');

const reducers = require('./reducers');
const { storeInit } = require('./actions');
const middleware = require('./middleware');

/**
 * @file DataLink objects in memory
 *
 * Representation of the data links are stored as adjacency list and converted to the graph - edges and its vertices
 */

/**
 * @typedef {Object} DbDataLink
 * @property {string} id UUID
 * @property {string} deviceIdFrom UUID
 * @property {string} interfaceNameFrom
 * @property {string} deviceIdTo UUID
 * @property {string} interfaceNameTo
 * @property {string} origin
 */

/**
 * @typedef {Object} DbDataLinkEdge
 * @property {string} deviceId UUID
 * @property {string} interfaceName
 */

const createDataLinkStore = (dal, DB) => {
  const store = createStore(reducers, undefined, applyMiddleware(
    thunk.withExtraArgument({ dal, DB }),
    middleware({ dal, DB })
  ));

  store.dispatch(storeInit());

  return store;
};


module.exports = createDataLinkStore;
