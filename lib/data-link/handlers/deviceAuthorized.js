'use strict';

const { Reader: reader } = require('monet');
const { lensEq } = require('ramda-adjunct');
const { Observable } = require('rxjs');
const { pathOr, view, set, merge } = require('ramda');

const { requestUpdate } = require('../actions');
const { dataLinkListByDeviceIdSelector } = require('../selectors');
const { deviceIdFromLens, siteIdentificationFromLens, siteIdentificationToLens } = require('../transformers/lenses');


/**
 * @param {CorrespondenceDevice} cmDevice
 * @param {Message} message
 * @return {Reader.<onDeviceAuthorizedHandler~callback>}
 */
module.exports = ({ device, deviceId }, message) => reader(
  ({ dataLinkStore, messageHub }) =>
    Observable.from(dataLinkListByDeviceIdSelector(dataLinkStore.getState(), deviceId))
      .do((dataLink) => {
        const siteLensToUpdate = lensEq(deviceIdFromLens, deviceId, dataLink)
        ? siteIdentificationFromLens
        : siteIdentificationToLens;

        const dataLinkSiteUpdated = merge(
          view(siteLensToUpdate),
          pathOr(null, ['identification', 'site', 'identification'], device)
        );
        dataLinkStore.dispatch(requestUpdate(set(siteLensToUpdate, dataLinkSiteUpdated, dataLink)));
      })
      .catch((error) => {
        messageHub.logError(message, error);
        return Observable.empty();
      })
      .toPromise()
);
