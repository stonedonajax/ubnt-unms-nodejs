'use strict';

const { Reader: reader } = require('monet');
const { pathOr, isNil, pipe, set, assocPath, always } = require('ramda');
const { Observable } = require('rxjs');
const { isNotEmpty } = require('ramda-adjunct');

require('../../util/observable');
const { DataLinkOriginEnum, DataLinkStatusEnum } = require('../../enums');
const { updateStatusByFromDeviceIdToDeviceId, requestSave } = require('../actions');
const { fromDb: fromDbDevice } = require('../../transformers/device');
const { dataLinkIdListBetweenDevicesSelector } = require('../selectors');
const { parseDummyDataLink } = require('../transformers/parsers');
const { mergeDeviceBoth } = require('../transformers/mergers');
const { isOnuDeviceType } = require('../../feature-detection/common');
const { interfaceNameFromLens, interfaceNameToLens } = require('../transformers/lenses');
const { parseDbInterfaceList } = require('../../transformers/interfaces/parsers');


/**
 * @param {{ device }} cmDeviceWithId
 * @param {Message} message
 * @return {Reader.<onDeviceConnectedHandler~callback>}
 */
module.exports = ({ device }, message) => reader(
  ({ dataLinkStore, DB, dal, messageHub }) => {
    const model = pathOr(null, ['identification', 'model'], device);

    if (isOnuDeviceType(model)) {
      const oltId = pathOr(null, ['onu', 'id'], device);
      const onuId = pathOr(null, ['identification', 'id'], device);

      const getDeviceWithSiteById$ = deviceInnerId => Observable.from(DB.device.findById(deviceInnerId))
        .mergeMap((dbDevice) => {
          if (isNil(dbDevice)) {
            return Observable.empty();
          }

          const siteId = pathOr(null, ['identification', 'site', 'id'], dbDevice);
          return Observable.if(
            always(siteId),
            Observable.of({ dbDevice, dbSite: null }),
            Observable.from(dal.siteRepository.findOneById(siteId)).map(dbSite => ({ dbDevice, dbSite }))
          );
        })
        .mergeEither(({ dbDevice, dbSite }) => fromDbDevice({ dbSite }, dbDevice))
        .map(cmDevice => assocPath(['interfaces'], parseDbInterfaceList({}, cmDevice.interfaces), cmDevice));

      return getDeviceWithSiteById$(onuId)
        .mergeMap((cmDeviceOnu) => {
          const existingDataLinkIds = dataLinkIdListBetweenDevicesSelector(
            dataLinkStore.getState(),
            { deviceIdFrom: oltId, deviceIdTo: onuId }
          );

          if (isNotEmpty(existingDataLinkIds)) {
            dataLinkStore.dispatch(
              updateStatusByFromDeviceIdToDeviceId({
                deviceIdFrom: oltId,
                deviceIdTo: onuId,
                status: DataLinkStatusEnum.Active,
              })
            );
            return Observable.empty();
          }

          const dummyDataLink = pipe(
            () => parseDummyDataLink({ origin: DataLinkOriginEnum.Unms }),
            set(interfaceNameFromLens, `pon${pathOr('1', ['onu', 'port'], cmDeviceOnu)}`),
            set(interfaceNameToLens, pathOr('pon0', ['interfaces', 0, 'identification', 'name'], cmDeviceOnu))
          )();

          return getDeviceWithSiteById$(oltId)
            .map(cmDeviceOlt => mergeDeviceBoth(dummyDataLink, cmDeviceOlt, cmDeviceOnu))
            .do(cmDataLinkMerged => dataLinkStore.dispatch(requestSave(cmDataLinkMerged)));
        })
        .catch((error) => {
          messageHub.logError(message, error);
          return Observable.empty();
        })
        .toPromise();
    }

    return Promise.resolve();
  }
);
