'use strict';

const { Reader: reader } = require('monet');
const { requestRemoveByDeviceId } = require('../actions');

/**
 * @param {string} deviceId
 * @param {Message} message
 * @return {Reader.<onDeviceRemoveHandler~callback>}
 */
module.exports = ({ deviceId }, message) => reader(
  ({ dataLinkStore, messageHub }) => {
    try {
      dataLinkStore.dispatch(requestRemoveByDeviceId(deviceId));
    } catch (error) {
      messageHub.logError(message);
    }
  }
);
