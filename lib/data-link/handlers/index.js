'use strict';

const { weave } = require('ramda-adjunct');

const stationConnected = require('./stationConnected');
const deviceRemoved = require('./deviceRemoved');
const stationDisconnected = require('./stationDisconnected');
const deviceAuthorized = require('./deviceAuthorized');
const deviceConnected = require('./deviceConnected');

exports.register = (server, messageHub, messages) => {
  const { dataLinkStore, dal, DB, deviceStore } = server.plugins;

  const deviceConnectedBound = weave(deviceConnected, { dataLinkStore, messageHub, DB, dal });
  const stationConnectedBound = weave(stationConnected, { dataLinkStore, messageHub, DB, dal });
  const stationDisconnectedBound = weave(stationDisconnected, { dataLinkStore, messageHub });
  const deviceRemovedBound = weave(deviceRemoved, { dataLinkStore, messageHub });
  const deviceAuthorizedBound = weave(deviceAuthorized, { dataLinkStore, messageHub, deviceStore });

  messageHub.subscribe(messages.deviceRemoved, deviceRemovedBound);
  messageHub.subscribe(messages.stationConnected, stationConnectedBound);
  messageHub.subscribe(messages.stationDisconnected, stationDisconnectedBound);
  messageHub.subscribe(messages.deviceAuthorized, deviceAuthorizedBound);
  messageHub.subscribe(messages.deviceConnected, deviceConnectedBound);
};
