'use strict';

const { Reader: reader } = require('monet');
const { pipe, pathOr, when, toLower, any, equals, isNil, assocPath } = require('ramda');
const { isString } = require('lodash/fp');
const { Observable } = require('rxjs');
const { isNotEmpty, isNotNull } = require('ramda-adjunct');

require('../../util/observable');
const { DataLinkOriginEnum, DataLinkStatusEnum } = require('../../enums');
const { updateStatusByFromDeviceIdToDeviceId, requestSave } = require('../actions');
const { mac2id } = require('../../util');
const { fromDb: fromDbDevice } = require('../../transformers/device');
const { dataLinkIdListBetweenDevicesSelector } = require('../selectors');
const { parseDummyDataLink } = require('../transformers/parsers');
const { mergeDeviceBoth } = require('../transformers/mergers');
const { isAP, isAirMaxDeviceType, isAirCubeDeviceType } = require('../../feature-detection/common');
const { parseDbInterfaceList } = require('../../transformers/interfaces/parsers');


/**
 * @param {{ deviceId, CorrespondenceStation }} deviceIdWithStation
 * @param {Message} message
 * @return {Reader.<onStationConnectedHandler~callback>}
 */
module.exports = ({ deviceId, station }, message) => reader(
  ({ dataLinkStore, DB, dal, messageHub }) => {
    const stationMac = pipe(pathOr(null, ['mac']), toLower)(station);
    const stationId = when(isString, mac2id, stationMac);

    const getDeviceWithSiteById$ = deviceInnerId => Observable.from(DB.device.findById(deviceInnerId))
      .catch(() => Observable.from(DB.device.findAll())
        .mergeAll()
        .find(dbDevice => any(pipe(
          pathOr(null, ['identification', 'mac']),
          when(isNotNull, toLower),
          equals(stationMac)
        ), dbDevice.interfaces))
      )
      .mergeMap((dbDevice) => {
        if (isNil(dbDevice)) {
          return Observable.empty();
        }

        return Observable.from(dal.siteRepository.findOneById(dbDevice.identification.site.id))
          .map(dbSite => [dbDevice, dbSite]);
      })
      .mergeEither(([dbDevice, dbSite]) => fromDbDevice({ dbSite }, dbDevice))
      .map(cmDevice => assocPath(['interfaces'], parseDbInterfaceList({}, cmDevice.interfaces), cmDevice));

    return getDeviceWithSiteById$(stationId)
      .mergeMap((cmDeviceStation) => {
        const existingDataLinkIds = dataLinkIdListBetweenDevicesSelector(
          dataLinkStore.getState(),
          { deviceIdFrom: deviceId, deviceIdTo: stationId }
        );

        if (isNotEmpty(existingDataLinkIds)) {
          dataLinkStore.dispatch(
            updateStatusByFromDeviceIdToDeviceId({
              deviceIdFrom: deviceId,
              deviceIdTo: stationId,
              status: DataLinkStatusEnum.Active,
            })
          );
          return Observable.empty();
        }

        return getDeviceWithSiteById$(deviceId)
          .map((cmDevice) => {
            const model = pathOr(null, ['identification', 'model'], cmDevice);
            let wifiMode = null;

            if (isAirMaxDeviceType(model)) { wifiMode = pathOr(null, ['airmax', 'wirelessMode'], cmDevice) }
            if (isAirCubeDeviceType(model)) { wifiMode = pathOr(null, ['aircube', 'wifiMode'], cmDevice) }

            if (isAP(wifiMode)) {
              return mergeDeviceBoth(
                parseDummyDataLink({ origin: DataLinkOriginEnum.Unms }),
                cmDevice,
                cmDeviceStation
              );
            }
            return mergeDeviceBoth(parseDummyDataLink({ origin: DataLinkOriginEnum.Unms }), cmDeviceStation, cmDevice);
          })
          .do(cmDataLinkMerged => dataLinkStore.dispatch(requestSave(cmDataLinkMerged)));
      })
      .catch((error) => {
        messageHub.logError(message, error);
        return Observable.empty();
      })
      .toPromise();
  }
);
