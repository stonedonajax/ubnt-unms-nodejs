'use strict';

const { Reader: reader } = require('monet');
const { pipe, pathOr, when, toLower } = require('ramda');
const { isString } = require('lodash/fp');
const { isNotEmpty } = require('ramda-adjunct');

const { DataLinkStatusEnum } = require('../../enums');
const { updateStatusByFromDeviceIdToDeviceId } = require('../actions');
const { mac2id } = require('../../util');
const { dataLinkIdListBetweenDevicesSelector } = require('../selectors');

/**
 * @param {CorrespondenceDevice} cmDevice
 * @param {Message} message
 * @return {Reader.<onStationDisconnectedHandler~callback>}
 */
module.exports = ({ deviceId, station }, message) => reader(
  ({ dataLinkStore, messageHub }) => {
    try {
      const stationId = pipe(pathOr(null, ['mac']), when(isString, pipe(toLower, mac2id)))(station);
      const existingDataLinkIds = dataLinkIdListBetweenDevicesSelector(
        dataLinkStore.getState(),
        { deviceIdFrom: deviceId, deviceIdTo: stationId }
      );

      if (isNotEmpty(existingDataLinkIds)) {
        dataLinkStore.dispatch(
          updateStatusByFromDeviceIdToDeviceId({
            deviceIdFrom: deviceId,
            deviceIdTo: stationId,
            status: DataLinkStatusEnum.Disconnected,
          })
        );
      }
    } catch (error) {
      messageHub.logError(message, error);
    }
  }
);
