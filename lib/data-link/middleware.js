'use strict';

const { pathEq, map, assocPath } = require('ramda');
const { Observable } = require('rxjs');

const {
  REMOVE_BY_ID_REQUEST, REMOVE_BY_DEVICE_ID_REQUEST, SAVE_REQUEST, UPDATE_REQUEST, SYNCHRONIZE_DATA_REQUEST,
  removeByIdSuccess, removeByIdFailure, removeByDeviceIdSuccess, removeByDeviceIdFailure, saveFailure, saveSuccess,
  updateFailure, updateSuccess, synchronizeDataFailure, requestUpdate,
} = require('./actions');
const { toDbDataLink } = require('../dal/repositories/dataLink/transformers/mappers');
const { DataLinkOriginEnum } = require('../enums');
const { dataLinkListSelector } = require('./selectors');
const { mergeDeviceList } = require('./transformers/mergers');
const { parseDbDeviceList } = require('../transformers/device/parsers');
const { mergeMetadataList } = require('../transformers/device/mergers');
const { parseDbInterfaceList } = require('../transformers/interfaces/parsers');
const { parseDbDeviceMetadataList } = require('../transformers/device/metadata/parsers');


const dataLinkMiddleware = ({ dal, DB }) => ({ dispatch, getState }) => next => (action) => {
  if (action.type === REMOVE_BY_ID_REQUEST) {
    dal.dataLinkRepository.remove(action.payload)
      .then(() => dispatch(removeByIdSuccess(action.payload)))
      .catch(err => dispatch(removeByIdFailure(err)));
  }

  if (action.type === REMOVE_BY_DEVICE_ID_REQUEST) {
    dal.dataLinkRepository.removeByDeviceId(action.payload)
      .then(() => dispatch(removeByDeviceIdSuccess(action.payload)))
      .catch(err => dispatch(removeByDeviceIdFailure(err)));
  }

  if (action.type === SAVE_REQUEST) {
    if (pathEq(['origin'], DataLinkOriginEnum.Manual, action.payload)) {
      const dbDataLink = toDbDataLink(action.payload);
      dal.dataLinkRepository.save(dbDataLink)
        .then(() => dispatch(saveSuccess(action.payload)))
        .catch(err => dispatch(saveFailure(err)));
    }
  }

  if (action.type === UPDATE_REQUEST) {
    const dbDataLink = toDbDataLink(action.payload);
    dal.dataLinkRepository.update(dbDataLink)
      .then(() => dispatch(updateSuccess(action.payload)))
      .catch(err => dispatch(updateFailure(err)));
  }

  // TODO(jan.beseda@ubnt.com): figure better performent method
  if (action.type === SYNCHRONIZE_DATA_REQUEST) {
    Observable.from(DB.device.list())
      .map(parseDbDeviceList({}))
      .mergeMap(cmDeviceList => Observable.from(dal.deviceMetadataRepository.findAll())
        .map(parseDbDeviceMetadataList({}))
        .map(cmMetadataList => mergeMetadataList(cmMetadataList, cmDeviceList))
      )
      .map(map(cmDevice => assocPath(['interfaces'], parseDbInterfaceList({}, cmDevice.interfaces), cmDevice)))
      .mergeMap(deviceList => mergeDeviceList(dataLinkListSelector(getState()), deviceList))
      .do(updatedDataLink => dispatch(requestUpdate(updatedDataLink)))
      .catch((err) => {
        dispatch(synchronizeDataFailure(err));
        return Observable.throw(err);
      })
      .toPromise();
  }

  return next(action);
};


module.exports = dataLinkMiddleware;
