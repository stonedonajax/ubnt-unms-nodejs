'use strict';

const { registerPlugin } = require('../util/hapi');
const createDataLinkStore = require('./createStore');
const { initialize, requestSynchronizeData } = require('./actions');
const handlers = require('./handlers');
const { toMs } = require('../util');

/*
 * Hapi plugin definition
 */
function register(server) {
  const { dal, DB, messageHub, scheduler } = server.plugins;
  const config = server.settings.app;

  const dataLinkStore = createDataLinkStore(dal, DB);

  server.expose(dataLinkStore);

  messageHub.registerHandlers(handlers);

  if (!config.demo) {
    scheduler.registerPeriodicTask(
      () => dataLinkStore.dispatch(requestSynchronizeData()),
      toMs('seconds', 11),
      'synchronizeDataLinksData'
    );
  }

  return dataLinkStore.dispatch(initialize());
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'dataLinkStore',
  version: '1.0.0',
  dependencies: ['dal', 'messageHub'],
};
