'use strict';

const { handleActions } = require('redux-actions');
const {
  pipe, assocPath, dissocPath, keys, reduce, merge, assoc, path, when, evolve, always, view, pathOr, any, equals,
} = require('ramda');
const { isNotNull, isNotEmpty } = require('ramda-adjunct');

const actions = require('../actions');
const {
  parseDataLinkMap, removeVerticeFromEdge, cleanUpEdge, removeVerticesByDeviceId, removeDataLinkById,
  removeEdgeByDeviceId, parseDataLink, addEdgeVertices, removeOldAddNewVerticeFromEdge,
} = require('./utils');
const {
  dataLinkByIdSelector, dataLinkMapSelector, graphInterfaceListSelector, dataLinkIdListByDeviceIdSelector,
  dataLinkIdListBetweenDevicesSelector,
} = require('../selectors');
const { edgeNotEq } = require('../utils');
const { toGraphDataLinkEdge } = require('../transformers/mappers');
const { deviceIdFromLens, deviceIdToLens } = require('../transformers/lenses');


const initialState = {
  dataLinkMap: {},
  dataLinkGraph: {},
};


/*
 * Case reducers
 */

const handleInitializeSuccess = (state, action) => pipe(
  assocPath(['dataLinkGraph'], {}),
  assocPath(['dataLinkMap'], {}),
  parseDataLinkMap(action.payload)
)(state);

const handleRequestRemoveById = (state, action) => {
  const dataLink = dataLinkByIdSelector(state, action.payload);

  if (isNotNull(dataLink)) {
    const dataLinkMap = dissocPath([action.payload], dataLinkMapSelector(state));
    const from = toGraphDataLinkEdge(dataLink.id, dataLink.from);
    const to = toGraphDataLinkEdge(dataLink.id, dataLink.to);

    return pipe(
      assocPath(['dataLinkMap'], dataLinkMap),
      removeVerticeFromEdge(from, to),
      removeVerticeFromEdge(to, from),
      cleanUpEdge(from),
      cleanUpEdge(to)
    )(state);
  }
  return state;
};

const handleRequestRemoveByDeviceId = (state, action) => {
  const interfaceList = graphInterfaceListSelector(state, action.payload);
  const dataLinkIdListToDelete = dataLinkIdListByDeviceIdSelector(state, action.payload);

  return pipe(
    reduce(removeVerticesByDeviceId(interfaceList, action.payload), state),
    removeEdgeByDeviceId(action.payload),
    innerState => reduce(
      removeDataLinkById,
      innerState,
      dataLinkIdListToDelete
    )
  )(keys(interfaceList));
};

const handleRequestSave = (state, action) => {
  if (isNotEmpty(
    dataLinkIdListBetweenDevicesSelector(
      state,
      {
        deviceIdFrom: view(deviceIdFromLens, action.payload),
        deviceIdTo: view(deviceIdToLens, action.payload),
      })
  )) {
    return state;
  }

  return parseDataLink(state, action.payload);
};

const handleRequestUpdate = (state, action) => {
  const dataLinkId = pathOr(null, ['id'], action.payload);
  const oldDataLink = dataLinkByIdSelector(state, dataLinkId);

  if (any(equals(null), [dataLinkId, oldDataLink])) {
    return state;
  }

  const dataLink = merge(oldDataLink, action.payload);
  const dataLinkMap = assoc(path(['id'], dataLink), dataLink, dataLinkMapSelector(state));

  const oldFrom = toGraphDataLinkEdge(dataLinkId, oldDataLink.from);
  const oldTo = toGraphDataLinkEdge(dataLinkId, oldDataLink.to);
  const from = toGraphDataLinkEdge(dataLinkId, dataLink.from);
  const to = toGraphDataLinkEdge(dataLinkId, dataLink.to);

  return pipe(
    assocPath(['dataLinkMap'], dataLinkMap),
    when(() => edgeNotEq(oldFrom, from), pipe(
      removeVerticeFromEdge(oldTo, oldFrom),
      addEdgeVertices(oldTo, [from]),
      removeVerticeFromEdge(oldFrom, oldTo),
      addEdgeVertices(from, [to]),
      cleanUpEdge(oldFrom)
    )),
    when(() => edgeNotEq(oldTo, to), pipe(
      removeOldAddNewVerticeFromEdge(oldFrom, oldTo, to),
      removeVerticeFromEdge(oldTo, oldFrom),
      removeVerticeFromEdge(oldTo, from),
      addEdgeVertices(to, [from]),
      cleanUpEdge(oldTo)
    ))
  )(state);
};

const handleUpdateStatusByFromDeviceIdToDeviceId = (state, action) => {
  const { deviceIdFrom, deviceIdTo, status } = action.payload;

  const dataLinkIdList = dataLinkIdListBetweenDevicesSelector(state, { deviceIdFrom, deviceIdTo });

  if (isNotEmpty(dataLinkIdList)) {
    const updateDataLinkMap = (acc, dataLinkId) => {
      const updatedDataLink = evolve({
        from: {
          device: {
            status: always(status),
          },
          interface: {
            status: {
              plugged: always(status),
            },
          },
        },
        to: {
          device: {
            status: always(status),
          },
          interface: {
            status: {
              plugged: always(status),
            },
          },
        },
      }, dataLinkByIdSelector(state, dataLinkId));

      acc[updatedDataLink.id] = updatedDataLink;
      return acc;
    };

    const dataLinkMap = reduce(updateDataLinkMap, dataLinkMapSelector(state), dataLinkIdList);

    return assocPath(['dataLinkMap'], dataLinkMap, state);
  }

  return state;
};


/*
 * Root reducer
 */


const dataLinkReducers = handleActions({
  [actions.initializeSuccess]: handleInitializeSuccess,
  [actions.requestRemoveById]: handleRequestRemoveById,
  [actions.requestRemoveByDeviceId]: handleRequestRemoveByDeviceId,
  [actions.requestSave]: handleRequestSave,
  [actions.requestUpdate]: handleRequestUpdate,
  [actions.updateStatusByFromDeviceIdToDeviceId]: handleUpdateStatusByFromDeviceIdToDeviceId,
}, initialState);


module.exports = dataLinkReducers;
