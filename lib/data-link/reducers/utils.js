'use strict';

const { curry, reduce, pipe, assocPath, path, without, dissocPath, isEmpty, when, not } = require('ramda');
const { has } = require('lodash/fp');
const { isUndefined } = require('ramda-adjunct');

const { dataLinkMapSelector, dataLinkGraphSelector } = require('../selectors');
const { toGraphDataLinkEdge } = require('../transformers/mappers');
const { isDataLink } = require('../utils');


const addEdgeVertices = curry((edge, vertices, state) => {
  const dataLinkGraph = dataLinkGraphSelector(state);
  const dataLinkDevice = path([edge.deviceId], dataLinkGraph);
  if (isUndefined(dataLinkDevice)) {
    dataLinkGraph[edge.deviceId] = {};
  }
  if (isUndefined(dataLinkGraph[edge.deviceId][edge.interfaceName])) {
    dataLinkGraph[edge.deviceId][edge.interfaceName] = vertices;
  } else {
    dataLinkGraph[edge.deviceId][edge.interfaceName] = dataLinkDevice[edge.interfaceName].concat(vertices);
  }

  return assocPath(['dataLinkGraph'], dataLinkGraph, state);
});

const parseDataLink = curry((state, dataLink) => {
  if (not(isDataLink(dataLink))) {
    return state;
  }

  const id = path(['id'], dataLink);
  const dataLinkMap = dataLinkMapSelector(state);
  dataLinkMap[id] = dataLink;
  const from = toGraphDataLinkEdge(dataLink.id, dataLink.from);
  const to = toGraphDataLinkEdge(dataLink.id, dataLink.to);
  return pipe(
    addEdgeVertices(from, [to]),
    addEdgeVertices(to, [from]),
    assocPath(['dataLinkMap'], dataLinkMap)
  )(state);
});

const parseDataLinkMap = curry((dataLinkMap, state) => reduce(parseDataLink, state, dataLinkMap));

const removeVerticeFromEdge = curry((edge, vertice, state) => pipe(
  dataLinkGraphSelector,
  (graph => without([vertice], graph[edge.deviceId][edge.interfaceName])),
  (vertices => assocPath(['dataLinkGraph', edge.deviceId, edge.interfaceName], vertices, state))
)(state));

const cleanUpEdge = curry((edge, state) => {
  let graph = dataLinkGraphSelector(state);

  if (isEmpty(graph[edge.deviceId][edge.interfaceName])) {
    graph = dissocPath([edge.deviceId, edge.interfaceName], graph);
    if (isEmpty(graph[edge.deviceId])) {
      graph = dissocPath([edge.deviceId], graph);
    }
  }
  return assocPath(['dataLinkGraph'], graph, state);
});

const removeVerticeFromInterface = curry((deviceId, interfaceName, state, edge) => pipe(
  removeVerticeFromEdge(edge, { dataLinkId: edge.dataLinkId, deviceId, interfaceName }),
  cleanUpEdge(edge)
)(state));

const removeVerticesByDeviceId = curry((interfaceList, deviceId, state, interfaceName) =>
  reduce(removeVerticeFromInterface(deviceId, interfaceName), state, interfaceList[interfaceName]));

const removeDataLinkById = curry((state, dataLinkId) => dissocPath(['dataLinkMap', dataLinkId], state));

const removeEdgeByDeviceId = curry((deviceId, state) => dissocPath(['dataLinkGraph', deviceId], state));

const removeOldAddNewVerticeFromEdge = (oldEdge, oldVertice, newVertice) =>
  when(innerState => has([oldEdge.deviceId, oldEdge.interfaceName], dataLinkGraphSelector(innerState)), pipe(
    removeVerticeFromEdge(oldEdge, oldVertice),
    addEdgeVertices(oldEdge, [newVertice])
));

module.exports = {
  parseDataLinkMap,
  parseDataLink,
  addEdgeVertices,
  removeVerticeFromEdge,
  cleanUpEdge,
  removeVerticesByDeviceId,
  removeVerticeFromInterface,
  removeDataLinkById,
  removeEdgeByDeviceId,
  removeOldAddNewVerticeFromEdge,
};
