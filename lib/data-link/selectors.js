'use strict';

const {
  filter, values, nthArg, path, map, pipe, when, contains, flatten, view, pathOr, pathEq,
} = require('ramda');
const { isNotEmpty, isNotNull, lensEq } = require('ramda-adjunct');
const { createSelector } = require('reselect');

const { dataLinkContainsSiteId } = require('./utils');
const {
  deviceIdFromLens, interfaceNameFromLens, deviceIdToLens, interfaceNameToLens,
} = require('./transformers/lenses');


/*
 * Input selectors
 */

const dataLinkMapSelector = state => state.dataLinkMap;

const dataLinkGraphSelector = state => state.dataLinkGraph;

/*
 * Combined selectors
 */

 // map selectors

const dataLinkListSelector = createSelector(
  dataLinkMapSelector,
  values
);

const dataLinkByIdSelector = createSelector(
  dataLinkMapSelector,
  nthArg(1),
  (dataLinkMap, dataLinkId) => pathOr(null, [dataLinkId], dataLinkMap)
);

const dataLinkListByIdListSelector = createSelector(
  dataLinkMapSelector,
  nthArg(1),
  (dataLinkMap, dataLinkIdList) => pipe(
    values,
    filter(dataLink => contains(dataLink.id, dataLinkIdList))
  )(dataLinkMap)
);

const dataLinkIdListByDeviceIdSelector = createSelector(
  dataLinkGraphSelector,
  nthArg(1),
  (dataLinkGraph, deviceId) => pipe(
    pathOr([], [deviceId]),
    when(isNotEmpty, pipe(values, flatten, map(path(['dataLinkId']))))
    )(dataLinkGraph)
  );

const dataLinkListByDeviceIdSelector = createSelector(
  dataLinkMapSelector,
  dataLinkIdListByDeviceIdSelector,
  (dataLinkMap, dataLinkIdList) => pipe(
    values,
    filter(dataLink => contains(dataLink.id, dataLinkIdList))
  )(dataLinkMap)
);

const dataLinkListBySiteIdSelector = createSelector(
  dataLinkMapSelector,
  nthArg(1),
  (dataLinkMap, siteId) => filter(dataLinkContainsSiteId(siteId), values(dataLinkMap))
);

const isDataLinkExistsSelector = createSelector(
  dataLinkMapSelector,
  nthArg(1),
  (dataLinkMap, dataLinkId) => isNotNull(pathOr(null, [dataLinkId], dataLinkMap))
);

// graph selectors

const graphInterfaceListSelector = createSelector(
  dataLinkGraphSelector,
  nthArg(1),
  (dataLinkGraph, deviceId) => pathOr(null, [deviceId], dataLinkGraph)
);

const isDataLinkConnectionExistsSelector = createSelector(
  dataLinkGraphSelector,
  nthArg(1),
  (dataLinkGraph, dataLink) => {
    const vectorsFrom = pathOr(
      [], [view(deviceIdFromLens, dataLink), view(interfaceNameFromLens, dataLink)], dataLinkGraph
    );
    const vectorsTo = pathOr([], [view(deviceIdToLens, dataLink), view(interfaceNameToLens, dataLink)], dataLinkGraph);

    const vectorsFromExists = vectorsFrom.some(
      vector => lensEq(deviceIdToLens, vector.deviceId, dataLink)
        && lensEq(interfaceNameToLens, vector.interfaceName, dataLink),
      vectorsFrom
    );

    const vectorsToExists = vectorsTo.some(
      vector => lensEq(deviceIdFromLens, vector.deviceId, dataLink)
        && lensEq(interfaceNameFromLens, vector.interfaceName, dataLink),
      vectorsTo
    );

    return vectorsFromExists || vectorsToExists;
  }
);

const dataLinkIdListBetweenDevicesSelector = createSelector(
  dataLinkGraphSelector,
  nthArg(1),
  (dataLinkGraph, { deviceIdFrom, deviceIdTo }) => pipe(
    pathOr([], [deviceIdFrom]),
    when(isNotEmpty, pipe(values,
      flatten,
      filter(pathEq(['deviceId'], deviceIdTo)),
      map(path(['dataLinkId'])))
    )
  )(dataLinkGraph)
);


module.exports = {
  dataLinkMapSelector,
  dataLinkGraphSelector,

  dataLinkListSelector,
  dataLinkByIdSelector,
  dataLinkListByIdListSelector,
  dataLinkListByDeviceIdSelector,
  dataLinkIdListByDeviceIdSelector,
  dataLinkListBySiteIdSelector,
  isDataLinkExistsSelector,
  graphInterfaceListSelector,
  isDataLinkConnectionExistsSelector,
  dataLinkIdListBetweenDevicesSelector,
};
