'use strict';

const { lensPath, compose } = require('ramda');


const edgeFromLens = lensPath(['from']);
const edgeToLens = lensPath(['to']);

const deviceFromLens = compose(edgeFromLens, lensPath(['device']));
const deviceIdentificationFromLens = compose(deviceFromLens, lensPath(['identification']));
const deviceIdFromLens = compose(deviceIdentificationFromLens, lensPath(['id']));
const siteIdentificationFromLens = compose(edgeFromLens, lensPath(['site', 'identification']));
const siteIdFromLens = compose(siteIdentificationFromLens, lensPath(['id']));
const interfaceFromLens = compose(edgeFromLens, lensPath(['interface']));
const interfaceFromLensIdentification = compose(interfaceFromLens, lensPath(['identification']));
const interfaceNameFromLens = compose(interfaceFromLensIdentification, lensPath(['name']));

const deviceToLens = compose(edgeToLens, lensPath(['device']));
const deviceIdentificationToLens = compose(deviceToLens, lensPath(['identification']));
const deviceIdToLens = compose(deviceIdentificationToLens, lensPath(['id']));
const siteIdentificationToLens = compose(edgeToLens, lensPath(['site', 'identification']));
const siteIdToLens = compose(siteIdentificationToLens, lensPath(['id']));
const interfaceToLens = compose(edgeToLens, lensPath(['interface']));
const interfaceToLensIdentification = compose(interfaceToLens, lensPath(['identification']));
const interfaceNameToLens = compose(interfaceToLensIdentification, lensPath(['name']));


module.exports = {
  edgeFromLens,
  edgeToLens,

  deviceFromLens,
  deviceIdentificationFromLens,
  deviceIdFromLens,
  siteIdentificationFromLens,
  siteIdFromLens,
  interfaceFromLens,
  interfaceNameFromLens,

  deviceToLens,
  deviceIdentificationToLens,
  deviceIdToLens,
  siteIdentificationToLens,
  siteIdToLens,
  interfaceToLens,
  interfaceNameToLens,
};
