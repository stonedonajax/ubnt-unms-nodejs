'use strict';

const { pathOr } = require('ramda');

 /**
  * Model of dataLink used in graphStructure
  * @typedef {Object} GraphDataLinkEdge
  * @property {string} deviceId
  * @property {string} interfaceName
  */

/**
 * @param {string} id of dataLink
 * @param {DataLinkEdge} dataLinkEdge
 * @return {GraphDataLinkEdge}
 */
const toGraphDataLinkEdge = (id, dataLinkEdge) => ({
  dataLinkId: id,
  deviceId: pathOr(null, ['device', 'identification', 'id'], dataLinkEdge),
  interfaceName: pathOr(null, ['interface', 'identification', 'name'], dataLinkEdge),
});


module.exports = {
  toGraphDataLinkEdge,
};
