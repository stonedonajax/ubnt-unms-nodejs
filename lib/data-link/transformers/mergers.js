'use strict';

const { path, pathOr, map, assocPath, pipe, omit, merge, curry, defaultTo } = require('ramda');
const { keyBy, spread } = require('lodash/fp');

const { parseDeviceInterface } = require('./parsers');
const { StatusEnum } = require('../../enums');


// mergeDataLinkEdgeDevice :: (Object, Object) -> DataLinkEdgeCorrespondence
//     DataLinkEdgeCorrespondence = Object
const mergeDataLinkEdgeDevice = (dataLinkEdgeCorrespondence, deviceCorrespondence) => {
  if (deviceCorrespondence === null) { return dataLinkEdgeCorrespondence }

  return merge(dataLinkEdgeCorrespondence,
    {
      site: {
        identification: defaultTo(
          pathOr(null, ['site', 'identification'], dataLinkEdgeCorrespondence),
          path(['identification', 'site', 'identification'], deviceCorrespondence)
        ),
      },
      device: {
        identification: pipe(path(['identification']), omit(['site']))(deviceCorrespondence),
        status: pathOr(StatusEnum.Disconnected, ['overview', 'status'], deviceCorrespondence),
        ssid: pathOr(null, ['airmax', 'ssid'], deviceCorrespondence),
        frequency: pathOr(null, ['airmax', 'frequency'], deviceCorrespondence),
        meta: pathOr(null, ['meta'], deviceCorrespondence),
      },
      interface: parseDeviceInterface(deviceCorrespondence, dataLinkEdgeCorrespondence),
    }
  );
};

// mergeDevice :: String -> DataLinkCorrespondence -> DeviceCorrespondence -> DataLinkCorrespondence
//     DataLinkCorrespondence = Object
//     DeviceCorrespondence = Object
const mergeDeviceBySide = (edgeSide, dataLinkCorrespondence) =>
  assocPath([edgeSide], mergeDataLinkEdgeDevice(path([edgeSide], dataLinkCorrespondence)));

// mergeDeviceBoth :: String -> DataLinkCorrespondence -> DeviceCorrespondence -> DataLinkCorrespondence
//     DataLinkCorrespondence = Object
//     DeviceCorrespondence = Object
const mergeDeviceBoth = curry((dataLinkCorrespondence, fromDeviceCorrespondence, toDeviceCorrespondence) => pipe(
  assocPath(['from'], mergeDataLinkEdgeDevice(path(['from'], dataLinkCorrespondence), fromDeviceCorrespondence)),
  assocPath(['to'], mergeDataLinkEdgeDevice(path(['to'], dataLinkCorrespondence), toDeviceCorrespondence))
)(dataLinkCorrespondence));

// mergedDeviceList :: (Array.<DataLinkList>, Array.<DeviceList>) -> DataLinkList
//     DataLinkList = Array.<DataLinkCorrespondence>
//     DeviceList = Array.<DeviceCorrespondence>
//     DataLinkCorrespondence = Object
//     DeviceCorrespondence = Object
const mergeDeviceList = curry((dataLinkCorrespondenceList, deviceCorrespondenceList) => {
  const deviceCorrespondenceMap = keyBy(path(['identification', 'id']), deviceCorrespondenceList);
  const deviceFromId = path(['from', 'device', 'identification', 'id']);
  const deviceToId = path(['to', 'device', 'identification', 'id']);

  const toDataLinkFromDeviceTriples = dataLink =>
    [
      dataLink,
      pathOr(null, [deviceFromId(dataLink)], deviceCorrespondenceMap),
      pathOr(null, [deviceToId(dataLink)], deviceCorrespondenceMap),
    ];
  const deviceFromToPairs = dataLinkCorrespondenceList.map(toDataLinkFromDeviceTriples);

  return map(spread(mergeDeviceBoth), deviceFromToPairs);
});


module.exports = {
  mergeDataLinkEdgeDevice,
  mergeDeviceBySide,
  mergeDeviceBoth,
  mergeDeviceList,
};
