'use strict';

const { curry, path, pathOr, pipe, find, map } = require('ramda');
const aguid = require('aguid');
const { isNull } = require('lodash/fp');

const { pickInterfaceByNameFromList, isWifiInterfaceName } = require('../../transformers/interfaces/utils');
const { parseDbInterface } = require('../../transformers/interfaces/parsers');
const { DataLinkOriginEnum } = require('../../enums');

/**
 * Correspondence model of dataLink
 * @typedef {Object} DataLink
 * @property {string} id UUID
 * @property {DataLinkEdge} from
 * @property {DataLinkEdge} to
 * @property {string} origin
 */

/**
 * @typedef {Object} DataLinkEdge
 * @property {Object} site of device Correspondence
 * @property {Object} device
 * @property {Object} interface
 */

const pickFirstWifiInterfaceName = find(isWifiInterfaceName);

const parseDeviceInterface = (deviceCorrespondence, dataLinkEdgeCorrespondence) => {
  let interfaceName = pathOr(null, ['interface', 'identification', 'name'], dataLinkEdgeCorrespondence);
  const deviceInterfaceList = path(['interfaces'], deviceCorrespondence);
  if (isNull(interfaceName)) {
    interfaceName = pipe(
      map(path(['identification', 'name'])),
      pickFirstWifiInterfaceName
    )(deviceInterfaceList);
  }

  return pickInterfaceByNameFromList(interfaceName, deviceInterfaceList);
};

// parseDataLinkEdge :: Object -> Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseDataLinkEdge = curry((auxiliaries, dataLinkEdge) => ({
  site: {
    identification: {
      id: pathOr(null, ['siteId'], dataLinkEdge),
      name: null,
      type: null,
      status: null,
      parentId: null,
    },
  },
  device: {
    identification: {
      id: dataLinkEdge.deviceId,
      name: null,
      displayName: null,
      type: null,
      category: null,
      authorized: null,
    },
    status: null,
    ssid: null,
    frequency: null,
  },
  interface: parseDbInterface({}, { identification: { name: dataLinkEdge.interfaceName } }),
}));

const parseDummyDataLink = ({ id = null, origin = null }) => ({
  id: isNull(id) ? aguid() : id,
  from: null,
  to: null,
  origin: isNull(origin) ? DataLinkOriginEnum.Manual : origin,
});

module.exports = {
  parseDeviceInterface,
  parseDataLinkEdge,
  parseDummyDataLink,
};
