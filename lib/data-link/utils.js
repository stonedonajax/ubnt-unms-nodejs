'use strict';

const { curry, pathEq, allPass, not, anyPass } = require('ramda');
const { lensEq } = require('ramda-adjunct');
const { has } = require('lodash/fp');

const { siteIdFromLens, siteIdToLens, deviceIdFromLens, deviceIdToLens } = require('./transformers/lenses');


const isDeviceIdEq = curry((edge, edgeToCompare) => pathEq(['deviceId'], edge.deviceId, edgeToCompare));

const isInterfaceNameEq = curry((edge, edgeToCompare) => pathEq(['interfaceName'], edge.interfaceName, edgeToCompare));

const edgeEq = curry((edge, edgeToCompare) => allPass([isDeviceIdEq(edge), isInterfaceNameEq(edge)])(edgeToCompare));

const edgeNotEq = curry((edge, edgeToCompare) =>
  not(allPass([isDeviceIdEq(edge), isInterfaceNameEq(edge)])(edgeToCompare)));

const dataLinkContainsDeviceId = deviceId =>
  anyPass([lensEq(deviceIdFromLens, deviceId), lensEq(deviceIdToLens, deviceId)]);

const dataLinkContainsSiteId = siteId =>
  anyPass([lensEq(siteIdFromLens, siteId), lensEq(siteIdToLens, siteId)]);

const isDataLink = allPass([
  has('id'), has(['from', 'device', 'identification', 'id']), has(['from', 'interface', 'identification', 'name']),
  has(['to', 'device', 'identification', 'id']), has(['to', 'interface', 'identification', 'name']), has('origin'),
]);

const isNotDataLink = dataLink => not(isDataLink(dataLink));


module.exports = {
  isDeviceIdEq,
  isInterfaceNameEq,
  edgeEq,
  edgeNotEq,
  dataLinkContainsDeviceId,
  dataLinkContainsSiteId,
  isDataLink,
  isNotDataLink,
};
