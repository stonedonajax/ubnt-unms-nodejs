'use strict';

const { Subject, Observable, Scheduler: RxScheduler } = require('rxjs/Rx');
const { identity } = require('lodash/fp');

const { NotImplementedError, DeviceNotFoundError } = require('../device-store/errors');

class DeviceBackupQueue {
  /**
   * @param {DeviceBackups} repository
   * @param {Logging} logging
   * @param {Object} [options]
   * @param {number} options.delay
   * @param {number} options.concurrency
   * @param {RxScheduler} [rxScheduler]
   */
  constructor(repository, logging, options = {}, rxScheduler = RxScheduler.async) {
    const { concurrency = 1, delay: delayPeriod = 0 } = options;
    this.repository = repository;
    this.logging = logging;
    this.scheduled = new Set();
    this.queue = new Subject();

    const createDeviceBackup = this.createDeviceBackup.bind(this);
    const delay$ = Observable.timer(delayPeriod, rxScheduler);
    this.subscription = this.queue
      .filter(deviceId => !this.scheduled.has(deviceId))
      .groupBy(identity, identity, group => group.switchMapTo(delay$))
      .mergeMap(group => group.last())
      .do(deviceId => this.scheduled.add(deviceId))
      .mergeMap(createDeviceBackup, concurrency)
      .subscribe({
        error: (error) => { this.logging.error('Backup queue failed', error) },
      });
  }

  /**
   * @param {string} deviceId
   * @return {Observable.<CorrespondenceDeviceBackup>}
   */
  createDeviceBackup(deviceId) {
    return this.repository.create(deviceId, true) // will not create backup if the previous is the same
      .catch((error) => {
        if (!(error instanceof NotImplementedError) && !(error instanceof DeviceNotFoundError)) {
          this.logging.error(`Backup for device ${deviceId} failed`, error);
        }
        return Observable.empty();
      })
      .finally(() => {
        this.scheduled.delete(deviceId);
      });
  }

  /**
   * @param {string} deviceId
   * @return {void}
   */
  scheduleBackup(deviceId) {
    this.queue.next(deviceId);
  }

  /**
   * Does necessary cleanup.
   *
   * Required to call on server stop
   *
   * @return {void}
   */
  destroy() {
    if (this.subscription !== null) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }
}

module.exports = DeviceBackupQueue;
