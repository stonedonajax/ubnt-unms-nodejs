'use strict';

class BackupNotFoundError extends Error {
  constructor(message = 'Device backup not found') {
    super(message);
  }
}

module.exports = {
  BackupNotFoundError,
};
