'use strict';

const { weave } = require('ramda-adjunct');

const removeBackups = require('./removeBackups');

exports.register = (server, messageHub, messages) => {
  const { deviceBackups } = server.plugins;

  const removeBackupsBound = weave(removeBackups, { deviceBackups, messageHub });

  const { deviceRemoved } = messages;

  messageHub.subscribe(deviceRemoved, removeBackupsBound);
};
