'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader } = require('monet');
const moment = require('moment-timezone');
const { get, compact } = require('lodash/fp');
const { weave, isNotPlainObj } = require('ramda-adjunct');
const fse = require('fs-extra');
const pump = require('pump');
const path = require('path');
const targz = require('tar.gz');

const { cleanFiles, toCanonicalKebabCase } = require('../util');
const { fromDbList: fromDbDeviceList } = require('../transformers/device');
const { parseDbBackup } = require('./transformers/parsers');
const repository = require('./repository');

const pumpAsObservable = Observable.bindNodeCallback(pump);

/**
 * @return {Reader.<Observable>}
 */
const cleanUp = () => reader(
  ({ backupConfig }) => {
    const { multiBackup } = backupConfig;
    return Observable.from(cleanFiles(multiBackup.ttl, backupConfig.path(multiBackup.dir)));
  }
);

// isBackupSupported :: CmDevice -> Boolean
const isBackupSupported = cmDevice => reader(
  ({ backupConfig }) => !backupConfig.forbiddenDeviceTypes.has(cmDevice.identification.type)
);

/**
 * @param {string} dirName
 * @param {CorrespondenceDevice} cmDevice
 * @param {CorrespondenceDeviceBackup} cmDeviceBackup
 * @return {string}
 */
const createBackupName = (dirName, cmDevice, cmDeviceBackup) => {
  const siteName = get(['identification', 'site', 'identification', 'name'], cmDevice);
  const deviceName = get(['identification', 'name'], cmDevice);
  const dateTime = moment(cmDeviceBackup.timestamp).format('YYYY-MM-DD-HH-mm-ss');

  const filename = compact([siteName, deviceName, dateTime]).map(toCanonicalKebabCase).join('__');
  return path.join(dirName, `${filename}.${cmDeviceBackup.extension}`);
};

const create = deviceIds => reader(
  ({ DB, dal, backupConfig, deviceStore }) => {
    const { multiBackup } = backupConfig;

    const dateString = moment().format('YYYY-MM-DD-HH-mm-ss');
    const backupDir = backupConfig.path(multiBackup.dir, `unms_devices_backup_${dateString}`);
    const isBackupSupportedBound = weave(isBackupSupported, { backupConfig });
    const createBackupBound = weave(repository.create, { DB, backupConfig, deviceStore });
    const readBackupFileBound = weave(repository.readFile, { backupConfig });

    fse.ensureDirSync(backupDir);

    return Observable.from(dal.siteRepository.findAllSites())
      .mergeMap(dbSiteList => Observable.from(deviceIds)
        .mergeMap(deviceId => DB.device.findById(deviceId))
        .toArray()
        .mergeEither(fromDbDeviceList({ dbSiteList }))
        .mergeAll()
      )
      .filter(isBackupSupportedBound)
      .mergeMap((cmDevice) => {
        const deviceId = cmDevice.identification.id;
        return Observable.from(DB.device.findLatestBackup(deviceId))
          .mergeMap((dbDeviceBackup) => {
            if (isNotPlainObj(dbDeviceBackup)) {
              return createBackupBound(deviceId);
            }

            return Observable.of(parseDbBackup(dbDeviceBackup));
          })
          .mergeMap((cmDeviceBackup) => {
            const filename = createBackupName(backupDir, cmDevice, cmDeviceBackup);
            return pumpAsObservable(readBackupFileBound(deviceId, cmDeviceBackup), fse.createWriteStream(filename));
          })
          .catch(() => Observable.empty()); // ignore if backup fails
      })
      .ignoreElements() // do everything but emit nothing
      .concat(Observable.defer(() => Observable.of(targz().createReadStream(backupDir))));
  }
);

module.exports = {
  cleanUp,
  create,
};
