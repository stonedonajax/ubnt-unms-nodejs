'use strict';

const Boom = require('boom');
const { weave } = require('ramda-adjunct');
const path = require('path');

const { registerPlugin } = require('../util/hapi');
const DeviceBackupQueue = require('./device-backup-queue');
const repository = require('./repository');
const multiRepository = require('./multi-repository');
const handlers = require('./handlers');
const { BackupNotFoundError } = require('./errors');

function register(server) {
  const config = server.settings.app;
  const { DB, dal, logging, scheduler, deviceStore, messageHub } = server.plugins;

  const backupConfig = {
    ...config.deviceConfigBackup,
    path: path.join.bind(path, config.deviceConfigBackup.dir),
    forbiddenDeviceTypes: new Set(config.deviceConfigBackup.forbiddenDeviceTypes),
  };

  const dependencies = { DB, dal, backupConfig, deviceStore };
  /**
   * @alias DeviceBackups
   */
  const pluginApi = {
    backupConfig,
    listBackups: weave(repository.listBackups, dependencies),
    create: weave(repository.create, dependencies),
    findById: weave(repository.findById, dependencies),
    removeAll: weave(repository.removeAll, dependencies),
    remove: weave(repository.remove, dependencies),
    multi: {
      create: weave(multiRepository.create, dependencies),
      cleanUp: weave(multiRepository.cleanUp, dependencies),
    },
    cleanUp: weave(repository.cleanUp, dependencies),
    save: weave(repository.save, dependencies),
    apply: weave(repository.apply, dependencies),
    readFile: weave(repository.readFile, dependencies),
    loadFile: weave(repository.loadFile, dependencies),
  };

  const deviceBackupQueue = new DeviceBackupQueue(pluginApi, logging, config.deviceConfigBackup.queue);

  server.expose(pluginApi);
  server.expose('queue', deviceBackupQueue);

  server.once('stop', () => { deviceBackupQueue.destroy() });

  messageHub.registerHandlers(handlers);

  // register scheduled tasks
  if (!config.demo) {
    scheduler.registerDailyTask(pluginApi.multi.cleanUp, 'cleanUpMultiBackups');
  }

  server.ext('onPreResponse', (request, reply) => {
    const response = request.response;
    if (!response.isBoom) { // if not error then continue
      return reply.continue();
    }

    if (response instanceof BackupNotFoundError) {
      return reply(Boom.notFound(response.message));
    }

    return reply.continue();
  });
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'deviceBackups',
  version: '1.0.0',
  dependencies: ['DB', 'dal', 'deviceStore', 'scheduler', 'logging', 'messageHub'],
};
