'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader } = require('monet');
const { isNull, isNil, isError, castArray } = require('lodash/fp');
const fse = require('fs-extra');
const zlib = require('zlib');
const pump = require('pump');
const concatStream = require('concat-stream');

const { parseDbBackupList, parseDbBackup } = require('./transformers/parsers');
const { toDbBackup } = require('./transformers/mappers');
const { entityExistsCheck } = require('../util');
const { BackupTypeEnum, EntityEnum } = require('../enums');
const { BackupNotFoundError } = require('./errors');

const byTimestamp = (a, b) => b.timestamp - a.timestamp;

/**
 * @param {string} deviceId
 * @return {Reader.<Observable>}
 */
const listBackups = deviceId => reader(
  ({ DB }) => Observable.from(DB.device.findById(deviceId))
    .do(entityExistsCheck(EntityEnum.Device))
    .mergeMap(() => DB.device.listBackups(deviceId))
    .map(parseDbBackupList)
);

/**
 * @param {string} deviceId
 * @return {Reader.<Observable>}
 */
const removeAll = deviceId => reader(
  ({ DB, backupConfig }) => {
    const dirName = backupConfig.path(deviceId);
    return Observable.merge(DB.device.removeAllBackups(deviceId), fse.remove(dirName))
      .last()
      .mapTo(true);
  }
);

/**
 * @param {string} deviceId
 * @param {string[]|string} backupIds
 * @return {Reader.<Observable>}
 */
const remove = (deviceId, backupIds) => reader(
  ({ DB, backupConfig }) => Observable.from(DB.device.removeBackups(deviceId, castArray(backupIds)))
    .mergeMap(() => backupIds)
    .map(backupId => backupConfig.path(deviceId, backupId))
    .mergeMap(backupFilename => fse.remove(backupFilename).catch(null))
    .last()
    .mapTo(backupIds)
);

/**
 * @param {string} deviceId
 * @return {Reader.<Observable>}
 */
const cleanUp = deviceId => reader(
  ({ DB, backupConfig }) => Observable.from(DB.device.listBackups(deviceId))
    .map(parseDbBackupList)
    .mergeMap((cmBackupList) => {
      const { minimumFiles, ttl } = backupConfig;

      if (cmBackupList.length < minimumFiles) {
        return Observable.empty();
      }

      const historyLimit = Date.now() - ttl;
      const backupsIdsToRemove = cmBackupList.sort(byTimestamp)
        .slice(minimumFiles)
        .filter(cmBackup => cmBackup.timestamp < historyLimit)
        .map(cmBackup => cmBackup.id);

      if (backupsIdsToRemove.length > 0) {
        return remove(deviceId, backupsIdsToRemove).run({ DB, backupConfig });
      }

      return Observable.empty();
    })
);

/**
 * @param {string} deviceId
 * @param {CorrespondenceDeviceBackup} cmBackup
 * @return {Reader.<Observable>}
 */
const save = (deviceId, cmBackup) => reader(
  ({ DB, backupConfig }) => {
    const writeFile$ = Observable.if(
      () => isNull(cmBackup.source),
      Observable.of(null),
      Observable.defer(() => fse.outputFile(backupConfig.path(deviceId, cmBackup.id), cmBackup.source))
    );

    const saveToDb$ = Observable.defer(() => {
      const dbDeviceBackup = toDbBackup(cmBackup);
      return DB.device.insertBackup(deviceId, dbDeviceBackup);
    });

    return writeFile$
      .mergeMapTo(saveToDb$)
      .mapTo(cmBackup);
  }
);

/**
 * Result will be empty if the backup is not created
 *
 * @param {string} deviceId
 * @param {boolean} createOnlyIfDifferent
 * @return {Reader.<Observable>}
 */
const create = (deviceId, createOnlyIfDifferent = false) => reader(
  ({ DB, backupConfig, deviceStore }) => deviceStore.getOrThrow(deviceId)
    .mergeMap(commDevice => commDevice.createBackup())
    .mergeMap((cmDeviceBackup) => {
      if (createOnlyIfDifferent) {
        return Observable.from(DB.device.findLatestBackup(deviceId))
          .map(parseDbBackup)
          .filter(prevCmDeviceBackup => prevCmDeviceBackup.crc !== cmDeviceBackup.crc)
          .mapTo(cmDeviceBackup); // is empty if the crc is the same
      }

      return Observable.of(cmDeviceBackup);
    })
    .mergeMap(cmDeviceBackup => save(deviceId, cmDeviceBackup).run({ DB, backupConfig }))
    .tapO(() => cleanUp(deviceId).run({ DB, backupConfig }))
);

/**
 * @param {string} deviceId
 * @param {CorrespondenceDeviceBackup} cmBackup
 * @return {Reader.<ReadStream>}
 */
const readFile = (deviceId, cmBackup) => reader(
  ({ backupConfig }) => {
    const backupId = cmBackup.id;
    const fileStream = fse.createReadStream(backupConfig.path(deviceId, backupId));
    if (cmBackup.type === BackupTypeEnum.Cfg) {
      return pump(fileStream, zlib.createGunzip());
    }

    return fileStream;
  }
);

/**
 * @param {string} deviceId
 * @param {CorrespondenceDeviceBackup} cmBackup
 * @return {Reader.<Observable>}
 */
const loadFile = (deviceId, cmBackup) => reader(
  ({ backupConfig }) => Observable.create((subscriber) => {
    const fileStream = readFile(deviceId, cmBackup).run({ backupConfig });

    pump(fileStream, concatStream(buffer => subscriber.next(buffer)), (err) => {
      if (isError(err)) {
        subscriber.error(err);
      } else {
        subscriber.complete();
      }
    });
  })
);

/**
 * @param {string} deviceId
 * @param {string} backupId
 * @param {'buffer'|'stream'|null} [sourceType] It's null by default
 * @return {Reader.<Observable>}
 */
const findById = (deviceId, backupId, sourceType = null) => reader(
  ({ DB, backupConfig }) => Observable.from(DB.device.findBackupById(deviceId, backupId))
    .mergeMap((dbBackup) => {
      if (isNil(dbBackup)) {
        return Observable.throw(new BackupNotFoundError());
      }

      const cmBackup = parseDbBackup(dbBackup);
      if (sourceType === 'stream') {
        Object.defineProperty(cmBackup, 'source', {
          get: () => readFile(deviceId, cmBackup).run({ backupConfig }),
        });
      } else if (sourceType === 'buffer') {
        return loadFile(deviceId, cmBackup).run({ backupConfig })
          .map(fileBuffer => Object.assign(cmBackup, { source: fileBuffer }));
      }

      return Observable.of(cmBackup);
    })
);

/**
 * @param {string} deviceId
 * @param {string} backupId
 * @return {Reader.<Observable>}
 */
const apply = (deviceId, backupId) => reader(
  ({ deviceStore, DB, backupConfig }) => deviceStore.getOrThrow(deviceId)
    .mergeMap(commDevice => findById(deviceId, backupId, 'buffer').run({ DB, backupConfig })
      .mergeMap(cmDeviceBackup => commDevice.applyBackup(cmDeviceBackup))
    )
);


module.exports = {
  findById,
  listBackups,
  save,
  create,
  cleanUp,
  removeAll,
  remove,
  readFile,
  loadFile,
  apply,
};
