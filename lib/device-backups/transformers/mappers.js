'use strict';

const aguid = require('aguid');
const { applySpec, when } = require('ramda');
const { getOr, flow, now, isNil, get } = require('lodash/fp');

const { BackupTypeEnum } = require('../../enums');

const toDbBackup = applySpec({
  id: flow(get('id'), when(isNil, aguid)),
  type: getOr(BackupTypeEnum.Tar, 'type'),
  extension: getOr('backup', 'extension'),
  timestamp: flow(get('timestamp'), when(isNil, now)),
  crc: getOr(0, 'crc'),
});

module.exports = {
  toDbBackup,
};
