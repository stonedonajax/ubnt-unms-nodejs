'use strict';

const crc32 = require('crc-32');
const aguid = require('aguid');
const zlib = require('zlib');
const { applySpec, ifElse, nAry, unary } = require('ramda');
const { stubNull } = require('ramda-adjunct');
const {
  toString, constant, sortBy, flow, split, reject, startsWith, join, identity, getOr, map, now, get, stubArray, isNil,
} = require('lodash/fp');

const { BackupTypeEnum } = require('../../enums');

/**
 * @typedef {Object} CorrespondenceDeviceBackup
 * @property {string} id
 * @property {BackupTypeEnum|string} type
 * @property {string} extension
 * @property {number} timestamp
 * @property {Buffer|ReadStream|null} source
 * @property {number} crc
 */

/**
 * @function parseCfgBackupCrc
 * @param {Buffer} buffer
 * @return {number}
 */
const parseCfgBackupCrc = flow(
  toString,
  split('\n'),
  reject(startsWith('#')), // ignore comments in the backup
  sortBy(identity),
  join('\n'),
  crc32.str
);

/**
 * @function parseTarBackupCrc
 * @param {Buffer} buffer
 * @return {number}
 */
const parseTarBackupCrc = (buffer) => {
  if (buffer.byteLength < 8) {
    return 0;
  }

  // in gzipped archive crc32 checksum uint32, placed right before the last 4 bytes
  return buffer.readUIntBE(buffer.byteLength - 8, 4);
};

const newId = nAry(0, aguid);

/**
 * @function parseTarBackup
 * @param {Buffer} buffer
 * @return {CorrespondenceDeviceBackup}
 */
const parseTarBackup = applySpec({
  id: newId,
  type: constant(BackupTypeEnum.Tar),
  extension: constant('tar.gz'),
  timestamp: now,
  source: identity,
  crc: parseTarBackupCrc,
});

/**
 * @function parseCfgBackup
 * @param {Buffer} buffer
 * @return {CorrespondenceDeviceBackup}
 */
const parseCfgBackup = applySpec({
  id: newId,
  type: constant(BackupTypeEnum.Cfg),
  extension: constant('cfg'),
  timestamp: now,
  source: unary(zlib.gzipSync),
  crc: parseCfgBackupCrc,
});

/**
 * @function parseDbBackup
 * @param {DbBackup} dbBackup
 * @return {CorrespondenceDeviceBackup}
 */
const parseDbBackup = applySpec({
  id: get('id'),
  type: getOr(BackupTypeEnum.Tar, 'type'),
  extension: getOr('backup', 'extension'),
  timestamp: getOr(0, 'timestamp'),
  source: stubNull,
  crc: getOr(0, 'crc'),
});

/**
 * @function parseDbBackupList
 * @param {DbBackup[]} dbBackupList
 * @return {CorrespondenceDeviceBackup[]}
 */
const parseDbBackupList = ifElse(isNil, stubArray, map(parseDbBackup));

module.exports = {
  parseCfgBackup,
  parseTarBackup,
  parseDbBackup,
  parseDbBackupList,
};
