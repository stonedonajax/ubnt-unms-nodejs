'use strict';

module.exports = {
  Register: 'airfiber-register',
  Close: 'airfiber-close',
  Update: 'airfiber-update',
  Statistics: 'airfiber-statistics',
  ConfigChange: 'airfiber-config-change',
};
