'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader, Either } = require('monet');
const { weave } = require('ramda-adjunct');

const { fromDb: fromDbDevice } = require('../../transformers/device');
const { mergeDbWithHw } = require('../../transformers/device/airfiber/mergers');
const { merge: mergeM } = require('../../transformers');
const { toDbAirFiber } = require('./utils');

const create = cmAirFiber => reader(
  ({ messageHub, DB }) => {
    const { messages } = messageHub;
    return Observable.fromEither(toDbAirFiber(cmAirFiber))
      .mergeMap(dbAirFiber => DB.airFiber.insert(dbAirFiber))
      .do(() => messageHub.publish(messages.deviceSaved(cmAirFiber, true)))
      .mapTo(cmAirFiber);
  }
);

const update = cmAirFiber => reader(
  ({ DB, messageHub }) => {
    const { messages } = messageHub;
    return Observable.fromEither(toDbAirFiber(cmAirFiber))
      .mergeMap(dbAirFiber => DB.airFiber.update(dbAirFiber))
      .do(() => messageHub.publish(messages.deviceSaved(cmAirFiber, false)))
      .mapTo(cmAirFiber);
  }
);

const createOrUpdate = cmAirFiber => reader(
  ({ DB, messageHub }) => {
    const deviceId = cmAirFiber.identification.id;

    return Observable.from(DB.airFiber.findById(deviceId))
      .mergeMap((dbAirFiber) => {
        const isNew = dbAirFiber === null;
        if (isNew) {
          return create(cmAirFiber).run({ DB, messageHub });
        }

        return Observable.fromEither(fromDbDevice({}, dbAirFiber))
          .mergeEither(mergeM(mergeDbWithHw, Either.of(cmAirFiber)))
          .mergeMap(weave(update, { DB, messageHub }));
      });
  }
);

const registerHandler = ({ deviceId, payload: device }) => reader(
  ({ DB, messageHub }) => {
    const { messages } = messageHub;

    return createOrUpdate(device).run({ DB, messageHub })
      .do(cmAirFiber => messageHub.publish(messages.deviceConnected(cmAirFiber)))
      .do(() => messageHub.publish(messages.airFiberConfigChangeEvent(deviceId)));
  }
);

module.exports = registerHandler;
