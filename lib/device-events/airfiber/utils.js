'use strict';

const { Either } = require('monet');

const { toDb: toDbDevice } = require('../../transformers/device');
const { mergeInterfaces } = require('../../transformers/device/mergers');
const { toDbInterfaceList } = require('../../transformers/interfaces');
const { merge: mergeM } = require('../../transformers');

/**
 * @function toDbAirFiber
 * @param {CorrespondenceDevice} cmAirFiber
 * @return {Either.<DbDevice>}
 */
const toDbAirFiber = cmAirFiber => Either.of(cmAirFiber)
  .chain(mergeM(mergeInterfaces, toDbInterfaceList(cmAirFiber.interfaces)))
  .chain(toDbDevice);

module.exports = {
  toDbAirFiber,
};
