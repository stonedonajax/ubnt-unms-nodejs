'use strict';

const { Observable, Subject } = require('rxjs/Rx');
const { isNull, flow, reduce, getOr, isString, isArray, get } = require('lodash/fp');
const { when } = require('ramda');
const { isNotNull } = require('ramda-adjunct');

const toCidr = getOr('0.0.0.0/0', 'cidr');

const extractMacsAndIps = flow(
  getOr([], 'interfaces'),
  reduce((accumulator = { ips: [], macs: [] }, iface) => {
    if (isString(iface.identification.mac)) {
      accumulator.macs.push(iface.identification.mac);
    }

    if (isArray(iface.addresses) && iface.addresses.length > 0) {
      accumulator.ips.push(...iface.addresses.map(toCidr));
    }

    return accumulator;
  }, undefined)
);

class DeviceAuthorizationQueue {
  constructor(automaticAuthorizeDeviceService, dal, config, rxScheduler) {
    this.rxScheduler = rxScheduler;
    this.repository = dal.deviceSiteRepository;
    this.automaticAuthorizeDeviceService = automaticAuthorizeDeviceService;
    this.queue = null;
    this.concurencyLimit = config.authorizationQueue.concurencyLimit;
    this.destroyQueueDelay = config.authorizationQueue.destroyQueueDelay;
    this.startProcessDelay = config.authorizationQueue.startProcessDelay;
  }

  add(event) {
    this.ensureQueue();
    this.queue.next(event);
  }

  ensureQueue() {
    if (isNull(this.queue)) {
      this.queue = new Subject();
      const process = this.process.bind(this);
      const delayed = this.queue
        .delay(this.startProcessDelay, this.rxScheduler)
        .share();

      delayed
        .mergeMap(process, null, this.concurencyLimit)
        .takeUntil(delayed.switchMapTo(Observable.timer(this.destroyQueueDelay, this.rxScheduler)))
        .subscribe({
          complete: () => { this.queue = null },
        });
    }
  }

  tryAuthorize(cmDevice) {
    const { macs, ips } = extractMacsAndIps(cmDevice);

    return this.repository.findOneAuthorizationSiteId(ips, macs)
      .then(when(isNotNull, get('siteId')))
      .then((siteId) => {
        if (siteId === null) {
          return null;
        }

        this.add({
          siteId,
          deviceId: cmDevice.identification.id,
        });
        return null;
      });
  }

  addPriority(event) {
    this.process(event);
  }

  process({ deviceId, siteId }) {
    return Observable.from(this.automaticAuthorizeDeviceService(deviceId, siteId));
  }
}

module.exports = DeviceAuthorizationQueue;
