'use strict';

const { Reader: reader } = require('monet');

const configChangeHandler = ({ deviceId }) => reader(
  ({ deviceBackups, settings }) => {
    const { autoBackups } = settings.getSettings();

    if (autoBackups) {
      deviceBackups.queue.scheduleBackup(deviceId);
    }
  }
);

module.exports = configChangeHandler;
