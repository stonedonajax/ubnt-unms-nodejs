'use strict';

module.exports = {
  Register: 'epower-register',
  Close: 'epower-close',
  Update: 'epower-update',
  Statistics: 'epower-statistics',
  ConfigChange: 'epower-config-change',
};
