'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader, Either } = require('monet');
const { weave } = require('ramda-adjunct');

const { fromDb: fromDbDevice } = require('../../transformers/device');
const { mergeDbWithHw } = require('../../transformers/device/epower/mergers');
const { merge: mergeM } = require('../../transformers');
const { toDbEpower } = require('./utils');

const create = cmEpower => reader(
  ({ messageHub, DB }) => {
    const { messages } = messageHub;
    return Observable.fromEither(toDbEpower(cmEpower))
      .mergeMap(dbEpower => DB.epower.insert(dbEpower))
      .do(() => messageHub.publish(messages.deviceSaved(cmEpower, true)))
      .mapTo(cmEpower);
  }
);

const update = cmEpower => reader(
  ({ DB, messageHub }) => {
    const { messages } = messageHub;
    return Observable.fromEither(toDbEpower(cmEpower))
      .mergeMap(dbEpower => DB.epower.update(dbEpower))
      .do(() => messageHub.publish(messages.deviceSaved(cmEpower, false)))
      .mapTo(cmEpower);
  }
);

const createOrUpdate = cmEpower => reader(
  ({ DB, messageHub }) => {
    const deviceId = cmEpower.identification.id;

    return Observable.from(DB.epower.findById(deviceId))
      .mergeMap((dbEpower) => {
        const isNew = dbEpower === null;
        if (isNew) {
          return create(cmEpower).run({ DB, messageHub });
        }

        return Observable.fromEither(fromDbDevice({}, dbEpower))
          .mergeEither(mergeM(mergeDbWithHw, Either.of(cmEpower)))
          .mergeMap(weave(update, { DB, messageHub }));
      });
  }
);

const registerHandler = ({ deviceId, payload: device }) => reader(
  ({ DB, messageHub }) => {
    const { messages } = messageHub;

    return createOrUpdate(device).run({ DB, messageHub })
      .do(cmEpower => messageHub.publish(messages.deviceConnected(cmEpower)))
      .do(() => messageHub.publish(messages.epowerConfigChangeEvent(deviceId)));
  }
);

module.exports = registerHandler;
