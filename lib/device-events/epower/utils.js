'use strict';

const { Either } = require('monet');

const { toDb: toDbDevice } = require('../../transformers/device');
const { mergeInterfaces } = require('../../transformers/device/mergers');
const { toDbInterfaceList } = require('../../transformers/interfaces');
const { merge: mergeM } = require('../../transformers');

/**
 * @function toDbAirFiber
 * @param {CorrespondenceDevice} cmEpower
 * @return {Either.<DbDevice>}
 */
const toDbEpower = cmEpower => Either.of(cmEpower)
  .chain(mergeM(mergeInterfaces, toDbInterfaceList(cmEpower.interfaces)))
  .chain(toDbDevice);

module.exports = {
  toDbEpower,
};
