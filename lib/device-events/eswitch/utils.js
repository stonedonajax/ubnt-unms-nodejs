'use strict';

const { Either } = require('monet');

const { toDb: toDbDevice } = require('../../transformers/device');
const { mergeInterfaces } = require('../../transformers/device/mergers');
const { toDbInterfaceList } = require('../../transformers/interfaces');
const { merge: mergeM } = require('../../transformers');

/**
 * @function toDbEswitch
 * @param {CorrespondenceDevice} cmEswitch
 * @return {Either.<DbDevice>}
 */
const toDbEswitch = cmEswitch => Either.of(cmEswitch)
  .chain(mergeM(mergeInterfaces, toDbInterfaceList(cmEswitch.interfaces)))
  .chain(toDbDevice);

module.exports = {
  toDbEswitch,
};
