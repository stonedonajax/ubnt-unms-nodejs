'use strict';

const { Reader: reader } = require('monet');
const { cata, isNotNull } = require('ramda-adjunct');
const { pathSatisfies } = require('ramda');

const { entityExistsCheck, rejectP, resolveP, tapP } = require('../../util');
const { EntityEnum } = require('../../enums');
const { fromDb } = require('../../transformers/device');


// isDeviceAuthorized :: Object -> Boolean
const isDeviceAuthorized = pathSatisfies(isNotNull, ['identification', 'siteId']);

// deviceSaved :: Object -> Boolean -> Message -> Object
module.exports = ({ deviceId }, message) => reader(
  /**
   * @function authorizeDevice
   * @param {MessageHub} messageHub
   * @param {DB} DB
   * @param {DeviceEvents} deviceEvents
   * @return {Promise}
   */
  ({ messageHub, DB, deviceEvents }) => DB.device.findById(deviceId)
      .then(tapP(entityExistsCheck(EntityEnum.Device)))
      .then(fromDb({}))
      .then(cata(rejectP, resolveP))
      .then((cmDevice) => {
        if (isDeviceAuthorized(cmDevice)) { return null }

        return deviceEvents.deviceAuthorizationQueue.tryAuthorize(cmDevice);
      })
      .catch(messageHub.logError(message))
);
