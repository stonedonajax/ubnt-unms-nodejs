'use strict';

const { Reader: reader } = require('monet');
const { pathEq, when, always, assocPath } = require('ramda');
const { isNull, getOr } = require('lodash/fp');
const { Observable } = require('rxjs/Rx');

const { entityExistsCheck, resolveP } = require('../../util');
const { EntityEnum } = require('../../enums');
const { fromDb: fromDbSite, toDb: toDbSite } = require('../../transformers/site');


// assignGpsToSite :: Object -> Promise()
const assignGpsToSite = cmDevice => reader(
  ({ dal, site }) => {
    const siteId = getOr(null, ['identification', 'site', 'id'], cmDevice);
    const location = getOr(null, ['overview', 'location'], cmDevice);

    if (isNull(location) || isNull(siteId)) { return resolveP(null) }

    return Observable.fromPromise(dal.siteRepository.findOneById(siteId))
      .do(entityExistsCheck(EntityEnum.Site))
      .mergeEither(fromDbSite({}))
      .mergeMap(when(
        pathEq(['description', 'location'], null),
        cmSite => Observable.of(assocPath(['description', 'location'], location, cmSite))
          .mergeEither(toDbSite)
          .mergeMap(site.updateSite)
      ))
      .catch(always(Observable.empty()))
      .toPromise();
  }
);


// deviceAuthorizedEvent :: Object -> Message -> Object
module.exports = ({ deviceId, device }, message) => reader(
  /**
   * @function deviceAuthorizedEvent
   * @param {MessageHub} messageHub
   * @param {Dal} dal
   * @param {Site} site
   * @return {Promise}
   */
  ({ messageHub, dal, site }) => assignGpsToSite(device).run({ dal, site })
    .catch(messageHub.logError(message))
);
