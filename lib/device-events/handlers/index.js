'use strict';

const { weave } = require('ramda-adjunct');

const consumeSocketEvent = require('./consumeSocketEvent');
const automaticalyAuthorize = require('./automaticalyAuthorize');
const deviceAuthorizedEvent = require('./deviceAuthorizedEvent');

exports.register = (server, messageHub, messages) => {
  const { deviceEvents, DB, dal, site } = server.plugins;

  const { deviceConnected, deviceAuthorized } = messages;
  messageHub.subscribe('socket.#', weave(consumeSocketEvent, { deviceEvents }));
  messageHub.subscribe(deviceConnected, weave(automaticalyAuthorize, { messageHub, DB, deviceEvents }));
  messageHub.subscribe(deviceAuthorized, weave(deviceAuthorizedEvent, { messageHub, dal, site }));
};
