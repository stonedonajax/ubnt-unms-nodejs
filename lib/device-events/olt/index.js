'use strict';

const { weave } = require('ramda-adjunct');

const events = require('./events');
const registerHandler = require('./register');
const closeHandler = require('./close');
const systemHandler = require('./system');
const interfacesHandler = require('./interfaces');
const ponHandler = require('./pon');
const onuListHandler = require('./onu-list');
const updateHandler = require('../common/update');
const configChangeHandler = require('../common/config-change');

const register = (server, queue) => {
  const { DB, deviceBackups, eventLog, messageHub, site, settings, dal } = server.plugins;

  queue.registerHandlers({
    [events.Register]: weave(registerHandler, { DB, messageHub }),
    [events.Close]: weave(closeHandler, { DB, messageHub, dal }),
    [events.System]: weave(systemHandler, { dal }),
    [events.Pon]: ponHandler,
    [events.OnuList]: weave(onuListHandler, { messageHub, DB, site, dal }),
    [events.Interfaces]: weave(interfacesHandler, { eventLog }),
    [events.Update]: weave(updateHandler, { DB }),
    [events.ConfigChange]: weave(configChangeHandler, { deviceBackups, settings }),
  });
};

module.exports = register;
