'use strict';

const { bindAll } = require('lodash/fp');
const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../util/hapi');
const handlers = require('./handlers');
const DeviceEventQueue = require('./queue');
const DeviceAuthorizationQueue = require('./authorization-queue');

const { automaticAuthorizeDevice } = require('../api/v2.1/devices/service');
const config = require('../../config');
const erouterEventHandlers = require('./erouter');
const eswitchEventHandlers = require('./eswitch');
const epowerEventHandlers = require('./epower');
const oltEventHandlers = require('./olt');
const airmaxEventHandlers = require('./airmax');
const aircubeEventHandlers = require('./aircube');
const airfiberEventHandlers = require('./airfiber');
const toughswitchEventHandlers = require('./toughswitch');

/*
 * Hapi plugin definition
 */
function register(server) {
  const { logging, messageHub, site, user, dal, eventLog, DB } = server.plugins;

  const deviceQueue = new DeviceEventQueue(logging);
  const automaticAuthorizeDeviceService = weave(automaticAuthorizeDevice, { site, user, dal, eventLog, DB });
  const deviceAuthorizationQueue = new DeviceAuthorizationQueue(automaticAuthorizeDeviceService, dal, config);

  erouterEventHandlers(server, deviceQueue);
  eswitchEventHandlers(server, deviceQueue);
  epowerEventHandlers(server, deviceQueue);
  oltEventHandlers(server, deviceQueue);
  airmaxEventHandlers(server, deviceQueue);
  aircubeEventHandlers(server, deviceQueue);
  airfiberEventHandlers(server, deviceQueue);
  toughswitchEventHandlers(server, deviceQueue);

  server.expose(bindAll(['add'], deviceQueue));
  server.expose('deviceAuthorizationQueue', deviceAuthorizationQueue);

  messageHub.registerHandlers(handlers);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'deviceEvents',
  dependencies: ['DB', 'logging', 'messageHub', 'eventLog', 'site', 'user', 'dal'],
};
