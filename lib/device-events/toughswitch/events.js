'use strict';

module.exports = {
  Register: 'toughswitch-register',
  Close: 'toughswitch-close',
  Update: 'toughswitch-update',
  Statistics: 'toughswitch-statistics',
  ConfigChange: 'toughswitch-config-change',
};
