'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader, Either } = require('monet');
const { weave } = require('ramda-adjunct');

const { fromDb: fromDbDevice } = require('../../transformers/device');
const { mergeDbWithHw } = require('../../transformers/device/toughswitch/mergers');
const { merge: mergeM } = require('../../transformers');
const { toDbToughSwitch } = require('./utils');

const create = cmToughSwitch => reader(
  ({ messageHub, DB }) => {
    const { messages } = messageHub;
    return Observable.fromEither(toDbToughSwitch(cmToughSwitch))
      .mergeMap(dbToughSwitch => DB.toughSwitch.insert(dbToughSwitch))
      .do(() => messageHub.publish(messages.deviceSaved(cmToughSwitch, true)))
      .mapTo(cmToughSwitch);
  }
);

const update = cmToughSwitch => reader(
  ({ DB, messageHub }) => {
    const { messages } = messageHub;
    return Observable.fromEither(toDbToughSwitch(cmToughSwitch))
      .mergeMap(dbToughSwitch => DB.toughSwitch.update(dbToughSwitch))
      .do(() => messageHub.publish(messages.deviceSaved(cmToughSwitch, false)))
      .mapTo(cmToughSwitch);
  }
);

const createOrUpdate = cmToughSwitch => reader(
  ({ DB, messageHub }) => {
    const deviceId = cmToughSwitch.identification.id;

    return Observable.from(DB.toughSwitch.findById(deviceId))
      .mergeMap((dbToughSwitch) => {
        const isNew = dbToughSwitch === null;
        if (isNew) {
          return create(cmToughSwitch).run({ DB, messageHub });
        }

        return Observable.fromEither(fromDbDevice({}, dbToughSwitch))
          .mergeEither(mergeM(mergeDbWithHw, Either.of(cmToughSwitch)))
          .mergeMap(weave(update, { DB, messageHub }));
      });
  }
);

const registerHandler = ({ deviceId, payload: device }) => reader(
  ({ DB, messageHub }) => {
    const { messages } = messageHub;

    return createOrUpdate(device).run({ DB, messageHub })
      .do(cmToughSwitch => messageHub.publish(messages.deviceConnected(cmToughSwitch)))
      .do(() => messageHub.publish(messages.toughSwitchConfigChangeEvent(deviceId)));
  }
);

module.exports = registerHandler;
