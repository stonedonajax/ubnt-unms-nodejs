'use strict';

const { Either } = require('monet');

const { toDb: toDbDevice } = require('../../transformers/device');
const { mergeInterfaces } = require('../../transformers/device/mergers');
const { toDbInterfaceList } = require('../../transformers/interfaces');
const { merge: mergeM } = require('../../transformers');

/**
 * @function toDbToughSwitch
 * @param {CorrespondenceDevice} cmToughSwitch
 * @return {Either.<DbDevice>}
 */
const toDbToughSwitch = cmToughSwitch => Either.of(cmToughSwitch)
  .chain(mergeM(mergeInterfaces, toDbInterfaceList(cmToughSwitch.interfaces)))
  .chain(toDbDevice);

module.exports = {
  toDbToughSwitch,
};
