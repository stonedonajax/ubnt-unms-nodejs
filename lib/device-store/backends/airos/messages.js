'use strict';

const { partial } = require('lodash/fp');

const { rpcRequest, commandRequest, GET_NETWORK_INFO } = require('../ubridge/messages');
const { MessageNameEnum } = require('./enums');

const deviceStatusRequest = partial(rpcRequest, [{}, MessageNameEnum.Status]);

const stationListRequest = partial(rpcRequest, [{}, MessageNameEnum.StationList]);

const GPS_INFO_FILES = [
  '/proc/ubnthal/gps_info',
  '/proc/sys/dev/ubnt_poll/gps_info',
  '/proc/sys/dev/uph/gps_info',
];

/* eslint-disable max-len */
// should work for AirMax M, AC, AirFiber, Epower
const DEVICE_CONFIG_CMD = [
  `echo "${MessageNameEnum.Config}"`,
  'echo -e -n "[firmware]\\nversion="',
  'cat /etc/version 2>/dev/null',
  'echo "[board]"',
  'cat /proc/ubnthal/board.info 2>/dev/null || cat /etc/board.info',
  'echo "[configuration]"',
  'cat /tmp/system.cfg 2>/dev/null',
  'echo "[gps]"',
  `cat ${GPS_INFO_FILES.join(' ')} 2>/dev/null | awk 'BEGIN { FS="," } ; { print "lat="$(NF-2)"\\nlng="$(NF-1)  }'`,
  GET_NETWORK_INFO,
].join('; ');
/* eslint-enable max-len */

const deviceConfigRequest = partial(commandRequest, [DEVICE_CONFIG_CMD]);

const CONFIG_CHECK_CMD = `echo "${MessageNameEnum.ConfigCheck}"; md5sum /tmp/running.cfg | cut -d" " -f1`;

const configCheckRequest = partial(commandRequest, [CONFIG_CHECK_CMD]);

// eslint-disable-next-line max-len
const STATUS_CMD = `echo "${MessageNameEnum.Status}"; /usr/www/status.cgi 2>/dev/null | sed '1,/^[[:space:]]*$/d' | tr -d "\\n\\r\\t"`;

/**
 * Used only for AirFiber, EdgePower - temporarily
 * @deprecated
 */
const deviceStatusRequestCommand = partial(commandRequest, [STATUS_CMD]);

// only M

// eslint-disable-next-line max-len
const INTERFACE_STATS_CMD = `echo "${MessageNameEnum.InterfaceStats}"; /usr/www/ifstats.cgi 2>/dev/null | sed '1,/^[[:space:]]*$/d' | tr -d "\\n\\r\\t"`;

const interfacesStatsRequest = partial(commandRequest, [INTERFACE_STATS_CMD]);

// only AC
const AIR_VIEW_CMD = `echo "${MessageNameEnum.AirView}"; cat /tmp/airview/data 2>&1`;

const airViewRequest = partial(commandRequest, [AIR_VIEW_CMD]);

module.exports = {
  deviceStatusRequest,
  deviceStatusRequestCommand,
  stationListRequest,
  interfacesStatsRequest,
  deviceConfigRequest,
  configCheckRequest,
  airViewRequest,
};
