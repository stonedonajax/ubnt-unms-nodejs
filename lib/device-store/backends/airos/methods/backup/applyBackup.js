'use strict';

const { Observable } = require('rxjs/Rx');
const { constant } = require('lodash/fp');
const { isNotString } = require('ramda-adjunct');

const { applyBackupRequest } = require('../../../ubridge/messages');
const { toHwBackup } = require('../../transformers/backup/mappers');
const { RpcTimeoutError } = require('../../../../../ws/errors');

const ignoreTimeoutError = (error) => {
  if (error instanceof RpcTimeoutError) {
    return Observable.of(true); // device has disconnected
  }

  return Observable.throw(error);
};

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceDeviceBackup} cmDeviceBackup
 * @return {Observable.<string>}
 */
function applyBackup(cmDeviceBackup) {
  const hwBackup = toHwBackup(cmDeviceBackup);
  if (isNotString(hwBackup) || hwBackup.length === 0) {
    return Observable.throw(new TypeError('Invalid backup type'));
  }
  return this.connection.rpc(applyBackupRequest(hwBackup))
    .catch(ignoreTimeoutError);
}

module.exports = constant(applyBackup);
