'use strict';

const { constant } = require('lodash/fp');

const { createBackupRequest } = require('../../../ubridge/messages');
const { parseHwBackup } = require('../../transformers/backup/parsers');


const BACKUP_TIMEOUT = 180000; // 2 minutes

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @return {Observable.<string>}
 */
function createBackup() {
  return this.connection.rpc(createBackupRequest(), BACKUP_TIMEOUT)
    .map(parseHwBackup);
}

module.exports = constant(createBackup);

