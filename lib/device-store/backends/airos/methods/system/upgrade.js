'use strict';

const { Observable } = require('rxjs/Rx');
const { pathEq } = require('ramda');
const semver = require('semver');
const { includes } = require('lodash/fp');

const { subscribeCommandRequest, unsubscribeCommandRequest, commandRequest } = require('../../../ubridge/messages');
const { MessageNameEnum } = require('../../enums');
const { toMs } = require('../../../../../util');

const UPGRADE_STATS_INTERVAL = toMs('seconds', 3);
const UPGRADE_DELAY = toMs('seconds', 10);

const isUpgradeStatsEvent = pathEq(['name'], MessageNameEnum.SystemUpgradeStats);

const upgradeScript = firmwareUrl => `#!/bin/sh
FIRMWARE_FILE="/tmp/fwupdate.bin"
LOCK_FILE="/tmp/fwup-started";
LOCKED=""

cleanup () {
  rm -- "$0"
  if [ -n $LOCKED ]; then
    rm -f "$LOCK_FILE"
  fi
}

die () {
  echo "\${1}"
  rm "$FIRMWARE_FILE"
  exit 0
}

trap "cleanup" EXIT

if [ -f "$LOCK_FILE" ]; then
  die "*ALREADY IN PROGRESS*"
fi

LOCKED=1
touch "$LOCK_FILE"

HOST=$(grep "unms.uri=" /tmp/running.cfg | cut -d"/" -f3- | cut -d"+" -f1)
LUA_DOWNLOAD='
  local io = require("io")
  local ltn12 = require("ltn12")
  local https = require("ssl.https")

  local req_params = {
    method = "GET",
    url = url,
    sink = ltn12.sink.file(io.stdout),
    redirect = false,
    headers = {
        ["Accept"] = "*/*",
        ["User-Agent"] = "ubnt-airos",
    },
    cafile = "/usr/etc/ssl/cert.pem",
    mode = "client",
    options = {"all", "no_sslv2", "no_sslv3"},
    protocol = "tlsv1",
    verify = "none"
  }

  https.request(req_params)
'
rm -f "$FIRMWARE_FILE"

TRIGGER_URL=$(which trigger_url 2>/dev/null)

if [ -n "$TRIGGER_URL" ]; then
  $TRIGGER_URL "https://\${HOST}${firmwareUrl}" > "$FIRMWARE_FILE" || die "*DOWNLOAD FAILED*"
else
  lua -e "local url=\\"https://\${HOST}${firmwareUrl}\\"; $LUA_DOWNLOAD" > "$FIRMWARE_FILE" || die "*DOWNLOAD FAILED*"
fi

/sbin/fwupdate -m || die "*UPGRADE FAILED*"
`;

const systemUpgradeStatsCmd = stdoutFilename => `cat ${stdoutFilename} | grep -oE '\\*[^\\*]*\\*'`;

const hasDownloadFailed = includes('*DOWNLOAD FAILED*');
const isUpgradeAlreadyInProgress = includes('*ALREADY IN PROGRESS*');
const hasUpgradeScriptFailed = includes('*UPGRADE FAILED*');

const trackUpgradeProgress = (commDevice, upgradeStats$) => upgradeStats$
  .mergeMap((upgradeStats) => {
    const output = upgradeStats.data;
    if (hasDownloadFailed(output)) {
      return Observable.throw(new Error('Downloading firmware from device has failed'));
    } else if (isUpgradeAlreadyInProgress(output)) {
      return Observable.throw(new Error('Device upgrade already in progress'));
    } else if (hasUpgradeScriptFailed(output)) {
      return Observable.throw(new Error('Device upgrade has failed'));
    }

    return Observable.of(true);
  })
  .takeUntil(commDevice.connection.close$);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {string} firmwareUrl
 * @return {Observable.<SystemUpgradeStats>}
 */
function systemUpgradeNotifications(firmwareUrl) {
  return this.execScript(upgradeScript(firmwareUrl))
    .mergeMap(stdoutFilename => this.connection.rpc(subscribeCommandRequest({
      name: MessageNameEnum.SystemUpgradeStats,
      cmd: systemUpgradeStatsCmd(stdoutFilename),
      cmdInterval: UPGRADE_STATS_INTERVAL,
    })))
    .mergeMap(() => {
      const upgradeStats$ = this.connection.messages$.filter(isUpgradeStatsEvent);
      return trackUpgradeProgress(this, upgradeStats$);
    })
    .materialize()
    .mergeMap((notification) => {
      const kind = notification.kind;
      if ((kind === 'E' || kind === 'C') && !this.connection.isClosed) {
        // unsubscribe
        return this.connection.rpc(unsubscribeCommandRequest(MessageNameEnum.SystemUpgradeStats))
          .catch(error => Observable.of(error)) // ignore errors
          .mergeMapTo(notification.toObservable());
      }

      return notification.toObservable();
    })
    .concat(Observable.timer(UPGRADE_DELAY));
}

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {string} firmwareUrl
 * @return {Observable.<SystemUpgradeStats>}
 */
function systemUpgradePolling(firmwareUrl) {
  return this.execScript(upgradeScript(firmwareUrl))
    .mergeMap((stdoutFilename) => {
      const command = `echo "${MessageNameEnum.SystemUpgradeStats}"; ${systemUpgradeStatsCmd(stdoutFilename)}`;
      const upgradeStats$ = Observable.defer(() => this.connection.cmd(commandRequest(command)))
        .repeatWhen(notifications => notifications.delay(UPGRADE_STATS_INTERVAL));

      return trackUpgradeProgress(this, upgradeStats$);
    })
    .concat(Observable.timer(UPGRADE_DELAY));
}

/**
 * @param {CorrespondenceSysInfo} sysInfo
 * @return {*}
 */
module.exports = (sysInfo) => {
  if (semver.gte(sysInfo.udapi, '0.2.0')) {
    return systemUpgradeNotifications;
  }

  return systemUpgradePolling;
};
