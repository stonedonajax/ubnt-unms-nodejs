'use strict';

const { flow, get } = require('lodash/fp');

const { toHexString } = require('../../../ubridge/transformers/backup/mappers');

const toHwBackup = flow(
  get('source'),
  toHexString
);

module.exports = {
  toHwBackup,
};
