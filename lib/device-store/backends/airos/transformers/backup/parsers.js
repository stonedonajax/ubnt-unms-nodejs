'use strict';

const { flow, get } = require('lodash/fp');

const { parseCfgBackup } = require('../../../../../device-backups/transformers/parsers');
const { fromHexString } = require('../../../ubridge/transformers/backup/parsers');

/**
 * @function parseHwBackup
 * @param {CorrespondenceIncomingMessage} message
 * @return {CorrespondenceDeviceBackup}
 */
const parseHwBackup = flow(
  get('data'),
  fromHexString,
  parseCfgBackup
);

module.exports = {
  parseHwBackup,
};
