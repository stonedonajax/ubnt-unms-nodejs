'use strict';

const { pathOr, any, map, filter, equals, length, divide, __, when, applySpec } = require('ramda');
const {
  getOr, flow, clamp, floor, get, toInteger, parseInt, remove, every, eq, head, defaultTo, isNull, zip, mapValues,
} = require('lodash/fp');
const { isNotPlainObj, isNotNull, isNotInteger } = require('ramda-adjunct');
const { Html5Entities } = require('html-entities');

const { isDeviceModelSupported } = require('../../../../../feature-detection/common');
const { parseCommFirmwareVersion } = require('../../../../../transformers/semver/parsers');
const { parseDeviceDescription } = require('../../../../../transformers/device/model/parsers');
const { roundTo } = require('../../../../../util');
const { isValidLocation } = require('../../../../../util/gps');

// parseHwDeviceName :: HwStatus -> DeviceName
//     HwStatus = Object
//     DeviceName = String
const parseHwDeviceName = flow(
  getOr('ubnt', ['data', 'host', 'hostname']),
  Html5Entities.decode
);

// parseHwFirmwareVersion :: HwStatus -> FirmwareVersion
//     HwStatus = Object
//     FirmwareVersion = String
const parseHwFirmwareVersion = flow(
  getOr(null, ['data', 'host', 'fwversion']),
  parseCommFirmwareVersion
);

// parseHwAntenna :: HwBoardInfo -> String|Null
const parseHwAntenna = (hwDevice) => {
  const radio = 1;
  const antennaId = get(['configuration', `radio.${radio}.antenna.id`], hwDevice);

  return getOr(null, ['board', `radio.${radio}.antenna.${antennaId}.name`], hwDevice);
};

// parseHwModel :: HwStatus -> Number
//     HwStatus = Object
const parseHwModel = ({ hwStatus }, hwDeviceConfig) => {
  const boardModel = getOr(null, ['board.model'], hwDeviceConfig.board);

  if (boardModel !== null && isDeviceModelSupported(boardModel)) {
    return boardModel;
  }

  // some legacy firmwares do not return devmodel in status
  const devModel = getOr(null, ['data', 'host', 'devmodel'], hwStatus);
  if (devModel !== null) {
    const model = parseDeviceDescription(devModel);
    if (isDeviceModelSupported(model)) {
      return model;
    }
  }

  const boardSubtype = getOr(null, ['board.subtype'], hwDeviceConfig.board);
  const boardShortName = getOr(null, ['board.shortname'], hwDeviceConfig.board);
  const boardName = getOr('', ['board.name'], hwDeviceConfig.board);

  if (boardSubtype !== null) {
    if (boardShortName !== null) {
      const model = `${boardShortName}-${boardSubtype}`;
      if (isDeviceModelSupported(model)) { return model }
    }

    const name = `${boardName} ${boardSubtype}`;
    const model = parseDeviceDescription(name);
    if (isDeviceModelSupported(model)) { return model }
  }

  if (boardShortName !== null && isDeviceModelSupported(boardShortName)) {
    return boardShortName;
  }

  const deviceName = `${boardName} ${boardSubtype}`;
  return parseDeviceDescription(deviceName);
};

// parseHwCpuUsage :: HwStatus -> Number
//     HwStatus = Object
const parseHwCpuUsage = flow(getOr(0, ['data', 'host', 'cpuload']), floor, clamp(0, 100));

// parseHwMemoryUsage :: HwStatus -> Number
//     HwStatus = Object
const parseHwMemoryUsage = (hwStatus) => {
  const total = pathOr(null, ['data', 'host', 'totalram'], hwStatus);
  const free = pathOr(null, ['data', 'host', 'freeram'], hwStatus);

  if (any(isNull, [total, free])) { return 0 }

  const used = total - free;

  return floor((used / total) * 100);
};

// parseHwUptime :: HwStatus -> Number
//     HwStatus = Object
const parseHwUptime = flow(
  getOr(0, ['data', 'host', 'uptime']),
  when(isNotInteger, parseInt(10))
);

// parseHwSSID :: HwStatus -> String|Null
//     HwStatus = Object
const parseHwSSID = getOr(null, ['data', 'wireless', 'essid']);

// parseHwFrequency :: HwStatus -> Number
//     HwStatus = Object
const parseHwFrequency = flow(
  getOr(0, ['data', 'wireless', 'frequency']),
  when(isNotInteger, parseInt(10))
);

// parseHwNetworkMode :: HwStatus -> String
const parseHwNetworkMode = get(['data', 'host', 'netrole']);

// parseHwPingStatsErrors :: HwPingStats -> Number
//     HwPingStats = Object
const parseHwPingStatsErrors = flow(getOr(0, ['data', 'failureRate']), toInteger);

// parseHwPingStatsLatency :: HwPingStats -> Number
//     HwPingStats = Object
const parseHwPingStatsLatency = (hwPingStats) => {
  const errors = parseHwPingStatsErrors(hwPingStats);

  if (errors > 0) { return null }

  return flow(getOr(0, ['data', 'latency']), roundTo(3))(hwPingStats);
};

const parseHwLocationFromSystem = flow(
  applySpec({
    latitude: getOr(null, ['configuration', 'system.latitude']),
    longitude: getOr(null, ['configuration', 'system.longitude']),
  }),
  mapValues(when(isNotNull, Number))
);

const parseHwLocationFromGps = flow(
  applySpec({
    latitude: getOr(null, ['gps', 'lat']),
    longitude: getOr(null, ['gps', 'lng']),
  }),
  mapValues(when(isNotNull, Number))
);

const parseHwLocation = (hwDeviceConfig) => {
  const gpsLocation = parseHwLocationFromGps(hwDeviceConfig);
  if (isValidLocation(gpsLocation)) {
    return gpsLocation;
  }

  const systemLocation = parseHwLocationFromSystem(hwDeviceConfig);
  if (isValidLocation(systemLocation)) {
    return systemLocation;
  }

  return null;
};

// parseHwChainMaskToChains :: String -> Number
const parseHwChainMaskToChains = flow(
  parseInt(10),
  m => m.toString(2),
  map(filter(equals(1))),
  length
);

/**
 * @function parseHwLatestAnze
 * @param {Array.<Array.<number>>} anzeList
 * @return {Array.<number>}
 */
const parseHwLatestAnze = flow(
  remove(every(eq(0))),
  head,
  defaultTo([])
);

/**
 * @function parseHwLatestAnze
 * @param {Array.<number>} frequencies
 * @return {Array.<number>}
 */
const parseAirViewFrequencies = map(flow(divide(__, 10000), floor));

/**
 * Code taken from AirOS sources file: js/apps/airview/channel_usage_stats.js
 *
 * @param {Object} hwAirView
 * @return {Array}
 */
const parseHwAirView = (hwAirView) => {
  if (isNotPlainObj(hwAirView)) {
    return null;
  }

  const frequencies = parseAirViewFrequencies(hwAirView.ltFreqGridLabels);
  let latestAnze = parseHwLatestAnze(hwAirView.latestAnze);

  // generate latestAnze
  if (latestAnze.length === 0) {
    const delta = (hwAirView.ltFreqGridLabels[1] - hwAirView.ltFreqGridLabels[0]) / 2;
    let position = 0;
    latestAnze = hwAirView.ltFreqGridLabels.map((value) => {
      let sum = 0;
      if (position >= hwAirView.stFreqGridLabels.length) {
        return sum;
      }

      for (; position < hwAirView.stFreqGridLabels.length; position += 1) {
        if (Math.abs(value - hwAirView.stFreqGridLabels[position]) <= delta) {
          sum += hwAirView.latestPower[position];
        } else {
          return sum;
        }
      }

      return sum;
    });
  }

  return zip(frequencies, latestAnze);
};

module.exports = {
  parseHwDeviceName,
  parseHwFirmwareVersion,
  parseHwModel,
  parseHwCpuUsage,
  parseHwMemoryUsage,
  parseHwUptime,
  parseHwAntenna,
  parseHwSSID,
  parseHwFrequency,
  parseHwNetworkMode,
  parseHwPingStatsLatency,
  parseHwPingStatsErrors,
  parseHwChainMaskToChains,
  parseHwAirView,
  parseHwLocation,
};
