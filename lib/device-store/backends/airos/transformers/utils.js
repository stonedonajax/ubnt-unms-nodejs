'use strict';

const { match } = require('ramda');
const { startsWith, flow, head, parseInt } = require('lodash/fp');


const { InterfaceIdentificationTypeEnum } = require('../../../../enums');


// isEthernetInterfaceType :: InterfaceName -> Boolean
//    InterfaceName = String
const isEthernetInterfaceType = startsWith('eth');

// isWifiInterfaceType :: InterfaceName -> Boolean
//    InterfaceName = String
const isWifiInterfaceType = startsWith('wifi');

// isLogicalInterfaceType :: InterfaceName -> Boolean
//    InterfaceName = String
const isLogicalWifiInterfaceType = startsWith('ath');

// isBridgeInterfaceType :: InterfaceName -> Boolean
//    InterfaceName = String
const isBridgeInterfaceType = startsWith('br');

// TODO(vladimir.gorej@gmail.com): we return null for ath[0-9]+ for now
// interfaceNameToType :: String -> InterfaceType
//     InterfaceType = String|Null
const interfaceNameToType = (interfaceName) => {
  if (isEthernetInterfaceType(interfaceName)) {
    return InterfaceIdentificationTypeEnum.Ethernet;
  } else if (isBridgeInterfaceType(interfaceName)) {
    return InterfaceIdentificationTypeEnum.Bridge;
  } else if (isLogicalWifiInterfaceType(interfaceName)) {
    return InterfaceIdentificationTypeEnum.Logical;
  } else if (isWifiInterfaceType(interfaceName)) {
    return InterfaceIdentificationTypeEnum.Wifi;
  }
  return null;
};

// interfaceNameToPosition :: String -> Number
const interfaceNameToPosition = flow(match(/\d+$/), head, parseInt(10));

// isIntStationMode :: String -> Boolean
const isInStationMode = startsWith('sta');

module.exports = {
  isEthernetInterfaceType,
  isWifiInterfaceType,
  isLogicalWifiInterfaceType,
  isBridgeInterfaceType,
  interfaceNameToPosition,
  interfaceNameToType,
  isInStationMode,
};
