'use strict';

const { Observable } = require('rxjs');
const { noop } = require('lodash/fp');
const { evolve, always } = require('ramda');
const semver = require('semver');

const { subscribeCommandRequest, commandRequest, unsubscribeCommandRequest } = require('../messages');

// for udapi-bridge < 0.2.0
class PollCmdDecorator {
  constructor(connection) {
    this.connection = connection;
  }

  subscribeCmd(name, cmd, cmdInterval) {
    return Observable.defer(() => this.connection.cmd(commandRequest(cmd)))
      .repeatWhen(notifications => notifications.delay(cmdInterval))
      .map(evolve({
        name: always(name),
      }));
  }
}

// since udapi-bridge >= 2.0
class SubscribeCmdDecorator {
  constructor(connection) {
    this.subscriptions = new Set();
    this.connection = connection;
  }

  subscribeCmd(name, cmd, cmdInterval) {
    this.subscriptions.add(name);
    return this.connection.rpc(subscribeCommandRequest({ name, cmd, cmdInterval }))
      .mergeMapTo(this.connection.messages$)
      .filter(message => message.name === name)
      .finally(() => { this.unsubscribeCmd(name) });
  }

  unsubscribeCmd(name) {
    if (this.subscriptions.has(name) && !this.connection.isClosed) {
      this.subscriptions.delete(name);
      this.connection.rpc(unsubscribeCommandRequest(name))
        .catch(() => Observable.empty())
        .subscribe(noop, noop);
    }
  }
}

const decorate = (sysInfo, connection) => {
  const decoration = semver.gte(sysInfo.udapi, '0.2.0')
    ? new SubscribeCmdDecorator(connection)
    : new PollCmdDecorator(connection);

  return decoration.subscribeCmd.bind(decoration);
};

module.exports = decorate;
