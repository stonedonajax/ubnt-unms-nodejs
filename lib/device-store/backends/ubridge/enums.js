'use strict';

const MessageNameEnum = Object.freeze({
  SpeedTestStats: 'speed-test-stats',
});

module.exports = {
  MessageNameEnum,
};
