'use strict';

const aguid = require('aguid');
const { applySpec } = require('ramda');
const { partial, __, identity, castArray, flow } = require('lodash/fp');

const { MessageTypeEnum, MessageNameEnum } = require('../../../transformers/socket/enums');

const castNotifications = applySpec({
  notifications: castArray,
});

const rpcRequest = (request, name, socket = 'sys', meta = {}) => ({
  id: aguid(),
  type: MessageTypeEnum.Rpc,
  socket,
  name,
  request,
  meta,
});

const sysInfoRequest = partial(rpcRequest, [{ GETDATA: 'sys_info' }, MessageNameEnum.GetSysInfo, 'sys']);

const createBackupRequest = partial(rpcRequest, [{ CONFIG: { action: 'save' } }, 'backupConfig', 'sys']);

const applyBackupRequest = flow(
  applySpec({ SET: { backup_config_data: identity } }),
  partial(rpcRequest, [__, 'backupConfigApply', 'sys'])
);

const commandRequest = partial(rpcRequest, [__, MessageNameEnum.Cmd, 'sys']);

const subscribeCommandRequest = flow(
  castNotifications,
  partial(rpcRequest, [__, MessageNameEnum.SubscribeCmd, 'sys'])
);

const unsubscribeCommandRequest = flow(
  castNotifications,
  partial(rpcRequest, [__, MessageNameEnum.UnsubscribeCmd, 'sys'])
);

const pingRequest = partial(rpcRequest, [{}, MessageNameEnum.Ping, 'sys']);

const pingStatsRequest = partial(rpcRequest, [{}, 'getPingStats', 'sys']);

const shellConnectRequest = partial(rpcRequest, [{}, MessageNameEnum.ShellConnect, 'sys']);

const shellDisconnectRequest = partial(rpcRequest, [{}, MessageNameEnum.ShellDisconnect, 'sys']);

const shellCmdRequest = partial(rpcRequest, [__, MessageNameEnum.Shell, 'sys']);

/*
 ip addr show | sed -rn '
 # if an empty line, check for interface
 /^[0-9]+: / b interface
 # else add it to the hold buffer
 H
 # at end of file, check interface
 $ b interface
 # now branch to end of script
 b
 :interface
 # return the entire interface
 # into the pattern space
 x
 y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
 s/^[0-9]+: ([a-z0-9.]+)/name=\1\n / # interface name
 s/(([0-9]{1,3}\.){3}[0-9]{1,3}\/[0-9]{1,2})/\n\1\n /g # extract IPs
 s/(([a-f0-9]{2}:){5}[a-f0-9]{2})/\nmac=\1\n / # extract MACs
 p
 ' | sed -r '/^$/d;/(^[[:space:]].*)/d' | awk 'BEGIN { FS="=" } ; {
 if ($1 ~ /name/) {
 print "["$2"]"
 path = "cat /sys/class/net/"$2"/addr_assign_type 2>/dev/null"
 path | getline type
 print "type="type
 }
 print $1"="$2
 }'
 */


/* eslint-disable max-len */
const GET_NETWORK_INFO = [
  `ip addr show | sed -rn '/^[0-9]+: /bi;H;$bi;b;:i;x;
y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
s/^[0-9]+: ([a-z0-9.]+)/name=\\1\\n /
s/(([0-9]{1,3}\\.){3}[0-9]{1,3}\\/[0-9]{1,2})/\\ncidr[]=\\1\\n /g
s/(([a-f0-9]{2}:){5}[a-f0-9]{2})/\\nmac=\\1\\n /
p
' | sed -r '/^$/d;/(^[[:space:]].*)/d' | awk 'BEGIN { FS="=" } ; {
 if ($1 ~ /name/) {
  print "["$2"]"
  l[$2]=0;
  path = "cat /sys/class/net/"$2"/addr_assign_type 2>/dev/null"
  path | getline type
  print "type="type
 }
 print $1"="$2
} ; END { print "[interfaces]"; for (i in l) { print "names[]="i } }'`,
  'echo [routes]',
  'ip route show default 0/0 | sed -rn \'s/[^0-9]*(([0-9]{1,3}\\.){3}[0-9]{1,3}){0,1} *dev *([a-z.0-9-]*).*/\\3=\\1/p\'',
].join('; ');
/* eslint-enable max-len */

const startIperfClient = ({ id, keepaliveInterval, ipList, parallelConn, windowSize }) => `\
${ipList.map((ip, index) => `IPS${index}="${ip}"`).join('\n')}

counter=0
totalLength=${ipList.length}
SERVER_IP=""
export KEEP_FILE=/tmp/st-keepalive-${id}

touch "$KEEP_FILE"

while [ $counter -lt $totalLength ]
do
  touch "$KEEP_FILE"
  eval pingIP="\\$IPS$counter"

  if ping -c 1 -W 1 $pingIP > /dev/null 2>&1; then
    export SERVER_IP=$pingIP
    break
  fi

  counter=\`expr $counter + 1\`
done

if [ "$SERVER_IP" == "" ]; then
  echo "No route to the host"
  exit 1
fi

echo -e '#!/bin/sh

TICK_TIME=${keepaliveInterval}
IPERF_PIPE="/tmp/st-pipe-${id}"
PID_FILE="/tmp/st-client-${id}.pid"
SUBSHELL_PID=""

log() {
  now=$(date +"%T")
  echo "$now $1"
}

log "Server IP: $SERVER_IP"
log "KeepAlive interval: $TICK_TIME sec"

log "Starting client"
touch "$KEEP_FILE"

cleanup () {
  trap '' EXIT INT TERM
  log "Performing cleanup"
  log "Removing script"
  rm -- "$0" || log "Removing script failed"
  log "Subshell PID: $SUBSHELL_PID"
  log "Killing subshell children"
  pkill -TERM -P $SUBSHELL_PID || log "Killing children failed"
  kill $SUBSHELL_PID
  log "Removing keepalive file"
  rm -f "$KEEP_FILE" || log "Removing keepalive file failed"
  rm -f "$PID_FILE" || log "Removing pid file failed"
  log "Removing pipe"
  rm "$IPERF_PIPE" || log "Failed to remove pipe"
}

trap "cleanup" EXIT PIPE TERM INT QUIT
log "Trap initialized"

log "Saving PID"
echo -n $$ > "$PID_FILE"

log "About to check for pipe"
if [[ ! -p $IPERF_PIPE ]]; then
  log "Pipe does not exist - creating"
  mkfifo "$IPERF_PIPE" || log "Creating pipe failed"
  log "Pipe created"
else
  log "Pipe exists"
fi

log "Starting iperf"
${parallelConn && parallelConn > 1 ? `\
(iperf -c "$SERVER_IP" -P ${parallelConn} -F /dev/zero \
-t 1000000 -i 1 -y C ${windowSize ? `-l ${windowSize} -w ${windowSize}` : ''} | \
{ c=0; trap "" PIPE; while [ true ]; do read a; c=$((c+1)); \
if [ $c -gt ${parallelConn} ]; then c=0; echo $a > "$IPERF_PIPE"; fi; done; }) &
` : `
(iperf -c "$SERVER_IP" -F /dev/zero -t 1000000 -i 1 -y C \
${windowSize ? `-l ${windowSize} -w ${windowSize}` : ''} > "$IPERF_PIPE") &
`}

SUBSHELL_PID=$!

while [ \`date -r $KEEP_FILE +%s\` -gt "$((\`date +%s\`-$TICK_TIME))" ]; do
  log "Waiting for $TICK_TIME sec"
  sleep "$TICK_TIME"
  kill -0 $SUBSHELL_PID || break;
done

log "Finishing"
' > /tmp/st-client-${id}.sh && \
(sh /tmp/st-client-${id}.sh > /tmp/st-client.log 2>&1 &)
`;

module.exports = {
  pingRequest,
  rpcRequest,
  pingStatsRequest,
  createBackupRequest,
  applyBackupRequest,
  commandRequest,
  subscribeCommandRequest,
  unsubscribeCommandRequest,
  sysInfoRequest,
  shellConnectRequest,
  shellDisconnectRequest,
  shellCmdRequest,
  GET_NETWORK_INFO,
  startIperfClient,
};
