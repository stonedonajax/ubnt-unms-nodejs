'use strict';

const { Observable } = require('rxjs/Rx');
const { constant } = require('lodash/fp');
const { isNotString } = require('ramda-adjunct');

const { applyBackupRequest } = require('../../messages');
const { toHwBackup } = require('../../transformers/backup/mappers');

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceDeviceBackup} cmDeviceBackup
 * @return {Observable.<string>}
 */
function applyBackup(cmDeviceBackup) {
  const hwBackup = toHwBackup(cmDeviceBackup);
  if (isNotString(hwBackup) || hwBackup.length === 0) {
    return Observable.throw(new TypeError('Invalid backup type'));
  }
  return this.connection.rpc(applyBackupRequest(hwBackup));
}

module.exports = constant(applyBackup);
