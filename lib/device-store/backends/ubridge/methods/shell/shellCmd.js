'use strict';

const { constant } = require('lodash/fp');

const { shellCmdRequest } = require('../../messages');

/**
 * @typedef ShellCmdPayload
 * @param {GUID} deviceId
 * @param {GUID?} sessionId
 * @param {string} message
 */

/**
 * @typedef ShellCmd
 * @param {string} name
 * @param {ShellCmdPayload} payload
 */

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {string} command
 * @return {Observable.<string>}
 */
function shellCmd(command) {
  return this.connection.rpc(shellCmdRequest(command)).pluck('data');
}


module.exports = constant(shellCmd);
