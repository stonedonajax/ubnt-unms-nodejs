'use strict';

const { constant } = require('lodash/fp');

const { shellConnectRequest } = require('../../messages');

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @return {Observable.<string>}
 */
function shellConnect() {
  return this.connection.rpc(shellConnectRequest())
    .pluck('data', 'sessionId');
}

module.exports = constant(shellConnect);
