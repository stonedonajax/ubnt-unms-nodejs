'use strict';

const { constant } = require('lodash/fp');

const { shellDisconnectRequest } = require('../../messages');

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @return {Observable.<string>}
 */
function shellDisconnect() {
  return this.connection.rpc(shellDisconnectRequest())
    .pluck('data');
}

module.exports = constant(shellDisconnect);
