'use strict';

const { constant } = require('lodash/fp');
const { pathEq } = require('ramda');

const { pathNotEq } = require('../../../../../util');
const { MessageNameEnum } = require('../../../../../transformers/socket/enums');


const isShellEvent = message => pathEq(['name'], MessageNameEnum.Shell, message) && pathNotEq(['data'], 'ok', message);

// const isShellMessageInSession = sessionId => pathEq(['payload', 'sessionId'], sessionId);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {string} sessionId
 * @return {Observable.<Object>}
 */
function shellResponse() {
  return this.connection.messages$
    .filter(isShellEvent)
    .map(rpcMessage => rpcMessage.data);
    // use once implemented message with sessionId
    // .filter(isShellMessageInSession(sessionId))
    // .merge(this.connection.close$.mapTo({ type: 'deviceDisconnect' }));
}


module.exports = constant(shellResponse);
