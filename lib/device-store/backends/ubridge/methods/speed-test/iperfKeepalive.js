'use strict';

const { always } = require('ramda');

const { commandRequest } = require('../../messages');

const CMD = id => `touch "/tmp/st-keepalive-${id}"`;

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {string} id
 * @return {Observable.<CommandMessage>}
 */
function iperfKeepalive(id) {
  return this.connection.cmd(commandRequest(CMD(id)));
}

module.exports = always(iperfKeepalive);
