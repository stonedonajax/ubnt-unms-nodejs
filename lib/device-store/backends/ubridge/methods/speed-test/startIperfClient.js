'use strict';

const { Observable } = require('rxjs/Rx');
const moment = require('moment-timezone');
const { unary, always, ifElse, pathSatisfies, pipe, path, equals, last } = require('ramda');
const { isNilOrEmpty } = require('ramda-adjunct');

const { commandRequest, startIperfClient: startIperfClientFactory } = require('../../messages');
const { MessageNameEnum } = require('../../enums');
const { toMs } = require('../../../../../util');
const { UbntServiceUnavailableError, UbntError } = require('../../../../../util/errors');

const { parseSpeedTestNotification } = require('../../transformers/speed-test/parsers');

const UPDATE_STATS_INTERVAL = toMs('seconds', 0.5);
const parallelConn = 10;

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {string} id
 * @param {number} keepAlive
 * @param {Array.<string>} ipList
 * @return {Observable.<SpeedTestNotification>}
 */
function startIperfClient(id, keepAlive, ipList) {
  const keepaliveInterval = moment.duration(keepAlive, 'ms').as('s');
  const CMD = startIperfClientFactory({ id, keepaliveInterval, ipList, parallelConn });

  return this.connection
    .cmd(commandRequest(CMD))
    .mergeMap(unary(ifElse(
      pathSatisfies(isNilOrEmpty, ['data', 'output', 0]),
      unary(Observable.of),
      pipe(
        path(['data', 'output']),
        last,
        ifElse(
          equals('No route to the host'),
          msg => new UbntServiceUnavailableError(msg),
          msg => new UbntError(msg)
        )
      )
    )))
    .delay(1000)
    .mergeMap(() => this.connection.subscribeCmd(
      MessageNameEnum.SpeedTestStats,
      `[ -p "/tmp/st-pipe-${id}" ] && read data < "/tmp/st-pipe-${id}" && echo $data || echo "No pipe"`,
      UPDATE_STATS_INTERVAL
    ))
    .mergeMap((notification) => {
      const { output } = notification.data;
      if (output[0] === 'No pipe') {
        return Observable.throw(new Error('Speedtest error'));
      }

      return Observable.of(notification);
    })
    .map(unary(parseSpeedTestNotification(this.deviceId)));
}

module.exports = always(startIperfClient);
