'use strict';

const { always } = require('ramda');
const { commandRequest } = require('../../messages');
const moment = require('moment-timezone');

const CMD = ({ id, keepaliveInterval }) => `\
echo -e '#!/bin/sh

KEEP_FILE="/tmp/st-keepalive-${id}"
PID_FILE="/tmp/st-server-${id}.pid"
TICK_TIME=${keepaliveInterval}

log() {
  now=$(date +"%T")
  echo "$now $1"
}

log "Starting server"

touch "$KEEP_FILE"

cleanup () {
  trap '' EXIT INT TERM
  log "Performing cleanup"
  rm -- "$0" || log "Removing script failed"
  kill "$IPERF_PID" || log "Killing server failed"
  rm -f "$PID_FILE" || log "Removing pid file failed"
  rm -f "$KEEP_FILE" || log "Removing keepalive file failed"
  sleep 1
  kill -0 "$IPERF_PID" 2>/dev/null && log "Server still running, terminating..." && kill -KILL "$IPERF_PID"
  exit 0
}

trap "cleanup" EXIT PIPE TERM INT QUIT
log "Trap initialized"

log "Saving PID"
echo -n $$ > "$PID_FILE"

log "Starting iperf"
iperf -s > /dev/null &
IPERF_PID=$!

while [ \`date -r $KEEP_FILE +%s\` -gt "$((\`date +%s\`-$TICK_TIME))" ]; do
  log "Waiting for $TICK_TIME sec"
  sleep "$TICK_TIME"
done

log "Finishing"
' > /tmp/st-server-${id}.sh && \
(sh /tmp/st-server-${id}.sh > /tmp/st-server.log 2>&1 &)
`;

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {string} id
 * @param {number} keepaliveInterval
 * @return {Observable.<CommandMessage>}
 */
function startIperfServer(id, keepaliveInterval) {
  return this.connection.cmd(commandRequest(CMD({
    id,
    keepaliveInterval: moment.duration(keepaliveInterval, 'ms').as('s'),
  })));
}

module.exports = always(startIperfServer);
