'use strict';

const { always } = require('ramda');

const { commandRequest } = require('../../messages');

const CMD = id => [
  `[ -f "/tmp/st-client-${id}.pid" ] && kill -TERM $(cat "/tmp/st-client-${id}.pid")`,
  'sleep 1',
  `[ -f "/tmp/st-server-${id}.pid" ] && kill -TERM $(cat "/tmp/st-server-${id}.pid")`,
  `rm -f "/tmp/st-keepalive-${id}"`,
].join('; ');

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {string} id
 * @return {Observable.<CommandMessage>}
 */
function stopIperf(id) {
  return this.connection.cmd(commandRequest(CMD(id)));
}

module.exports = always(stopIperf);
