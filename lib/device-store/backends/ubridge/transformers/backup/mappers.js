'use strict';

const { flow, get } = require('lodash/fp');

const toHexString = buffer => buffer.toString('hex');

const toHwBackup = flow(
  get('source'),
  toHexString
);

module.exports = {
  toHexString,
  toHwBackup,
};
