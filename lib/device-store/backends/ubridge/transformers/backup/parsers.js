'use strict';

const { partialRight, flow, get } = require('lodash/fp');

const { parseTarBackup } = require('../../../../../device-backups/transformers/parsers');

const fromHexString = partialRight(Buffer.from, ['hex']);

/**
 * @function parseHwBackup
 * @param {CorrespondenceIncomingMessage} message
 * @return {CorrespondenceDeviceBackup}
 */
const parseHwBackup = flow(
  get('data'),
  fromHexString,
  parseTarBackup
);

module.exports = {
  fromHexString,
  parseHwBackup,
};
