'use strict';

const { defaultTo, when, converge, pick, values, filter } = require('ramda');
const { flatMap, isString, isNil } = require('lodash/fp');
const { isIP } = require('validator');
const { isNotNull } = require('ramda-adjunct');
const { flow, getOr, nth, first, sortBy, zipObject, toPairs, map, startsWith } = require('lodash/fp');
const ip = require('ip');

const { isCidr } = require('../../../../../util/ip');

const isValidIp = ipAddress => isCidr(ipAddress)
  && !startsWith('0.', ipAddress)
  && !startsWith('127.0.0', ipAddress)
  && ipAddress !== '::1/128' // TODO(michal.sedlak@ubnt.com): add more
  && !startsWith('169.254', ipAddress); // airMax auto IP

// parseDefaultRoute :: RouteList -> String|null
//     RouteList = Object
const parseDefaultRoute = flow(
  getOr({}, ['data', 'routes']),
  toPairs,
  sortBy(nth(0)),
  first,
  defaultTo(null),
  when(isNotNull, zipObject(['interface', 'gateway']))
);

const parseDefaultRoutes = flow(
  getOr({}, ['data', 'routes']),
  toPairs,
  sortBy(nth(0)),
  map(zipObject(['interface', 'gateway']))
);

const parseAvailableIps = flow(
  converge(pick, [getOr([], ['data', 'interfaces', 'names']), getOr({}, 'data')]),
  values,
  flatMap(getOr([], 'cidr')),
  filter(isValidIp)
);

/**
 * @param {Object} hwNetworkInfo
 * @return {string|null}
 */
const parseHwIpAddress = (hwNetworkInfo) => {
  const routes = parseDefaultRoutes(hwNetworkInfo);
  let ipAddress = null;

  for (const route of routes) {
    const cidrList = getOr(null, ['data', route.interface, 'cidr'], hwNetworkInfo);

    if (cidrList !== null && isIP(String(route.gateway))) {
      ipAddress = cidrList.find(cidr => isCidr(cidr) && ip.cidrSubnet(cidr).contains(route.gateway));

      if (isString(ipAddress)) {
        break;
      }
    }
  }

  if (isNil(ipAddress)) {
    ipAddress = first(parseAvailableIps(hwNetworkInfo));
  }

  return defaultTo(null, ipAddress);
};

module.exports = {
  parseDefaultRoute,
  parseHwIpAddress,
  isValidIp,
};
