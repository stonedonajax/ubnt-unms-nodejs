'use strict';

const {
  curry, pipe, path, split, applySpec, always, view, lensIndex, defaultTo, last,
} = require('ramda');

// "20180219133227,10.43.21.87,56317,10.43.21.65,5001,12,0.0-1.0,2752512,22020096"

const parseSpeedTestNotification = curry((deviceId, notification) => pipe(
  path(['data', 'output', 0]),
  split(','),
  applySpec({
    deviceId: always(deviceId),
    timestamp: () => Date.now(),
    index: pipe(view(lensIndex(6)), parseInt, defaultTo(null)),
    bps: pipe(last, Number),
  })
)(notification));

module.exports = {
  parseSpeedTestNotification,
};
