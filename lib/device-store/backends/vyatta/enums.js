'use strict';

const MessageNameEnum = Object.freeze({
  GetNetworkInfo: 'getNetworkInfo',
  GetConfig: 'getConfig',
  GetSystem: 'getSystem',
  GetServices: 'getServices',
  GetInterfaces: 'getInterfaces',
  SetConfig: 'setConfig',
  SetOnuConfig: 'setOnuConfig',
  Interfaces: 'interfaces',
  SystemStats: 'system-stats',
  PonStats: 'pon-stats',
  ConfigChange: 'config-change',
});

module.exports = {
  MessageNameEnum,
};
