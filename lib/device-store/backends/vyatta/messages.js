'use strict';

const { partial, omitBy } = require('lodash/fp');
const { isNilOrEmpty } = require('ramda-adjunct');

const { MessageNameEnum } = require('./enums');
const { rpcRequest, commandRequest, GET_NETWORK_INFO } = require('../ubridge/messages');

const deviceConfigRequest = partial(rpcRequest, [
  {
    GET: {
      interfaces: null,
      service: null,
      system: null,
      layer2: null,
      protocols: { static: null },
    },
  },
  MessageNameEnum.GetConfig,
  'sys',
]);

const systemRequest = partial(rpcRequest, [{ GET: { system: null } }, MessageNameEnum.GetSystem, 'sys']);

const servicesRequest = partial(rpcRequest, [
  { GET: { system: null, service: null } },
  MessageNameEnum.GetServices,
  'sys',
]);

const pingRequest = partial(rpcRequest, [{ PING: null }, 'ping', 'sys']);

const setConfigRequest = (setData, deleteData) => {
  const request = omitBy(isNilOrEmpty, { SET: setData, DELETE: deleteData });

  return rpcRequest(request, MessageNameEnum.SetConfig, 'sys');
};

const networkInfoCommand = partial(commandRequest, [`echo "${MessageNameEnum.GetNetworkInfo}"; ${GET_NETWORK_INFO}`]);

module.exports = {
  deviceConfigRequest,
  systemRequest,
  servicesRequest,
  pingRequest,
  setConfigRequest,
  networkInfoCommand,
};
