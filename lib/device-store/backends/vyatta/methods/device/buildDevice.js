'use strict';

const { Observable } = require('rxjs/Rx');
const { constant, isPlainObject } = require('lodash/fp');
const { allPass, pathSatisfies } = require('ramda');

const { deviceConfigRequest, networkInfoCommand } = require('../../messages');
const { parseHwDevice, parseHwDeviceId } = require('../../transformers/device/parsers');
const { mergeDeviceUpdate } = require('../../../../../transformers/device/mergers');

const CONFIG_RETRY_DELAY = 1000;

const isValidDeviceConfig = allPass([
  pathSatisfies(isPlainObject, ['data', 'interfaces']),
  pathSatisfies(isPlainObject, ['data', 'service']),
  pathSatisfies(isPlainObject, ['data', 'system']),
]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceDevice} cmDeviceStub
 * @return {Observable.<CorrespondenceDevice>}
 */
function buildDevice(cmDeviceStub) {
  const hasDeviceId = cmDeviceStub.identification.id !== null;

  // config might not be available after boot, retry until available
  const deviceConfig$ = this.connection.rpc(deviceConfigRequest())
    .first(isValidDeviceConfig)
    .retryWhen(errors => errors.take(2).delay(CONFIG_RETRY_DELAY));
  const networkInfo$ = this.connection.cmd(networkInfoCommand());

  return Observable.forkJoin(deviceConfig$, networkInfo$)
    .map(([hwDeviceConfig, hwNetworkInfo]) => {
      let cmDevice = cmDeviceStub;

      if (!hasDeviceId) {
        cmDevice = mergeDeviceUpdate(cmDevice, parseHwDeviceId({}, hwNetworkInfo));
      }

      return mergeDeviceUpdate(
        cmDevice,
        parseHwDevice({ features: this.features, hwNetworkInfo }, hwDeviceConfig)
      );
    });
}

module.exports = constant(buildDevice);
