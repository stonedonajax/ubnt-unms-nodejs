'use strict';

const { defaultTo, match, allPass, pathEq } = require('ramda');
const { isNotNil, stubUndefined } = require('ramda-adjunct');
const { flow, getOr, get, nth, has, filter, values, first, sortBy } = require('lodash/fp');

const { pathNotEq, mac2id } = require('../../../../../util');
const { StatusEnum } = require('../../../../../enums');
const { parseHwInterfaceConfig } = require('../interfaces/parsers');
const { parseDefaultRoute, parseHwIpAddress } = require('../../../ubridge/transformers/device/parsers');
const { deviceTypeFromModel, deviceCategoryFromType } = require('../../../../../feature-detection/common');

// parsePlatformId :: FullFirmwareVersion -> PlatformId
//     FullFirmwareVersion = String
//     PlatformId = String
const parsePlatformId = flow(match(/ER-(e\d+)\./), nth(1), defaultTo(null));

// findCanonicalInterface :: HwNetworkInfo -> Object|null
//     HwNetworkInfo = Object
const findCanonicalInterface = flow(
  values,
  filter(allPass([
    has('mac'),
    has('name'),
    pathEq(['type'], '0'), // see https://dongqixue.blogspot.cz/2017/05/linux-kernel-documentation-for.html
    pathNotEq(['name'], 'lo'),
    pathNotEq(['mac'], '00:00:00:00:00:00'),
  ])),
  sortBy('name'),
  first,
  defaultTo(null)
);

// parseHwDeviceName :: HwStatus -> DeviceName
//     HwStatus = Object
//     DeviceName = String
const parseHwDeviceName = getOr('ubnt', ['system', 'host-name']);

// parseHwGateway :: (HwNetworkInfo, HwDevice) -> Gateway
//     HwNetworkInfo = Object
//     HwStatus = Object
//     Gateway = String
const parseHwGateway = (hwNetworkInfo, hwDevice) => {
  const gateway = getOr(null, ['data', 'system', 'gateway-address'], hwDevice);
  if (gateway === null) {
    const route = parseDefaultRoute(hwNetworkInfo.data.routes);
    if (route !== null) {
      return route.gateway;
    }
  }

  return gateway;
};

/**
 * @param {CorrespondenceSysInfo} sysInfo
 * @return {CorrespondenceDevice}
 */
const edgeMaxDeviceStub = (sysInfo) => {
  const type = deviceTypeFromModel(sysInfo.model);
  const category = deviceCategoryFromType(type);

  return {
    identification: {
      id: sysInfo.deviceId,
      enabled: true,
      siteId: null,
      site: null,
      mac: sysInfo.mac,
      name: 'ubnt',
      serialNumber: null,
      firmwareVersion: sysInfo.firmwareVersion,
      platformId: sysInfo.platformId,
      model: sysInfo.model,
      updated: 0,
      authorized: false,
      type,
      category,
      ipAddress: null,
    },
    overview: {
      status: StatusEnum.Unauthorized,
      canUpgrade: false,
      location: null,
      isLocating: false,
      cpu: null,
      ram: null,
      voltage: null,
      temperature: null,
      signal: null,
      distance: null,
      biasCurrent: null,
      receivePower: null,
      receiveRate: null,
      receiveBytes: null,
      receiveErrors: null,
      receiveDropped: null,
      transmitPower: null,
      transmitRate: null,
      transmitBytes: null,
      transmitErrors: null,
      transmitDropped: null,
      lastSeen: 0,
      uptime: null,
      gateway: null,
    },
    meta: null,
    firmware: null,
    upgrade: null,
    mode: null,
    olt: null,
    onu: null,
    airmax: null,
    airfiber: null,
    interfaces: [],
    unmsSettings: null,
  };
};

// eslint-disable-next-line valid-jsdoc
/**
 * Parses Erouter/OLT configuration into correspondence device
 *
 * @param {CorrespondenceSysInfo} sysInfo
 * @param {number} currentTimestamp
 * @param {DeviceFeatures} features
 * @param {NetworkInfoMessage} hwNetworkInfo
 * @param {ConfigMessage} hwDevice
 * @return {CorrespondenceDevice}
 */
const parseHwDevice = ({ currentTimestamp = Date.now(), features, hwNetworkInfo }, hwDevice) => {
  const deviceName = parseHwDeviceName(hwDevice.data);

  return {
    identification: {
      name: deviceName,
      updated: currentTimestamp,
      ipAddress: parseHwIpAddress(hwNetworkInfo),
    },
    overview: {
      lastSeen: currentTimestamp,
      gateway: parseHwGateway(hwDevice, hwNetworkInfo),
    },
    interfaces: parseHwInterfaceConfig({ features, hwNetworkInfo }, hwDevice.data),
  };
};

const parseHwDeviceId = (auxiliaries, hwNetworkInfo) => {
  const iface = findCanonicalInterface(hwNetworkInfo.data);

  const mac = get(['mac'], iface);

  return {
    identification: {
      id: isNotNil(mac) ? mac2id(mac) : stubUndefined(),
      mac,
    },
  };
};

module.exports = {
  parsePlatformId,
  edgeMaxDeviceStub,
  parseHwDevice,
  parseHwDeviceId,
};
