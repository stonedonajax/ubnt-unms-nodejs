'use strict';

const createDefaultCommDevice = require('../default');

const buildDevice = require('./methods/device/buildDevice');
const restartDevice = require('../../backends/airos/methods/device/restartDevice');
const getStations = require('./methods/stations/getStations');
const createBackup = require('./methods/backup/createBackup');
const applyBackup = require('./methods/backup/applyBackup');
const getFrequencyLists = require('./methods/device/getFrequencyLists');
const updateWirelessConfig = require('./methods/device/updateWirelessConfig');
const getNetworkConfig = require('./methods/device/getNetworkConfig');
const updateNetworkConfig = require('./methods/device/updateNetworkConfig');
const getSystemConfig = require('./methods/device/getSystemConfig');
const updateSystemConfig = require('./methods/device/updateSystemConfig');

const setSetup = require('../../backends/ubridge/methods/unms/setSetup');

const createDevice = (connection, sysInfo) => {
  const commDevice = createDefaultCommDevice(sysInfo, connection);

  Object.assign(commDevice, {
    createBackup: createBackup(sysInfo),
    applyBackup: applyBackup(sysInfo),

    buildDevice: buildDevice(sysInfo),
    restartDevice: restartDevice(sysInfo),

    setSetup: setSetup(sysInfo),
    getStations: getStations(sysInfo),

    getFrequencyLists: getFrequencyLists(sysInfo),
    updateWirelessConfig: updateWirelessConfig(sysInfo),
    getNetworkConfig: getNetworkConfig(sysInfo),
    updateNetworkConfig: updateNetworkConfig(sysInfo),
    getSystemConfig: getSystemConfig(sysInfo),
    updateSystemConfig: updateSystemConfig(sysInfo),
  });

  return commDevice;
};

module.exports = createDevice;
