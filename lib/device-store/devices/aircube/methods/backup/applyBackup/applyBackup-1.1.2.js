'use strict';

const { Observable } = require('rxjs/Rx');
const aguid = require('aguid');
const { isNotString } = require('ramda-adjunct');

const { commandRequest } = require('../../../../../backends/ubridge/messages');
const { ubusRequest } = require('../../../../../backends/openwrt/messages');
const { toHwBackup } = require('../../../transformers/backup/mappers');


const restoreBackupCommand = fileName =>
  commandRequest(`lua -e "dofile('/usr/share/ubnt/config'); restoreConfig('${fileName}')"`);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceDeviceBackup} cmDeviceBackup
 * @return {Observable.<string>}
 */
function applyBackup(cmDeviceBackup) {
  const fileName = `/tmp/${aguid()}.cfg`;
  const hwBackup = toHwBackup(cmDeviceBackup);
  if (isNotString(hwBackup) || hwBackup.length === 0) {
    return Observable.throw(new TypeError('Invalid backup type'));
  }

  return this.connection
    .rpc(ubusRequest({
      path: 'file',
      method: 'write',
      args: {
        path: fileName,
        data: hwBackup,
        base64: true,
        mode: 777,
      },
    }))
    .mergeMap(() => this.connection.cmd(restoreBackupCommand(fileName)))
    .mergeMap(() => this.connection.rpc(ubusRequest({
      path: 'uci',
      method: 'reload_config',
      args: {},
    })));
}


module.exports = applyBackup;
