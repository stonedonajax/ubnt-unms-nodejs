'use strict';

const { partial } = require('ramda');

const { commandRequest } = require('../../../../../backends/ubridge/messages');
const { parseHwBackup } = require('../../../transformers/backup/parsers');

const CMD = 'lua -e "dofile(\'/usr/share/ubnt/config\'); downloadConfig()" | hexdump -v -e \'/1 "%02X"\'';
const BACKUP_TIMEOUT = 180000; // 2 minutes

const backupCommand = partial(commandRequest, [CMD]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @return {Observable.<string>}
 */
function createBackup() {
  return this.connection.cmd(backupCommand(), BACKUP_TIMEOUT)
    .map(parseHwBackup);
}

module.exports = createBackup;
