'use strict';

const { partial } = require('lodash/fp');

const { ubusRequest } = require('../../../../../backends/openwrt/messages');
const { parseHwSystem } = require('../../../transformers/device/parsers');
const { mergeDeviceUpdate } = require('../../../../../../transformers/device/mergers');
const { AirCubeWlanEnum } = require('../../../../../../enums');

const systemConfigRequest = partial(ubusRequest, [[
  {
    id: 'hwSystemBoard',
    path: 'system',
    method: 'board',
    args: {},
  },
  {
    id: 'hwSystemInfo',
    path: 'system',
    method: 'info',
    args: {},
  },
  {
    id: 'hwConfigHwctl',
    path: 'uci',
    method: 'get',
    args: { config: 'ubnt', section: 'hwctl' },
  },
  {
    id: 'hwNetworkInterfaces',
    path: 'network.interface',
    method: 'dump',
    args: {},
  },
  {
    id: 'hwRadio2GhzInfo',
    path: 'iwinfo',
    method: 'info',
    args: { device: AirCubeWlanEnum.Radio2Ghz },
  },
  {
    id: 'hwRadio5GhzInfo',
    path: 'iwinfo',
    method: 'info',
    args: { device: AirCubeWlanEnum.Radio5Ghz },
  },
  {
    id: 'hwInterfaceList',
    path: 'network.device',
    method: 'status',
    args: {},
  },
  {
    id: 'hwWanConfig',
    path: 'uci',
    method: 'get',
    args: { config: 'wireless' },
  },
]]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceDevice} cmDeviceStub
 * @return {Observable.<CorrespondenceDevice>}
 */
function buildDevice(cmDeviceStub) {
  return this.connection.rpc(systemConfigRequest())
    .map(hwSystem => mergeDeviceUpdate(
      cmDeviceStub,
      parseHwSystem({}, hwSystem)
    ));
}

module.exports = buildDevice;
