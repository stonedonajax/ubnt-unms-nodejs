'use strict';

const { partial } = require('lodash/fp');

const { ubusRequest } = require('../../../../../backends/openwrt/messages');
const { parseHwFrequencyLists } = require('../../../transformers/device/parsers');
const { AirCubeWlanEnum } = require('../../../../../../enums');


const frequencyListsRequest = partial(ubusRequest, [[
  {
    id: 'hwRadio2GhzFrequencyList',
    path: 'iwinfo',
    method: 'freqlist',
    args: { device: AirCubeWlanEnum.Radio2Ghz },
  },
  {
    id: 'hwRadio5GhzFrequencyList',
    path: 'iwinfo',
    method: 'freqlist',
    args: { device: AirCubeWlanEnum.Radio5Ghz },
  },
]]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @return {Observable.<CorrespondenceFreqLists>}
 */
function getFrequencyLists() {
  return this.connection.rpc(frequencyListsRequest())
    .map((hwFreqLists) => {
      const { hwRadio2GhzFrequencyList, hwRadio5GhzFrequencyList } = hwFreqLists.data;

      return parseHwFrequencyLists({}, hwRadio2GhzFrequencyList, hwRadio5GhzFrequencyList);
    });
}

module.exports = getFrequencyLists;
