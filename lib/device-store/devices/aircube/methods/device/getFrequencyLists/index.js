'use strict';

const { DeviceModelEnum } = require('../../../../../../enums');

const getFrequencyListsAC = require('./getFrequencyListsAC');
const getFrequencyListsISP = require('./getFrequencyListsISP');

module.exports = (sysInfo) => {
  if (sysInfo.model === DeviceModelEnum.ACBAC) {
    return getFrequencyListsAC;
  }

  return getFrequencyListsISP;
};
