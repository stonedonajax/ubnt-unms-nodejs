'use strict';

const { partial, constant } = require('lodash/fp');

const { ubusRequest } = require('../../../../backends/openwrt/messages');
const { parseHwNetworkConfig } = require('../../transformers/device/parsers');


const getNetworkConfigRequest = partial(ubusRequest, [[
  {
    id: 'hwConfigNetwork',
    path: 'uci',
    method: 'get',
    args: { config: 'network' },
  },
  {
    id: 'hwConfigDhcp',
    path: 'uci',
    method: 'get',
    args: { config: 'dhcp' },
  },
  {
    id: 'hwConfigFirewall',
    path: 'uci',
    method: 'get',
    args: { config: 'firewall' },
  },
]]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @return {Observable.<CorrespondenceNetworkConfig>}
 */
function getNetworkConfig() {
  return this.connection.rpc(getNetworkConfigRequest())
    .map((hwNetworkConfig) => {
      const { hwConfigNetwork, hwConfigDhcp, hwConfigFirewall } = hwNetworkConfig.data;

      return parseHwNetworkConfig({}, { hwConfigNetwork, hwConfigDhcp, hwConfigFirewall });
    });
}

module.exports = constant(getNetworkConfig);
