'use strict';

const { partial, constant } = require('lodash/fp');

const { ubusRequest } = require('../../../../backends/openwrt/messages');
const { parseHwSystemConfig } = require('../../transformers/device/parsers');


const getSystemConfigRequest = partial(ubusRequest, [[
  {
    id: 'hwConfigSystem',
    path: 'uci',
    method: 'get',
    args: { config: 'system', section: 'aircube' },
  },
  {
    id: 'hwConfigLed',
    path: 'uci',
    method: 'get',
    args: { config: 'led', section: 'night' },
  },
  {
    id: 'hwConfigUbnt',
    path: 'uci',
    method: 'get',
    args: { config: 'ubnt', section: 'hwctl' },
  },
  {
    id: 'hwConfigRpcd',
    path: 'uci',
    method: 'get',
    args: { config: 'rpcd', section: 'rpcd' },
  },
]]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @return {Observable.<CorrespondenceSystemConfig>}
 */
function getSystemConfig() {
  return this.connection.rpc(getSystemConfigRequest())
    .map((hwSystemConfig) => {
      const { hwConfigSystem, hwConfigLed, hwConfigUbnt, hwConfigRpcd } = hwSystemConfig.data;

      return parseHwSystemConfig({}, { hwConfigSystem, hwConfigLed, hwConfigUbnt, hwConfigRpcd });
    });
}

module.exports = constant(getSystemConfig);
