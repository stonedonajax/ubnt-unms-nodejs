'use strict';

const { constant } = require('lodash/fp');

const { ubusRequest } = require('../../../../backends/openwrt/messages');
const { toHwNetworkConfig } = require('../../transformers/device/mappers');


const updateNetworkConfigRequest = ({ hwConfigNetwork, hwConfigDhcp, hwConfigFirewall }) => ubusRequest([
  {
    id: 'setNetworkLan',
    path: 'uci',
    method: 'set',
    args: {
      config: 'network',
      section: 'lan',
      values: {
        ifname: hwConfigNetwork.values.lan.ifname,
        ipaddr: hwConfigNetwork.values.lan.ipaddr,
        netmask: hwConfigNetwork.values.lan.netmask,
        gateway: hwConfigNetwork.values.lan.gateway,
        proto: hwConfigNetwork.values.lan.proto,
        dns: hwConfigNetwork.values.lan.dns,
      },
    },
  },
  {
    id: 'setNetworkWan',
    path: 'uci',
    method: 'set',
    args: {
      config: 'network',
      section: 'wan',
      values: {
        enabled: hwConfigNetwork.values.wan.enabled,
        ifname: hwConfigNetwork.values.wan.ifname,
        ipaddr: hwConfigNetwork.values.wan.ipaddr,
        netmask: hwConfigNetwork.values.wan.netmask,
        gateway: hwConfigNetwork.values.wan.gateway,
        proto: hwConfigNetwork.values.wan.proto,
        dns: hwConfigNetwork.values.wan.dns,
        username: hwConfigNetwork.values.wan.username,
        password: hwConfigNetwork.values.wan.password,
        service: hwConfigNetwork.values.wan.service,
      },
    },
  },
  {
    id: 'setDHCPLan',
    path: 'uci',
    method: 'set',
    args: {
      config: 'dhcp',
      section: 'lan',
      values: {
        ignore: hwConfigDhcp.values.lan.ignore,
        start: hwConfigDhcp.values.lan.start,
        limit: hwConfigDhcp.values.lan.limit,
      },
    },
  },
  {
    id: 'setFirewallICMP',
    path: 'uci',
    method: 'set',
    args: {
      config: 'firewall',
      section: 'wan_icmp',
      values: {
        enabled: hwConfigFirewall.values.wan_icmp.enabled,
      },
    },
  },
  {
    id: 'setFirewallSSH',
    path: 'uci',
    method: 'set',
    args: {
      config: 'firewall',
      section: 'wan_ssh',
      values: {
        enabled: hwConfigFirewall.values.wan_ssh.enabled,
      },
    },
  },
  {
    id: 'setFirewallWWW',
    path: 'uci',
    method: 'set',
    args: {
      config: 'firewall',
      section: 'wan_www',
      values: {
        enabled: hwConfigFirewall.values.wan_www.enabled,
      },
    },
  },
  {
    id: 'setFirewallDiscovery',
    path: 'uci',
    method: 'set',
    args: {
      config: 'firewall',
      section: 'wan_discovery',
      values: {
        enabled: hwConfigFirewall.values.wan_discovery.enabled,
      },
    },
  },
  {
    id: 'commitNetwork',
    path: 'uci',
    method: 'commit',
    args: {
      config: 'network',
    },
  },
  {
    id: 'commitDHCP',
    path: 'uci',
    method: 'commit',
    args: {
      config: 'dhcp',
    },
  },
  {
    id: 'commitFirewall',
    path: 'uci',
    method: 'commit',
    args: {
      config: 'firewall',
    },
  },
  {
    id: 'applyNetworkConfig',
    path: 'uci',
    method: 'apply',
    args: { rollback: false },
  },
  {
    id: 'saveNetworkConfig',
    path: 'rpc-sys',
    method: 'save',
    args: {},
  },
]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceNetworkConfig} networkConfig
 * @return {Observable.<void>}
 */
function updateNetworkConfig(networkConfig) {
  return this.connection.rpc(updateNetworkConfigRequest(toHwNetworkConfig(networkConfig)));
}

module.exports = constant(updateNetworkConfig);
