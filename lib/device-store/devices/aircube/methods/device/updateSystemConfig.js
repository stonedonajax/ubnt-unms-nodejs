'use strict';

const { constant, isNull } = require('lodash/fp');
const { Observable } = require('rxjs');

const { ubusRequest } = require('../../../../backends/openwrt/messages');
const { toHwSystemConfig } = require('../../transformers/device/mappers');

const updateSystemkConfigRequest = ({ hwConfigSystem, hwConfigLed, hwConfigUbnt, hwConfigRpcd }) => ubusRequest([
  {
    id: 'hwConfigSystem',
    path: 'uci',
    method: 'set',
    args: {
      config: 'system',
      section: 'aircube',
      values: hwConfigSystem.values,
    },
  },
  {
    id: 'hwConfigLed',
    path: 'uci',
    method: 'set',
    args: {
      config: 'led',
      section: 'night',
      values: hwConfigLed.values,
    },
  },
  {
    id: 'hwConfigUbnt',
    path: 'uci',
    method: 'set',
    args: {
      config: 'ubnt',
      section: 'hwctl',
      values: hwConfigUbnt.values,
    },
  },
  {
    id: 'hwConfigRpcd',
    path: 'uci',
    method: 'set',
    args: {
      config: 'rpcd',
      section: 'rpcd',
      values: hwConfigRpcd.values,
    },
  },
  {
    id: 'commitLedConfig',
    path: 'uci',
    method: 'commit',
    args: {
      config: 'led',
    },
  },
  {
    id: 'commitSystemConfig',
    path: 'uci',
    method: 'commit',
    args: {
      config: 'system',
    },
  },
  {
    id: 'commitUbntConfig',
    path: 'uci',
    method: 'commit',
    args: {
      config: 'ubnt',
    },
  },
  {
    id: 'commitRpcdConfig',
    path: 'uci',
    method: 'commit',
    args: {
      config: 'rpcd',
    },
  },
  {
    id: 'applySystemConfig',
    path: 'uci',
    method: 'apply',
    args: { rollback: false },
  },
]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceSystemConfig} systemConfig
 * @return {Observable.<void>}
 */
function updateSystemConfig(systemConfig) {
  return this.connection.rpc(updateSystemkConfigRequest(toHwSystemConfig(systemConfig)))
    .mergeMap((obj) => {
      if (isNull(systemConfig.newPassword)) {
        return Observable.of(obj);
      }
      return this.connection.rpc(ubusRequest({
        path: 'rpc-sys',
        method: 'password_set',
        args: {
          user: 'ubnt', // static admin user for aircube
          password: systemConfig.newPassword,
        },
      }));
    })
    .mergeMap(() => this.connection.rpc(ubusRequest({
      path: 'rpc-sys',
      method: 'save',
      args: {},
    })));
}

module.exports = constant(updateSystemConfig);
