'use strict';

const { DeviceModelEnum } = require('../../../../../../enums');

const updateWirelessConfigAC = require('./updateWirelessConfigAC');
const updateWirelessConfigISP = require('./updateWirelessConfigISP');

module.exports = (sysInfo) => {
  if (sysInfo.model === DeviceModelEnum.ACBAC) {
    return updateWirelessConfigAC;
  }

  return updateWirelessConfigISP;
};
