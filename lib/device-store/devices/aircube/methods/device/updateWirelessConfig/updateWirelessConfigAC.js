'use strict';

const { ubusRequest } = require('../../../../../backends/openwrt/messages');
const { AirCubeWlanEnum, AirCubeRadioEnum } = require('../../../../../../enums');


const updateWirelessConfigRequest = cmWirelessConfig => ubusRequest([
  {
    id: 'setRadio2Ghz',
    path: 'uci',
    method: 'set',
    args: {
      config: 'wireless',
      section: AirCubeRadioEnum.Radio2Ghz,
      values: {
        channel: cmWirelessConfig.wifi2Ghz.channel,
        txpower: cmWirelessConfig.wifi2Ghz.txPower,
        country: cmWirelessConfig.wifi2Ghz.country,
      },
    },
  },
  {
    id: 'setWifi2Ghz',
    path: 'uci',
    method: 'set',
    args: {
      config: 'wireless',
      section: AirCubeWlanEnum.Radio2Ghz,
      values: {
        mode: cmWirelessConfig.wifi2Ghz.mode,
        ssid: cmWirelessConfig.wifi2Ghz.ssid,
        encryption: cmWirelessConfig.wifi2Ghz.authentication,
        key: cmWirelessConfig.wifi2Ghz.key,
      },
    },
  },
  {
    id: 'setRadio5Ghz',
    path: 'uci',
    method: 'set',
    args: {
      config: 'wireless',
      section: AirCubeRadioEnum.Radio5Ghz,
      values: {
        channel: cmWirelessConfig.wifi5Ghz.channel,
        txpower: cmWirelessConfig.wifi5Ghz.txPower,
        country: cmWirelessConfig.wifi5Ghz.country,
      },
    },
  },
  {
    id: 'setWifi5Ghz',
    path: 'uci',
    method: 'set',
    args: {
      config: 'wireless',
      section: AirCubeWlanEnum.Radio5Ghz,
      values: {
        mode: cmWirelessConfig.wifi5Ghz.mode,
        ssid: cmWirelessConfig.wifi5Ghz.ssid,
        encryption: cmWirelessConfig.wifi5Ghz.authentication,
        key: cmWirelessConfig.wifi5Ghz.key,
      },
    },
  },
  {
    id: 'commitWireless',
    path: 'uci',
    method: 'commit',
    args: {
      config: 'wireless',
    },
  },
  {
    id: 'applyWifiConfig',
    path: 'uci',
    method: 'apply',
    args: { rollback: false },
  },
  {
    id: 'saveWifiConfig',
    path: 'rpc-sys',
    method: 'save',
    args: {},
  },
]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceWirelessConfig} wirelessConfig
 * @return {Observable.<void>}
 */
function updateWirelessConfig(wirelessConfig) {
  return this.connection.rpc(updateWirelessConfigRequest(wirelessConfig));
}

module.exports = updateWirelessConfig;
