'use strict';

const { ubusRequest } = require('../../../../../backends/openwrt/messages');
const { WirelessModeEnum } = require('../../../../../../enums');
const { AirCubeWlanEnum, AirCubeRadioEnum } = require('../../../../../../enums');


const updateWirelessConfigRequest = cmWirelessConfig => ubusRequest([
  {
    id: 'setRadio2Ghz',
    path: 'uci',
    method: 'set',
    args: {
      config: 'wireless',
      section: AirCubeRadioEnum.Radio2Ghz,
      values: {
        channel: cmWirelessConfig.wifi2Ghz.channel,
        txpower: cmWirelessConfig.wifi2Ghz.txPower,
        country: cmWirelessConfig.wifi2Ghz.country,
      },
    },
  },
  {
    id: 'setWifi2Ghz',
    path: 'uci',
    method: 'set',
    args: {
      config: 'wireless',
      section: AirCubeWlanEnum.Radio2Ghz,
      values: {
        mode: WirelessModeEnum.Ap,
        ssid: cmWirelessConfig.wifi2Ghz.ssid,
        encryption: cmWirelessConfig.wifi2Ghz.authentication,
        key: cmWirelessConfig.wifi2Ghz.key,
      },
    },
  },
  {
    id: 'commitWireless',
    path: 'uci',
    method: 'commit',
    args: {
      config: 'wireless',
    },
  },
  {
    id: 'applyWifiConfig',
    path: 'uci',
    method: 'apply',
    args: { rollback: false },
  },
  {
    id: 'saveWifiConfig',
    path: 'rpc-sys',
    method: 'save',
    args: {},
  },
]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceWirelessConfig} wirelessConfig
 * @return {Observable.<void>}
 */
function updateWirelessConfig(wirelessConfig) {
  return this.connection.rpc(updateWirelessConfigRequest(wirelessConfig));
}

module.exports = updateWirelessConfig;
