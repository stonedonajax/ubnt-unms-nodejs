'use strict';

const { evolve, mapObjIndexed } = require('ramda');
const { has, isUndefined, merge } = require('lodash/fp');

const { MessageNameEnum } = require('../../../backends/openwrt/enums');

const getSpeed = (oldBytes, newBytes, timeElapsed) => {
  let bytesTransferred;

  if (oldBytes > newBytes) {
    bytesTransferred = newBytes + (0xFFFFFFFF - oldBytes);
  } else {
    bytesTransferred = newBytes - oldBytes;
  }

  return Math.floor((bytesTransferred * 8) / (timeElapsed / 1000));
};

class AirCubeInterfaceStatsMiddleware {
  constructor() {
    // checking config
    this.interfaceStats = {
      lastSeen: 0,
      stats: {},
    };
  }

  handleInterfaceListMessage(message) {
    if (!has(['data', 'hwInterfaceList'], message)) {
      return message;
    }

    return evolve({
      data: {
        hwInterfaceList: this.appendThroughput.bind(this),
      },
    }, message);
  }

  appendThroughput(hwInterfaceList) {
    const ts = Date.now();
    const timeElapsed = ts - this.interfaceStats.lastSeen;

    this.interfaceStats.lastSeen = ts;

    return mapObjIndexed((intfc, interfaceName) => {
      const newStats = {
        rxbytes: intfc.statistics.rx_bytes,
        txbytes: intfc.statistics.tx_bytes,
      };

      let oldStats = this.interfaceStats.stats[interfaceName];

      if (isUndefined(oldStats)) {
        oldStats = newStats;
      }

      this.interfaceStats.stats[interfaceName] = newStats;

      return merge(intfc, {
        statistics: {
          custom_tx_throughput: getSpeed(oldStats.txbytes, newStats.txbytes, timeElapsed),
          custom_rx_throughput: getSpeed(oldStats.rxbytes, newStats.rxbytes, timeElapsed),
        },
      });
    }, hwInterfaceList);
  }

  handleIncoming(message) {
    switch (message.name) {
      case MessageNameEnum.UbusBatch:
        return this.handleInterfaceListMessage(message);
      default:
      // do nothing
    }

    return message;
  }
}

const createMiddleware = () => new AirCubeInterfaceStatsMiddleware();

module.exports = createMiddleware;
