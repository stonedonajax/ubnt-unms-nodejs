'use strict';

const { Observable } = require('rxjs/Rx');
const { partial } = require('lodash/fp');

const { pingStatsRequest } = require('../../../backends/ubridge/messages');
const parsers = require('../transformers/device/parsers');

const parseHwDeviceStatistics = partial(parsers.parseHwDeviceStatistics, [{}]);

class AirCubePeriodicEventsMiddleware {
  constructor(messageHub, periodicActions, cmDevice, commDevice) {
    this.deviceId = commDevice.deviceId;
    this.commDevice = commDevice;
    this.cmDevice = cmDevice;
    this.periodicActions = periodicActions;
    this.messageHub = messageHub;
  }

  notifyDeviceUpdate(cmDeviceUpdate) {
    const messages = this.messageHub.messages;
    this.messageHub.publish(messages.airCubeUpdateEvent(this.deviceId, cmDeviceUpdate));
  }

  notifyStats(cmStats) {
    const messages = this.messageHub.messages;
    this.messageHub.publish(messages.airCubeStatisticsEvent(this.deviceId, cmStats));
  }

  updateAction() {
    return Observable.forkJoin(
      this.connection.rpc(pingStatsRequest()),
      this.commDevice.buildDevice({})
    )
      .do(([hwPingStats, cmAirCubeUpdate]) => {
        const cmStats = parseHwDeviceStatistics({ hwPingStats, cmDevice: cmAirCubeUpdate });

        this.notifyStats(cmStats);
        this.notifyDeviceUpdate(cmAirCubeUpdate);
      })
      .catch(error => this.connection.handleError(error, true));
  }

  setupPeriodicActions() {
    const updateAction = this.updateAction.bind(this);

    this.periodicActions.schedule(this.deviceId, updateAction, 'airCubeUpdateInterval');
  }

  handleEstablish(connection) {
    const messages = this.messageHub.messages;
    this.connection = connection;

    return Observable.defer(() => this.messageHub.publishAndConfirm(messages.airCubeRegisterEvent(this.cmDevice)))
      .do(() => {
        this.cmDevice = null;
        this.setupPeriodicActions();
      });
  }

  handleClose() {
    const messages = this.messageHub.messages;
    const deviceId = this.deviceId;

    this.periodicActions.stop(deviceId);
    this.messageHub.publish(messages.airCubeCloseEvent(deviceId));
  }
}

const createMiddleware = ({ messageHub, periodicActions, cmAirCube, commDevice }) =>
  new AirCubePeriodicEventsMiddleware(messageHub, periodicActions, cmAirCube, commDevice);

module.exports = createMiddleware;
