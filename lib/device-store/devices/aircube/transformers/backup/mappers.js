'use strict';

const { flow, get } = require('lodash/fp');

const toBase64String = buffer => buffer.toString('base64');

const toHwBackup = flow(
  get('source'),
  toBase64String
);

module.exports = {
  toHwBackup,
};
