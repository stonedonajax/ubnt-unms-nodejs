'use strict';

const { flow, get } = require('lodash/fp');
const { assoc } = require('ramda');

const { parseTarBackup } = require('../../../../../device-backups/transformers/parsers');
const { fromHexString } = require('../../../../backends/ubridge/transformers/backup/parsers');

/**
 * @function parseHwBackup
 * @param {CorrespondenceIncomingMessage} message
 * @return {CorrespondenceDeviceBackup}
 */
const parseHwBackup = flow(
  get(['data', 'output', 0]),
  fromHexString,
  parseTarBackup,
  assoc('extension', 'cfg')
);

module.exports = {
  parseHwBackup,
};
