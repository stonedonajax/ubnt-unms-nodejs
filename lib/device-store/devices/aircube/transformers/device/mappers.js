'use strict';

const { pipe, split, head, last } = require('ramda');
const ip = require('ip');

const { NetworkModeEnum } = require('../../../../../enums');
const { nullsToEmptyString } = require('./utils');


const toHwNetworkConfig = cmNetworkConfig => pipe(
  nullsToEmptyString,
  netConf => ({
    hwConfigNetwork: {
      values: {
        lan: {
          type: netConf.lan.type,
          ifname: netConf.mode === NetworkModeEnum.Bridge ? 'lan0 wan0' : 'lan0',
          gateway: netConf.lan.gateway,
          ipaddr: pipe(split('/'), head)(netConf.lan.cidr),
          netmask: pipe(split('/'), last, mask => parseInt(mask, 10), ip.fromPrefixLen)(netConf.lan.cidr),
          proto: netConf.lan.proto,
          dns: netConf.lan.dns,
        },
        wan: {
          enabled: netConf.mode === NetworkModeEnum.Router ? '1' : '0',
          ifname: netConf.mode === NetworkModeEnum.Router ? 'wan0' : ' ',
          gateway: netConf.wan.gateway,
          ipaddr: pipe(split('/'), head)(netConf.wan.cidr),
          netmask: pipe(split('/'), last, mask => parseInt(mask, 10), ip.fromPrefixLen)(netConf.wan.cidr),
          proto: netConf.wan.proto,
          dns: netConf.wan.dns,
          username: netConf.wan.username,
          password: netConf.wan.password,
          service: netConf.wan.service,
        },
      },
    },
    hwConfigDhcp: {
      values: {
        lan: {
          ignore: netConf.mode === NetworkModeEnum.Bridge ? '1' : '0',
          start: pipe(
            () => ip.cidrSubnet(netConf.lan.cidr).networkAddress,
            ip.toLong,
            netAddrNum => ip.toLong(netConf.lan.dhcp.rangeStart) - netAddrNum
          )(),
          limit: ip.toLong(netConf.lan.dhcp.rangeEnd) - ip.toLong(netConf.lan.dhcp.rangeStart),
        },
      },
    },
    hwConfigFirewall: {
      values: {
        wan_icmp: {
          enabled: netConf.blockManagementAccess ? '0' : '1',
        },
        wan_ssh: {
          enabled: netConf.blockManagementAccess ? '0' : '1',
        },
        wan_www: {
          enabled: netConf.blockManagementAccess ? '0' : '1',
        },
        wan_discovery: {
          enabled: netConf.blockManagementAccess ? '0' : '1',
        },
      },
    },
  })
)(cmNetworkConfig);

const toHwSystemConfig = cmSystemConfig => pipe(
  nullsToEmptyString,
  sysConf => ({
    hwConfigSystem: {
      values: {
        hostname: sysConf.deviceName,
        timezone: sysConf.timezone,
        zonename: sysConf.zonename,
      },
    },
    hwConfigLed: {
      values: {
        enable: sysConf.ledNightMode.enable ? 1 : 0,
        start: sysConf.ledNightMode.enable ? sysConf.ledNightMode.start : 0,
        end: sysConf.ledNightMode.enable ? sysConf.ledNightMode.end : 0,
      },
    },
    hwConfigUbnt: {
      values: {
        poe_pass: sysConf.poePassthrough ? '1' : '0',
      },
    },
    hwConfigRpcd: {
      values: {
        username: sysConf.username,
      },
    },
  })
)(cmSystemConfig);


module.exports = {
  toHwNetworkConfig,
  toHwSystemConfig,
};
