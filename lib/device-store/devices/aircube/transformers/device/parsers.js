'use strict';

const { flow, getOr, floor, toInteger, get, clamp } = require('lodash/fp');
const {
  pipe, pathOr, prop, when, ifElse, always, find, pathEq, curry, equals, contains, values, pathSatisfies, none, split,
  not, isNil, indexBy,
} = require('ramda');
const { isNotNull, stubNull, isNotNil } = require('ramda-adjunct');
const ip = require('ip');

const { parseHwInterfacesList, parseCmInterfaceStatistics } = require('../interfaces/parsers');
const { parseCommFirmwareVersion } = require('../../../../../transformers/semver/parsers');
const {
  StatusEnum, NetworkModeEnum, WirelessModeEnum, WifiSecurityEnum, WifiAuthenticationEnum, AirCubeTxPowerEnum,
  AirCubeWlanEnum,
} = require('../../../../../enums');
const { deviceTypeFromModel, deviceCategoryFromType } = require('../../../../../feature-detection/common');
const { emptyStringsToNull } = require('./utils');

// parseHwDeviceName :: HwSystemBoard -> String
//     HwConfigSystem = Object
const parseHwDeviceName = getOr('airCube', ['hostname']);

// parseHwFirmwareVersion :: HwSystemBoard -> FirmwareVersion
//     HwSystemBoard = Object
//     FirmwareVersion = String
const parseHwFirmwareVersion = flow(
  getOr(null, ['release', 'version']),
  parseCommFirmwareVersion
);

// parseHwMemoryUsage :: Object -> Number
const parseHwMemoryUsage = (hwSystemInfo) => {
  const total = parseInt(hwSystemInfo.memory.total, 10);
  const free = parseInt(hwSystemInfo.memory.free, 10);
  const used = total - free;

  return floor((used / total) * 100);
};

// parseHwUptime :: HwSystemInfo -> Number
//     HwSystemInfo = Object
const parseHwPingStatsLatency = flow(get(['data', 'latency']), toInteger);

// parseHwUptime :: HwSystemInfo -> Number
//     HwSystemInfo = Object
const parseHwPingStatsErrors = flow(get(['data', 'failureRate']), toInteger);

// parseHwUptime :: HwSystemInfo -> Number
//     HwSystemInfo = Object
const parseHwUptime = flow(get(['uptime']), toInteger);

// parseHwCpuUsage :: HwSystemInfo -> Number
//     HwSystemInfo = Object
const parseHwCpuUsage = flow(getOr(0, ['cpu']), toInteger, clamp(0, 100));

/**
 * @function
 * @param {Object} hwNetworkInterfacesMap
 * @return {string|null}
 */
const parseHwGateway = pipe(
  ifElse(
    pipe(prop('wan'), pathEq(['up'], true)),
    prop('wan'),
    prop('lan')
  ),
  pathOr([], ['route']),
  find(pathEq(['target'], '0.0.0.0')),
  pathOr(null, ['nexthop'])
);

/**
 * @function
 * @param {Object} hwNetworkInterfacesMap
 * @return {string|null}
 */
const parseHwIpAddress = pipe(
  ifElse(
    pipe(prop('wan'), pathEq(['up'], true)),
    prop('wan'),
    prop('lan')
  ),
  pathOr(null, ['ipv4-address', 0]),
  when(equals('0.0.0.0'), stubNull),
  when(isNotNull, ipObj => `${ipObj.address}/${ipObj.mask}`)
);

/**
 * @function parseHwMode
 * @param {Object} hwWanInterface
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwMode = ifElse(
  pathEq(['up'], true),
  always(NetworkModeEnum.Router),
  always(NetworkModeEnum.Bridge)
);

const parseHwNetworkInterfacesMap = pipe(
  prop('interface'),
  indexBy(prop('interface'))
);

const parseHwTxPower = (txPower) => {
  if (txPower === null) {
    return null;
  }

  const txPowerNum = parseInt(txPower, 10);
  if (contains(txPowerNum, values(AirCubeTxPowerEnum))) {
    return txPowerNum;
  }
  return AirCubeTxPowerEnum.Auto;
};

const parseHwWlanInterface = (wlanName, hwInterface, hwWanConfig) => {
  const hwWlanConfig = pathOr(null, ['values', wlanName], hwWanConfig);
  const hwRadioConfig = pipe(
    pathOr(null, ['device']),
    when(isNotNull, radioName => pathOr(null, ['values', radioName], hwWanConfig))
  )(hwWlanConfig);

  return {
    ssid: pathOr(null, ['ssid'], hwWlanConfig),
    key: pathOr(null, ['key'], hwWlanConfig),
    available: isNotNil(hwInterface),
    mode: pathOr(null, ['mode'], hwWlanConfig),
    mac: pathOr(null, ['macaddr'], hwInterface),
    country: pathOr(null, ['country'], hwInterface),
    channel: pathOr(null, ['channel'], hwRadioConfig),
    isChannelAuto: pathEq(['channel'], 'auto', hwRadioConfig), // channel auto can be found only in config
    frequency: pathOr(null, ['frequency'], hwInterface),
    encryption: ifElse(
      pathEq(['encryption'], WifiAuthenticationEnum.PSK2),
      always(WifiSecurityEnum.WPA2),
      always(WifiSecurityEnum.None)
    )(hwWlanConfig),
    authentication: pathOr(WifiAuthenticationEnum.None, ['encryption'], hwWlanConfig),
    txPower: parseHwTxPower(pathOr(null, ['txpower'], hwRadioConfig)),
  };
};

const parseHwWanPoe = pathEq(['values', 'poe_pass'], '1');

const parseHwWifiMode = ({ wifi2Ghz, wifi5Ghz }) => {
  if (isNil(wifi5Ghz)) {
    return wifi2Ghz.mode;
  }

  return wifi2Ghz.mode === wifi5Ghz.mode
    ? WirelessModeEnum.Ap
    : WirelessModeEnum.Sta;
};

/**
 * @function parseHwSystem
 * @param {number} currentTimestamp
 * @param {Object} hwSystem
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwSystem = ({ currentTimestamp = Date.now() }, hwSystem) => {
  const {
    hwSystemBoard, hwSystemInfo, hwNetworkInterfaces, hwInterfaceList, hwRadio2GhzInfo, hwRadio5GhzInfo,
    hwConfigHwctl, hwWanConfig,
  } = hwSystem.data;

  const wifi2Ghz = parseHwWlanInterface(AirCubeWlanEnum.Radio2Ghz, hwRadio2GhzInfo, hwWanConfig);
  const wifi5Ghz = parseHwWlanInterface(AirCubeWlanEnum.Radio5Ghz, hwRadio5GhzInfo, hwWanConfig);

  const hwNetworkInterfacesMap = parseHwNetworkInterfacesMap(hwNetworkInterfaces);

  return {
    identification: {
      name: parseHwDeviceName(hwSystemBoard),
      serialNumber: null,
      firmwareVersion: parseHwFirmwareVersion(hwSystemBoard),
      ipAddress: parseHwIpAddress(hwNetworkInterfacesMap),
      updated: currentTimestamp,
    },
    mode: parseHwMode(hwNetworkInterfacesMap.wan),
    overview: {
      cpu: parseHwCpuUsage(hwSystemInfo),
      ram: parseHwMemoryUsage(hwSystemInfo),
      uptime: parseHwUptime(hwSystemInfo),
      gateway: parseHwGateway(hwNetworkInterfacesMap),
      lastSeen: currentTimestamp,
    },
    aircube: {
      poe: parseHwWanPoe(hwConfigHwctl),
      wifiMode: parseHwWifiMode({ wifi2Ghz, wifi5Ghz }),
      wifi2Ghz,
      wifi5Ghz,
    },
    interfaces: parseHwInterfacesList({ currentTimestamp, hwNetworkInterfaces }, hwInterfaceList),
  };
};

/**
 * @function parseHwDeviceStatistics
 * @param {number} currentTimestamp
 * @param {Object} hwSystemInfo
 * @param {Object} hwInterfaceList
 * @param {Object} hwPingStats
 * @return {CorrespondenceStatistics}
 */
const parseHwDeviceStatistics = (
  { currentTimestamp = Date.now() },
  { hwPingStats, cmDevice }
) => ({
  timestamp: currentTimestamp,
  weight: 1,
  interfaces: parseCmInterfaceStatistics(cmDevice.interfaces),
  stats: {
    ping: parseHwPingStatsLatency(hwPingStats),
    errors: parseHwPingStatsErrors(hwPingStats),
    cpu: cmDevice.overview.cpu,
    ram: cmDevice.overview.ram,
  },
});

const parseHwFrequencyLists = (auxiliaries, hwRadio2GhzFrequencyList, hwRadio5GhzFrequencyList) => ({
  radio2GhzFrequencyList: pathOr(null, ['results'], hwRadio2GhzFrequencyList),
  radio5GhzFrequencyList: pathOr(null, ['results'], hwRadio5GhzFrequencyList),
});

const parseRangeToIp = curry((rangeNum, address) =>
  ip.fromLong(ip.toLong(address) + parseInt(rangeNum, 10))
);

const toCidrString = (ipAddress, subnet) => {
  if (subnet !== null) {
    return `${ipAddress}/${subnet.subnetMaskLength}`;
  }
  return null;
};

const parseHwNetworkConfig = (auxiliaries, { hwConfigNetwork, hwConfigDhcp, hwConfigFirewall }) => {
  const lanIp = pathOr(null, ['values', 'lan', 'ipaddr'], hwConfigNetwork);
  const lanMask = pathOr(null, ['values', 'lan', 'netmask'], hwConfigNetwork);
  const wanIp = pathOr(null, ['values', 'wan', 'ipaddr'], hwConfigNetwork);
  const wanMask = pathOr(null, ['values', 'wan', 'netmask'], hwConfigNetwork);
  const lanDHCPRangeStart = pathOr(null, ['values', 'lan', 'start'], hwConfigDhcp);
  const lanDHCPRangeLimit = pathOr(null, ['values', 'lan', 'limit'], hwConfigDhcp);
  const lanSubnet = none(equals(null), [lanIp, lanMask]) ? ip.subnet(lanIp, lanMask) : null;
  const wanSubnet = none(equals(null), [wanIp, wanMask]) ? ip.subnet(wanIp, wanMask) : null;
  const rangeStartAddress = none(equals(null), [lanSubnet, lanDHCPRangeStart])
    ? parseRangeToIp(lanDHCPRangeStart, lanSubnet.networkAddress)
    : null;
  const lanInterfaceNames = pipe(pathOr('', ['values', 'lan', 'ifname']), split(' '))(hwConfigNetwork);
  const wanInterfaceNames = [pathOr(null, ['values', 'wan', 'ifname'], hwConfigNetwork)];
  const wanICMPEnabled = pathSatisfies(equals('1'), ['values', 'wan_icmp', 'enabled'], hwConfigFirewall);
  const wanSSHEnabled = pathSatisfies(equals('1'), ['values', 'wan_ssh', 'enabled'], hwConfigFirewall);
  const wanWWWPEnabled = pathSatisfies(equals('1'), ['values', 'wan_www', 'enabled'], hwConfigFirewall);
  const wanDiscoveryEnabled = pathSatisfies(equals('1'), ['values', 'wan_discovery', 'enabled'], hwConfigFirewall);

  return emptyStringsToNull({
    mode: contains('wan0', lanInterfaceNames) && not(contains('wan0', wanInterfaceNames))
      ? NetworkModeEnum.Bridge
      : NetworkModeEnum.Router,
    blockManagementAccess: wanICMPEnabled && wanSSHEnabled && wanWWWPEnabled && wanDiscoveryEnabled,
    lan: {
      type: pathOr(null, ['values', 'lan', 'type'], hwConfigNetwork),
      interfaceNames: lanInterfaceNames,
      gateway: pathOr(null, ['values', 'lan', 'gateway'], hwConfigNetwork),
      cidr: toCidrString(lanIp, lanSubnet),
      proto: pathOr(null, ['values', 'lan', 'proto'], hwConfigNetwork),
      dns: pathOr([], ['values', 'lan', 'dns'], hwConfigNetwork),
      dhcp: {
        ignore: pathSatisfies(equals('1'), ['values', 'lan', 'ignore'], hwConfigDhcp),
        interface: pathOr(null, ['values', 'lan', 'interface'], hwConfigDhcp),
        rangeStart: rangeStartAddress,
        rangeEnd: isNotNull(rangeStartAddress) ? parseRangeToIp(lanDHCPRangeLimit, rangeStartAddress) : null,
        leaseTime: pathOr(null, ['values', 'lan', 'leasetime'], hwConfigDhcp),
      },
    },
    wan: {
      enabled: pathSatisfies(equals('1'), ['values', 'lan', 'ignore'], hwConfigDhcp),
      interfaceNames: wanInterfaceNames,
      cidr: toCidrString(wanIp, wanSubnet),
      gateway: pathOr(null, ['values', 'wan', 'gateway'], hwConfigNetwork),
      proto: pathOr(null, ['values', 'wan', 'proto'], hwConfigNetwork),
      dns: pathOr([], ['values', 'wan', 'dns'], hwConfigNetwork),
      service: pathOr(null, ['values', 'wan', 'service'], hwConfigNetwork),
      username: pathOr(null, ['values', 'wan', 'username'], hwConfigNetwork),
      password: pathOr(null, ['values', 'wan', 'password'], hwConfigNetwork),
    },
  });
};

const parseHwSystemConfig = (auxiliaries, { hwConfigSystem, hwConfigLed, hwConfigUbnt, hwConfigRpcd }) => pipe(
  emptyStringsToNull,
  (parsedHwInput) => {
    const ledNightModeStart = parseInt(pathOr(0, ['values', 'start'], parsedHwInput.hwConfigLed), 10);
    const ledNightModeEnd = parseInt(pathOr(0, ['values', 'end'], parsedHwInput.hwConfigLed), 10);

    return {
      deviceName: pathOr(null, ['values', 'hostname'], parsedHwInput.hwConfigSystem),
      timezone: pathOr(null, ['values', 'timezone'], parsedHwInput.hwConfigSystem),
      zonename: pathOr(null, ['values', 'zonename'], parsedHwInput.hwConfigSystem),
      username: pathOr(null, ['values', 'username'], parsedHwInput.hwConfigRpcd),
      ledNightMode: {
        enable: pathSatisfies(equals('1'), ['values', 'enable'], parsedHwInput.hwConfigLed),
        start: ledNightModeStart,
        end: ledNightModeEnd,
      },
      poePassthrough: pathSatisfies(equals('1'), ['values', 'poe_pass'], parsedHwInput.hwConfigUbnt),
    };
  }
)({ hwConfigSystem, hwConfigLed, hwConfigUbnt, hwConfigRpcd });

const airCubeDeviceStub = (sysInfo) => {
  const type = deviceTypeFromModel(sysInfo.model);
  const category = deviceCategoryFromType(type);

  return {
    identification: {
      id: sysInfo.deviceId,
      enabled: true,
      siteId: null,
      site: null,
      mac: sysInfo.mac,
      name: 'ubnt',
      serialNumber: null,
      firmwareVersion: sysInfo.firmwareVersion,
      platformId: sysInfo.platformId,
      model: sysInfo.model,
      updated: 0,
      authorized: false,
      type,
      category,
      ipAddress: null,
    },
    overview: {
      status: StatusEnum.Unauthorized,
      canUpgrade: false,
      location: null,
      isLocating: false,
      cpu: null,
      ram: null,
      voltage: null,
      temperature: null,
      signal: null,
      distance: null,
      biasCurrent: null,
      receivePower: null,
      receiveRate: null,
      receiveBytes: null,
      receiveErrors: null,
      receiveDropped: null,
      transmitPower: null,
      transmitRate: null,
      transmitBytes: null,
      transmitErrors: null,
      transmitDropped: null,
      lastSeen: 0,
      uptime: null,
      gateway: null,
    },
    meta: null,
    firmware: null,
    upgrade: null,
    mode: null,
    olt: null,
    onu: null,
    airmax: null,
    airfiber: null,
    aircube: {
      wifiMode: null,
      poe: null,
      stations: null,
      wifi2Ghz: {
        available: false,
        mode: null,
        mac: null,
        ssid: null,
        country: null,
        channel: null,
        frequency: null,
        encryption: null,
        authentication: null,
        txPower: null,
      },
      wifi5Ghz: {
        available: false,
        mode: null,
        mac: null,
        ssid: null,
        country: null,
        channel: null,
        frequency: null,
        encryption: null,
        authentication: null,
        txPower: null,
      },
    },
    interfaces: [],
    unmsSettings: null,
  };
};

module.exports = {
  parseHwSystem,
  parseHwDeviceStatistics,
  parseHwFrequencyLists,
  parseHwNetworkConfig,
  parseHwSystemConfig,
  parseRangeToIp,
  airCubeDeviceStub,
};
