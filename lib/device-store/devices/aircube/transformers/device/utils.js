'use strict';

const { map, equals } = require('ramda');
const { isObj, isNotArray, isString } = require('ramda-adjunct');


const isEmptyString = s => isString(s) && s.trim() === '';

const emptyStringsToNull = map((val) => {
  if (isObj(val)) {
    return emptyStringsToNull(val);
  }
  return isEmptyString(val) ? null : val;
});

const nullsToEmptyString = map((val) => {
  if (isObj(val) && isNotArray(val)) {
    return nullsToEmptyString(val);
  }
  return equals(null, val) ? ' ' : val;
});


module.exports = {
  emptyStringsToNull,
  nullsToEmptyString,
};
