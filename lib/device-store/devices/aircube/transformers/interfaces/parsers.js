'use strict';

const {
  pipe, pathOr, slice, when, equals, anyPass, test, ifElse, always, map, toPairs, reject, trim, pathEq, prop,
  indexBy, filter, reduce,
} = require('ramda');
const { isNotNull, isNotNil } = require('ramda-adjunct');

const { InterfaceIdentificationTypeEnum, IpAddressTypeEnum, PoeOutputEnum } = require('../../../../../enums');

const isEthernetInterfaceType = anyPass([
  test(/^wan/i),
  test(/^eth/i),
  test(/^lan/i),
]);

const isWifiInterfaceType = test(/^wlan\d+/);

const isBridgeInterfaceType = test(/^br/);

const interfaceNameToType = (interfaceName) => {
  if (isEthernetInterfaceType(interfaceName)) {
    return InterfaceIdentificationTypeEnum.Ethernet;
  } else if (isBridgeInterfaceType(interfaceName)) {
    return InterfaceIdentificationTypeEnum.Bridge;
  } else if (isWifiInterfaceType(interfaceName)) {
    return InterfaceIdentificationTypeEnum.Wifi;
  }
  return null;
};

/**
 * @function
 * @param {Object} cmInterface
 * @return {boolean}
 */
const isMeasurableInterface = pipe(
  pathOr(null, ['identification', 'name']),
  anyPass([isBridgeInterfaceType, isEthernetInterfaceType, isWifiInterfaceType])
);

const parseHwInterfaceDuplex = pipe(
  pathOr(null, ['speed']),
  when(isNotNull, pipe(slice(-1, Infinity), equals('F')))
);

const parseHwInterfaceAddresses = (hwNetworkDevicesMap, hwIntfcName) => {
  const hwAddressList = pathOr([], [hwIntfcName, 'ipv4-address'], hwNetworkDevicesMap);
  if (hwAddressList.length === 0) {
    return [];
  }

  const type = pathEq([hwIntfcName, 'proto'], 'dhcp', hwNetworkDevicesMap)
    ? IpAddressTypeEnum.Dhcp
    : IpAddressTypeEnum.Static;

  return hwAddressList.map(ip => ({ cidr: `${ip.address}/${ip.mask}`, type }));
};

// parseHwInterface :: [HwInterfaceName, HwInterface] -> CmInterface
//    HwInterfaceName = String
//    HwInterface = Object
//    CmInterface = Object
const parseHwInterface = ({ currentTimestamp = Date.now(), hwNetworkDevicesMap = {} }, [hwIntfcName, hwInterface]) => ({
  identification: {
    position: null,
    type: interfaceNameToType(hwIntfcName),
    name: hwIntfcName,
    description: null,
    mac: hwInterface.macaddr,
  },
  statistics: {
    timestamp: currentTimestamp,
    rxrate: pathOr(0, ['statistics', 'custom_rx_throughput'], hwInterface),
    rxbytes: pathOr(0, ['statistics', 'rx_bytes'], hwInterface),
    txrate: pathOr(0, ['statistics', 'custom_tx_throughput'], hwInterface),
    txbytes: pathOr(0, ['statistics', 'tx_bytes'], hwInterface),
    dropped: pathOr(0, ['statistics', 'tx_dropped'], hwInterface),
    errors: pathOr(0, ['statistics', 'tx_errors'], hwInterface),
    previousTxbytes: 0,
    previousRxbytes: 0,
    previousDropped: 0,
    previousErrors: 0,
  },
  addresses: parseHwInterfaceAddresses(hwNetworkDevicesMap, hwIntfcName),
  mtu: hwInterface.mtu,
  poe: ifElse(
    equals('wan0'),
    always([PoeOutputEnum.PASSTHROUGH]),
    always(null)
  )(hwIntfcName),
  // TODO(jan.beseda@ubnt.com): parse info from config file network.lan and network.wan using uci
  enabled: hwInterface.up,
  proxyARP: null,
  switch: null,
  bridgeGroup: null,
  onSwitch: false, // TODO(michal.sedlak@ubnt.com): parse correct values if possible
  isSwitchedPort: false,
  status: {
    autoneg: false,
    duplex: parseHwInterfaceDuplex(hwInterface),
    speed: ifElse(
      isNotNil,
      pipe(trim, slice(0, -1), Number),
      always(null)
    )(hwInterface.speed),
    description: null,
    plugged: hwInterface.up,
    sfp: null,
    lag: null,
  },
  vlan: null,
  pppoe: null,
  pon: null,
  bridge: null,
  ospf: {
    ospfCapable: false,
    ospfConfig: null,
  },
});

// parseHwInterface :: (Auxiliaries, HwInterfaceList) -> CmInterface[]
//    Auxiliaries = Object
//    HwInterfaceList = Object
//    CmInterface = Object
const parseHwInterfacesList = ({ currentTimestamp = Date.now(), hwNetworkInterfaces = {} }, hwInterfaceList) => {
  const hwNetworkDevicesMap = indexBy(prop('device'), pathOr([], ['interface'], hwNetworkInterfaces));

  return pipe(
    toPairs,
    reject(([hwIntfcName]) => hwIntfcName === 'lo'),
    map(hwInterface => parseHwInterface({ currentTimestamp, hwNetworkDevicesMap }, hwInterface))
  )(hwInterfaceList);
};

// extractInterfaceStatistics :: (Accumulator, HwInterface) -> CorrespondenceStatistics
//     Auxiliaries = Object
//     HwInterface = Object
//     CorrespondenceStatistics = Object
const extractInterfaceStatistics = (accumulator = {}, cmInterface) => {
  // eslint-disable-next-line no-param-reassign
  accumulator[cmInterface.identification.name] = {
    weight: 1,
    stats: {
      rx_bps: pathOr(0, ['statistics', 'rxrate'], cmInterface),
      tx_bps: pathOr(0, ['statistics', 'txrate'], cmInterface),
    },
  };

  return accumulator;
};

/**
 * @function
 * @param {Object} cmInterfaceList
 * @return {Object}
 */
const parseCmInterfaceStatistics = pipe(
  filter(isMeasurableInterface),
  reduce(extractInterfaceStatistics, undefined)
);

module.exports = {
  parseHwInterfacesList,
  parseCmInterfaceStatistics,
  isWifiInterfaceType,
};
