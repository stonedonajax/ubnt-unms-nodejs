'use strict';

const { Reader: reader } = require('monet');

const { parseSysInfoMessage } = require('../../backends/airos/transformers/socket/parsers');
const { airFiberDeviceStub } = require('./transformers/device/parsers');

const createPingMiddleware = require('../../backends/ubridge/middlewares/ping');
const parseMessagesMiddleware = require('../../backends/airos/middlewares/parse-messages');
const createEventsMiddleware = require('./middlewares/events');
const interfaceStatsMiddleware = require('./middlewares/LTU/interface-stats');
const createStationsThroughputMiddleware = require('./middlewares/LTU/stations-throughput');
const createInterfaceThroughputMiddleware = require('../../backends/airos/middlewares/interface-throughput');
const { AirFiberSeriesEnum } = require('../../../enums');

const createCommDevice = require('./factory');

/**
 * @param {WebSocketConnection} connection
 * @param {CorrespondenceIncomingMessage} sysInfoMessage
 * @return {Reader.<Observable.<CommDevice>>}
 */
const bootstrapConnection = (connection, sysInfoMessage) => reader(
  ({ messageHub, periodicActions }) => {
    const sysInfo = parseSysInfoMessage(sysInfoMessage);
    const cmAirFiberStub = airFiberDeviceStub(sysInfo);

    connection.use(parseMessagesMiddleware); // handle incoming olt messages

    const commDevice = createCommDevice(connection, sysInfo);

    return commDevice.buildDevice(cmAirFiberStub)
      .map((cmAirFiber) => {
        const series = cmAirFiber.airfiber.series;
        if (series === AirFiberSeriesEnum.LTU) {
          connection.use(interfaceStatsMiddleware);
          connection.use(createStationsThroughputMiddleware());
        }
        connection.use(createInterfaceThroughputMiddleware());
        connection.use(createEventsMiddleware({ messageHub, periodicActions, cmAirFiber, commDevice }));
        connection.use(createPingMiddleware({ periodicActions, commDevice }));

        return commDevice;
      });
  }
);

module.exports = bootstrapConnection;
