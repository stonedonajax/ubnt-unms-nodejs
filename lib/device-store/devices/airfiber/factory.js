'use strict';

const createDefaultCommDevice = require('../default');

const buildDevice = require('./methods/device/buildDevice');
const restartDevice = require('../../backends/airos/methods/device/restartDevice');
const systemUpgrade = require('../../backends/airos/methods/system/upgrade');

const getStations = require('./methods/stations/getStations');

const setSetup = require('../../backends/ubridge/methods/unms/setSetup');
const createBackup = require('../../backends/airos/methods/backup/createBackup');
const applyBackup = require('../../backends/airos/methods/backup/applyBackup');

const createDevice = (connection, sysInfo) => {
  const commDevice = createDefaultCommDevice(sysInfo, connection);

  Object.assign(commDevice, {
    buildDevice: buildDevice(sysInfo),
    restartDevice: restartDevice(sysInfo),

    systemUpgrade: systemUpgrade(sysInfo),

    setSetup: setSetup(sysInfo),
    createBackup: createBackup(sysInfo),
    applyBackup: applyBackup(sysInfo),

    getStations: getStations(sysInfo),
  });

  return commDevice;
};

module.exports = createDevice;
