'use strict';

const { partial } = require('lodash/fp');

const { commandRequest } = require('../../backends/ubridge/messages');
const { MessageNameEnum } = require('../../backends/airos/enums');

// eslint-disable-next-line max-len
const STATUS_CMD = `echo "${MessageNameEnum.Status}"; /usr/www/status.cgi -p wireless.stats 2>/dev/null | sed '1,/^[[:space:]]*$/d' | tr -d "\\n\\r\\t"`;

/**
 * Used only for AirFiber
 */
const deviceStatusRequestCommand = partial(commandRequest, [STATUS_CMD]);

// eslint-disable-next-line max-len
const STATIONS_CMD = `echo "${MessageNameEnum.StationList}"; /usr/www/sta.cgi 2>/dev/null | sed '1,/^[[:space:]]*$/d'`;

/**
 * Used only for AirFiber
 */
const stationsRequestCommand = partial(commandRequest, [STATIONS_CMD]);

module.exports = {
  deviceStatusRequestCommand,
  stationsRequestCommand,
};
