'use strict';

const { Observable } = require('rxjs/Rx');
const { constant } = require('lodash/fp');

const { deviceConfigRequest } = require('../../../../backends/airos/messages');
const { deviceStatusRequestCommand } = require('../../messages');
const { parseHwDeviceConfig, parseHwStatus } = require('../../transformers/device/parsers');
const { mergeDeviceUpdate } = require('../../../../../transformers/device/mergers');

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceDevice} cmDeviceStub
 * @return {Observable.<CorrespondenceDevice>}
 */
function buildDevice(cmDeviceStub) {
  const deviceConfig$ = this.connection.cmd(deviceConfigRequest());
  const deviceStatus$ = this.connection.cmd(deviceStatusRequestCommand());

  return Observable.forkJoin(deviceConfig$, deviceStatus$)
    .map(([hwDeviceConfig, hwStatus]) => mergeDeviceUpdate(
        cmDeviceStub,
        parseHwStatus({ hwDeviceConfig }, hwStatus),
        parseHwDeviceConfig({}, hwDeviceConfig)
      ));
}

module.exports = constant(buildDevice);
