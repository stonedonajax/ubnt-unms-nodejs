'use strict';

const { constant } = require('lodash/fp');

const { stationsRequestCommand } = require('../../messages');
const { parseHwStationList } = require('../../transformers/stations/parsers');

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @return {Observable.<void>}
 */
function getStations() {
  return this.connection.cmd(stationsRequestCommand())
    .pluck('data')
    .map(parseHwStationList);
}

module.exports = constant(getStations);
