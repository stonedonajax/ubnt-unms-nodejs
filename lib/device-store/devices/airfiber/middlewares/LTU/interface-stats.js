'use strict';

const { getOr, find, flow, isPlainObject } = require('lodash/fp');

const { MessageNameEnum } = require('../../../../backends/airos/enums');

const HW_BRIDGE_ROLE = 'bridge_h1';
const findAth0 = flow(getOr([], 'interfaces'), find({ ifname: 'ath0' }));

const fixAth0Bytes = (hwStatus) => {
  const ath0 = findAth0(hwStatus);
  const role = getOr(null, ['host', 'netrole'], hwStatus);

  // only if ath0 exists and device has HW bridge active
  if (isPlainObject(ath0) && isPlainObject(ath0.status) && role === HW_BRIDGE_ROLE) {
    ath0.status.rx_bytes = getOr(0, ['wireless', 'stats', 'rx_bytes'], hwStatus);
    ath0.status.tx_bytes = getOr(0, ['wireless', 'stats', 'tx_bytes'], hwStatus);
  }
};

const handleIncoming = (incomingMessage) => {
  if (incomingMessage.name === MessageNameEnum.Status) {
    fixAth0Bytes(incomingMessage.data);
  }

  return incomingMessage;
};

module.exports = {
  handleIncoming,
};
