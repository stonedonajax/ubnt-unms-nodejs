'use strict';

const { evolve } = require('ramda');
const { map } = require('lodash/fp');
const { isNotString, isNotPlainObj, isNotUndefined } = require('ramda-adjunct');
const Cache = require('stale-lru-cache');

const { MessageNameEnum } = require('../../../../backends/airos/enums');

const CACHE_OPTIONS = { maxAge: 60, maxSize: 200 };

class StationsThroughputMiddleware {
  constructor() {
    // used for computing correct throughput
    this.lastUptime = 0;
    this.lastTimestamp = 0;
    this.stationsThroughput = new Cache(CACHE_OPTIONS);

    this.computeStationThroughput = this.computeStationThroughput.bind(this);
  }

  /* eslint-disable no-param-reassign */
  computeStationThroughput(station) {
    if (isNotPlainObj(station) || isNotString(station.mac)) {
      return station;
    }

    const lastData = this.stationsThroughput.get(station.mac);
    const currentData = {
      time: (station.stats.time_sec * 1000) + station.stats.time_msec,
      rxBytes: station.stats.rx_bytes,
      txBytes: station.stats.tx_bytes,
      rx: 0,
      tx: 0,
    };

    if (isNotUndefined(lastData)) {
      const timeElapsed = currentData.time - lastData.time;

      // sometimes the time doesn't change, in that case just copy tx/rx from the last pass
      if (timeElapsed > 0) {
        currentData.rx = (((currentData.rxBytes - lastData.rxBytes) * 1000) / timeElapsed) * 8;
        currentData.tx = (((currentData.txBytes - lastData.txBytes) * 1000) / timeElapsed) * 8;
      } else {
        currentData.rx = lastData.rx;
        currentData.tx = lastData.tx;
      }
    }

    station.stats.rxrate = currentData.rx;
    station.stats.txrate = currentData.tx;

    this.stationsThroughput.set(station.mac, currentData);
    return station;
  }
  /* eslint-enable no-param-reassign */

  handleIncoming(message) {
    switch (message.name) {
      case MessageNameEnum.StationList:
        return evolve({ data: map(this.computeStationThroughput) }, message);
      default:
      // do nothing
    }

    return message;
  }
}

const createMiddleware = () => new StationsThroughputMiddleware();

module.exports = createMiddleware;

