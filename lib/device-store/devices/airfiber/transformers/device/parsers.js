'use strict';

const {
  match, over, lensProp, pickBy, when, converge, subtract, ifElse, applySpec, always, unless, pathEq, find,
} = require('ramda');
const { getOr, flow, eq, nth, get, size, round, constant, lt, __, toInteger } = require('lodash/fp');
const { isNotNull, isNotNumber, stubUndefined } = require('ramda-adjunct');

const {
  StatusEnum, DeviceTypeEnum, WifiSecurityEnum, WifiAuthenticationEnum, DeviceCategoryEnum,
} = require('../../../../../enums');
const { isValidLocation } = require('../../../../../util/gps');
const {
  parseHwAntenna, parseHwDeviceName, parseHwFirmwareVersion, parseHwCpuUsage, parseHwMemoryUsage,
  parseHwUptime, parseHwNetworkMode, parseHwSSID, parseHwFrequency, parseHwAirView,
  parseHwPingStatsLatency, parseHwPingStatsErrors, parseHwLocation,
} = require('../../../../backends/airos/transformers/device/parsers');
const { parseHwIpAddress } = require('../../../../backends/ubridge/transformers/device/parsers');
const {
  parseHwInterfaceList, parseHwInterfaceStatistics,
} = require('../../../../backends/airos/transformers/interfaces/parsers');
const { deviceModelToSeries } = require('../../../../../feature-detection/airfiber');

// TODO(michal.sedlak@ubnt.com): Check if this actually works
// parsePlatformId :: FullFirmwareVersion -> PlatformId
//     FullFirmwareVersion = String
//     PlatformId = String
const parsePlatformId = flow(match(/^(\w+)\./), nth(1));

// parseHwPlatformId :: HwDevice -> PlatformId
//     HwDevice = Object
//     PlatformId = String
const parseHwPlatformId = flow(
  getOr('', ['firmware', 'version']),
  parsePlatformId
);

// parses wireless distance in meters
// parseHwDistance :: HwStatus -> Number
//     HwStatus = Object
const parseHwDistance = flow(
  getOr(0, ['data', 'wireless', 'distance']),
  when(lt(__, 150), constant(150)),
  when(eq(100000), constant(0))
);


// parseHwStationListCount :: HwStatus -> Number
const parseHwStationListCount = flow(get(['data', 'wireless', 'sta']), size);

// parseHwSignal :: HwStatus -> Number
//     HwStatus = Object
const parseHwSignal = flow(
  when(
    flow(parseHwStationListCount, eq(1)),
    converge(
      subtract,
      [
        getOr(0, ['data', 'wireless', 'sta', '0', 'receive_power0']),
        getOr(0, ['data', 'wireless', 'sta', '0', 'receive_cal0']),
      ]
    )
  ),
  when(isNotNumber, constant(null)),
  when(isNotNull, round)
);

// parseHwRemoteSignal :: HwStatus -> Number
//     HwStatus = Object
const parseHwRemoteSignal = flow(
  when(
    flow(parseHwStationListCount, eq(1)),
    converge(
      subtract,
      [
        getOr(0, ['data', 'wireless', 'sta', '0', 'remote', 'receive_power0']),
        getOr(0, ['data', 'wireless', 'sta', '0', 'remote', 'receive_cal0']),
      ]
    )
  ),
  when(isNotNumber, constant(null)),
  when(isNotNull, round)
);

// parseHwTransmitPower :: HwStatus -> Number
const parseHwTransmitPower = getOr(0, ['data', 'wireless', 'txpower']);

// parseHwWirelessMode :: HwStatus -> String
const parseHwWirelessMode = getOr(null, ['data', 'wireless', 'mode']);

// parseHwFrameLength :: HwStatus -> Number
const parseHwFrameLength = flow(
  getOr(null, ['data', 'wireless', 'frame_length']),
  when(isNotNull, toInteger)
);

// parseHwWirelessSecurity :: HwStatus -> String
const parseHwWirelessSecurity = ifElse(
  pathEq(['data', 'wireless', 'security'], 'WPA-PSK'),
  always(WifiSecurityEnum.WPA2),
  always(WifiSecurityEnum.None)
);

// parseHwWirelessAuthentication :: HwStatus -> String
const parseHwWirelessAuthentication = ifElse(
  pathEq(['data', 'wireless', 'security'], 'WPA-PSK'),
  always(WifiAuthenticationEnum.PSK),
  always(WifiAuthenticationEnum.None)
);

// parseWirelessMac :: HwStatus -> String|null
//  HwStatus = Object
const parseWirelessMac = flow(
  getOr([], ['data', 'interfaces']),
  find(pathEq(['ifname'], 'ath0')),
  getOr(null, 'hwaddr')
);

// parseHwChannelWidth :: HwStatus -> Number
//     HwStatus = Object
const parseHwChannelWidth = getOr(0, ['data', 'wireless', 'chanbw']);

// parseCountryCode :: HwBoardInfo -> String|Null
const parseCountryCode = flow(
  getOr(null, ['configuration', 'radio.countrycode']),
  when(isNotNull, toInteger)
);

// parsePreSharedKey :: HwDeviceConfig -> String
const parsePreSharedKey = getOr(null, ['configuration', 'wireless.1.security.psk']);

// parseHwStatusLocation :: HwDeviceConfig -> String
const parseHwStatusLocation = flow(
  applySpec({
    latitude: getOr(null, ['data', 'gps', 'lat']),
    longitude: getOr(null, ['data', 'gps', 'lon']),
  }),
  unless(isValidLocation, stubUndefined) // undefined so it's not overwritten by merge
);

/**
 * @param {Object} auxiliaries
 * @param {Object} hwDeviceConfig
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwDeviceConfig = (auxiliaries, hwDeviceConfig) => ({
  identification: {
    platformId: parseHwPlatformId(hwDeviceConfig.data),
    ipAddress: parseHwIpAddress(hwDeviceConfig),
  },
  overview: {
    location: parseHwLocation(hwDeviceConfig.data),
  },
  airfiber: {
    countryCode: parseCountryCode(hwDeviceConfig.data),
    antenna: parseHwAntenna(hwDeviceConfig.data),
    key: parsePreSharedKey(hwDeviceConfig.data),
  },
});

/**
 * @param {number} currentTimestamp
 * @param {Object} hwStatus
 * @param {Object} hwDeviceConfig
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwStatus = ({ currentTimestamp = Date.now(), hwDeviceConfig }, hwStatus) => ({
  identification: {
    name: parseHwDeviceName(hwStatus),
    firmwareVersion: parseHwFirmwareVersion(hwStatus),
    updated: currentTimestamp,
  },
  overview: {
    cpu: parseHwCpuUsage(hwStatus),
    ram: parseHwMemoryUsage(hwStatus),
    signal: parseHwSignal(hwStatus),
    distance: parseHwDistance(hwStatus),
    transmitPower: parseHwTransmitPower(hwStatus),
    lastSeen: currentTimestamp,
    uptime: parseHwUptime(hwStatus),
    location: parseHwStatusLocation(hwStatus), // will be undefined if invalid
  },
  mode: parseHwNetworkMode(hwStatus),
  airfiber: {
    ssid: parseHwSSID(hwStatus),
    frequency: parseHwFrequency(hwStatus),
    wirelessMode: parseHwWirelessMode(hwStatus),
    channelWidth: parseHwChannelWidth(hwStatus),
    security: parseHwWirelessSecurity(hwStatus),
    authentication: parseHwWirelessAuthentication(hwStatus),
    frameLength: parseHwFrameLength(hwStatus),
    mac: parseWirelessMac(hwStatus),
  },
  interfaces: parseHwInterfaceList({ currentTimestamp, hwDeviceConfig }, hwStatus),
});

/**
 * @param {Object} auxiliaries
 * @param {Object} hwAirView
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwFrequencyBands = (auxiliaries, hwAirView) => ({
  airfiber: {
    frequencyBands: parseHwAirView(hwAirView),
  },
});

/**
 * @param {number} currentTimestamp
 * @param {Object} hwStatus
 * @param {Object} hwPingStats
 * @return {CorrespondenceStatistics}
 */
const parseHwDeviceStatistics = ({ currentTimestamp = Date.now() }, { hwStatus, hwPingStats }) => {
  const cmStatistics = {
    timestamp: currentTimestamp,
    weight: 1,
    interfaces: parseHwInterfaceStatistics(hwStatus),
    stats: {
      signal: parseHwSignal(hwStatus),
      remoteSignal: parseHwRemoteSignal(hwStatus),
      ping: parseHwPingStatsLatency(hwPingStats),
      errors: parseHwPingStatsErrors(hwPingStats),
      ram: parseHwMemoryUsage(hwStatus),
      cpu: parseHwCpuUsage(hwStatus),
    },
  };

  // collect metrics only when it makes sense and at least one station device is connected
  return over(lensProp('stats'), pickBy(isNotNull), cmStatistics);
};

/**
 * @param {CorrespondenceSysInfo} sysInfo
 * @return {CorrespondenceDevice}
 */
const airFiberDeviceStub = sysInfo => ({
  identification: {
    id: sysInfo.deviceId,
    enabled: true,
    siteId: null,
    site: null,
    mac: sysInfo.mac,
    name: 'ubnt',
    serialNumber: null,
    firmwareVersion: sysInfo.firmwareVersion,
    platformId: sysInfo.platformId,
    model: sysInfo.model,
    updated: 0,
    authorized: false,
    type: DeviceTypeEnum.AirFiber,
    category: DeviceCategoryEnum.Wireless,
    ipAddress: null,
  },
  overview: {
    status: StatusEnum.Unauthorized,
    canUpgrade: false,
    location: null,
    isLocating: false,
    cpu: null,
    ram: null,
    voltage: null,
    temperature: null,
    signal: null,
    distance: null,
    biasCurrent: null,
    receivePower: null,
    receiveRate: null,
    receiveBytes: null,
    receiveErrors: null,
    receiveDropped: null,
    transmitPower: null,
    transmitRate: null,
    transmitBytes: null,
    transmitErrors: null,
    transmitDropped: null,
    lastSeen: 0,
    uptime: null,
    gateway: null,
  },
  meta: null,
  firmware: null,
  upgrade: null,
  mode: null,
  onu: null,
  olt: null,
  aircube: null,
  airmax: null,
  airfiber: {
    // TODO(michal.sedlak@ubnt.com): MORE
    series: deviceModelToSeries(sysInfo.model),
    wirelessMode: null,
    ssid: null,
    frequency: null,
    antenna: null,
    frequencyBands: null,
    channelWidth: null,
    countryCode: null,
    security: null,
    authentication: null,
    key: null,
    mac: null,
  },
  interfaces: [],
  unmsSettings: null,
});

module.exports = {
  airFiberDeviceStub,
  parseHwDeviceConfig,
  parsePlatformId,
  parseHwPlatformId,
  parseHwDeviceStatistics,
  parseHwFrequencyBands,
  parseHwStatus,
};
