'use strict';

const { getOr, map, now, flow, multiply, round } = require('lodash/fp');
const { when, equals, applySpec, converge, subtract } = require('ramda');
const { stubNull } = require('ramda-adjunct');

const { parseDeviceVendor } = require('../../../../../transformers/device/vendor/parser');

const parseHwStation = applySpec({
  timestamp: now,
  name: getOr(null, ['remote', 'hostname']),
  mac: getOr(null, ['mac']),
  vendor: flow(getOr('', ['mac']), parseDeviceVendor),
  radio: stubNull,
  ipAddress: flow(getOr(null, ['lastip']), when(equals('0.0.0.0'), stubNull)),
  upTime: getOr(null, ['uptime']),
  latency: getOr(null, ['tx_latency']),
  distance: flow(getOr(null, ['distance']), when(equals(100000), stubNull)),
  rxBytes: getOr(null, ['stats', 'rx_bytes']),
  txBytes: getOr(null, ['stats', 'tx_bytes']),
  rxRate: flow(getOr(0, ['stats', 'rxrate']), round),
  txRate: flow(getOr(0, ['stats', 'txrate']), round),
  rxSignal: converge(subtract, [getOr(0, ['receive_power0']), getOr(0, ['receive_cal0'])]),
  txSignal: converge(subtract, [getOr(0, ['remote', 'receive_power0']), getOr(0, ['remote', 'receive_cal0'])]),
  downlinkCapacity: flow(getOr(null, ['airmax', 'downlink_capacity']), multiply(1000)),
  uplinkCapacity: flow(getOr(null, ['airmax', 'uplink_capacity']), multiply(1000)),
  deviceIdentification: stubNull,
});

// parseHwStationList :: HwStations -> Array<CmStation>
//     HwStations = Object
//     CmStation = CorrespondenceModelStation = Object
const parseHwStationList = map(parseHwStation);

module.exports = {
  parseHwStationList,
};
