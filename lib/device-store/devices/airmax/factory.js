'use strict';

const createDefaultCommDevice = require('../default');

const buildDevice = require('./methods/device/buildDevice');
const restartDevice = require('./methods/device/restartDevice');
const systemUpgrade = require('../../backends/airos/methods/system/upgrade');

const getStations = require('./methods/stations/getStations');

const setSetup = require('../../backends/ubridge/methods/unms/setSetup');
const createBackup = require('../../backends/airos/methods/backup/createBackup');
const applyBackup = require('../../backends/airos/methods/backup/applyBackup');

const shellConnect = require('../../backends/ubridge/methods/shell/shellConnect');
const shellDisconnect = require('../../backends/ubridge/methods/shell/shellDisconnect');
const shellCmd = require('../../backends/ubridge/methods/shell/shellCmd');
const shellResponse = require('../../backends/ubridge/methods/shell/shellResponse');

const iperfKeepalive = require('../../backends/ubridge/methods/speed-test/iperfKeepalive');
const startIperfServer = require('../../backends/ubridge/methods/speed-test/startIperfServer');
const startIperfClient = require('./methods/speed-test/startIperfClient');
const stopIperf = require('../../backends/ubridge/methods/speed-test/stopIperf');

const createDevice = (connection, sysInfo) => {
  const commDevice = createDefaultCommDevice(sysInfo, connection);

  Object.assign(commDevice, {
    buildDevice: buildDevice(sysInfo),
    restartDevice: restartDevice(sysInfo),

    systemUpgrade: systemUpgrade(sysInfo),

    setSetup: setSetup(sysInfo),
    createBackup: createBackup(sysInfo),
    applyBackup: applyBackup(sysInfo),

    getStations: getStations(sysInfo),

    shellConnect: shellConnect(sysInfo),
    shellDisconnect: shellDisconnect(sysInfo),
    shellCmd: shellCmd(sysInfo),
    shellResponse: shellResponse(sysInfo),

    iperfKeepalive: iperfKeepalive(sysInfo),
    startIperfClient: startIperfClient(sysInfo),
    startIperfServer: startIperfServer(sysInfo),
    stopIperf: stopIperf(sysInfo),
  });

  return commDevice;
};

module.exports = createDevice;
