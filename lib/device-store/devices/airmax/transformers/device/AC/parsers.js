'use strict';

const { over, lensProp, when, always } = require('ramda');
const { isNotNull, isNotNumber } = require('ramda-adjunct');
const { getOr, flow, pickBy, round, get, size, filter, map, uniq } = require('lodash/fp');

const { WirelessModeEnum } = require('../../../../../../enums');
const { isLogicalWifiInterfaceType } = require('../../../../../backends/airos/transformers/utils');
const {
  parseHwDeviceName, parseHwFirmwareVersion, parseHwCpuUsage, parseHwMemoryUsage,
  parseHwUptime, parseHwPingStatsErrors, parseHwPingStatsLatency, parseHwSSID, parseHwFrequency,
  parseHwNetworkMode, parseHwChainMaskToChains, parseHwAirView,
} = require('../../../../../backends/airos/transformers/device/parsers');
const { parseHwDistance, parseLanStatus, parseWifiMac, parseHwSecurity } = require('../parsers');
const {
  parseHwInterfaceList, parseHwInterfaceStatistics,
} = require('../../../../../backends/airos/transformers/interfaces/parsers');


// parseHwStationListCount :: HwStatus -> Number
const parseHwStationListCount = flow(get(['data', 'wireless', 'sta']), size);

// parseHwSignal :: HwStatus -> Number
//     HwStatus = Object
const parseHwSignal = flow(
  when(
    hwStatus => parseHwStationListCount(hwStatus) === 1,
    getOr(null, ['data', 'wireless', 'sta', '0', 'signal'])
  ),
  when(isNotNumber, always(null)),
  when(isNotNull, round)
);

// parseHwRemoteSignal :: HwStatus -> Number
//     HwStatus = Object
const parseHwRemoteSignal = flow(
  when(
    hwStatus => parseHwStationListCount(hwStatus) === 1,
    getOr(null, ['data', 'wireless', 'sta', '0', 'remote', 'signal'])
  ),
  when(isNotNumber, always(null)),
  when(isNotNull, round)
);

// parseHwChannelWidth :: HwStatus -> Number
//     HwStatus = Object
const parseHwChannelWidth = getOr(0, ['data', 'wireless', 'chanbw']);

// parseHwFrequencyCenter :: HwStatus -> Number
const parseHwFrequencyCenter = getOr(0, ['data', 'wireless', 'center1_freq']);

// parseHwTransmitPower :: HwStatus -> Number
const parseHwTransmitPower = getOr(0, ['data', 'wireless', 'txpower']);

// parseHwTransmitChains :: HwStatus -> Number
const parseHwTransmitChains = flow(getOr(0, ['data', 'wireless', 'tx_chainmask']), parseHwChainMaskToChains);

// parseHwReceiveChains :: HwStatus -> Number
const parseHwReceiveChains = flow(getOr(0, ['data', 'wireless', 'rx_chainmask']), parseHwChainMaskToChains);

// parseHwApMacAddress :: HwStatus -> String|Null
const parseHwApMacAddress = getOr(null, ['data', 'wireless', 'apmac']);

// parseHwNoiseFloor :: HwStatus -> Number
const parseHwNoiseFloor = getOr(0, ['data', 'wireless', 'noisef']);

// parseHwWlanInterfaceMacAddress :: HwStatus -> Array<String>|Null
const parseHwWlanInterfaceMacAddress = flow(
  get(['data', 'interfaces']),
  filter(flow(getOr(null, ['ifname']), isLogicalWifiInterfaceType)),
  map(getOr(null, ['hwaddr'])),
  uniq
);

// parseHwWirelessMode :: HwStatus -> Mode
//     HwStatus = Object
//     Mode = String
const parseHwWirelessMode = (hwStatus) => {
  const mode = getOr(null, ['data', 'wireless', 'mode'], hwStatus);
  const compact11n = getOr(null, ['data', 'wireless', 'compat_11n'], hwStatus);

  if (mode === WirelessModeEnum.ApPtmp && compact11n) { return WirelessModeEnum.ApPtmpAirmaxMixed }
  if (mode === WirelessModeEnum.ApPtmp && !compact11n) { return WirelessModeEnum.ApPtmpAirmaxAc }

  return mode;
};

/**
 * @param {Object} auxiliaries
 * @param {number?} [auxiliaries.currentTimestamp]
 * @param {Object?} [auxiliaries.hwDeviceConfig]
 * @param {Object} hwStatus
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwStatus = ({ currentTimestamp = Date.now(), hwDeviceConfig }, hwStatus) => {
  const interfaces = parseHwInterfaceList({ currentTimestamp, hwDeviceConfig }, hwStatus);

  return {
    identification: {
      name: parseHwDeviceName(hwStatus),
      firmwareVersion: parseHwFirmwareVersion(hwStatus),
      updated: currentTimestamp,
    },
    overview: {
      cpu: parseHwCpuUsage(hwStatus),
      ram: parseHwMemoryUsage(hwStatus),
      signal: parseHwSignal(hwStatus),
      distance: parseHwDistance(hwStatus),
      transmitPower: parseHwTransmitPower(hwStatus),
      lastSeen: currentTimestamp,
      uptime: parseHwUptime(hwStatus),
    },
    mode: parseHwNetworkMode(hwStatus),
    airmax: {
      ssid: parseHwSSID(hwStatus),
      mac: parseWifiMac(hwStatus),
      frequency: parseHwFrequency(hwStatus),
      frequencyCenter: parseHwFrequencyCenter(hwStatus),
      security: parseHwSecurity(hwStatus),
      channelWidth: parseHwChannelWidth(hwStatus),
      noiseFloor: parseHwNoiseFloor(hwStatus),
      stationsCount: parseHwStationListCount(hwStatus),
      wirelessMode: parseHwWirelessMode(hwStatus),
      remoteSignal: parseHwRemoteSignal(hwStatus),
      lanStatus: parseLanStatus(interfaces),
      transmitChains: parseHwTransmitChains(hwStatus),
      receiveChains: parseHwReceiveChains(hwStatus),
      apMac: parseHwApMacAddress(hwStatus),
      wlanMac: parseHwWlanInterfaceMacAddress(hwStatus),
    },
    interfaces,
  };
};

/**
 * @param {Object} auxiliaries
 * @param {Object} hwAirView
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwFrequencyBands = (auxiliaries, hwAirView) => ({
  airmax: {
    frequencyBands: parseHwAirView(hwAirView),
  },
});

/**
 * @param {number} currentTimestamp
 * @param {Object} hwStatus
 * @param {Object} hwPingStats
 * @return {CorrespondenceStatistics}
 */
const parseHwDeviceStatistics = ({ currentTimestamp = Date.now() }, { hwStatus, hwPingStats }) => {
  const cmStatistics = {
    timestamp: currentTimestamp,
    weight: 1,
    interfaces: parseHwInterfaceStatistics(hwStatus),
    stats: {
      signal: parseHwSignal(hwStatus),
      remoteSignal: parseHwRemoteSignal(hwStatus),
      ping: parseHwPingStatsLatency(hwPingStats),
      errors: parseHwPingStatsErrors(hwPingStats),
      ram: parseHwMemoryUsage(hwStatus),
      cpu: parseHwCpuUsage(hwStatus),
    },
  };

  // collect metrics only when it makes sense and at least one station device is connected
  return over(lensProp('stats'), pickBy(isNotNull), cmStatistics);
};


module.exports = {
  parseHwStatus,
  parseHwDeviceStatistics,
  parseHwFrequencyBands,
};
