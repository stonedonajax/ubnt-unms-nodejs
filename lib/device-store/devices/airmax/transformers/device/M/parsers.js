'use strict';

const { isNotNull, isNotNumber } = require('ramda-adjunct');
const { lensProp, over, flip, divide, multiply, when, always } = require('ramda');
const { getOr, flow, pickBy, size, round } = require('lodash/fp');

const { roundTo } = require('../../../../../../util');
const { WirelessModeEnum } = require('../../../../../../enums');
const {
  parseHwInterfaceStatistics, parseHwInterfaceList,
} = require('../../../../../backends/airos/transformers/interfaces/parsers');
const { parseHwDistance, parseLanStatus, parseWifiMac, parseHwSecurity } = require('../parsers');
const {
  parseHwDeviceName, parseHwFirmwareVersion, parseHwCpuUsage, parseHwMemoryUsage, parseHwNetworkMode,
  parseHwUptime, parseHwPingStatsLatency, parseHwPingStatsErrors, parseHwSSID, parseHwFrequency,
} = require('../../../../../backends/airos/transformers/device/parsers');


// parseHwStationListCount :: HwStationList -> Number
const parseHwStationListCount = size;

// parseHwSignal :: HwStationList -> Number|Null
//     HwStationList = Object
const parseHwSignal = flow(
  when(
    (stationList => stationList.length === 1),
    getOr(null, [0, 'signal'])
  ),
  when(isNotNumber, always(null)),
  when(isNotNull, round)
);

// parseHwRemoteSignal :: HwStationList -> Number|Null
//     HwStationList = Object
const parseHwRemoteSignal = flow(
  when(
    (stationList => stationList.length === 1),
    getOr(null, [0, 'remote', 'signal'])
  ),
  when(isNotNumber, always(null)),
  when(isNotNull, round)
);

// parseHwChannelWidth :: HwStatus -> Number
//     HwStatus = Object
const parseHwChannelWidth = getOr(0, ['data', 'wireless', 'chanbw']);

// parseHwCCQ :: HwStatus -> Number
//     HwStatus = Object
const parseHwCCQ = getOr(0, ['data', 'wireless', 'ccq']);

// parseHwCCQInPercent :: HwStatus -> Percent
//     HwStatus = Object
//     Percent = Number<0, 100>
const parseHwCCQInPercent = flow(parseHwCCQ, flip(divide)(1000), multiply(100), roundTo(2));

// parseHwWds :: HwStatus -> Boolean
//     HwStatus = Object
const parseHwWds = flow(
  getOr(false, ['wireless', 'wds']),
  Boolean
);

// parseHwPollingEnabled :: HwStatus -> Boolean
//     HwStatus = Object
const parseHwPollingEnabled = flow(getOr(false, ['data', 'wireless', 'polling', 'enabled']), Boolean);

// parseHwWirelessMode :: HwStatus -> Mode
//     HwStatus = Object
//     Mode = String
const parseHwWirelessMode = (hwStatus) => {
  const mode = getOr(null, ['data', 'wireless', 'mode'], hwStatus);
  const apRepeaterEnabled = getOr(null, ['data', 'wireless', 'aprepeater'], hwStatus);

  if (apRepeaterEnabled) { return WirelessModeEnum.ApRep }

  return mode;
};

// parseHwDeviceStatistics :: (Auxiliaries, HwStatus) -> CorrespondenceStatistics
//     Auxiliaries = {hwPingStats: HwPingStats, hwStationList: HwStationList}
//     HwStatus = Object
//     CorrespondenceStatistics = Object
const parseHwDeviceStatistics = ({ currentTimestamp = Date.now() }, { hwStatus, hwStationList, hwPingStats }) => {
  const cmStatistics = {
    timestamp: currentTimestamp,
    weight: 1,
    interfaces: parseHwInterfaceStatistics(hwStatus),
    stats: {
      signal: parseHwSignal(hwStationList),
      remoteSignal: parseHwRemoteSignal(hwStationList),
      ping: parseHwPingStatsLatency(hwPingStats),
      errors: parseHwPingStatsErrors(hwPingStats),
      ram: parseHwMemoryUsage(hwStatus),
      cpu: parseHwCpuUsage(hwStatus),
    },
  };

  // collect metrics only when it makes sense and at least one station device is connected
  return over(lensProp('stats'), pickBy(isNotNull), cmStatistics);
};

/**
 * @param {Object} auxiliaries
 * @param {number?} [auxiliaries.currentTimestamp]
 * @param {Object?} [auxiliaries.hwDeviceConfig]
 * @param {Object} hwStatus
 * @param {Object} hwStationList
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwStatus = ({ currentTimestamp = Date.now(), hwDeviceConfig }, { hwStatus, hwStationList }) => {
  const interfaces = parseHwInterfaceList({ currentTimestamp, hwDeviceConfig }, hwStatus);

  return {
    identification: {
      name: parseHwDeviceName(hwStatus),
      firmwareVersion: parseHwFirmwareVersion(hwStatus),
      updated: currentTimestamp,
    },
    overview: {
      cpu: parseHwCpuUsage(hwStatus),
      ram: parseHwMemoryUsage(hwStatus),
      signal: parseHwSignal(hwStationList),
      distance: parseHwDistance(hwStatus),
      lastSeen: currentTimestamp,
      uptime: parseHwUptime(hwStatus),
    },
    mode: parseHwNetworkMode(hwStatus),
    airmax: {
      ssid: parseHwSSID(hwStatus),
      mac: parseWifiMac(hwStatus),
      antenna: getOr(null, ['data', 'wireless', 'antenna'], hwStatus),
      frequency: parseHwFrequency(hwStatus),
      security: parseHwSecurity(hwStatus),
      channelWidth: parseHwChannelWidth(hwStatus),
      ccq: parseHwCCQInPercent(hwStatus),
      wds: parseHwWds(hwStatus),
      stationsCount: parseHwStationListCount(hwStationList),
      wirelessMode: parseHwWirelessMode(hwStatus),
      remoteSignal: parseHwRemoteSignal(hwStationList),
      lanStatus: parseLanStatus(interfaces),
      polling: {
        enabled: parseHwPollingEnabled(hwStatus),
      },
    },
    interfaces,
  };
};

module.exports = {
  parseHwStatus,
  parseHwDeviceStatistics,
};
