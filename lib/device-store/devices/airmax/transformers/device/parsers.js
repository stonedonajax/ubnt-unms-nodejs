'use strict';

const {
  match, when, pathEq, ifElse, toLower, has, propEq, prop, propSatisfies, pathSatisfies, isNil, both,
} = require('ramda');
const {
  getOr, __, flow, nth, cond, T, lt, eq, constant, find, split, isUndefined, toInteger, isArray, includes,
} = require('lodash/fp');
const { stubNull, isNotNull } = require('ramda-adjunct');

const { deviceModelToSeries } = require('../../../../../feature-detection/airmax');
const { StatusEnum, DeviceTypeEnum, DeviceCategoryEnum, WifiAuthenticationEnum } = require('../../../../../enums');
const { isCidr } = require('../../../../../util/ip');
const { parseHwAntenna, parseHwLocation } = require('../../../../backends/airos/transformers/device/parsers');
const { parseHwIpAddress, isValidIp } = require('../../../../backends/ubridge/transformers/device/parsers');

// parsePlatformId :: FullFirmwareVersion -> PlatformId
//     FullFirmwareVersion = String
//     PlatformId = String
const parsePlatformId = flow(match(/^(\w+)\./), nth(1));

// parseHwPlatformId :: HwDevice -> PlatformId
//     HwDevice = Object
//     PlatformId = String
const parseHwPlatformId = flow(
  getOr('', ['firmware', 'version']),
  parsePlatformId
);

// parseLanStatus :: CmInterfaceList -> CmLanStatus
//     HwStatus = Object
//     CmInterfaceList = Object
const parseLanStatus = (cmInterfaceList) => {
  const eth0 = find(pathEq(['identification', 'name'], 'eth0'), cmInterfaceList);
  const eth1 = find(pathEq(['identification', 'name'], 'eth1'), cmInterfaceList);

  return {
    eth0: {
      description: eth0.status.description,
      plugged: eth0.status.plugged,
      speed: eth0.status.speed,
      duplex: eth0.status.duplex,
    },
    eth1: (() => {
      if (isUndefined(eth1)) { return null }

      return {
        description: eth1.status.description,
        plugged: eth1.status.plugged,
        speed: eth1.status.speed,
        duplex: eth1.status.duplex,
      };
    })(),
  };
};

// parsePreSharedKey :: HwBoardInfo -> String|Null
const parsePreSharedKey = ifElse(
  pathEq(['configuration', 'aaa.status'], 'enabled'),
  getOr(null, ['configuration', 'aaa.1.wpa.psk']),
  stubNull
);

// parseCountryCode :: HwBoardInfo -> String|Null
const parseCountryCode = flow(
  getOr(null, ['configuration', 'radio.countrycode']),
  when(isNotNull, toInteger)
);

const parseAuthenticationType = ifElse(
  pathEq(['configuration', 'aaa.status'], 'enabled'),
  flow(
    getOr('', ['configuration', 'aaa.1.wpa.key.1.mgmt']),
    split('-'),
    nth(1),
    cond([
      [eq('EAP'), constant(WifiAuthenticationEnum.Enterprise)],
      [eq('PSK'), constant(WifiAuthenticationEnum.PSK)],
      [T, constant(WifiAuthenticationEnum.None)],
    ])
  ),
  constant(WifiAuthenticationEnum.None)
);

// parseWifiMac :: HwStatus -> String|null
//  HwStatus = Object
const parseWifiMac = flow(
  getOr([], ['data', 'interfaces']),
  find(pathEq(['ifname'], 'ath0')),
  getOr(null, 'hwaddr')
);

// parseHwSecurity :: HwStatus -> String|Null
//     HwStatus = Object
const parseHwSecurity = flow(
  getOr(null, ['data', 'wireless', 'security']),
  toLower // some devices, E.g. NanoBeam 2AC returns upper case value
);

// parses wireless distance in meters
// parseHwDistance :: HwStatus -> Number
//     HwStatus = Object
const parseHwDistance = flow(
  getOr(0, ['data', 'wireless', 'distance']),
  when(lt(__, 150), constant(150)),
  when(eq(100000), constant(0))
);

const isValidManagementIp = both(isCidr, isValidIp);

const isVlan = includes('.');

const parseManagementIpAddress = (hwDeviceConfig) => {
  const configuration = getOr({}, ['data', 'configuration'], hwDeviceConfig);

  let index = 1;
  let ipAddress = null;
  let managementInterface = null;

  while (has(`netconf.${index}.devname`, configuration)) {
    if (
      propEq(`netconf.${index}.role`, 'mlan', configuration) &&
      propEq(`netconf.${index}.status`, 'enabled', configuration) &&
      propSatisfies(isVlan, `netconf.${index}.devname`, configuration)
    ) {
      managementInterface = prop(`netconf.${index}.devname`, configuration);
      break;
    }
    index += 1;
  }

  if (managementInterface !== null && pathSatisfies(isArray, ['data', managementInterface, 'cidr'], hwDeviceConfig)) {
    ipAddress = find(isValidManagementIp, hwDeviceConfig.data[managementInterface].cidr);
  }

  return isNil(ipAddress) ? parseHwIpAddress(hwDeviceConfig) : ipAddress;
};

/**
 * @param {number} currentTimestamp
 * @param {Object} hwDeviceConfig
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwDeviceConfig = ({ currentTimestamp = Date.now() }, hwDeviceConfig) => ({
  identification: {
    platformId: parseHwPlatformId(hwDeviceConfig.data),
    ipAddress: parseManagementIpAddress(hwDeviceConfig),
  },
  overview: {
    location: parseHwLocation(hwDeviceConfig.data),
  },
  airmax: {
    antenna: parseHwAntenna(hwDeviceConfig.data),
    countryCode: parseCountryCode(hwDeviceConfig.data),
    authentication: parseAuthenticationType(hwDeviceConfig.data),
    key: parsePreSharedKey(hwDeviceConfig.data),
  },
});

/**
 * @param {CorrespondenceSysInfo} sysInfo
 * @return {CorrespondenceDevice}
 */
const airMaxDeviceStub = sysInfo => ({
  identification: {
    id: sysInfo.deviceId,
    enabled: true,
    siteId: null,
    site: null,
    mac: sysInfo.mac,
    name: 'ubnt',
    serialNumber: null,
    firmwareVersion: sysInfo.firmwareVersion,
    platformId: sysInfo.platformId,
    model: sysInfo.model,
    updated: 0,
    authorized: false,
    type: DeviceTypeEnum.AirMax,
    category: DeviceCategoryEnum.Wireless,
    ipAddress: null,
  },
  overview: {
    status: StatusEnum.Unauthorized,
    canUpgrade: false,
    isLocating: false,
    location: null,
    cpu: null,
    ram: null,
    voltage: null,
    temperature: null,
    signal: null,
    distance: null,
    biasCurrent: null,
    receivePower: null,
    receiveRate: null,
    receiveBytes: null,
    receiveErrors: null,
    receiveDropped: null,
    transmitPower: null,
    transmitRate: null,
    transmitBytes: null,
    transmitErrors: null,
    transmitDropped: null,
    lastSeen: 0,
    uptime: null,
    gateway: null,
  },
  meta: null,
  firmware: null,
  upgrade: null,
  mode: null,
  onu: null,
  olt: null,
  aircube: null,
  airfiber: null,
  airmax: {
    series: deviceModelToSeries(sysInfo.model),
    ssid: null,
    frequency: null,
    frequencyBands: null,
    frequencyCenter: null,
    security: null,
    authentication: null,
    channelWidth: null,
    countryCode: null,
    antenna: null,
    noiseFloor: null,
    ccq: null,
    wds: null,
    stationsCount: null,
    wirelessMode: null,
    remoteSignal: null,
    lanStatus: null,
    transmitChains: null,
    receiveChains: null,
    apMac: null,
    wlanMac: null,
    mac: null,
    key: null,
    polling: {
      enabled: true,
    },
  },
  interfaces: [],
  unmsSettings: null,
});

module.exports = {
  airMaxDeviceStub,
  parseManagementIpAddress,
  parseHwDeviceConfig,
  parsePlatformId,
  parseHwPlatformId,
  parseHwSecurity,
  parseHwDistance,
  parseLanStatus,
  parseWifiMac,
};
