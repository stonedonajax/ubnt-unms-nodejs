'use strict';

const { Reader: reader } = require('monet');

const { parseSysInfoMessage } = require('../../backends/airos/transformers/socket/parsers');
const { epowerDeviceStub } = require('./transformers/device/parsers');

const createPingMiddleware = require('../../backends/ubridge/middlewares/ping');
const parseMessagesMiddleware = require('../../backends/airos/middlewares/parse-messages');
const parseEpowerMessagesMiddleware = require('./middlewares/parse-messages');
const createEventsMiddleware = require('./middlewares/events');
const createInterfaceThroughputMiddleware = require('../../backends/airos/middlewares/interface-throughput');

const createCommDevice = require('./factory');

/**
 * @param {WebSocketConnection} connection
 * @param {CorrespondenceIncomingMessage} sysInfoMessage
 * @return {Reader.<Observable.<CommDevice>>}
 */
const bootstrapConnection = (connection, sysInfoMessage) => reader(
  ({ messageHub, periodicActions }) => {
    const sysInfo = parseSysInfoMessage(sysInfoMessage);
    const cmEpowerStub = epowerDeviceStub(sysInfo);

    connection.use(parseMessagesMiddleware); // handle incoming airOs messages
    connection.use(parseEpowerMessagesMiddleware); // handle incoming edge power messages

    const commDevice = createCommDevice(connection, sysInfo);

    return commDevice.buildDevice(cmEpowerStub)
      .map((cmEpower) => {
        connection.use(createInterfaceThroughputMiddleware());
        connection.use(createEventsMiddleware({ messageHub, periodicActions, cmEpower, commDevice }));
        connection.use(createPingMiddleware({ periodicActions, commDevice }));

        return commDevice;
      });
  }
);

module.exports = bootstrapConnection;
