'use strict';

const { Observable } = require('rxjs/Rx');
const { partial } = require('lodash/fp');

const { pingStatsRequest } = require('../../../backends/ubridge/messages');
const { deviceStatusRequest } = require('../../../backends/airos/messages');
const parsers = require('../transformers/device/parsers');

const parseHwStatus = partial(parsers.parseHwStatus, [{}]);
const parseHwDeviceStatistics = partial(parsers.parseHwDeviceStatistics, [{}]);

class EpowerEventsMiddleware {
  constructor(messageHub, periodicActions, cmDevice, commDevice) {
    this.deviceId = commDevice.deviceId;
    this.commDevice = commDevice;
    this.cmDevice = cmDevice;
    this.periodicActions = periodicActions;
    this.messageHub = messageHub;
  }

  notifyDeviceUpdate(cmDeviceUpdate) {
    const messages = this.messageHub.messages;
    this.messageHub.publish(messages.epowerUpdateEvent(this.deviceId, cmDeviceUpdate));
  }

  notifyStats(cmStats) {
    const messages = this.messageHub.messages;
    this.messageHub.publish(messages.epowerStatisticsEvent(this.deviceId, cmStats));
  }

  statusAction() {
    return Observable.forkJoin(
      this.connection.rpc(pingStatsRequest()),
      this.connection.rpc(deviceStatusRequest())
    )
      .do(([hwPingStats, hwStatus]) => {
        this.notifyDeviceUpdate(parseHwStatus(hwStatus));
        this.notifyStats(parseHwDeviceStatistics({ hwPingStats, hwStatus }));
      })
      .catch(error => this.connection.handleError(error, true));
  }

  setupPeriodicActions() {
    const statusAction = this.statusAction.bind(this);

    this.periodicActions.schedule(this.deviceId, statusAction, 'epowerUpdateInterval');
  }

  handleEstablish(connection) {
    const messages = this.messageHub.messages;
    this.connection = connection;

    return Observable.defer(() => this.messageHub.publishAndConfirm(messages.epowerRegisterEvent(this.cmDevice)))
      .do(() => {
        this.cmDevice = null;
        this.setupPeriodicActions();
      });
  }

  handleClose() {
    const messages = this.messageHub.messages;
    const deviceId = this.deviceId;

    this.periodicActions.stop(deviceId);
    this.messageHub.publish(messages.epowerCloseEvent(deviceId));
  }
}

const createMiddleware = ({ messageHub, periodicActions, cmEpower, commDevice }) =>
  new EpowerEventsMiddleware(messageHub, periodicActions, cmEpower, commDevice);

module.exports = createMiddleware;
