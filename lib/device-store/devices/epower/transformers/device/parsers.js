'use strict';

const { match, over, lensProp, pickBy, any, assocPath, max, when, multiply } = require('ramda');
const { getOr, flow, nth, isNull, floor, reduce, isNumber, round } = require('lodash/fp');
const { isNotNull, isNotArray } = require('ramda-adjunct');

const { StatusEnum, DeviceTypeEnum, DeviceCategoryEnum } = require('../../../../../enums');
const {
  parseHwDeviceName, parseHwFirmwareVersion, parseHwUptime, parseHwPingStatsLatency,
  parseHwPingStatsErrors,
} = require('../../../../backends/airos/transformers/device/parsers');
const { parseHwIpAddress } = require('../../../../backends/ubridge/transformers/device/parsers');
const { parseHwInterfaceList } = require('../../../../backends/airos/transformers/interfaces/parsers');

// TODO(michal.sedlak@ubnt.com): Check if this actually works
// parsePlatformId :: FullFirmwareVersion -> PlatformId
//     FullFirmwareVersion = String
//     PlatformId = String
const parsePlatformId = flow(match(/^(\w+)\./), nth(1));

// parseHwPlatformId :: HwDevice -> PlatformId
//     HwDevice = Object
//     PlatformId = String
const parseHwPlatformId = flow(
  getOr('', ['firmware', 'version']),
  parsePlatformId
);

// parseHwCurrent :: HwStatus -> Number
//     HwStatus = Object
const parseHwCurrent = flow(
  getOr(null, ['data', 'sensors', 'current']),
  when(isNumber, max(0))
);

// parseHwVoltage :: HwStatus -> Number
//     HwStatus = Object
const parseHwVoltage = flow(
  getOr(null, ['data', 'sensors', 'voltage']),
  when(isNumber, max(0))
);

// parseHwPower :: HwStatus -> Number
//     HwStatus = Object
const parseHwPower = flow(
  getOr(null, ['data', 'sensors', 'power']),
  when(isNumber, flow(multiply(0.001), round))
);

// parseHwConsumption :: HwStatus -> Number
//     HwStatus = Object
const parseHwConsumption = flow(
  getOr(null, ['data', 'sensors', 'power_meter']),
  when(isNumber, multiply(1000))
);

const parseBatteryVoltage = flow(
  getOr(null, ['battery', 'voltage']),
  when(isNumber, multiply(1000))
);

const parseHwPsuStatistics = flow(
  getOr([], ['data', 'sensors', 'PSU']),
  reduce((accumulator, psu) => assocPath([`psu${psu.id}`], {
    weight: 1,
    stats: {
      voltage: parseBatteryVoltage(psu),
    },
  }, accumulator), {})
);

// TODO(michal.sedlak@ubnt.com): using loadavg is not very precise, find other way of getting cpu usage
const parseHwCpuUsage = (hwStatus) => {
  const loadAvg = getOr(null, ['data', 'host', 'loadavg'], hwStatus);

  if (isNotArray(loadAvg)) { return 0 }

  return Math.min(100, floor(loadAvg[0] * 100));
};

const parseHwMemoryUsage = (hwStatus) => {
  const total = getOr(null, ['data', 'host', 'meminfo', 'total'], hwStatus);
  const free = getOr(null, ['data', 'host', 'meminfo', 'free'], hwStatus);

  if (any(isNull, [total, free])) { return 0 }

  const used = total - free;

  return floor((used / total) * 100);
};

/**
 * @param {number} currentTimestamp
 * @param {Object} hwDeviceConfig
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwDeviceConfig = ({ currentTimestamp = Date.now() }, hwDeviceConfig) => ({
  identification: {
    platformId: parseHwPlatformId(hwDeviceConfig.data),
    ipAddress: parseHwIpAddress(hwDeviceConfig),
  },
});

/**
 * @param {Object} auxiliaries
 * @param {number} [auxiliaries.currentTimestamp]
 * @param {Object} [auxiliaries.hwDeviceConfig]
 * @param {Object} hwStatus
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwStatus = ({ currentTimestamp = Date.now(), hwDeviceConfig }, hwStatus) => ({
  identification: {
    name: parseHwDeviceName(hwStatus),
    firmwareVersion: parseHwFirmwareVersion(hwStatus),
    updated: currentTimestamp,
  },
  overview: {
    cpu: parseHwCpuUsage(hwStatus),
    ram: parseHwMemoryUsage(hwStatus),
    lastSeen: currentTimestamp,
    uptime: parseHwUptime(hwStatus),
  },
  interfaces: parseHwInterfaceList({ currentTimestamp }, hwStatus),
});

/**
 * @param {number} currentTimestamp
 * @param {Object} hwStatus
 * @param {Object} hwPingStats
 * @return {CorrespondenceStatistics}
 */
const parseHwDeviceStatistics = ({ currentTimestamp = Date.now() }, { hwStatus, hwPingStats }) => {
  const cmStatistics = {
    timestamp: currentTimestamp,
    weight: 1,
    psu: parseHwPsuStatistics(hwStatus),
    stats: {
      ping: parseHwPingStatsLatency(hwPingStats),
      errors: parseHwPingStatsErrors(hwPingStats),
      ram: parseHwMemoryUsage(hwStatus),
      cpu: parseHwCpuUsage(hwStatus),
      current: parseHwCurrent(hwStatus),
      voltage: parseHwVoltage(hwStatus),
      power: parseHwPower(hwStatus),
      consumption: parseHwConsumption(hwStatus),
    },
  };

  // collect metrics only when it makes sense and at least one station device is connected
  return over(lensProp('stats'), pickBy(isNotNull), cmStatistics);
};

/**
 * @param {CorrespondenceSysInfo} sysInfo
 * @return {CorrespondenceDevice}
 */
const epowerDeviceStub = sysInfo => ({
  identification: {
    id: sysInfo.deviceId,
    enabled: true,
    siteId: null,
    site: null,
    mac: sysInfo.mac,
    name: 'ubnt',
    serialNumber: null,
    firmwareVersion: sysInfo.firmwareVersion,
    platformId: sysInfo.platformId,
    model: sysInfo.model,
    updated: 0,
    authorized: false,
    type: DeviceTypeEnum.Epower,
    category: DeviceCategoryEnum.Accessories,
    ipAddress: null,
  },
  overview: {
    status: StatusEnum.Unauthorized,
    canUpgrade: false,
    location: null,
    isLocating: false,
    cpu: null,
    ram: null,
    voltage: null,
    temperature: null,
    signal: null,
    distance: null,
    biasCurrent: null,
    receivePower: null,
    receiveRate: null,
    receiveBytes: null,
    receiveErrors: null,
    receiveDropped: null,
    transmitPower: null,
    transmitRate: null,
    transmitBytes: null,
    transmitErrors: null,
    transmitDropped: null,
    lastSeen: 0,
    uptime: null,
    gateway: null,
  },
  meta: null,
  firmware: null,
  upgrade: null,
  mode: null,
  onu: null,
  olt: null,
  aircube: null,
  airmax: null,
  airfiber: null,
  interfaces: [],
  unmsSettings: null,
});

module.exports = {
  epowerDeviceStub,
  parseHwDeviceConfig,
  parsePlatformId,
  parseHwPlatformId,
  parseHwDeviceStatistics,
  parseHwStatus,
};
