'use strict';

const { getOr } = require('lodash/fp');
const { evolve } = require('ramda');

const { MessageNameEnum } = require('../../../../backends/airos/enums');
const { MessageTypeEnum } = require('../../../../../transformers/socket/enums');

const parseStatusMessage = evolve({
  data: getOr(null, 'status'),
});

/**
 * @function parseIncomingMessage
 * @param {CorrespondenceIncomingMessage} incomingMessage
 * @return {Object}
 */
const parseIncomingMessage = (incomingMessage) => {
  if (incomingMessage.type === MessageTypeEnum.Rpc) {
    switch (incomingMessage.name) {
      case MessageNameEnum.Status:
        return parseStatusMessage(incomingMessage);
      default:
      // do nothing
    }
  }

  return incomingMessage;
};

module.exports = {
  parseIncomingMessage,
};
