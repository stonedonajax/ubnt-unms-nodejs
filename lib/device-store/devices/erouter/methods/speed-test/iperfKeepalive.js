'use strict';

const { gt } = require('semver');

const iperfKeepalive = require('../../../../backends/ubridge/methods/speed-test/iperfKeepalive');

module.exports = (sysInfo) => {
  if (gt(sysInfo.firmwareVersion, '1.9.999')) {
    return iperfKeepalive(sysInfo);
  }

  return null;
};
