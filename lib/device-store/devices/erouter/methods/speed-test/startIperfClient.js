'use strict';

const { gt } = require('semver');

const startIperfClient = require('../../../../backends/ubridge/methods/speed-test/startIperfClient');

module.exports = (sysInfo) => {
  if (gt(sysInfo.firmwareVersion, '1.9.999')) {
    return startIperfClient(sysInfo);
  }

  return null;
};
