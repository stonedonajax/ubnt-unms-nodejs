'use strict';

const { gt } = require('semver');

const startIperfServer = require('../../../../backends/ubridge/methods/speed-test/startIperfServer');

module.exports = (sysInfo) => {
  if (gt(sysInfo.firmwareVersion, '1.9.999')) {
    return startIperfServer(sysInfo);
  }

  return null;
};
