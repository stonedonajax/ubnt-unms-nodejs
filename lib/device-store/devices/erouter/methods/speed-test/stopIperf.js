'use strict';

const { gt } = require('semver');

const stopIperf = require('../../../../backends/ubridge/methods/speed-test/stopIperf');

module.exports = (sysInfo) => {
  if (gt(sysInfo.firmwareVersion, '1.9.999')) {
    return stopIperf(sysInfo);
  }

  return null;
};
