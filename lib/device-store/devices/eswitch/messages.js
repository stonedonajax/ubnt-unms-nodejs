'use strict';

const { partial } = require('lodash/fp');

const { rpcRequest, commandRequest } = require('../../backends/ubridge/messages');
const { MessageNameEnum } = require('./enums');

/* eslint-disable max-len */
const DEVICE_IP_CMD = '/sbin/ifconfig -a | grep inet | grep -v inet6 | grep -v ::1/128 | grep -v :127.0.0 | tail -n1 | awk \'{ print $2,",",$4 }\'';

const DEVICE_CONFIG_CMD = [
  `echo ${MessageNameEnum.GetConfig}`,
  'echo -e -n "[config]\\nversion="',
  'cat /etc/version 2>&1',
  'echo -e -n "hostname="',
  'hostname',
  'echo -e -n "network="',
  DEVICE_IP_CMD,
].join('; ');
/* eslint-enable max-len */

/* eslint-disable max-len */
const GET_NETWORK_INFO = [
  `ip link show | sed -rn '/^[0-9]+: /bi;H;$bi;b;:i;x;
y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
s/^[0-9]+: ([a-z0-9.]+)/name=\\1\\n /
s/(([a-f0-9]{2}:){5}[a-f0-9]{2})/\\nmac=\\1\\n /
p
' | sed -r '/^$/d;/(^[[:space:]].*)/d' | awk 'BEGIN { FS="=" } ; {
 if ($1 ~ /name/) {
  print "["$2"]"
  l[$2]=0;
  path = "ifconfig "$2" 2>/dev/null | grep -e \\"inet \\""
  path | getline cfg
  nf = split(cfg, a, /[ ]+/)
  for (i = 3; i <= nf; i++) {
    split(a[i], b, ":")
    printf "%s=%s\\n", tolower(b[1]), tolower(b[2])
  }
 }
 print $1"="$2
} ; END { print "[interfaces]"; for (i in l) { print "names[]="i } }'`,
  'echo [routes]',
  'route | grep default | awk \'{ print $8"="$2 }\'',
].join('; ');
/* eslint-enable max-len */

const deviceConfigRequest = partial(commandRequest, [DEVICE_CONFIG_CMD]);

const networkInfoCommand = partial(commandRequest, [`echo ${MessageNameEnum.GetNetworkConfig}; ${GET_NETWORK_INFO}`]);

const subscribeEventsRequest = partial(rpcRequest, [
  {
    SUBSCRIBE: [
      { name: 'system-stats' },
      { name: 'config-change' },
    ],
  },
  'subscribeEvents',
  'sys',
]);

module.exports = {
  deviceConfigRequest,
  networkInfoCommand,
  subscribeEventsRequest,
};
