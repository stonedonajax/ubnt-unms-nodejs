'use strict';

const { Observable } = require('rxjs/Rx');
const { constant } = require('lodash/fp');

const { networkInfoCommand } = require('../../messages');
const { sysInfoRequest } = require('../../../../backends/ubridge/messages');
const { parseHwSysInfo, parseHwNetworkInfo } = require('../../transformers/device/parsers');
const { mergeDeviceUpdate } = require('../../../../../transformers/device/mergers');

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceDevice} [cmDeviceStub]
 * @return {Observable.<CorrespondenceDevice>}
 */
function buildDevice(cmDeviceStub = {}) {
  return Observable.forkJoin(this.connection.cmd(networkInfoCommand()), this.connection.rpc(sysInfoRequest()))
    .map(([hwNetworkInfo, hwSysInfo]) => {
      const result = mergeDeviceUpdate(
        cmDeviceStub,
        parseHwSysInfo({}, hwSysInfo),
        parseHwNetworkInfo({}, hwNetworkInfo)
      );

      return result;
    });
}

module.exports = constant(buildDevice);
