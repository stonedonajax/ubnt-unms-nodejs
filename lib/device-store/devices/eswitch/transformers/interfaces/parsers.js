'use strict';

const { getOr, map, curry, compact } = require('lodash/fp');

const { interfaceNameToType, interfaceNameToPosition, isManagementInterfaceType } = require('../utils');
const { IpAddressTypeEnum } = require('../../../../../enums');

/**
 * @function
 * @param {Object} hwInterface
 * @return {boolean}
 */
const isMeasurableInterface = isManagementInterfaceType;


const parseHwInterfaceAddresses = (hwInterface) => {
  const addresses = getOr([], ['cidr'], hwInterface);
  // TODO(michal.sedlak@ubnt.com): we can't reliably detect if the address static
  return addresses.map(cidr => ({ type: IpAddressTypeEnum.Static, cidr }));
};

// parseHwInterface :: Object -> HwInterface -> Correspondence
const parseHwInterface = curry(({ currentTimestamp = Date.now(), hwDeviceConfig }, hwInterface) => ({
  identification: {
    position: interfaceNameToPosition(hwInterface.name),
    type: interfaceNameToType(hwInterface.name),
    name: hwInterface.name,
    description: null,
    mac: hwInterface.mac,
  },
  statistics: {
    timestamp: currentTimestamp,
    rxrate: 0,
    rxbytes: 0,
    txrate: 0,
    txbytes: 0,
    dropped: 0,
    errors: 0,
    previousTxbytes: 0,
    previousRxbytes: 0,
    previousDropped: 0,
    previousErrors: 0,
  },
  addresses: parseHwInterfaceAddresses(hwInterface),
  mtu: 1500,
  poe: null,
  enabled: true,
  proxyARP: null,
  switch: null,
  speed: null,
  bridgeGroup: null,
  onSwitch: false, // TODO(michal.sedlak@ubnt.com): parse correct values if possible
  isSwitchedPort: false,
  status: {
    autoneg: false,
    duplex: true,
    description: null,
    plugged: true,
    speed: 100,
    sfp: null,
    lag: null,
  },
  vlan: null,
  pppoe: null,
  pon: null,
  bridge: null,
  ospf: {
    ospfCapable: false,
    ospfConfig: null,
  },
  // TODO(michal.sedlak@ubnt.com): HACK, not part of correspondence
  shouldMerge: true,
}));

// parseHwInterfaceList :: (Object, HwStatus) -> Array.<Correspondence>
const parseHwInterfaceList = (auxiliaries, hwNetworkInfo) => {
  const hwInterfaces = getOr([], ['data', 'interfaces', 'names'], hwNetworkInfo)
    .filter(isMeasurableInterface)
    .map(name => getOr(null, ['data', name], hwNetworkInfo));

  return map(parseHwInterface(auxiliaries), compact(hwInterfaces));
};

module.exports = {
  parseHwInterface,
  parseHwInterfaceList,
};
