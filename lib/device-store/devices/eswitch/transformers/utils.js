'use strict';

const { match } = require('ramda');
const { startsWith, flow, head, parseInt } = require('lodash/fp');

const { InterfaceIdentificationTypeEnum } = require('../../../../enums');

// interfaceNameToPosition :: String -> Number
const interfaceNameToPosition = flow(match(/\d+$/), head, parseInt(10));

// isManagementInterfaceType :: InterfaceName -> Boolean
//    InterfaceName = String
const isManagementInterfaceType = startsWith('dtl');

// interfaceNameToType :: String -> InterfaceType
//     InterfaceType = String|Null
const interfaceNameToType = (interfaceName) => {
  if (isManagementInterfaceType(interfaceName)) {
    return InterfaceIdentificationTypeEnum.Ethernet;
  }
  return null;
};

module.exports = {
  isManagementInterfaceType,
  interfaceNameToType,
  interfaceNameToPosition,
};
