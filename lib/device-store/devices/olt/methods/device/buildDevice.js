'use strict';

const { Observable } = require('rxjs/Rx');
const { constant, isPlainObject } = require('lodash/fp');
const { allPass, pathSatisfies } = require('ramda');

const { deviceConfigRequest, networkInfoCommand } = require('../../../../backends/vyatta/messages');
const { parseHwDevice } = require('../../../../backends/vyatta/transformers/device/parsers');
const { parseHwLayer2 } = require('../../transformers/device/parsers');
const { mergeDeviceUpdate } = require('../../../../../transformers/device/mergers');

const CONFIG_RETRY_DELAY = 1000;

const isValidDeviceConfig = allPass([
  pathSatisfies(isPlainObject, ['data', 'interfaces']),
  pathSatisfies(isPlainObject, ['data', 'service']),
  pathSatisfies(isPlainObject, ['data', 'system']),
]);

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceDevice} cmDeviceStub
 * @return {Observable.<CorrespondenceDevice>}
 */
function buildDevice(cmDeviceStub) {
  // config might not be available after boot, retry until available
  const deviceConfig$ = this.connection.rpc(deviceConfigRequest())
    .first(isValidDeviceConfig)
    .retryWhen(errors => errors.take(2).delay(CONFIG_RETRY_DELAY));
  const networkInfo$ = this.connection.cmd(networkInfoCommand());

  return Observable.forkJoin(deviceConfig$, networkInfo$)
    .map(([hwDeviceConfig, hwNetworkInfo]) => mergeDeviceUpdate(
        cmDeviceStub,
        parseHwDevice({ features: this.features, hwNetworkInfo }, hwDeviceConfig),
        parseHwLayer2({}, hwDeviceConfig)
      ));
}

module.exports = constant(buildDevice);
