'use strict';

const { constant } = require('lodash/fp');
const { assocPath } = require('ramda');

const { setConfigRequest } = require('../../../../backends/vyatta/messages');

/**
 * Update device interface.
 *
 * @memberOf CommDevice
 * @this CommDevice
 * @param {HwInterfaceInstructionsDescriptor} updateInstructions
 * @return {Observable.<CorrespondenceDhcpServer[]>}
 */
function updateInterface(updateInstructions) {
  let setData = {};
  let deleteData = {};

  if (updateInstructions.lagEnabled) {
    deleteData = assocPath(['layer2', 'lag', 'disable'], "''", deleteData);
    setData = assocPath(['layer2', 'lag', 'mode'], 'static', setData);
  } else {
    setData = assocPath(['layer2', 'lag', 'disable'], "''", setData);
  }

  return this.connection.rpc(setConfigRequest(setData, deleteData));
}

module.exports = constant(updateInterface);
