'use strict';

const { constant /* , partial */ } = require('lodash/fp');

require('../../../../../util/observable');
const { rpcRequest } = require('../../../../backends/ubridge/messages');
const { parseOnuClients } = require('../../transformers/onu/parsers');

/**
 * @memberOf CommDevice
 * @this CommDevice
 * @param {string} onuSerialNumber
 * @return {Observable.<CorrespondenceOnuClients[]>}
 */
function getOnuClients(onuSerialNumber) {
  return this.connection.rpc(rpcRequest({ GET_ONU_MACS: { serial: onuSerialNumber } }, 'getOnuMacs', 'olt'))
    .map(parseOnuClients(({})));
}

module.exports = constant(getOnuClients);

