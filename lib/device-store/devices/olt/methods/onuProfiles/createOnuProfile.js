'use strict';

const Boom = require('boom');
const { Observable } = require('rxjs/Rx');
const { constant, max, add, getOr, isNull, isEmpty, get, defaultTo } = require('lodash/fp');
const { pipe, pathEq, filter, pathSatisfies, split, map, path, test, concat, assocPath } = require('ramda');
const { isNotEmpty, isNotNull } = require('ramda-adjunct');

const { NetworkModeEnum } = require('../../../../../enums');
const { setOnuConfigRequest } = require('../../messages');

const nextProfileId = pipe(
  filter(pathSatisfies(test(/^profile-[0-9]+$/i), ['id'])),
  map(pipe(path(['id']), split('-'), path([1]), Number)),
  max,
  defaultTo(0),
  add(1),
  String,
  concat('profile-')
);

/**
 * Create ONU profile.
 *
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceOnuProfile} cmProfile
 * @return {Observable.<CorrespondenceDhcpServer[]>}
 */
function createOnuProfile(cmProfile) {
  return this.getOnuProfiles()
    .mergeMap((cmOnuProfiles) => {
      const isNameConflict = cmOnuProfiles.some(pathEq(['name'], cmProfile.name));

      if (isNameConflict) {
        return Observable.throw(Boom.conflict('Profile with given name already exists.'));
      }

      const profileId = nextProfileId(cmOnuProfiles);

      const basePath = ['onu-profiles', profileId];

      // delete ONU profile prior to updating - does nothing for insert
      const deleteData = assocPath(basePath, "''", {});

      // set data relevant for all cases
      let setData = assocPath(basePath, {
        'admin-password': cmProfile.adminPassword,
        mode: cmProfile.mode,
        name: cmProfile.name,
        'lan-address': defaultTo('', get(['lanAddress'], cmProfile)),
        'lan-provisioned': cmProfile.lanProvisioned,
        port: { 1: { 'link-speed': getOr('', ['linkSpeed'], cmProfile) } },
      }, {});

      if (cmProfile.mode === NetworkModeEnum.Bridge) {
        setData = assocPath([...basePath, 'bridge-mode', 'port', '1'], {
          'native-vlan': cmProfile.bridge.nativeVlan || '',
          'include-vlan': isNotEmpty(cmProfile.bridge.includeVlans) ? cmProfile.bridge.includeVlans : '',
        }, setData);
      }

      if (cmProfile.mode === NetworkModeEnum.Router) {
        const dhcpPoolStart = getOr(null, ['router', 'dhcpPoolStart'], cmProfile);
        const dhcpPoolEnd = getOr(null, ['router', 'dhcpPoolEnd'], cmProfile);
        const dhcpPool = isNull(dhcpPoolStart) || isNull(dhcpPoolEnd) ? '' : `${dhcpPoolStart}-${dhcpPoolEnd}`;

        const dnsResolver = [];
        const primaryDns = getOr(null, ['router', 'primaryDns'], cmProfile);
        const secondaryDns = getOr(null, ['router', 'secondaryDns'], cmProfile);
        if (isNotNull(primaryDns)) { dnsResolver.push(primaryDns) }
        if (isNotNull(secondaryDns)) { dnsResolver.push(secondaryDns) }

        setData = assocPath([...basePath, 'router-mode'], {
          gateway: defaultTo('', get(['router', 'gateway'], cmProfile)),
          'wan-vlan': defaultTo('', get(['router', 'wanVlan'], cmProfile)),
          'wan-mode': defaultTo('', get(['router', 'wanMode'], cmProfile)),
          'dns-resolver': isEmpty(dnsResolver) ? '' : dnsResolver,
          'dhcp-server': defaultTo('', get(['router', 'dhcpServer'], cmProfile)),
          'dhcp-relay': defaultTo('', get(['router', 'relayAddress'], cmProfile)),
          'dhcp-lease-time': defaultTo('', get(['router', 'dhcpLeaseTime'], cmProfile)),
          'dns-proxy-enable': getOr(false, ['router', 'dnsProxyEnable'], cmProfile),
          'dhcp-pool': dhcpPool,
        }, setData);
      }

      return this.connection.rpc(setOnuConfigRequest(setData, deleteData));
    });
}

module.exports = constant(createOnuProfile);

