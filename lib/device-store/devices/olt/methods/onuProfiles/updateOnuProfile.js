'use strict';

const Boom = require('boom');
const { Observable } = require('rxjs/Rx');
const { constant, getOr, isNull, isEmpty, defaultTo, get } = require('lodash/fp');
const { both, pathEq, complement, assocPath } = require('ramda');
const { isNotEmpty, isNotNull } = require('ramda-adjunct');

const { NetworkModeEnum } = require('../../../../../enums');
const { setOnuConfigRequest } = require('../../messages');

/**
 * Update ONU profile.
 *
 * @memberOf CommDevice
 * @this CommDevice
 * @param {CorrespondenceOnuProfile} cmProfile
 * @param {string} profileId
 * @return {Observable.<CorrespondenceDhcpServer[]>}
 */
function updateOnuProfile(cmProfile, profileId) {
  return this.getOnuProfiles()
    .mergeMap((cmOnuProfiles) => {
      const isProfileExists = cmOnuProfiles.some(pathEq(['id'], profileId));

      if (!isProfileExists) {
        return Observable.throw(Boom.notFound('Profile does not exists.'));
      }

      const isNameConflict = cmOnuProfiles.some(both(
        pathEq(['name'], cmProfile.name),
        complement(pathEq(['id'], profileId))
      ));

      if (isNameConflict) {
        return Observable.throw(Boom.conflict('Profile with given name already exists.'));
      }

      const basePath = ['onu-profiles', profileId];

      // delete ONU profile prior to updating - does nothing for insert
      const deleteData = assocPath(basePath, "''", {});

      // set data relevant for all cases
      let setData = assocPath(basePath, {
        'admin-password': cmProfile.adminPassword,
        mode: cmProfile.mode,
        name: cmProfile.name,
        'lan-address': getOr('', ['lanAddress'], cmProfile),
        'lan-provisioned': cmProfile.lanProvisioned,
        port: { 1: { 'link-speed': getOr('', ['linkSpeed'], cmProfile) } },
      }, {});

      if (cmProfile.mode === NetworkModeEnum.Bridge) {
        setData = assocPath([...basePath, 'bridge-mode', 'port', '1'], {
          'native-vlan': cmProfile.bridge.nativeVlan || '',
          'include-vlan': isNotEmpty(cmProfile.bridge.includeVlans) ? cmProfile.bridge.includeVlans : '',
        }, setData);
      }

      if (cmProfile.mode === NetworkModeEnum.Router) {
        const dhcpPoolStart = getOr(null, ['router', 'dhcpPoolStart'], cmProfile);
        const dhcpPoolEnd = getOr(null, ['router', 'dhcpPoolEnd'], cmProfile);
        const dhcpPool = isNull(dhcpPoolStart) || isNull(dhcpPoolEnd) ? '' : `${dhcpPoolStart}-${dhcpPoolEnd}`;

        const dnsResolver = [];
        const primaryDns = getOr(null, ['router', 'primaryDns'], cmProfile);
        const secondaryDns = getOr(null, ['router', 'secondaryDns'], cmProfile);
        if (isNotNull(primaryDns)) { dnsResolver.push(primaryDns) }
        if (isNotNull(secondaryDns)) { dnsResolver.push(secondaryDns) }

        setData = assocPath([...basePath, 'router-mode'], {
          gateway: defaultTo('', get(['router', 'gateway'], cmProfile)),
          'wan-vlan': getOr('', ['router', 'wanVlan'], cmProfile),
          'wan-mode': getOr('', ['router', 'wanMode'], cmProfile),
          'dns-resolver': isEmpty(dnsResolver) ? '' : dnsResolver,
          'dhcp-server': getOr('', ['router', 'dhcpServer'], cmProfile),
          'dhcp-relay': defaultTo('', get(['router', 'relayAddress'], cmProfile)),
          'dhcp-lease-time': getOr('', ['router', 'dhcpLeaseTime'], cmProfile),
          'dns-proxy-enable': getOr(false, ['router', 'dnsProxyEnable'], cmProfile),
          'dhcp-pool': dhcpPool,
        }, setData);
      }

      return this.connection.rpc(setOnuConfigRequest(setData, deleteData));
    });
}

module.exports = constant(updateOnuProfile);

