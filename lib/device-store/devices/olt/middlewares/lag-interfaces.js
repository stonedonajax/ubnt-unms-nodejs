'use strict';

const { has, get, isPlainObject, flow } = require('lodash/fp');
const { pathSatisfies, both, complement } = require('ramda');

const { MessageNameEnum } = require('../../../backends/vyatta/enums');

const createLagInterfaceData = message => ({
  mode: message.data.layer2.lag.mode,
});

const isLagEnabled = both(
  pathSatisfies(isPlainObject, ['data', 'layer2', 'lag']),
  flow(get(['data', 'layer2', 'lag']), complement(has('disable')))
);

const handleIncoming = (message) => {
  switch (message.name) {
    case MessageNameEnum.GetInterfaces:
    case MessageNameEnum.GetConfig:
      if (isLagEnabled(message)) {
        // eslint-disable-next-line no-param-reassign
        message.data.interfaces.nni.lag1 = createLagInterfaceData(message);
      }
      break;
    default:
    // do nothing
  }

  return message;
};

module.exports = {
  handleIncoming,
};

