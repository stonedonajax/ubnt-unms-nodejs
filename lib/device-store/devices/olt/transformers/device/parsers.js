'use strict';

const { assoc, ifElse } = require('ramda');
const { isNotPlainObj, stubNull } = require('ramda-adjunct');
const { flow, getOr, get, has, reduce, omitBy, isUndefined, isPlainObject, toPairs } = require('lodash/fp');

// parseVlansInterfaces :: HwVlanInterfaces -> Object
//    Interfaces = Object
const parseVlansInterfaces = ifElse(
  isPlainObject,
  flow(
    toPairs,
    reduce((acc, [key, value]) => {
      const vlanNative = get(['vlan-native'], value);
      return assoc(key, { vlanNative: isUndefined(vlanNative) ? null : Number(vlanNative) }, acc);
    }, {})
  ),
  stubNull
);

// parseVlansVlanIds :: VlanIds -> Object
//    VlanIds = Object
const parseVlansVlanIds = omitBy(isNotPlainObj);

// parseVlans :: HwLayer2 -> Object
//    HwLayer2 = Object
const parseVlans = (hwLayer2) => {
  const vlans = getOr(null, ['vlans'], hwLayer2);

  if (isNotPlainObj(vlans)) {
    return null;
  }

  return {
    interface: parseVlansInterfaces(hwLayer2.vlans.interface),
    vlanId: parseVlansVlanIds(hwLayer2.vlans['vlan-id']),
  };
};

// parseHwLayer2 :: HwDeviceConfig -> Object
//    Layer2 = Object
const parseHwLayer2 = (auxiliaries, hwDeviceConfig) => {
  const hwLayer2 = getOr(null, ['data', 'layer2'], hwDeviceConfig);
  if (isNotPlainObj(hwLayer2)) {
    return {
      olt: { layer2: null },
    };
  }

  return {
    olt: {
      layer2: {
        lag: {
          enabled: !has('disable', hwLayer2.lag),
          mode: getOr(null, ['lag', 'mode'], hwLayer2),
        },
        vlans: parseVlans(hwLayer2),
      },
    },
  };
};

module.exports = {
  parseHwLayer2,
};
