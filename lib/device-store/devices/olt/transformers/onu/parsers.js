'use strict';

const { flow, map, get, curry } = require('lodash/fp');
const { parseDeviceVendor } = require('../../../../../transformers/device/vendor/parser');

// parseMacAddress :: String -> Object
const parseMacAddress = macAddress => ({
  mac: macAddress,
  vendor: parseDeviceVendor(macAddress),
});

// parseOnuClients :: HwOnuClients -> [Object]
const parseOnuClients = curry((auxiliaries, hwOnuClients) => flow(
    get(['data', 'GET_ONU_MACS', 'seen-macs']),
    map(parseMacAddress)
  )(hwOnuClients)
);

module.exports = {
  parseOnuClients,
};
