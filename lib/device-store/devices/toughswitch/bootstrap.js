'use strict';

const { Reader: reader } = require('monet');

const { parseSysInfoMessage } = require('../../backends/airos/transformers/socket/parsers');
const { toughSwitchDeviceStub } = require('./transformers/device/parsers');

const createPingMiddleware = require('../../backends/ubridge/middlewares/ping');
const parseMessagesMiddleware = require('../../backends/airos/middlewares/parse-messages');
const createEventsMiddleware = require('./middlewares/events');
const createThroughputMiddleware = require('./middlewares/throughput');

const createCommDevice = require('./factory');

/**
 * @param {WebSocketConnection} connection
 * @param {CorrespondenceIncomingMessage} sysInfoMessage
 * @return {Reader.<Observable.<CommDevice>>}
 */
const bootstrapConnection = (connection, sysInfoMessage) => reader(
  ({ messageHub, periodicActions }) => {
    const sysInfo = parseSysInfoMessage(sysInfoMessage);
    const cmToughSwitchStub = toughSwitchDeviceStub(sysInfo);

    connection.use(parseMessagesMiddleware); // handle incoming airos messages

    const commDevice = createCommDevice(connection, sysInfo);

    return commDevice.buildDevice(cmToughSwitchStub)
      .map((cmToughSwitch) => {
        connection.use(createThroughputMiddleware({ cmToughSwitch }));
        connection.use(createEventsMiddleware({ messageHub, periodicActions, cmToughSwitch, commDevice }));
        connection.use(createPingMiddleware({ periodicActions, commDevice }));

        return commDevice;
      });
  }
);

module.exports = bootstrapConnection;
