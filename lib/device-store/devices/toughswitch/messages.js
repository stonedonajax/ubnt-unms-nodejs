'use strict';

const { partial } = require('lodash/fp');

const { commandRequest } = require('../../backends/ubridge/messages');

// eslint-disable-next-line max-len
const STATUS_CMD = 'echo "getStatus"; c="cat /tmp/status_json"; s=`$c`; while [ -z "$s" ]; do sleep 1; s=`$c`; done; echo -n "$s"';

const deviceStatusRequestCommand = partial(commandRequest, [STATUS_CMD]);

module.exports = {
  deviceStatusRequestCommand,
};
