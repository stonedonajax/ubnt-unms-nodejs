'use strict';

const { Observable } = require('rxjs/Rx');
const { partial } = require('lodash/fp');

const { pingStatsRequest } = require('../../../backends/ubridge/messages');
const { deviceStatusRequestCommand } = require('../messages');
const parsers = require('../transformers/device/parsers');

const parseHwStatus = partial(parsers.parseHwStatus, [{}]);
const parseHwDeviceStatistics = partial(parsers.parseHwDeviceStatistics, [{}]);

class ToughSwitchEventsMiddleware {
  constructor(messageHub, periodicActions, cmDevice, commDevice) {
    this.deviceId = commDevice.deviceId;
    this.commDevice = commDevice;
    this.cmDevice = cmDevice;
    this.periodicActions = periodicActions;
    this.messageHub = messageHub;
  }

  notifyDeviceUpdate(cmDeviceUpdate) {
    const messages = this.messageHub.messages;
    this.messageHub.publish(messages.toughSwitchUpdateEvent(this.deviceId, cmDeviceUpdate));
  }

  notifyStats(cmStats) {
    const messages = this.messageHub.messages;
    this.messageHub.publish(messages.toughSwitchStatisticsEvent(this.deviceId, cmStats));
  }

  statusAction() {
    return Observable.forkJoin(
      this.connection.rpc(pingStatsRequest()),
      this.connection.cmd(deviceStatusRequestCommand())
    )
      .do(([hwPingStats, hwStatus]) => {
        this.notifyDeviceUpdate(parseHwStatus(hwStatus));
        this.notifyStats(parseHwDeviceStatistics({ hwPingStats, hwStatus }));
      })
      .catch(error => this.connection.handleError(error, true));
  }

  setupPeriodicActions() {
    const statusAction = this.statusAction.bind(this);

    this.periodicActions.schedule(this.deviceId, statusAction, 'toughSwitchUpdateInterval');
  }

  handleEstablish(connection) {
    const messages = this.messageHub.messages;
    this.connection = connection;

    return Observable.defer(() => this.messageHub.publishAndConfirm(messages.toughSwitchRegisterEvent(this.cmDevice)))
      .do(() => {
        this.cmDevice = null;
        this.setupPeriodicActions();
      });
  }

  handleClose() {
    const messages = this.messageHub.messages;
    const deviceId = this.deviceId;

    this.periodicActions.stop(deviceId);
    this.messageHub.publish(messages.toughSwitchCloseEvent(deviceId));
  }
}

const createMiddleware = ({ messageHub, periodicActions, cmToughSwitch, commDevice }) =>
  new ToughSwitchEventsMiddleware(messageHub, periodicActions, cmToughSwitch, commDevice);

module.exports = createMiddleware;
