'use strict';

const { evolve, assoc } = require('ramda');
const { map, getOr, isUndefined, merge, toPairs, flow } = require('lodash/fp');

const { MessageNameEnum } = require('../../../backends/airos/enums');
const { DeviceModelEnum } = require('../../../../enums');

const handleOverflow = (newValue, oldValue) => {
  if (newValue < oldValue) {
    // overflow happened
    if (oldValue > 0x7FFFFFFF) {
      return (0xFFFFFFFF - oldValue) + newValue;
    }

    if ((newValue + oldValue) < 0x8FFFFFFF) {
      return newValue; // not sure about this
    }

    return (0x7FFFFFFF - oldValue) + newValue;
  }

  return newValue - oldValue;
};

class ToughSwitchThroughputMiddleware {
  constructor(cmToughSwitch) {
    this.model = cmToughSwitch.identification.model;

    // used for computing correct throughput
    this.lastUptime = 0;
    this.lastTimestamp = 0;
    this.portThroughput = {};
  }

  computeThroughput(timeElapsed, port) {
    const position = port.position;
    const currentData = {
      rxBytes: port.stats.RxGoodByte,
      txBytes: port.stats.TxByte,
    };
    let lastData = this.portThroughput[position];

    if (isUndefined(lastData)) {
      lastData = currentData;
      this.portThroughput[position] = lastData;
    }

    const rates = {
      rxrate: ((handleOverflow(currentData.rxBytes, lastData.rxBytes) * 1000) / timeElapsed) * 8,
      txrate: ((handleOverflow(currentData.txBytes, lastData.txBytes) * 1000) / timeElapsed) * 8,
    };

    this.portThroughput[position] = currentData;

    return merge(port, {
      stats: rates,
    });
  }

  normalizePorts(stats) {
    const portCount = this.model === DeviceModelEnum.TSWPoe ? 5 : 8;

    const hwPorts = [];
    for (const [position, hwPort] of toPairs(stats)) {
      const positionNumber = Number(position);
      if (position <= portCount) {
        hwPorts.push(assoc('position', positionNumber, hwPort));
      }
    }

    return hwPorts;
  }

  handleStatusMessage(message) {
    const data = message.data;
    const uptime = getOr(0, ['host', 'uptime'], data);
    const timestamp = getOr(null, ['host', 'timestamp'], data);

    let timeElapsed = 0;

    const uptimeDiff = (uptime - this.lastUptime) * 1000;

    // replace timestamp with uptime
    if (timestamp === null) {
      timeElapsed = uptimeDiff;
    } else {
      timeElapsed = timestamp < this.lastTimestamp
        ? ((0xFFFFFFFF - this.lastTimestamp) + 1) + timestamp
        : timestamp - this.lastTimestamp;

      this.lastTimestamp = timestamp;
    }

    if (timeElapsed > (uptimeDiff + 1000)) {
      timeElapsed = uptimeDiff;
    }

    this.lastUptime = uptime;

    // sanity check
    if (timeElapsed <= 0) {
      return message;
    }

    return evolve({
      data: {
        stats: flow(
          this.normalizePorts.bind(this),
          map(this.computeThroughput.bind(this, timeElapsed))
        ),
      },
    }, message);
  }

  handleIncoming(message) {
    switch (message.name) {
      case MessageNameEnum.Status:
        return this.handleStatusMessage(message);
      default:
      // do nothing
    }

    return message;
  }
}

const createMiddleware = ({ cmToughSwitch }) => new ToughSwitchThroughputMiddleware(cmToughSwitch);

module.exports = createMiddleware;

