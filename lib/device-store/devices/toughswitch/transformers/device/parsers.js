'use strict';

const { over, lensProp, pickBy, converge, ifElse, pathSatisfies } = require('ramda');
const { getOr, constant, flow, join, get, isString } = require('lodash/fp');
const { list, isNotNull } = require('ramda-adjunct');

const { StatusEnum, DeviceTypeEnum, DeviceCategoryEnum, FirmwarePlatformIdEnum } = require('../../../../../enums');
const {
  parseHwPingStatsLatency, parseHwPingStatsErrors, parseHwCpuUsage, parseHwMemoryUsage, parseHwUptime,
} = require('../../../../backends/airos/transformers/device/parsers');
const { parseHwIpAddress } = require('../../../../backends/ubridge/transformers/device/parsers');
const { parseCommFirmwareVersion } = require('../../../../../transformers/semver/parsers');
const { parseHwPortList, parseHwInterfaceList, parseHwPortStatistics } = require('../interfaces/parsers');

// parsePlatformId :: FullFirmwareVersion -> PlatformId
//     FullFirmwareVersion = String
//     PlatformId = String
const parsePlatformId = constant(FirmwarePlatformIdEnum.SW);

const parseHwDeviceName = ifElse(
  pathSatisfies(isString, ['configuration', 'resolv.host.1.name']),
  get(['configuration', 'resolv.host.1.name']),
  flow(converge(list, [get(['board', 'board.name']), get(['board', 'board.subtype'])]), join(' '))
);

const parseHwFirmwareVersion = flow(
  getOr(null, ['firmware', 'version']),
  parseCommFirmwareVersion
);

/**
 * @param {number} currentTimestamp
 * @param {Object} hwDeviceConfig
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwDeviceConfig = ({ currentTimestamp = Date.now() }, hwDeviceConfig) => {
  const interfaces = parseHwInterfaceList({ currentTimestamp }, hwDeviceConfig);
  return {
    identification: {
      name: parseHwDeviceName(hwDeviceConfig.data),
      firmwareVersion: parseHwFirmwareVersion(hwDeviceConfig.data),
      platformId: parsePlatformId(),
      ipAddress: parseHwIpAddress(hwDeviceConfig),
      updated: currentTimestamp,
    },
    interfaces,
  };
};

/**
 * @param {number} currentTimestamp
 * @param {Object} hwStatus
 * @return {CorrespondenceDeviceUpdate}
 */
const parseHwStatus = ({ currentTimestamp = Date.now() }, hwStatus) => ({
  overview: {
    cpu: parseHwCpuUsage(hwStatus),
    ram: parseHwMemoryUsage(hwStatus),
    lastSeen: currentTimestamp,
    uptime: parseHwUptime(hwStatus),
  },
  interfaces: parseHwPortList({ currentTimestamp }, hwStatus),
});

/**
 * @param {number} currentTimestamp
 * @param {Object} hwStatus
 * @param {Object} hwPingStats
 * @return {CorrespondenceStatistics}
 */
const parseHwDeviceStatistics = ({ currentTimestamp = Date.now() }, { hwStatus, hwPingStats }) => {
  const cmStatistics = {
    timestamp: currentTimestamp,
    weight: 1,
    interfaces: parseHwPortStatistics(hwStatus),
    stats: {
      ping: parseHwPingStatsLatency(hwPingStats),
      errors: parseHwPingStatsErrors(hwPingStats),
      ram: parseHwMemoryUsage(hwStatus),
      cpu: parseHwCpuUsage(hwStatus),
    },
  };

  // collect metrics only when it makes sense and at least one station device is connected
  return over(lensProp('stats'), pickBy(isNotNull), cmStatistics);
};

/**
 * @param {CorrespondenceSysInfo} sysInfo
 * @return {CorrespondenceDevice}
 */
const toughSwitchDeviceStub = sysInfo => ({
  identification: {
    id: sysInfo.deviceId,
    enabled: true,
    siteId: null,
    site: null,
    mac: sysInfo.mac,
    name: 'ubnt',
    serialNumber: null,
    firmwareVersion: sysInfo.firmwareVersion,
    platformId: sysInfo.platformId,
    model: sysInfo.model,
    updated: 0,
    authorized: false,
    type: DeviceTypeEnum.ToughSwitch,
    category: DeviceCategoryEnum.Wired,
    ipAddress: null,
  },
  overview: {
    status: StatusEnum.Unauthorized,
    canUpgrade: false,
    location: null,
    isLocating: false,
    cpu: null,
    ram: null,
    voltage: null,
    temperature: null,
    signal: null,
    distance: null,
    biasCurrent: null,
    receivePower: null,
    receiveRate: null,
    receiveBytes: null,
    receiveErrors: null,
    receiveDropped: null,
    transmitPower: null,
    transmitRate: null,
    transmitBytes: null,
    transmitErrors: null,
    transmitDropped: null,
    lastSeen: 0,
    uptime: null,
    gateway: null,
  },
  meta: null,
  firmware: null,
  upgrade: null,
  mode: null,
  onu: null,
  olt: null,
  aircube: null,
  airmax: null,
  airfiber: null,
  interfaces: [],
  unmsSettings: null,
});

module.exports = {
  toughSwitchDeviceStub,
  parseHwDeviceConfig,
  parsePlatformId,
  parseHwDeviceStatistics,
  parseHwStatus,
};
