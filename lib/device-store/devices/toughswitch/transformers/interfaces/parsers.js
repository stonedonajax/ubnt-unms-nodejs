'use strict';

const {
  isNull, getOr, map, curry, get, defaultTo, filter, keyBy, mapValues, flow, pick, values, isNumber, sum, anyPass,
} = require('lodash/fp');

const { InterfaceIdentificationTypeEnum, IpAddressTypeEnum } = require('../../../../../enums');
const {
  isBridgeInterfaceType, isEthernetInterfaceType, isVlanInterfaceType, interfaceNameToPosition, interfaceNameToType,
} = require('../../../../../transformers/interfaces/utils');

const isValidInterfaceType = anyPass([
  isBridgeInterfaceType,
  isEthernetInterfaceType,
  isVlanInterfaceType,
]);

const parseHwPortErrors = flow(
  get('stats'),
  pick([
    'RxFcsErr',
    'RxAllignErr',
    'RxRunt',
    'RxFragment',
    'RxTooLong',
    'RxOverFlow',
    'RxJabber',
    'RxOversize',
    'TxUnderRun',
    'TxOverSize',
    'TxCollision',
    'TxAbortCol',
    'TxMultiCol',
    'TxSingalCol',
    'TxExcDefer',
    'TxDefer',
    'TxLateCol',
    'TxExcCol',
  ]),
  values,
  filter(isNumber),
  sum
);

const parseHwPortName = hwPort => `${InterfaceIdentificationTypeEnum.Port}${hwPort.position}`;

// parseHwInterface :: Object -> HwInterface -> Correspondence
const parseHwPort = curry(({ currentTimestamp = Date.now() }, hwPort) => {
  const enabled = hwPort.portStatus === '1';
  const plugged = enabled ? hwPort.linkStatus === '1' : null;
  const duplex = enabled ? hwPort.duplex === '1' : null;
  const speed = enabled && plugged ? Number(hwPort.portSpeed) : null;

  let description = null;
  if (isNull(speed)) {
    description = null;
  } else if (isNull(duplex)) {
    description = `${speed} Mbps`;
  } else {
    description = `${speed} Mbps - ${duplex ? 'Full Duplex' : 'Half Duplex'}`;
  }

  return {
    identification: {
      position: hwPort.position,
      type: InterfaceIdentificationTypeEnum.Port,
      name: parseHwPortName(hwPort),
      description: null,
      mac: null,
    },
    statistics: {
      timestamp: currentTimestamp,
      rxrate: defaultTo(0, hwPort.stats.rxrate),
      rxbytes: defaultTo(0, hwPort.stats.RxGoodByte),
      txrate: defaultTo(0, hwPort.stats.txrate),
      txbytes: defaultTo(0, hwPort.stats.TxByte),
      dropped: 0,
      errors: defaultTo(0, parseHwPortErrors(hwPort)),
      previousTxbytes: 0,
      previousRxbytes: 0,
      previousDropped: 0,
      previousErrors: 0,
    },
    addresses: [],
    mtu: Number(hwPort.maxFrameSize),
    poe: null,
    enabled,
    proxyARP: null,
    switch: null,
    speed: null,
    bridgeGroup: null,
    onSwitch: true,
    isSwitchedPort: true,
    status: {
      autoneg: false,
      duplex,
      description,
      plugged,
      speed,
      sfp: null,
    },
    vlan: null,
    pppoe: null,
    pon: null,
    bridge: null,
    ospf: {
      ospfCapable: false,
      ospfConfig: null,
    },
    // TODO(michal.sedlak@ubnt.com): HACK, not part of correspondence
    shouldMerge: true,
  };
});

// parseHwPortList :: (Object, HwStatus) -> Array.<Correspondence>
const parseHwPortList = (auxiliaries, hwStatus) => {
  const hwPorts = getOr([], ['data', 'stats'], hwStatus);

  return map(parseHwPort(auxiliaries), hwPorts);
};

// extractPortStatistics :: (hwPort) -> CorrespondenceStatistics
//     Auxiliaries = Object
//     hwPort = Object
//     CorrespondenceStatistics = Object
const extractPortStatistics = hwPort => (
  {
    weight: 1,
    stats: {
      rx_bps: hwPort.stats.rxrate,
      tx_bps: hwPort.stats.txrate,
    },
  }
);

/**
 * @function
 * @param {Object} hwStatus
 * @return {Object}
 */
const parseHwPortStatistics = flow(
  getOr([], ['data', 'stats']),
  keyBy(parseHwPortName),
  mapValues(extractPortStatistics)
);

/**
 * @function
 * @param {Object} hwInterface
 * @return {Array}
 */
const parseHwInterfaceAddresses = flow(
  getOr([], 'cidr'),
  map(cidr => ({ type: IpAddressTypeEnum.Static, cidr }))
);

// parseHwInterface :: Object -> HwInterface -> Correspondence
const parseHwInterface = curry(({ currentTimestamp = Date.now() }, hwInterface) => ({
  identification: {
    position: interfaceNameToPosition(hwInterface.name),
    type: interfaceNameToType(hwInterface.name),
    name: hwInterface.name,
    description: null,
    mac: defaultTo(null, hwInterface.mac),
  },
  statistics: {
    timestamp: currentTimestamp,
    rxrate: 0,
    rxbytes: 0,
    txrate: 0,
    txbytes: 0,
    dropped: 0,
    errors: 0,
    previousTxbytes: 0,
    previousRxbytes: 0,
    previousDropped: 0,
    previousErrors: 0,
  },
  addresses: parseHwInterfaceAddresses(hwInterface),
  mtu: null,
  poe: null,
  enabled: true,
  proxyARP: null,
  switch: null,
  speed: null,
  bridgeGroup: null,
  onSwitch: false, // TODO(michal.sedlak@ubnt.com): parse correct values if possible
  isSwitchedPort: false,
  status: {
    autoneg: false,
    duplex: true,
    description: null,
    plugged: false,
    speed: 100,
    sfp: null,
    lag: null,
  },
  vlan: null,
  pppoe: null,
  pon: null,
  bridge: null,
  ospf: {
    ospfCapable: false,
    ospfConfig: null,
  },
  // TODO(michal.sedlak@ubnt.com): HACK, not part of correspondence
  shouldMerge: true,
}));

// parseHwInterfaceList :: (Object, HwStatus) -> Array.<Correspondence>
const parseHwInterfaceList = (auxiliaries, hwDeviceConfig) => {
  const hwInterfaces = getOr([], ['data', 'interfaces', 'names'], hwDeviceConfig)
    .filter(isValidInterfaceType)
    .map(name => get(['data', name], hwDeviceConfig));

  return map(parseHwInterface(auxiliaries), hwInterfaces);
};

module.exports = {
  parseHwPort,
  parseHwPortList,
  parseHwInterfaceList,
  parseHwPortStatistics,
};
