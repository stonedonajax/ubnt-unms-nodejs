'use strict';

const semver = require('semver');
const { pipe, prop, defaultTo, toLower, replace, curry } = require('ramda');

const { DeviceTypeEnum } = require('../../../enums');

const Generic = require('./generic');
const Ac$8d5d0 = require('./airmax-8.5.0');

const extractFirmwareVersion = pipe(
  prop('firmwareVersion'),
  defaultTo('0.0.0'),
  toLower,
  replace(/(-cs)?(-aov)?$/ig, '')
);

const factory = (cmDiscoveryDevice) => {
  if (cmDiscoveryDevice.type === DeviceTypeEnum.AirFiber) {
    return Generic;
  }

  const fwVersion = extractFirmwareVersion(cmDiscoveryDevice);

  if (semver.lt(fwVersion, '8.4.999')) {
    return Generic;
  }

  return Ac$8d5d0;
};

const check = curry((cmDiscoveryDevice, authCredentials) =>
  factory(cmDiscoveryDevice).check(cmDiscoveryDevice, authCredentials));

const authenticate = curry((cmDiscoveryDevice, connection) =>
  factory(cmDiscoveryDevice).authenticate(connection));

module.exports = { check, authenticate };
