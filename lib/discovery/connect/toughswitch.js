'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader } = require('monet');
const { when, match } = require('ramda');
const { parseInt, contains, flow, nth } = require('lodash/fp');
const { isNotNull } = require('ramda-adjunct');
const fs = require('fs');

const https = require('../protocol/https');
const { authenticate } = require('../authentication/airos/generic');
const { upgradeFirmwareIfNeeded } = require('./generic');
const { DiscoveryConnectProgressEnum } = require('../../enums');
const { toMs, rejectP, resolveP } = require('../../util');

const FIRMWARE_UPGRADE_TIMEOUT = toMs('minute', 5);

const RECONNECT_DELAY = toMs('second', 5);

const POLLING_TIMEOUT = toMs('second', 10);
const POLLING_DELAY = toMs('second', 1);
const POLLING_COUNT = 3;

const tryReconnect = connection =>
  Observable.defer(() => connection.get('/poll.cgi', { timeout: POLLING_TIMEOUT }))
    .mergeMap((result) => {
      if (parseInt(10, result) === 300) { // special response when FW lock file exists
        return Observable.throw(result);
      }

      return Observable.of(result);
    })
    .retryWhen(errors => errors.delay(RECONNECT_DELAY)) // retry with delay
    .repeatWhen(notifications => notifications.delay(POLLING_DELAY).take(POLLING_COUNT)) // poll multiple times
    .last();

const handleFirmwareUploadError = (response) => {
  if (contains('window.parent.showError', response)) {
    return rejectP(new Error('Firmware upload failed'));
  }

  return resolveP(response);
};

const handleFirmwareUpgradeError = (response) => {
  if (!contains('class="popup"', response)) {
    return rejectP(new Error('Firmware upgrade failed to start'));
  }

  return resolveP(response);
};

const extractToken = flow(
  String,
  match(/\("X-TOKEN", '(.*)'\)/),
  when(isNotNull, nth(1))
);

const fetchToken = connection => Observable.from(connection.get('/index.cgi'))
  .map(extractToken);

const upgradeFirmware = firmware => reader(
  connection => Observable.from(connection.get('/uplfw.cgi?discard=1'))
    .mergeMap(() => connection.post('/uplfw.cgi', {
      timeout: FIRMWARE_UPGRADE_TIMEOUT,
      formData: { fwfile: fs.createReadStream(firmware.path), action: 'fwupload' },
    }))
    .mergeMap(handleFirmwareUploadError)
    .mergeMap(() => connection.post('/fwflash.cgi', { form: { do_update: 'do' } }))
    .mergeMap(handleFirmwareUpgradeError)
    .delay(RECONNECT_DELAY)
    .switchMap(() => tryReconnect(connection))
    .mergeMap(() => authenticate(connection))
    .timeoutWith(FIRMWARE_UPGRADE_TIMEOUT, Observable.throw(new Error('Firmware upgrade timeout')))
);

/**
 * @param {CorrespondenceDiscoveryDevice} cmDiscoveryDevice
 * @param {AuthCredentials} authCredentials
 * @return {Reader.<connect~callback>}
 */
const connect = (cmDiscoveryDevice, authCredentials) => reader(
  /**
   * @function connect~callback
   * @param {DiscoveryStatusUpdater} statusUpdater
   * @param {FirmwareDal} firmwareDal
   * @param {Function} connectionStringProvider
   * @return {Observable.<Object>}
   */
  ({ statusUpdater, firmwareDal, connectionStringProvider }) => {
    const { ip: host } = cmDiscoveryDevice;
    const { username, password, httpsPort: port } = authCredentials;

    const upgradeFirmwareIfNeededReader = upgradeFirmwareIfNeeded(upgradeFirmware, cmDiscoveryDevice);

    return Observable.of(https.createConnection({ host, port, username, password }))
      .mergeMap(authenticate)
      .mergeMap(connection => upgradeFirmwareIfNeededReader.run({ statusUpdater, firmwareDal, connection }))
      .tapO(() => statusUpdater
        .updateConnectProgress(cmDiscoveryDevice, DiscoveryConnectProgressEnum.SettingConnectionString))
      .mergeMap(connection => fetchToken(connection)
        .mergeMap(token => connection.post('/apply.cgi', {
          form: {
            'config[]': [
              `unms.uri=${connectionStringProvider()}`,
              `unms.uri.changed=${connectionStringProvider()}`,
              'unms.status=enabled',
            ],
            softRestart: '1',
          },
          headers: { 'X-TOKEN': token },
        }))
      );
  }
);

module.exports = { connect };

