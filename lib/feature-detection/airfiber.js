'use strict';

const { flow, eq } = require('lodash/fp');

const { isAirFiberDeviceType } = require('./common');
const { DeviceModelEnum, AirFiberSeriesEnum } = require('../enums');

// deviceModelToSeries :: String -> AirFiberSeriesEnum|Null
//     AirFiberSeriesEnum = String
const deviceModelToSeries = (model) => {
  if (!isAirFiberDeviceType(model)) { return null }

  /* eslint-disable no-fallthrough */
  switch (model) {
    // - Rocket
    case DeviceModelEnum.AF5XHD:
    case DeviceModelEnum.AFLTU:
      return AirFiberSeriesEnum.LTU;
    default:
      return AirFiberSeriesEnum.Other;
  }
  /* eslint-enable no-fallthrough */
};

// isLTUSeries :: DeviceModel -> Boolean
//     DeviceModel = String
const isLTUSeries = flow(deviceModelToSeries, eq(AirFiberSeriesEnum.LTU));

// isOtherSeries :: DeviceModel -> Boolean
//     DeviceModel = String
const isOtherSeries = flow(deviceModelToSeries, eq(AirFiberSeriesEnum.Other));

module.exports = {
  deviceModelToSeries,
  isLTUSeries,
  isOtherSeries,
};
