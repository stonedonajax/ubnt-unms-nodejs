'use strict';

const { invert, has, __, getOr, eq, flow, invertBy, identity, curry, isNil } = require('lodash/fp');
const { anyPass, cond, T, both, F, pipe, pathOr, pathSatisfies, equals } = require('ramda');
const semver = require('semver');

const {
  DeviceTypeEnum, DeviceCategoryEnum, DeviceModelEnum, DeviceTypeDetectionEnum, AirFiberSeriesEnum, WirelessModeEnum,
} = require('../enums');
const { interfaces } = require('../../config');
const { isVlanCapableOnSwitch } = require('./vlan');
const { POE_SUPPORT, PoeSupportEnum } = require('./poe');
const { isInterfaceVisible } = require('./interfaces');

/**
 * Reversed DeviceModelEnum
 * @readonly
 * @enum {string}
 */
const MODEL_MAP = invert(DeviceModelEnum);

/**
 * @param model {DeviceModelEnum}
 * @return {boolean}
 */
const isDeviceModelSupported = has(__, MODEL_MAP);

/**
 * @param {DeviceModelEnum} model
 * @return {?string}
 */
const deviceTypeFromModel = getOr(null, __, DeviceTypeDetectionEnum);

/**
 * @params {DeviceTypeEnum}
 * @return {Array.<DeviceModelEnum>}
 */
const deviceModelsForType = curry(
  (typeModelMap, deviceType) => typeModelMap[deviceType]
)(invertBy(identity, DeviceTypeDetectionEnum));

/**
 * @param {DeviceTypeEnum} type
 * @return {?string}
 */
const deviceCategoryFromType = (type) => {
  switch (type) {
    case DeviceTypeEnum.Onu:
      return DeviceCategoryEnum.Optical;
    case DeviceTypeEnum.Olt:
    case DeviceTypeEnum.Erouter:
    case DeviceTypeEnum.Eswitch:
    case DeviceTypeEnum.ToughSwitch:
      return DeviceCategoryEnum.Wired;
    case DeviceTypeEnum.AirMax:
    case DeviceTypeEnum.AirCube:
    case DeviceTypeEnum.AirFiber:
      return DeviceCategoryEnum.Wireless;
    case DeviceTypeEnum.Epower:
      return DeviceCategoryEnum.Accessories;
    default:
      return null;
  }
};

/* eslint-disable no-fallthrough */
const isDeviceSupported = (model) => {
  if (isNil(model)) { return false }
  const isModelSupported = isDeviceModelSupported(model);
  if (!isModelSupported) { return false }

  const deviceType = deviceTypeFromModel(model);
  switch (deviceType) {
    case DeviceTypeEnum.Onu:
    case DeviceTypeEnum.Olt:
    case DeviceTypeEnum.Erouter:
    case DeviceTypeEnum.Eswitch:
    case DeviceTypeEnum.AirCube:
    case DeviceTypeEnum.AirMax:
    case DeviceTypeEnum.AirFiber:
    case DeviceTypeEnum.ToughSwitch:
    case DeviceTypeEnum.Epower:
      return true;
    default:
      return false;
  }
};
/* eslint-enable no-fallthrough */

/**
 * @typedef {Object} DeviceFeatures
 * @property {Object} defaults
 * @property {number} defaults.mtu
 * @property {number} defaults.pppoeMtu
 * @property {boolean} isVlanCapableOnSwitch
 * @property {isInterfaceVisibleForModel} isInterfaceVisible
 * @property {boolean} supportsPoe
 * @property {Object.<string, string[]>} poe
 */

/**
 * @param {string} model
 * @return {DeviceFeatures}
 */
const modelFeatures = model => ({
  defaults: {
    mtu: interfaces.mtuDefault,
    pppoeMtu: interfaces.pppoeMtuDefault,
  },
  isVlanCapableOnSwitch: isVlanCapableOnSwitch(model),
  isInterfaceVisible: isInterfaceVisible(model),
  supportsPoe: getOr(false, [model], PoeSupportEnum),
  poe: getOr({}, [model], POE_SUPPORT),
});

// isOltDeviceType :: DeviceModel -> Boolean
//     DeviceModel = String
const isOltDeviceType = flow(deviceTypeFromModel, eq(DeviceTypeEnum.Olt));

// isErouterDeviceType :: DeviceModel -> Boolean
//     DeviceModel = String
const isErouterDeviceType = flow(deviceTypeFromModel, eq(DeviceTypeEnum.Erouter));

// isEswitchDeviceType :: DeviceModel -> Boolean
//     DeviceModel = String
const isEswitchDeviceType = flow(deviceTypeFromModel, eq(DeviceTypeEnum.Eswitch));

// isEpowerDeviceType :: DeviceModel -> Boolean
//     DeviceModel = String
const isEpowerDeviceType = flow(deviceTypeFromModel, eq(DeviceTypeEnum.Epower));

// isOnuDeviceType :: DeviceModel -> Boolean
//     DeviceModel = String
const isOnuDeviceType = flow(deviceTypeFromModel, eq(DeviceTypeEnum.Onu));

// isAirCubeDeviceType :: DeviceModel -> Boolean
//     DeviceModel = String
const isAirCubeDeviceType = flow(deviceTypeFromModel, eq(DeviceTypeEnum.AirCube));

// isAirMaxDeviceType :: DeviceModel -> Boolean
//     DeviceModel = String
const isAirMaxDeviceType = flow(deviceTypeFromModel, eq(DeviceTypeEnum.AirMax));

// isAirFiberDeviceType :: DeviceModel -> Boolean
//     DeviceModel = String
const isAirFiberDeviceType = flow(deviceTypeFromModel, eq(DeviceTypeEnum.AirFiber));

// isToughSwitchDeviceType :: DeviceModel -> Boolean
//     DeviceModel = String
const isToughSwitchDeviceType = flow(deviceTypeFromModel, eq(DeviceTypeEnum.ToughSwitch));

// isWirelessType :: DeviceType -> Boolean
//     DeviceType = String
const isWirelessType = anyPass([eq(DeviceTypeEnum.AirMax), eq(DeviceTypeEnum.AirCube), eq(DeviceTypeEnum.AirFiber)]);

// isFwVersionGraterThan :: Threshold, Version -> Boolean
//    Threshold = String
//    Version = string
const isFwVersionGraterThan = curry((threshold, version) => semver.gt(version, threshold));

// isErouterSpeedTestCapable :: CmDevice -> Boolean
//    CmDevice = Object
const isErouterSpeedTestCapable = pipe(
  pathOr('0.0.0', ['firmware', 'current']),
  isFwVersionGraterThan('1.9.9999')
);

// isAirFiberSpeedTestCapable :: CmDevice -> Boolean
//    CmDevice = Object
const isAirFiberSpeedTestCapable = pathSatisfies(equals(AirFiberSeriesEnum.LTU), ['airfiber', 'series']);

// isDeviceSpeedTestCapable :: CmDevice -> Boolean
//    CmDevice = Object
const isDeviceSpeedTestCapable = cond([
  [isAirMaxDeviceType, T],
  [isAirCubeDeviceType, T],
  [both(isErouterDeviceType, isErouterSpeedTestCapable), T],
  [both(isAirFiberDeviceType, isAirFiberSpeedTestCapable)],
  [T, F],
]);

// isAP :: WirelessMode -> Boolean
//     WirelessMode = String
const isAP = anyPass([
  equals(WirelessModeEnum.Ap),
  equals(WirelessModeEnum.ApPtp),
  equals(WirelessModeEnum.ApPtmp),
  equals(WirelessModeEnum.ApPtmpAirmaxMixed),
  equals(WirelessModeEnum.ApPtmpAirmaxAc),
  equals(WirelessModeEnum.ApRep),
]);


module.exports = {
  isDeviceModelSupported,
  isDeviceSupported,
  deviceTypeFromModel,
  deviceCategoryFromType,
  deviceModelsForType,
  modelFeatures,

  isOltDeviceType,
  isErouterDeviceType,
  isEswitchDeviceType,
  isEpowerDeviceType,
  isOnuDeviceType,
  isAirCubeDeviceType,
  isAirMaxDeviceType,
  isAirFiberDeviceType,
  isToughSwitchDeviceType,
  isWirelessType,
  isDeviceSpeedTestCapable,
  isAP,
};
