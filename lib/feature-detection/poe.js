'use strict';

const { memoize } = require('lodash');

const { PoeOutputEnum, DeviceModelEnum } = require('../enums');

const NONE = 0;
const OFF = 1;
const V48 = 2;
const V24 = 4;
const V54PAIR4 = 8;
const PASSTHROUGH = 16;
const V24PAIR4 = 32;


/* eslint-disable no-bitwise */

// toPoeOutput :: Number -> [PoeOutputEnum]
const toPoeOutput = memoize((capacity) => {
  const volts = [];

  if ((capacity & OFF) === OFF) { volts.push(PoeOutputEnum.OFF) }
  if ((capacity & V24) === V24) { volts.push(PoeOutputEnum.V24) }
  if ((capacity & V48) === V48) { volts.push(PoeOutputEnum.V48) }
  if ((capacity & V24PAIR4) === V24PAIR4) { volts.push(PoeOutputEnum.V24PAIR4) }
  if ((capacity & V54PAIR4) === V54PAIR4) { volts.push(PoeOutputEnum.V54PAIR4) }
  if ((capacity & PASSTHROUGH) === PASSTHROUGH) { volts.push(PoeOutputEnum.PASSTHROUGH) }

  return volts;
});

const POE_SUPPORT = {
  [DeviceModelEnum.ERX]: {
    eth0: toPoeOutput(NONE),
    eth1: toPoeOutput(NONE),
    eth2: toPoeOutput(NONE),
    eth3: toPoeOutput(NONE),
    eth4: toPoeOutput(OFF | PASSTHROUGH),
    eth5: toPoeOutput(-1),
  },
  [DeviceModelEnum.ER6P]: {
    eth0: toPoeOutput(OFF | V24),
    eth1: toPoeOutput(OFF | V24),
    eth2: toPoeOutput(OFF | V24),
    eth3: toPoeOutput(OFF | V24),
    eth4: toPoeOutput(OFF | V24),
    eth5: toPoeOutput(NONE),
  },
  [DeviceModelEnum.ERXSFP]: {
    eth0: toPoeOutput(OFF | V24),
    eth1: toPoeOutput(OFF | V24),
    eth2: toPoeOutput(OFF | V24),
    eth3: toPoeOutput(OFF | V24),
    eth4: toPoeOutput(OFF | V24),
    eth5: toPoeOutput(NONE),
    eth6: toPoeOutput(-1),
  },
  [DeviceModelEnum.ERPoe5]: {
    eth0: toPoeOutput(OFF | V24 | V48),
    eth1: toPoeOutput(OFF | V24 | V48),
    eth2: toPoeOutput(OFF | V24 | V48),
    eth3: toPoeOutput(OFF | V24 | V48),
    eth4: toPoeOutput(OFF | V24 | V48),
    eth5: toPoeOutput(-1),
  },
  [DeviceModelEnum.ERPro8]: {
    eth0: toPoeOutput(NONE),
    eth1: toPoeOutput(NONE),
    eth2: toPoeOutput(NONE),
    eth3: toPoeOutput(NONE),
    eth4: toPoeOutput(NONE),
    eth5: toPoeOutput(NONE),
    eth6: toPoeOutput(NONE),
    eth7: toPoeOutput(NONE),
  },
  [DeviceModelEnum.EPR6]: {
    eth0: toPoeOutput(OFF | V24),
    eth1: toPoeOutput(OFF | V24),
    eth2: toPoeOutput(OFF | V24),
    eth3: toPoeOutput(OFF | V24),
    eth4: toPoeOutput(OFF | V24),
    eth5: toPoeOutput(NONE),
    eth6: toPoeOutput(-1),
  },
  [DeviceModelEnum.EPR8]: {
    eth0: toPoeOutput(NONE),
    eth1: toPoeOutput(OFF | V54PAIR4 | V24PAIR4),
    eth2: toPoeOutput(OFF | V54PAIR4 | V24PAIR4),
    eth3: toPoeOutput(OFF | V24),
    eth4: toPoeOutput(OFF | V24),
    eth5: toPoeOutput(OFF | V24),
    eth6: toPoeOutput(OFF | V24),
    eth7: toPoeOutput(OFF | V24),
    eth8: toPoeOutput(-1),
  },
  [DeviceModelEnum.TSW8]: {
    br0: toPoeOutput(NONE),
    port1: toPoeOutput(NONE),
    port2: toPoeOutput(NONE),
    port3: toPoeOutput(NONE),
    port4: toPoeOutput(NONE),
    port5: toPoeOutput(NONE),
    port6: toPoeOutput(NONE),
    port7: toPoeOutput(NONE),
    port8: toPoeOutput(NONE),
  },
  [DeviceModelEnum.TSWPoePro]: {
    br0: toPoeOutput(NONE),
    port1: toPoeOutput(OFF | V24 | V48),
    port2: toPoeOutput(OFF | V24 | V48),
    port3: toPoeOutput(OFF | V24 | V48),
    port4: toPoeOutput(OFF | V24 | V48),
    port5: toPoeOutput(OFF | V24 | V48),
    port6: toPoeOutput(OFF | V24 | V48),
    port7: toPoeOutput(OFF | V24 | V48),
    port8: toPoeOutput(OFF | V24 | V48),
  },
  [DeviceModelEnum.TSWPoe]: {
    br0: toPoeOutput(NONE),
    port1: toPoeOutput(OFF | V24),
    port2: toPoeOutput(OFF | V24),
    port3: toPoeOutput(OFF | V24),
    port4: toPoeOutput(OFF | V24),
    port5: toPoeOutput(OFF | V24),
  },
};


const PoeSupportEnum = Object.freeze({
  // - ONU
  [DeviceModelEnum.NanoG]: false,
  [DeviceModelEnum.Loco]: false,

  // - OLT
  [DeviceModelEnum.UFOLT]: false,
  [DeviceModelEnum.UFOLT4]: false,

  // - EdgeRouter
  [DeviceModelEnum.ERX]: true,
  [DeviceModelEnum.ERXSFP]: true,
  [DeviceModelEnum.ERLite3]: false,
  [DeviceModelEnum.ERPoe5]: true,
  [DeviceModelEnum.ERPro8]: false,
  [DeviceModelEnum.ER8]: false,
  [DeviceModelEnum.ER8XG]: false,
  [DeviceModelEnum.ER4]: false,
  [DeviceModelEnum.ER6P]: true,

  // - EdgePoint
  [DeviceModelEnum.EPR8]: true,
  [DeviceModelEnum.EPR6]: true,
  [DeviceModelEnum.EPS16]: true,

  // - EdgeSwitch
  [DeviceModelEnum.ES12F]: false,
  [DeviceModelEnum.ES16150W]: true,
  [DeviceModelEnum.ES24250W]: true,
  [DeviceModelEnum.ES24500W]: true,
  [DeviceModelEnum.ES24LITE]: false,
  [DeviceModelEnum.ES48500W]: true,
  [DeviceModelEnum.ES48750W]: true,
  [DeviceModelEnum.ES48LITE]: false,
  [DeviceModelEnum.ES8150W]: true,
  [DeviceModelEnum.ES16XG]: false,

  // - EdgePower
  [DeviceModelEnum.EP54V150W]: false,

  // - ToughSwitch
  [DeviceModelEnum.TSWPoe]: true,
  [DeviceModelEnum.TSW8]: false,
  [DeviceModelEnum.TSWPoePro]: true,

  // TODO(michal.sedlak@ubnt.com): BELOW HERE IT'S INCOMPLETE, USE WITCH CAUTION
  // - AirCube
  [DeviceModelEnum.ACBAC]: false,
  [DeviceModelEnum.ACBISP]: false,
  [DeviceModelEnum.ACBLOCO]: false,

  // - AirFiber
  [DeviceModelEnum.AF11FX]: false,
  [DeviceModelEnum.AF24]: false,
  [DeviceModelEnum.AF24HD]: false,
  [DeviceModelEnum.AF2X]: false,
  [DeviceModelEnum.AF3X]: false,
  [DeviceModelEnum.AF4X]: false,
  [DeviceModelEnum.AF05]: false,
  [DeviceModelEnum.AF5]: false,
  [DeviceModelEnum.AF5U]: false,
  [DeviceModelEnum.AF5X]: false,
  [DeviceModelEnum.AF5XHD]: false,
  [DeviceModelEnum.AF5MX]: false,

  /* AirMax */

  // - Rocket
  [DeviceModelEnum.R2N]: false,
  [DeviceModelEnum.R2T]: false,
  [DeviceModelEnum.R5N]: false,
  [DeviceModelEnum.R6N]: false,
  [DeviceModelEnum.R36GPS]: false,
  [DeviceModelEnum.RM3GPS]: false,
  [DeviceModelEnum.R2NGPS]: false,
  [DeviceModelEnum.R5NGPS]: false,
  [DeviceModelEnum.R9NGPS]: false,
  [DeviceModelEnum.R5TGPS]: false,
  [DeviceModelEnum.RM3]: false,
  [DeviceModelEnum.R36]: false,
  [DeviceModelEnum.R9N]: false,
  // - NanoStation
  [DeviceModelEnum.N2N]: false,
  [DeviceModelEnum.N5N]: false,
  [DeviceModelEnum.N6N]: false,
  [DeviceModelEnum.NS3]: false,
  [DeviceModelEnum.N36]: false,
  [DeviceModelEnum.N9N]: false,
  [DeviceModelEnum.N9S]: false,
  [DeviceModelEnum.LM2]: false,
  [DeviceModelEnum.LM5]: false,
  // - Bullet
  [DeviceModelEnum.B2N]: false,
  [DeviceModelEnum.B2T]: false,
  [DeviceModelEnum.B5N]: false,
  [DeviceModelEnum.B5T]: false,
  // - AirGrid
  [DeviceModelEnum.AG2]: false,
  [DeviceModelEnum.AG2HP]: false,
  [DeviceModelEnum.AG5]: false,
  [DeviceModelEnum.AG5HP]: false,
  [DeviceModelEnum.P2N]: false,
  [DeviceModelEnum.P5N]: false,
  // - LiteStation
  [DeviceModelEnum.M25]: false,
  // - PowerBeam
  [DeviceModelEnum.P2B400]: false,
  [DeviceModelEnum.P5B300]: false,
  [DeviceModelEnum.P5B400]: false,
  [DeviceModelEnum.P5B620]: false,
  // - LiteBeam
  [DeviceModelEnum.LB5120]: false,
  [DeviceModelEnum.LB5]: false,
  // - NanoBeam
  [DeviceModelEnum.N5B]: false,
  [DeviceModelEnum.N5B16]: false,
  [DeviceModelEnum.N5B19]: false,
  [DeviceModelEnum.N5B300]: false,
  [DeviceModelEnum.N5B400]: false,
  [DeviceModelEnum.N5BClient]: false,
  [DeviceModelEnum.N2B]: false,
  [DeviceModelEnum.N2B13]: false,
  [DeviceModelEnum.N2B400]: false,
  // - PowerAP
  // supports only 5/10/20/40 channel widths
  [DeviceModelEnum.PAP]: false,
  // - AirRouter
  [DeviceModelEnum.LAPHP]: false,
  [DeviceModelEnum.LAP]: false,
  // - AirGateway
  // supports only 20/40 channel widths
  [DeviceModelEnum.AGW]: false,
  [DeviceModelEnum.AGWLR]: false,
  [DeviceModelEnum.AGWPro]: false,
  [DeviceModelEnum.AGWInstaller]: false,
  // - PowerBridge
  [DeviceModelEnum.PB5]: false,
  [DeviceModelEnum.PB3]: false,
  [DeviceModelEnum.P36]: false,
  [DeviceModelEnum.PBM10]: false,
  // - NanoBridge
  [DeviceModelEnum.NB5]: false,
  [DeviceModelEnum.NB2]: false,
  [DeviceModelEnum.NB3]: false,
  [DeviceModelEnum.B36]: false,
  [DeviceModelEnum.NB9]: false,
  // - LiteStation
  [DeviceModelEnum.SM5]: false,
  // - WispStation
  [DeviceModelEnum.WM5]: false,
  // - ISO Station
  [DeviceModelEnum.ISM5]: false,
  // AC devices
  // Note that devices with old firmware (5.x and older) will report device Product in the product
  // field instead of board name. This fix is introduced since 7.1.4 beta2.
  // Older 7.x firmware will report short-name which is non unique.
  // NanoStation
  [DeviceModelEnum.NS5ACL]: false,
  [DeviceModelEnum.NS5AC]: false,
  // - Rocket
  [DeviceModelEnum.R5ACPTMP]: false,
  [DeviceModelEnum.R5ACPTP]: false,
  [DeviceModelEnum.R5ACLite]: false,
  [DeviceModelEnum.R5ACPRISM]: false,
  [DeviceModelEnum.R2AC]: false,
  [DeviceModelEnum.RP5ACGen2]: false,
  // - NanoBeam
  [DeviceModelEnum.NBE2AC13]: false,
  [DeviceModelEnum.NBE5AC16]: false,
  [DeviceModelEnum.NBE5AC19]: false,
  [DeviceModelEnum.NBE5ACGen2]: false,
  // - PowerBeam
  [DeviceModelEnum.PBE5AC300]: false,
  [DeviceModelEnum.PBE5AC300ISO]: false,
  [DeviceModelEnum.PBE5AC400]: false,
  [DeviceModelEnum.PBE5AC400ISO]: false,
  [DeviceModelEnum.PBE5AC500]: false,
  [DeviceModelEnum.PBE5AC500ISO]: false,
  [DeviceModelEnum.PBE5AC620]: false,
  [DeviceModelEnum.PBE5AC620ISO]: false,
  [DeviceModelEnum.PBE2AC400]: false,
  [DeviceModelEnum.PBE2AC400ISO]: false,
  [DeviceModelEnum.PBE5ACXGen2]: false,
  [DeviceModelEnum.PBE5ACGen2]: false,
  [DeviceModelEnum.PBE5ACISOGen2]: false,
  [DeviceModelEnum.PBE5AC400ISOGen2]: false,
  // - LiteBeam
  [DeviceModelEnum.LBE5AC16120]: false,
  [DeviceModelEnum.LBE5AC23]: false,
  [DeviceModelEnum.LBE5ACGen2]: false,
  // - ISO Station
  [DeviceModelEnum.IS5AC]: false,
  // - PrismStation
  [DeviceModelEnum.PS5AC]: false,
});

module.exports = {
  toPoeOutput,
  POE_SUPPORT,
  PoeSupportEnum,
};
