'use strict';

const { assign } = require('lodash/fp');

const { DeviceTypeEnum } = require('../enums');
const { deviceTypeFromModel } = require('./common');


/**
 * @readonly
 * @enum {string}
 */
const StatisticsSupportEnum = {
  Always: 'always',
  Never: 'never',
  NonEmpty: 'non-empty',
};

/**
 * Statistics support
 *
 * @typedef {Object} DeviceStatisticsSupport
 * @property {StatisticsSupportEnum|string} cpu
 * @property {StatisticsSupportEnum|string} ram
 * @property {StatisticsSupportEnum|string} ping
 * @property {StatisticsSupportEnum|string} errors
 * @property {Object.<string, StatisticsSupportEnum>} interfaces
 * @property {Object.<string, StatisticsSupportEnum>} psu
 * @property {StatisticsSupportEnum|string} signal
 * @property {StatisticsSupportEnum|string} remoteSignal
 * @property {StatisticsSupportEnum|string} current
 * @property {StatisticsSupportEnum|string} voltage
 * @property {StatisticsSupportEnum|string} output
 * @property {StatisticsSupportEnum|string} consumption
 */

const emptyStats = {
  cpu: StatisticsSupportEnum.Never,
  ram: StatisticsSupportEnum.Never,
  ping: StatisticsSupportEnum.Never,
  errors: StatisticsSupportEnum.Never,
  interfaces: StatisticsSupportEnum.Never,
  psu: StatisticsSupportEnum.Never,
  signal: StatisticsSupportEnum.Never,
  remoteSignal: StatisticsSupportEnum.Never,
  current: StatisticsSupportEnum.Never,
  voltage: StatisticsSupportEnum.Never,
  power: StatisticsSupportEnum.Never,
  consumption: StatisticsSupportEnum.Never,
};

/**
 * @param {string} model
 * @return {DeviceStatisticsSupport}
 */
const supportedStatistics = (model) => {
  const deviceType = deviceTypeFromModel(model);

  switch (deviceType) {
    case DeviceTypeEnum.Onu:
      return assign(emptyStats, {
        cpu: StatisticsSupportEnum.Always,
        ram: StatisticsSupportEnum.Always,
        interfaces: {
          '*': StatisticsSupportEnum.NonEmpty,
        },
      });
    case DeviceTypeEnum.Olt:
    case DeviceTypeEnum.Erouter:
      return assign(emptyStats, {
        cpu: StatisticsSupportEnum.Always,
        ram: StatisticsSupportEnum.Always,
        ping: StatisticsSupportEnum.Always,
        errors: StatisticsSupportEnum.Always,
        interfaces: {
          '*': StatisticsSupportEnum.Always,
        },
      });
    case DeviceTypeEnum.AirMax:
    case DeviceTypeEnum.AirFiber:
      return assign(emptyStats, {
        cpu: StatisticsSupportEnum.Always,
        ram: StatisticsSupportEnum.Always,
        ping: StatisticsSupportEnum.Always,
        errors: StatisticsSupportEnum.Always,
        interfaces: {
          '*': StatisticsSupportEnum.Always,
        },
        signal: StatisticsSupportEnum.NonEmpty,
        remoteSignal: StatisticsSupportEnum.NonEmpty,
      });
    case DeviceTypeEnum.Epower:
      return assign(emptyStats, {
        cpu: StatisticsSupportEnum.Always,
        ram: StatisticsSupportEnum.Always,
        ping: StatisticsSupportEnum.Always,
        errors: StatisticsSupportEnum.Always,
        current: StatisticsSupportEnum.Always,
        voltage: StatisticsSupportEnum.Always,
        power: StatisticsSupportEnum.Always,
        consumption: StatisticsSupportEnum.Always,
        psu: {
          '*': StatisticsSupportEnum.NonEmpty,
        },
      });
    default:
      return assign(emptyStats, {
        cpu: StatisticsSupportEnum.Always,
        ram: StatisticsSupportEnum.Always,
        ping: StatisticsSupportEnum.Always,
        errors: StatisticsSupportEnum.Always,
        interfaces: {
          '*': StatisticsSupportEnum.Always,
        },
      });
  }
};

module.exports = {
  StatisticsSupportEnum,
  supportedStatistics,
};
