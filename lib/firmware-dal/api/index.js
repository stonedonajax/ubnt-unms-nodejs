'use strict';

const { Observable } = require('rxjs/Rx');

const { compareSemver } = require('../../util/semver');
const { FirmwareOriginEnum } = require('../../enums');
const request = require('./request');
const logging = require('../../logging');

const supportedFirmware = cmFirmware => cmFirmware.supports.UNMS;

/* eslint-disable max-len */
const firmwareType = cmFirmware =>
  `${cmFirmware.identification.origin}|${cmFirmware.identification.platformId}|${cmFirmware.supports.airMaxCustomScripts ? 1 : 0}`;
/* eslint-enable max-len */

const searchParams = cmFirmware => (firmware) => {
  const origin = firmware.identification.origin;
  const allowedOrigin = origin === FirmwareOriginEnum.Beta || origin === FirmwareOriginEnum.UBNT;
  return allowedOrigin && cmFirmware.supports.airMaxCustomScripts === firmware.supports.airMaxCustomScripts;
};

/**
 * @param {CorrespondenceFirmware} a
 * @param {CorrespondenceFirmware} b
 * @return {number}
 */
const firmwareComparator = (a, b) => {
  const cmp = compareSemver(a.semver, b.semver);
  if (cmp === 0) {
    return a.created - b.created;
  }

  return cmp;
};

class FirmwaresApi {
  get allowBetaFirmwares() {
    return this.firmwaresConfig().allowBetaFirmwares;
  }

  constructor(repository, firmwaresConfig) {
    this.repository = repository;
    this.firmwaresConfig = firmwaresConfig;
    this.running = null;
  }

  /**
   * Updates latest firmwares from the API
   *
   * Former firmwares will be deleted
   *
   * @return {Observable.<CorrespondenceFirmware[]>}
   */
  updateFirmwares() {
    if (this.running !== null) {
      return this.running;
    }

    const [stable$, beta$] = request.fetch()
      .filter(supportedFirmware)
      .groupBy(firmwareType)
      .mergeMap(group => group.max(firmwareComparator))
      .partition(cmFirmware => cmFirmware.identification.origin === FirmwareOriginEnum.UBNT);

    this.running = Observable.defer(() => (this.allowBetaFirmwares ? stable$.concat(beta$) : stable$))
      .concatMap((cmFirmware) => {
        const origin = cmFirmware.identification.origin;
        const isStable = origin === FirmwareOriginEnum.UBNT;
        const platformId = cmFirmware.identification.platformId;
        const latestFirmware = this.repository.findLatestFirmware(platformId, searchParams(cmFirmware));
        const latestStableFirmware = this.repository.findLatestFirmware(platformId, {
          identification: { origin: FirmwareOriginEnum.UBNT },
          supports: { airMaxCustomScripts: cmFirmware.supports.airMaxCustomScripts },
        });

        const isLatest = latestFirmware === null || firmwareComparator(cmFirmware, latestFirmware) > 0;
        const isLatestStable = isStable
          && (latestStableFirmware === null || firmwareComparator(cmFirmware, latestStableFirmware) > 0);

        if (isLatest || isLatestStable) {
          return Observable.from(this.repository.save(origin, request.download(cmFirmware.url), cmFirmware.md5))
            .tapO(() => {
              // replacing latest firmware
              if (latestFirmware !== null
                && isLatest
                && (latestFirmware.identification.origin === origin || isStable)) {
                return this.repository.remove(latestFirmware);
              }

              // replacing latest stable firmware
              if (latestStableFirmware !== null && isLatestStable) {
                return this.repository.remove(latestStableFirmware);
              }

              return Observable.empty();
            })
            .catch((err) => {
              logging.error(
                `${err.message}: ${platformId} ${cmFirmware.identification.version} ${cmFirmware.url}`,
                err
              );
              return Observable.empty();
            });
        }

        return Observable.empty();
      })
      .toArray()
      .finally(() => { this.running = null })
      .share();

    return this.running;
  }
}

module.exports = FirmwaresApi;
