'use strict';

const rp = require('request-promise-native');
const got = require('got');
const { Observable } = require('rxjs/Rx');
const { isNotNull } = require('ramda-adjunct');

const { parseApiFirmware } = require('../transformers/firmware-api/parsers');

const fetch = () => {
  const options = {
    uri: 'https://fw-update.ubnt.com/api/firmware',
    qs: {
      limit: 100000,
    },
    headers: {
      'User-Agent': 'Request-Promise',
    },
    json: true,
  };

  return Observable.from(rp(options))
    .pluck('_embedded', 'firmware')
    .mergeAll()
    .map(parseApiFirmware)
    .filter(isNotNull);
};

const download = url => got.stream(url);

module.exports = {
  fetch,
  download,
};
