'use strict';

const ChannelsEnum = Object.freeze({
  Release: 'release',
  Beta: 'beta-public',
});

module.exports = {
  ChannelsEnum,
};
