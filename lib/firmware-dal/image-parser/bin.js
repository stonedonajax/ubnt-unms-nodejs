'use strict';

const { SmartBuffer } = require('smart-buffer');
const { Writable } = require('stream');

const { isPlatformIdSupported } = require('../../feature-detection/firmware');
const { parseVersion, buildFirmwareFilename, UnknownFirmwareImage } = require('./common');

const HEADER_SIZE = 2000;

const extractFilename = (versionBuffer) => {
  const smartBuff = SmartBuffer.fromBuffer(versionBuffer);
  const parsedVersion = parseVersion(smartBuff.readStringNT());

  if (parsedVersion === null || !isPlatformIdSupported(parsedVersion.platformId)) { return null }

  return buildFirmwareFilename({
    platformId: parsedVersion.platformId,
    semver: parsedVersion.semver,
    compileDate: parsedVersion.compileDate,
    ext: 'bin',
  });
};

/**
 * @param {Buffer} buf
 * @return {boolean}
 */
const isOnuFirmware = buf => (
  buf.length > 0x68b
  && buf[0x684] === 0x55 // u
  && buf[0x685] === 0x42 // b
  && buf[0x686] === 0x4E // n
  && buf[0x687] === 0x54 // t
  && buf[0x688] === 0x5F // _
  && buf[0x689] === 0x53 // s
  && buf[0x68a] === 0x46 // f
  && buf[0x68b] === 0x55 // u
);

class BinFileParser extends Writable {
  constructor(callback) {
    super();
    this.callback = callback;
    this.bufferList = [];
    this.size = 0;
  }

  readHeader(chunk, next) {
    this.bufferList.push(chunk);
    this.size += chunk.length;

    if (this.size >= HEADER_SIZE) {
      const buff = Buffer.concat(this.bufferList);
      const smartBuff = SmartBuffer.fromBuffer(buff);
      const header = smartBuff.readString(4);

      if (header === 'AMEI') { // airFiber LTU
        smartBuff.skipTo(0x14); // skip leading bytes
      } else if (header !== 'UBNT') { // or it has to be UBNT
        next(new UnknownFirmwareImage()); // end stream
        return;
      }

      // for ONU the full version string is not in the header
      if (isOnuFirmware(buff)) {
        smartBuff.skip(4); // skip leading bytes 00 3A 03 20
      }

      // other have full firmware version in the header
      const filename = extractFilename(smartBuff.readBufferNT());

      if (filename === null) {
        next(new UnknownFirmwareImage()); // end stream
        return;
      }

      this.callback(filename);
    }

    next();
  }

  _write(chunk, encoding, next) {
    if (this.size < HEADER_SIZE) {
      this.readHeader(chunk, next);
    } else {
      next();
    }
  }
}

/**
 * @function extractInfoFromBin
 * @param {firmwareFilenameCallback} callback
 * @return {stream.Writable}
 */
module.exports = (callback) => {
  const stream = new BinFileParser(callback);
  stream.on('error', () => {
    callback(null);
  });

  return stream;
};
