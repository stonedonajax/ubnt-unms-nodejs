'use strict';

const { weave } = require('ramda-adjunct');
const { Observable } = require('rxjs/Rx');

const { registerPlugin } = require('../util/hapi');
const logging = require('../logging');
const FirmwaresStore = require('./store');
const FirmwaresApi = require('./api');
const storage = require('./storage');
const repository = require('./repository');
const firmwareImageParser = require('./image-parser');
const { createTempFile } = require('./utils');

function register(server) {
  const config = server.settings.app;
  const { scheduler, settings } = server.plugins;
  const firmwaresConfig = settings.firmwares;
  const { fetchUbntFirmwaresInterval } = firmwaresConfig();
  const firmwaresStore = new FirmwaresStore();

  /**
   * @name FirmwaresStorage
   */
  const firmwaresStorage = {
    /**
     * @function
     * @return {Promise.<CorrespondenceFirmware[]>}
     */
    load: weave(storage.load, { firmwaresConfig }),
    /**
     * @function
     * @param {string} origin
     * @param {string} filename
     * @return {Promise.<void>}
     */
    remove: weave(storage.remove, { firmwaresConfig }),
    /**
     * @function
     * @param {string} origin
     * @return {Promise.<void>}
     */
    removeAll: weave(storage.removeAll, { firmwaresConfig }),
    /**
     * @function
     * @param {string} origin
     * @param {stream.Readable} fileStream
     * @param {string} [md5]
     * @return {Promise.<CorrespondenceFirmware>}
     */
    save: weave(storage.save, { firmwaresConfig, firmwareImageParser, createTempFile }),
  };

  /**
   * @name FirmwareDal
   * @type {{
   *   findAll: function():CorrespondenceFirmware[],
   *   findById: function(firmwareId: string):CorrespondenceFirmware,
   *   findLatestFirmware: function(string, Object?):CorrespondenceFirmware,
   *   findFirmwareDetails: function(string, version: string):DeviceFirmwareDetails,
   *   load: function():Promise.<void>,
   *   save: function(origin: string, fileStream: stream.Readable):Promise.<CorrespondenceFirmware>,
   *   remove: function(firmware: CorrespondenceFirmware):Promise.<CorrespondenceFirmware>,
   *   removeAll: function(origin: string):Promise.<void>,
   *   countNewSince: function(timestamp: number):number,
   * }}
   */
  const firmwareDal = {
    findAll: weave(repository.findAll, { firmwaresStore }),
    findById: weave(repository.findById, { firmwaresStore }),
    findLatestFirmware: weave(repository.findLatestFirmware, { firmwaresStore }),
    findFirmwareDetails: weave(repository.findFirmwareDetails, { firmwaresStore }),
    load: weave(repository.load, { firmwaresStore, firmwaresStorage }),
    save: weave(repository.save, { firmwaresStore, firmwaresStorage }),
    remove: weave(repository.remove, { firmwaresStore, firmwaresStorage }),
    removeAll: weave(repository.removeAll, { firmwaresStore, firmwaresStorage }),
    countNewSince: weave(repository.countNewSince, { firmwaresStore }),
  };

  const firmwaresApi = new FirmwaresApi(firmwareDal, firmwaresConfig);

  const downloadUbntFirmwaresTask = () => {
    const { allowAutoUpdateUbntFirmwares } = firmwaresConfig();
    if (!allowAutoUpdateUbntFirmwares) { return Observable.empty() }

    return firmwaresApi.updateFirmwares()
      .takeUntil(Observable.fromEvent(server, 'stop'))
      .catch((error) => {
        logging.error('Fetching UBNT firmware failed', error);
        return Observable.empty();
      });
  };

  if (!config.demo) {
    scheduler.registerPeriodicTask(downloadUbntFirmwaresTask, fetchUbntFirmwaresInterval, 'downloadUbntFirmwares');
  }

  server.expose(firmwareDal);
  server.expose('api', firmwaresApi);

  return Observable.from(firmwaresStorage.load())
    .do(firmwares => firmwaresStore.init(firmwares))
    .do(() => downloadUbntFirmwaresTask().subscribe()) // start download as a side-effect
    .catch((error) => {
      logging.error('Firmware failed to load', error);
      return Observable.empty();
    })
    .toPromise();
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'firmwareDal',
  dependencies: ['settings', 'scheduler'],
};

