'use strict';

const { Reader: reader } = require('monet');
const { last, filter } = require('lodash/fp');
const { view, pathOr, pipe, lte, length } = require('ramda');

const {
  firmwareOriginLens, firmwareCreatedLens, firmwareFilenameLens, firmwareVersionLens, firmwareIdLens,
} = require('./utils');
const {
  isPlatformIdSupported, isFirmwareSupported, hasCustomScriptsSupport,
} = require('../feature-detection/firmware');
const { parseSemver, parseCommFirmwareVersion } = require('../transformers/semver/parsers');
const { viewOr, findLastOr } = require('../util');
const { compareSemver } = require('../util/semver');

/**
 * @param {?Object|Function} [query]
 * @return {Reader.<findAll~callback>}
 */
const findAll = (query = null) => reader(
  /**
   * @function findAll~callback
   * @param {FirmwaresStore} firmwaresStore
   * @return {CorrespondenceFirmware[]}
   */
  ({ firmwaresStore }) => {
    const firmwares = firmwaresStore.findAll();

    if (firmwares.length === null || query === null) { return firmwares }

    return filter(query, firmwares);
  }
);

/**
 * @param {string} firmwareId
 * @return {Reader.<findById~callback>}
 */
const findById = firmwareId => reader(
  /**
   * @function findById~callback
   * @param {FirmwaresStore} firmwaresStore
   * @return {?CorrespondenceFirmware}
   */
  ({ firmwaresStore }) => firmwaresStore.findById(firmwareId)
);

/**
 * @param {FirmwarePlatformIdEnum|string} platformId
 * @param {?Object|Function} [query]
 * @return {Reader.<findLatestFirmware~callback>}
 */
const findLatestFirmware = (platformId, query = null) => reader(
  /**
   * @function findLatestFirmware~callback
   * @param {FirmwaresStore} firmwaresStore
   * @return {?CorrespondenceFirmware}
   */
  ({ firmwaresStore }) => {
    const firmwares = firmwaresStore.findByPlatform(platformId);
    if (firmwares === null) { return null }

    if (query === null) {
      return last(firmwares);
    }

    return findLastOr(null, query, firmwares);
  }
);

/**
 * Firmware details
 *
 * @typedef {Object} DeviceFirmwareDetails
 * @property {string} current
 * @property {string} latest
 * @property {boolean} compatible
 * @property {Object} semver
 * @property {?CorrespondenceSemver} semver.current
 * @property {?CorrespondenceSemver} semver.latest
 */

/**
 * @param {FirmwarePlatformIdEnum|string} platformId
 * @param {string} rawVersion
 * @return {Reader.<findFirmwareDetails~callback>}
 */
const findFirmwareDetails = (platformId, rawVersion) => reader(
  /**
   * @function findFirmwareDetails~callback
   * @param {FirmwaresStore} firmwaresStore
   * @return {DeviceFirmwareDetails}
   */
  ({ firmwaresStore }) => {
    const currentVersion = parseCommFirmwareVersion(rawVersion);
    const currentSemver = parseSemver(currentVersion);

    if (!isPlatformIdSupported(platformId) || currentVersion === null) {
      return {
        current: rawVersion,
        latest: null,
        compatible: false,
        semver: {
          current: currentSemver,
          latest: null,
        },
      };
    }

    const hasUBNTFWCustomScripts = hasCustomScriptsSupport(platformId, currentVersion);
    const latestFirmware = findLatestFirmware(
      platformId,
      { supports: { airMaxCustomScripts: hasUBNTFWCustomScripts } }
    ).run({ firmwaresStore });

    const currentFirmwareVersion = pathOr(currentVersion, ['raw'], currentSemver);
    let latestFirmwareVersion = viewOr(null, firmwareVersionLens, latestFirmware);
    let latestFirmwareSemver = latestFirmwareVersion !== null ? latestFirmware.semver : null;

    // current firmware is newer or the same as the latest available
    if (latestFirmwareSemver === null || compareSemver(currentSemver, latestFirmwareSemver) >= 0) {
      latestFirmwareVersion = currentFirmwareVersion;
      latestFirmwareSemver = currentSemver;
    }

    return {
      current: currentFirmwareVersion,
      latest: latestFirmwareVersion,
      compatible: isFirmwareSupported(platformId, currentFirmwareVersion),
      semver: {
        current: currentSemver,
        latest: latestFirmwareSemver,
      },
    };
  }
);

/**
 * @return {Reader.<load~callback>}
 */
const load = () => reader(
  /**
   * @function load~callback
   * @param {FirmwaresStore} firmwaresStore
   * @param {FirmwaresStorage} firmwaresStorage
   * @return {Promise.<void>}
   */
  ({ firmwaresStore, firmwaresStorage }) => firmwaresStorage.load()
    .then(firmwares => firmwaresStore.init(firmwares))
);

/**
 * @param {FirmwareOriginEnum} origin
 * @param {stream.Readable} fileStream
 * @param {?string} md5
 * @return {Reader.<save~callback>}
 */
const save = (origin, fileStream, md5 = null) => reader(
  /**
   * @function save~callback
   * @param {FirmwaresStore} firmwaresStore
   * @param {FirmwaresStorage} firmwaresStorage
   * @return {Promise.<CorrespondenceFirmware>}
   */
  ({ firmwaresStore, firmwaresStorage }) => firmwaresStorage.save(origin, fileStream, md5)
    .then(firmware => firmwaresStore.save(firmware))
);

/**
 * @param {CorrespondenceFirmware} firmware
 * @return {Reader.<remove~callback>}
 */
const remove = firmware => reader(
  /**
   * @function remove~callback
   * @param {FirmwaresStore} firmwaresStore
   * @param {FirmwaresStorage} firmwaresStorage
   * @return {Promise.<CorrespondenceFirmware>}
   */
  ({ firmwaresStore, firmwaresStorage }) => {
    const firmwareId = view(firmwareIdLens, firmware);
    const filename = view(firmwareFilenameLens, firmware);
    const origin = view(firmwareOriginLens, firmware);

    return firmwaresStorage.remove(origin, filename)
      .then(() => firmwaresStore.remove(firmwareId));
  }
);

/**
 * @param {FirmwareOriginEnum} origin
 * @return {Reader.<remove~callback>}
 */
const removeAll = origin => reader(
  /**
   * @function removeAll~callback
   * @param {FirmwaresStore} firmwaresStore
   * @param {FirmwaresStorage} firmwaresStorage
   * @return {Promise.<void>}
   */
  ({ firmwaresStore, firmwaresStorage }) =>
    firmwaresStorage.removeAll(origin)
      .then(() => firmwaresStore.removeAll(origin))
);

/**
 * @param {number} timestamp
 * @return {Reader.<countNewSince~callback>}
 */
const countNewSince = ({ timestamp }) => findAll(pipe(
  view(firmwareCreatedLens),
  lte(timestamp)
)).map(length);

module.exports = {
  findAll,
  findById,
  findLatestFirmware,
  findFirmwareDetails,
  load,
  save,
  remove,
  removeAll,
  countNewSince,
};
