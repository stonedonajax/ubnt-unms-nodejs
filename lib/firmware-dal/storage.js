'use strict';

/**
 * @file Handles filesystem related tasks
 */

const { Reader: reader } = require('monet');
const { Observable } = require('rxjs/Rx');
const { isFunction, values, stubObject } = require('lodash/fp');
const boom = require('boom');
const httpStatus = require('http-status');
const cloneable = require('cloneable-readable');
const pump = require('pump');
const path = require('path');
const fse = require('fs-extra');
const crypto = require('crypto');
const { isNotNull, isNotNil } = require('ramda-adjunct');
const { isNil } = require('ramda');

const { rejectP } = require('../util');
const { FirmwareOriginEnum } = require('../enums');
const { UnknownFirmwareImage, MismatchMD5FirmwareImage } = require('./image-parser/common');
const { parseFile } = require('./transformers/parsers');

const origins = values(FirmwareOriginEnum);

const handleError = (error) => {
  if (error instanceof UnknownFirmwareImage || error instanceof MismatchMD5FirmwareImage) {
    return rejectP(boom.wrap(error, httpStatus.UNPROCESSABLE_ENTITY));
  }

  return rejectP(error);
};

/**
 * @return {Reader.<load~callback>}
 */
const load = () => reader(
  /**
   * @function load~callback
   * @param {Function} firmwaresConfig
   * @return {Promise.<CorrespondenceFirmware[]>}
   */
  ({ firmwaresConfig }) => {
    const { dir } = firmwaresConfig();

    return Observable.from(origins)
      .mergeMap((origin) => {
        const dirName = path.join(dir, origin);
        return Observable.from(fse.ensureDir(dirName))
          .mergeMap(() => fse.readdir(dirName))
          .mergeAll()
          .mergeMap(
            filename => fse.stat(path.join(dirName, filename)).catch(stubObject),
            (filename, stat) => parseFile(filename, origin, stat)
          );
      })
      .filter(isNotNull)
      .toArray()
      .toPromise();
  }
);

/**
 * @param {FirmwareOriginEnum|string} origin
 * @param {string} filename
 * @return {Reader.<remove~callback>}
 */
const remove = (origin, filename) => reader(
  /**
   * @function remove~callback
   * @param {Function} firmwaresConfig
   * @return {Promise.<void>}
   */
  ({ firmwaresConfig }) => {
    const { dir } = firmwaresConfig();
    const filePath = path.join(dir, origin, filename);
    return fse.unlink(filePath);
  }
);

/**
 * @param {FirmwareOriginEnum|string} origin
 * @return {Reader.<removeAll~callback>}
 */
const removeAll = origin => reader(
  /**
   * @function removeAll~callback
   * @param {Function} firmwaresConfig
   * @return {Promise.<void>}
   */
  ({ firmwaresConfig }) => {
    const { dir } = firmwaresConfig();
    const originDir = path.join(dir, origin);
    return fse.emptyDir(originDir);
  }
);

/**
 * @param {FirmwareOriginEnum|string} origin
 * @param {stream.Readable} fileStream
 * @param {?string} md5
 * @return {Reader.<save~callback>}
 */
const save = (origin, fileStream, md5 = null) => reader(
  /**
   * @function save~callback
   * @param {Function} firmwaresConfig
   * @param {Function} createTempFile
   * @param {firmwareImageParser} firmwareImageParser
   * @return {Promise.<CorrespondenceFirmware>}
   */
  ({ firmwaresConfig, createTempFile, firmwareImageParser }) => {
    const { dir } = firmwaresConfig();
    const uploadDir = path.join(dir, origin);
    const stream = cloneable(fileStream); // make cloneable early to prevent stream close
    let cleanupCallback = null;

    return createTempFile()
      .then(({ writeStream: tempFileStream, cleanup, filePath }) => new Promise((resolve, reject) => {
        cleanupCallback = cleanup;
        let result = null;
        let callCounter = isNotNull(md5) ? 3 : 2;

        const resolver = (firmwareFilename) => { result = { filePath, firmwareFilename } };

        const finalizer = (err) => {
          callCounter -= 1;
          if (isNotNil(err)) {
            reject(err);
          } else if (callCounter === 0) {
            if (result === null || result.firmwareFilename === null) {
              reject(new UnknownFirmwareImage());
            } else {
              resolve(result);
            }
          }
        };

        // check md5 hash
        if (isNotNull(md5)) {
          const hashStream = crypto.createHash('md5');
          hashStream.setEncoding('hex');

          const hashStreamFinalizer = (err) => {
            if (isNil(err) && hashStream.read() !== md5) {
              return finalizer(new MismatchMD5FirmwareImage());
            }
            return finalizer(err);
          };

          pump(stream.clone(), hashStream, hashStreamFinalizer);
        }
        // save to temp file
        pump(stream.clone(), tempFileStream, finalizer);

        // detect firmware type and version
        pump(stream, firmwareImageParser(resolver), finalizer);
      }))
      .then(({ filePath: tmpFilePath, firmwareFilename }) => {
        const filePath = path.join(uploadDir, firmwareFilename);
        return fse.move(tmpFilePath, filePath, { overwrite: true })
          .then(() => fse.stat(filePath).catch(stubObject))
          .then(stat => parseFile(firmwareFilename, origin, stat))
          .then(cmFirmware => (cmFirmware === null
            ? rejectP(new UnknownFirmwareImage())
            : cmFirmware));
      })
      .catch((error) => {
        if (isFunction(cleanupCallback)) { cleanupCallback() }
        return handleError(error);
      });
  }
);

module.exports = {
  load,
  save,
  remove,
  removeAll,
};
