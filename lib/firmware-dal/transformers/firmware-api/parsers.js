'use strict';

const moment = require('moment-timezone');
const { getOr, __, defaultTo } = require('lodash/fp');

const {
  modelsForPlatformId, isFirmwareSupported, hasCustomScriptsSupport,
} = require('../../../feature-detection/firmware');
const { parseStableVersion, parseSemver, parseCommFirmwareVersion } = require('../../../transformers/semver/parsers');
const { FirmwareOriginEnum, FirmwarePlatformIdEnum } = require('../../../enums');
const { ChannelsEnum } = require('../../enums');

const channelToOrigin = (channel) => {
  switch (channel) {
    case ChannelsEnum.Release:
      return FirmwareOriginEnum.UBNT;
    case ChannelsEnum.Beta:
      return FirmwareOriginEnum.Beta;
    default:
      return null;
  }
};

/**
 * Map product to our platform id for supported devices
 * @type {Object<string, string>}
 */
const UBNT_PRODUCT_TO_PLATFORM_ID_MAP = Object.freeze({
  e50: FirmwarePlatformIdEnum.E50,
  e100: FirmwarePlatformIdEnum.E100,
  e200: FirmwarePlatformIdEnum.E200,
  e300: FirmwarePlatformIdEnum.E300,
  'uf-olt': FirmwarePlatformIdEnum.E600,
  e1000: FirmwarePlatformIdEnum.E1000,
  'uf-nano': FirmwarePlatformIdEnum.NanoG,
  'uf-loco': FirmwarePlatformIdEnum.Loco,
  esgh: FirmwarePlatformIdEnum.ESGH,
  eswh: FirmwarePlatformIdEnum.ESWH,
  esxp: FirmwarePlatformIdEnum.SW,
  af5xhd: FirmwarePlatformIdEnum.AF5XHD,
  ACB: FirmwarePlatformIdEnum.ACB,
  XC: FirmwarePlatformIdEnum.XC,
  WA: FirmwarePlatformIdEnum.WA,
  TI: FirmwarePlatformIdEnum.TI,
  XM: FirmwarePlatformIdEnum.XM,
  XW: FirmwarePlatformIdEnum.XW,
  '2XC': FirmwarePlatformIdEnum.XC2,
  '2WA': FirmwarePlatformIdEnum.WA2,
});

const DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm:ssZZ';

const parsePlatformId = getOr(null, __, UBNT_PRODUCT_TO_PLATFORM_ID_MAP);

const parseApiFirmware = (firmware) => {
  const origin = channelToOrigin(firmware.channel);
  const platformId = parsePlatformId(firmware.product);
  const parsedVersion = parseSemver(parseCommFirmwareVersion(firmware.version));

  // not a valid semver, platform or channel - ignore
  if (platformId === null || origin === null || parsedVersion === null) { return null }

  // we will take updated time
  const date = moment(firmware.updated, DATE_TIME_FORMAT).valueOf();

  return {
    identification: {
      id: firmware.id,
      version: parsedVersion.raw,
      stable: parseStableVersion(parsedVersion),
      filename: null,
      origin,
      platformId,
      models: defaultTo([], modelsForPlatformId(platformId)),
    },
    supports: {
      airMaxCustomScripts: hasCustomScriptsSupport(platformId, firmware.version),
      UNMS: isFirmwareSupported(platformId, firmware.version),
    },
    semver: parsedVersion,
    path: null,
    url: getOr(null, ['_links', 'data', 'href'], firmware),
    secureUrl: null,
    size: getOr(0, 'file_size', firmware),
    md5: firmware.md5,
    date,
    created: date,
  };
};

module.exports = {
  parseApiFirmware,
};
