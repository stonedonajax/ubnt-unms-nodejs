'use strict';

const { compose, noop } = require('lodash/fp');
const { lensPath, tryCatch } = require('ramda');
const { isNotNil } = require('ramda-adjunct');
const tmp = require('tmp');
const fs = require('fs');

const { compareSemver } = require('../util/semver');

const firmwareIdentificationLens = lensPath(['identification']);
const firmwareSupportsLens = lensPath(['supports']);
const firmwareDateLens = lensPath(['date']);
const firmwareCreatedLens = lensPath(['created']);
const firmwareIdLens = compose(firmwareIdentificationLens, lensPath(['id']));
const firmwareFilenameLens = compose(firmwareIdentificationLens, lensPath(['filename']));
const firmwareOriginLens = compose(firmwareIdentificationLens, lensPath(['origin']));
const firmwareVersionLens = compose(firmwareIdentificationLens, lensPath(['version']));
const firmwarePlatformIdLens = compose(firmwareIdentificationLens, lensPath(['platformId']));
const firmwareDeviceModelsLens = compose(firmwareIdentificationLens, lensPath(['models']));
const firmwareHasCustomScriptsSupportLens = compose(firmwareSupportsLens, lensPath(['airMaxCustomScripts']));

/**
 * @return {Promise.<{ writeStream: Stream, cleanup: Function, filePath: string }>}
 */
const createTempFile = () => new Promise((resolve, reject) => {
  tmp.file({ keep: true }, (err, filePath, fd, cleanupCallback) => {
    if (isNotNil(err)) { return reject(err) }

    const cleanup = tryCatch(cleanupCallback, noop);
    const writeStream = fs.createWriteStream(filePath, { fd })
      .on('error', () => cleanup());
    return resolve({ writeStream, cleanup, filePath });
  });
});

/**
 * @param {CorrespondenceFirmware} a
 * @param {CorrespondenceFirmware} b
 * @return {number}
 */
const firmwareComparator = (a, b) => {
  const result = compareSemver(a.semver, b.semver);
  if (result === 0) {
    const idA = a.identification.id;
    const idB = b.identification.id;
    return idA < idB ? -1 : Number(idA > idB);
  }

  return result;
};

module.exports = {
  firmwareIdLens,
  firmwareFilenameLens,
  firmwareDateLens,
  firmwareCreatedLens,
  firmwareOriginLens,
  firmwareDeviceModelsLens,
  firmwarePlatformIdLens,
  firmwareVersionLens,
  firmwareHasCustomScriptsSupportLens,
  firmwareComparator,
  createTempFile,
};
