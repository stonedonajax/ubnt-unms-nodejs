'use strict';

const { pathSatisfies } = require('ramda');
const { isNilOrEmpty } = require('ramda-adjunct');

class AmqpState {
  constructor() {
    this.initialized = false;
    this.connection = null;
    this.consumerTags = {};
    this.queueChannels = {};
    this.publishChannel = null;
    this.serviceChannel = null;
  }

  initialize(connection) {
    this.initialized = true;
    this.connection = connection;
    return this;
  }

  setPublishChannel(channel) {
    this.publishChannel = channel;
    return this;
  }

  getPublishChannel() {
    return this.publishChannel;
  }

  getConnection() {
    return this.connection;
  }

  setQueueChannel(queueId, channel) {
    this.queueChannels[queueId] = channel;
    return this;
  }

  getQueueChannel(queueId) {
    return this.queueChannels[queueId];
  }

  deleteQueueChannel(queueId) {
    delete this.queueChannels[queueId];
    return this;
  }

  getConsumerTag(name) {
    if (pathSatisfies(isNilOrEmpty, [name], this.consumerTags)) {
      return null;
    }

    return this.consumerTags[name];
  }

  storeConsumerTag(name, tag) {
    this.consumerTags[name] = tag;
    return this;
  }

  removeConsumerTag(name) {
    delete this.consumerTags[name];
    return this;
  }
}

const getAmqpState = () => new AmqpState();

module.exports = getAmqpState;
