'use strict';

const { Reader: reader } = require('monet');
const uuid = require('aguid');

const { WsNotificationTypeEnum } = require('../enums');

const deviceNotificationMessage = (deviceId, type, payload) => reader(
  ({ config }) => ({
    exchange: config.rabbitMQ.notifyExchange,
    routingKey: `${config.rabbitMQ.notifyExchange}.device.${deviceId}.${type}`,
    content: Buffer.from(JSON.stringify(payload)),
    options: {
      messageId: uuid(),
      persistent: false,
      contentType: 'application/json',
      type: WsNotificationTypeEnum.DeviceNotifications,
      headers: {
        payloadType: type,
        entity: deviceId,
      },
      timestamp: Date.now(),
    },
  })
);

const speedTestNotificationMessage = (speedTestId, type, payload) => reader(
  ({ config }) => ({
    exchange: config.rabbitMQ.notifyExchange,
    routingKey: `${config.rabbitMQ.notifyExchange}.speed-test.${type}`,
    content: Buffer.from(JSON.stringify(payload)),
    options: {
      messageId: uuid(),
      persistent: false,
      contentType: 'application/json',
      type: WsNotificationTypeEnum.SpeedTestNotifications,
      headers: {
        payloadType: type,
        entity: speedTestId,
      },
      timestamp: Date.now(),
    },
  })
);

module.exports = {
  deviceNotificationMessage,
  speedTestNotificationMessage,
};
