'use strict';

const amqplib = require('amqplib');
const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../util/hapi');
const aqmpStateFactory = require('./amqp-state');
const {
  createExclusiveQueue, removeExclusiveQueue, subscribeToDevice, publish, unsubscribeFromDevice,
  subscribeToSpeedTests, unsubscribeFromSpeedTests, ensurePublishExchange,
} = require('./service');
const { deviceNotificationMessage, speedTestNotificationMessage } = require('./messages');


function register(server, options) {
  const config = server.settings.app;

  const amqpState = aqmpStateFactory();

  const messages = {
    deviceNotificationMessage: weave(deviceNotificationMessage, { config }),
    speedTestNotificationMessage: weave(speedTestNotificationMessage, { config }),
  };

  const pluginApi = {
    messages,
    createExclusiveQueue: weave(createExclusiveQueue, { amqpState, config }),
    removeExclusiveQueue: weave(removeExclusiveQueue, { amqpState, config }),
    subscribeToDevice: weave(subscribeToDevice, { amqpState, config }),
    unsubscribeFromDevice: weave(unsubscribeFromDevice, { amqpState, config }),
    subscribeToSpeedTests: weave(subscribeToSpeedTests, { amqpState, config }),
    unsubscribeFromSpeedTests: weave(unsubscribeFromSpeedTests, { amqpState, config }),
    publish: weave(publish, { amqpState, config }),
  };

  server.expose(pluginApi);

  return amqplib.connect(`amqp://${options.host}:${options.port}`)
    .then((conn) => {
      amqpState.initialize(conn);

      return conn.createChannel();
    })
    .then(channel => amqpState.setPublishChannel(channel))
    .then(weave(ensurePublishExchange, { amqpState, config }));
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'notificationHub',
  version: '1.0.0',
  dependencies: [],
};
