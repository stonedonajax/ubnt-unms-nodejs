'use strict';

const { Observable } = require('rxjs/Rx');
const { Reader: reader } = require('monet');
const { isNull, noop } = require('ramda-adjunct');

const { parseAmqpNotification } = require('./transformers/parsers');
const { getDeviceExchangeName, getSpeedTestExchangeName, getQueueName } = require('./util');
const { UbntNotFoundError } = require('../util/errors');


const createTempChannel = () => reader(
  ({ amqpState }) => Observable.create((subscriber) => {
    let openChannel;
    let isOpen = true;

    amqpState.getConnection().createChannel()
      .then((channel) => {
        channel.once('error', (e) => {
          isOpen = false;
          subscriber.error(e);
        });
        openChannel = channel;
        subscriber.next(channel);
      })
      .catch((e) => {
        isOpen = false;
        subscriber.error(e);
      });

    return () => {
      if (isOpen) {
        openChannel.close();
      }
    };
  })
);

const checkIfQueueExists = queueId => reader(
  ({ amqpState }) => {
    if (amqpState.getConsumerTag(queueId) === null) {
      return Observable.throw(new UbntNotFoundError('Connection not found.'));
    }

    return Observable.of(true);
  }
);

const ensurePublishExchange = () => reader(
  ({ amqpState, config }) => Observable.from(
    amqpState.getConnection()
      .createChannel()
      .then(channel => channel.assertExchange(config.rabbitMQ.notifyExchange, 'topic', { durable: false }))
  )
);

const createExclusiveQueue = queueId => reader(
  ({ amqpState }) => Observable.create((subscriber) => {
    const onMessage = (message) => {
      if (isNull(message)) {
        subscriber.complete();
      } else {
        try {
          subscriber.next(parseAmqpNotification(message));
        } catch (e) {
          subscriber.error(e);
        }
      }
    };

    amqpState.getConnection()
      .createChannel()
      .then(channel => amqpState.setQueueChannel(queueId, channel))
      .then(() => amqpState.getQueueChannel(queueId).assertQueue(getQueueName(queueId), {
        exclusive: true,
        autoDelete: true,
        durable: false,
      }))
      .then(() => amqpState.getQueueChannel(queueId).consume(getQueueName(queueId), onMessage, {
        noAck: true,
        exclusive: true,
      }))
      .then(({ consumerTag }) => amqpState.storeConsumerTag(queueId, consumerTag))
      .then(() => subscriber.next(JSON.stringify('WS_CHANNEL_OPEN')))
      .catch(error => subscriber.error(error));

    return () => {
      amqpState.getQueueChannel(queueId).cancel(amqpState.getConsumerTag(queueId))
        .then(() => amqpState.getQueueChannel(queueId).close())
        .then(() => amqpState.deleteQueueChannel(queueId));
    };
  })
);

const removeExclusiveQueue = queueId => reader(
  ({ amqpState }) => Observable.create((subscriber) => {
    amqpState.getQueueChannel(queueId).cancel(amqpState.getConsumerTag(queueId))
      .then(() => amqpState.getQueueChannel(queueId).close())
      .then(() => amqpState.deleteQueueChannel(queueId))
      .then(() => subscriber.complete())
      .catch(error => subscriber.error(error));

    return noop;
  })
);

const ensureDeviceExchange = deviceId => reader(
  ({ amqpState, config }) => createTempChannel()
    .run({ amqpState })
    .mergeMap(channel => Observable.from(channel.assertExchange(config.rabbitMQ.notifyExchange, 'topic', {
      durable: false,
    })).mapTo(channel))
    .mergeMap(channel => Observable.from(channel.assertExchange(getDeviceExchangeName(deviceId), 'topic', {
      durable: false,
      internal: true,
    })).mapTo(channel))
    .mergeMap(channel => Observable.from(
      channel.bindExchange(
        getDeviceExchangeName(deviceId), config.rabbitMQ.notifyExchange, `${getDeviceExchangeName(deviceId)}.#`)
      )
    )
    .mapTo(true)
);

const subscribeToDevice = (queueId, entity) => reader(
  ({ amqpState }) => checkIfQueueExists(queueId)
    .run({ amqpState })
    .mergeMap(() => ensureDeviceExchange(entity).run({ amqpState }))
    .mergeMap(() => createTempChannel().run({ amqpState }))
    .mergeMap(channel => Observable.from(
      channel.bindQueue(getQueueName(queueId), getDeviceExchangeName(entity), '#')
    ))
);

const unsubscribeFromDevice = (queueId, entity) => reader(
  ({ amqpState }) => checkIfQueueExists(queueId)
    .run({ amqpState })
    .mergeMap(() => ensureDeviceExchange(entity).run({ amqpState }))
    .mergeMap(() => createTempChannel().run({ amqpState })))
    .mergeMap(channel => Observable.from(
      channel.unbindQueue(getQueueName(queueId), getDeviceExchangeName(entity), '#')
    )
);

const subscribeToSpeedTests = queueId => reader(
  ({ amqpState, config }) => checkIfQueueExists(queueId)
    .run({ amqpState })
    .mergeMap(() => createTempChannel().run({ amqpState }))
    .mergeMap(
      channel => Observable.from(channel.assertExchange(getSpeedTestExchangeName(), 'topic', {
        durable: false,
        internal: true,
      })).mapTo(channel)
    )
    .mergeMap(channel => Observable.from(
      channel.bindExchange(
        getSpeedTestExchangeName(), config.rabbitMQ.notifyExchange, `${getSpeedTestExchangeName()}.#`
      )
    ).mapTo(channel))
    .mergeMap(channel => Observable.from(
      channel.bindQueue(getQueueName(queueId), getSpeedTestExchangeName(), '#')
    ).mapTo(true))
);

const unsubscribeFromSpeedTests = queueId => reader(
  ({ amqpState }) => checkIfQueueExists(queueId)
    .run({ amqpState })
    .mergeMap(() => createTempChannel().run({ amqpState }))
    .mergeMap(channel => Observable.from(
      channel.unbindQueue(getQueueName(queueId), getSpeedTestExchangeName(), '#')
    ))
    .mapTo(true)
);

const publish = message => reader(
  ({ amqpState }) => amqpState.getPublishChannel()
    .publish(message.exchange, message.routingKey, message.content, message.options)
);

module.exports = {
  ensurePublishExchange,
  createExclusiveQueue,
  removeExclusiveQueue,
  subscribeToDevice,
  unsubscribeFromDevice,
  subscribeToSpeedTests,
  unsubscribeFromSpeedTests,
  publish,
};
