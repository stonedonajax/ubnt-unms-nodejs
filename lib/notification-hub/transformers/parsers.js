'use strict';

const { applySpec, pipe, pathOr, when, cond, equals, T, converge } = require('ramda');
const { isNull } = require('ramda-adjunct');

const parseAmqpNotificationPayload = (contentType, data) => cond([
  [equals('application/json'), () => JSON.parse(data.toString())],
  // insert more content-type transformers...
  [T, () => data.toString()],
])(contentType);

const parseAmqpNotification = applySpec({
  id: pathOr(null, ['properties', 'messageId']),
  timestamp: pathOr(null, ['properties', 'timestamp']),
  type: pipe(pathOr(null, ['properties', 'type']), when(isNull, () => { throw new Error('Unknown message type') })),
  payloadType: pipe(
    pathOr(null, ['properties', 'headers', 'payloadType']),
    when(isNull, () => { throw new Error('Unknown message payloadType') })
  ),
  entity: pipe(
    pathOr(null, ['properties', 'headers', 'entity']),
    when(isNull, () => { throw new Error('Unknown entity') })
  ),
  data: converge(parseAmqpNotificationPayload, [
    pathOr('application/json', ['properties', 'contentType']),
    pathOr(Buffer.from([]), ['content']),
  ]),
});

module.exports = {
  parseAmqpNotification,
};
