'use strict';

const { rabbitMQ } = require('../../config');

const getDeviceExchangeName = name => `${rabbitMQ.notifyExchange}.device.${name}`;
const getSpeedTestExchangeName = () => `${rabbitMQ.notifyExchange}.speed-test`;
const getUnmsEventExchangeName = name => `${rabbitMQ.notifyExchange}.unms.${name}`;
const getQueueName = id => `${rabbitMQ.notifyExchange}.${id}`;

module.exports = {
  getDeviceExchangeName,
  getSpeedTestExchangeName,
  getQueueName,
  getUnmsEventExchangeName,
};
