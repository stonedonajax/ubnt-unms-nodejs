'use strict';

const { weave, weaveLazy } = require('ramda-adjunct');

const { registerPlugin } = require('../util/hapi');
const { toMs } = require('../util');

// const handlers = require('./handlers');
const service = require('./service');

function register(server) {
  const { DB, deviceStore, logging, scheduler, dal, statistics } = server.plugins;
  const config = server.settings.app;

  const pluginApi = {
    createSite: weave(service.createSite, { dal }),
    updateSite: weave(service.updateSite, { dal }),
    increaseImageVersion: weave(service.increaseImageVersion, { dal }),
    clearNotification: weave(service.clearNotification, { dal }),
    addSiteNotification: weave(service.addSiteNotification, { dal }),
    getSites: weave(service.getSites, { dal }),
    getSite: weave(service.getSite, { dal }),
    deleteSite: weave(service.deleteSite, { dal }),
    getImages: weave(service.getImages, { dal }),
    saveImage: weave(service.saveImage, { dal }),
    getImage: weave(service.getImage, { dal }),
    updateImage: weave(service.updateImage, { dal }),
    deleteImage: weave(service.deleteImage, { dal }),
    reorderImage: weave(service.reorderImage, { dal }),
    getSiteEndpointsCount: weave(service.getSiteEndpointsCount, { dal }),
    getDevicesBySiteId: weave(service.getDevicesBySiteId, { DB }),
    synchronizeSitesStatus: weave(service.synchronizeSitesStatus, { DB, deviceStore, logging, dal }),
    synchronizeSiteStatus: weave(service.synchronizeSiteStatus, { DB, deviceStore, logging, dal }),
    synchronizeSiteStatusByDevice: weave(service.synchronizeSiteStatusByDevice, { DB, deviceStore, logging, dal }),
    handleSitesImport: weaveLazy(
      service.handleSitesImport,
      () => ({ dal, DB, deviceEvents: server.plugins.deviceEvents })
    ),
    clearAllDeviceSiteRelations: weave(service.clearAllDeviceSiteRelations, { dal }),
    getSiteAndEndpointCountsByStatus: weave(service.getSiteAndEndpointCountsByStatus, { dal }),
    saveSiteStatistics: weave(service.saveSiteStatistics, { dal, statistics }),
  };

  // register periodic tasks
  if (!config.demo) {
    scheduler.registerPeriodicTask(pluginApi.synchronizeSitesStatus, toMs('seconds', 10), 'synchronizeSitesStatus');
    scheduler.registerPeriodicTask(pluginApi.saveSiteStatistics, toMs('minute', 1), 'saveSiteStatistics');
  }

  server.expose(pluginApi);
  // messageHub.registerHandlers(handlers);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'site',
  version: '1.0.0',
  dependencies: ['DB', 'deviceStore', 'deviceEvents', 'messageHub', 'scheduler', 'logging', 'dal'],
};
