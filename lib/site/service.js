'use strict';

const { Reader: reader } = require('monet');
const {
  pathEq, pathSatisfies, ifElse, assocPath, filter, prop, concat, converge, assoc, when, isEmpty, propEq,
} = require('ramda');
const {
  isNil, reject, flow, keyBy, groupBy, mapValues, get, map, constant, identity, every, assign, getOr,
  toLower, has, sumBy,
} = require('lodash/fp');
const log = require('../logging');
const { Observable } = require('rxjs/Rx');
const { cata, weave, isNotNull } = require('ramda-adjunct');

const {
  fromDbList: fromDbSiteList, fromDb: fromDbSite, toDb: toDbSite, fromDbImageList, fromDbImage, toDbImage,
} = require('../transformers/site');
const { fromDbList: fromDbDeviceList } = require('../transformers/device');
const { allP, rejectP, resolveP, tapP } = require('../util');
const { StatusEnum, EntityEnum } = require('../enums');
require('../util/observable');
const { SiteTypeEnum } = require('../enums');
const { createNewSite } = require('../api/v2.1/sites/util');
const { entityExistsCheck, pathNotEq } = require('../util');

const siteOrEndpointAlreadyExists = (site, sitesAndEndpointsMap) =>
  has([`${site.type}${toLower(site.name)}`], sitesAndEndpointsMap);

// isEndpoint :: Object -> Boolean
const isEndpoint = propEq('type', SiteTypeEnum.Endpoint);

// isSite :: Object -> Boolean
const isSite = propEq('type', SiteTypeEnum.Site);

const calculateSiteStatus = flow(
  every(pathEq(['overview', 'status'], StatusEnum.Active)),
  ifElse(identity, constant(StatusEnum.Active), constant(StatusEnum.Disconnected))
);

const isDeviceInSite = pathEq(['identification', 'siteId']);

const isDeviceUnauthorized = isDeviceInSite(null);

const indexSitesAndEndpoints = keyBy(converge(concat, [
  get(['identification', 'type']),
  flow(get(['identification', 'name']), toLower),
]));

const getSitesAndEndpointsMap = () => reader(
  ({ dal }) => Observable.from(dal.siteRepository.findAllSites())
    .mergeEither(fromDbSiteList({}))
    .map(indexSitesAndEndpoints)
);

const getSiteAndEndpointCountsByStatus = () => reader(
  ({ dal }) => dal.siteRepository.getSiteAndEndpointCountsByStatus()
);

const synchronizeSiteStatus = siteId => reader(
  ({ DB, deviceStore, logging, dal }) => allP([
    dal.siteRepository.findOneById(siteId)
      .then(tapP(entityExistsCheck(EntityEnum.Site)))
      .then(fromDbSite({}))
      .then(cata(rejectP, resolveP)),
    DB.device.list()
      .then(fromDbDeviceList({ deviceStore }))
      .then(cata(rejectP, resolveP))
      .then(filter(isDeviceInSite(siteId))),
  ])
    .then(([cmSite, cmDeviceList]) => {
      const status = isEmpty(cmDeviceList) ? StatusEnum.Inactive : calculateSiteStatus(cmDeviceList);
      if (cmSite.identification.status !== status) {
        return toDbSite(assocPath(['identification', 'status'], status, cmSite))
          .cata(rejectP, dal.siteRepository.saveSite);
      }
      return resolveP();
    })
    .catch(error => logging.error('Failed to synchronize site status', error))
);

const clearNotification = siteId => reader(
  ({ dal }) => Observable.from(dal.siteNotificationRepository.clearNotification(siteId))
);

const addSiteNotification = (siteId, user) => reader(
  ({ dal }) => Observable.from(dal.siteNotificationRepository.addSiteNotification(siteId, user.id))
);

const getSites = siteIds => reader(
  ({ dal }) => Observable.from(dal.siteRepository.findWithEndpointsAndUsers(siteIds))
    .mergeEither(fromDbSiteList({}))
);

const getSite = siteId => reader(
  ({ dal }) => Observable.from(dal.siteRepository.findOneWithEndpointsAndUsers(siteId))
    .do(entityExistsCheck(SiteTypeEnum.Site))
    .mergeEither(fromDbSite({}))
);

const updateSite = dbSite => reader(
  ({ dal }) => Observable.from(dal.siteRepository.saveSite(dbSite))
    .mergeMap(() => getSite(dbSite.identification.id).run({ dal }))
);

const synchronizeSitesStatus = () => reader(
  ({ DB, deviceStore, logging, dal }) => allP([
    dal.siteRepository.findAllSites().then(fromDbSiteList({})).then(cata(rejectP, resolveP)),
    DB.device.list().then(fromDbDeviceList({ deviceStore })).then(cata(rejectP, resolveP)),
  ])
    .then(([cmSiteList, cmDeviceList]) => {
      const sitesById = keyBy(get(['identification', 'id']), cmSiteList);
      const siteStatus = flow(
        reject(pathSatisfies(isNil, ['identification', 'siteId'])),
        groupBy(get(['identification', 'siteId'])),
        mapValues(calculateSiteStatus),
        assign(mapValues(constant(StatusEnum.Inactive), sitesById))
      )(cmDeviceList);

      return cmSiteList
        .filter((cmSite) => {
          const newStatus = siteStatus[cmSite.identification.id];
          return cmSite.identification.status !== newStatus;
        })
        .map(cmSite => assocPath(['identification', 'status'], siteStatus[cmSite.identification.id], cmSite));
    })
    .then(map(toDbSite))
    .then(map(cata(rejectP, weave(updateSite, { dal }))))
    .then(allP)
    .catch(error => logging.error('Failed to synchronize sites status', error))
);

const synchronizeSiteStatusByDevice = cmDevice => reader(
  ({ DB, deviceStore, logging, dal }) => resolveP(getOr(null, ['identification', 'siteId'], cmDevice))
    .then(when(isNotNull, weave(synchronizeSitesStatus, { DB, deviceStore, logging, dal })))
);

const createSite = site => reader(
  ({ dal }) => Observable.from(dal.siteRepository.saveNewSite(site))
    .switchMap(newSite => getSite(newSite.id).run({ dal }))
);

const getSiteEndpointsCount = siteId => reader(
  ({ dal }) => Observable.from(dal.siteRepository.findOneWithEndpointsCount(siteId))
    .do(entityExistsCheck(SiteTypeEnum.Site))
    .map(prop('endpointsCount'))
);

const deleteSite = siteId => reader(
  ({ dal }) => Observable.from(dal.siteRepository.remove(siteId))
);

const getDevicesBySiteId = siteId => reader(
  ({ DB }) => Observable.from(DB.device.list(siteId))
    .map(filter(pathEq(['identification', 'site', 'id'], siteId)))
);

const getImages = (siteId, hostname) => reader(
  ({ dal }) => Observable.from(dal.siteImageRepository.findImages({ siteId }))
    .mergeEither(fromDbImageList({ hostname }))
);

const getImage = imageId => reader(
  ({ dal }) => Observable.from(dal.siteImageRepository.findImage(imageId))
    .do(entityExistsCheck('siteImage'))
    .mergeEither(fromDbImage({}))
);

const increaseImageVersion = cmImage => reader(
  ({ dal }) => Observable.of(cmImage)
    .map(assoc('version', cmImage.version + 1))
    .mergeEither(toDbImage)
    .mergeMap(dal.siteImageRepository.updateImage)
);

const updateImage = cmImage => reader(
  ({ dal }) => Observable.of(cmImage)
    .mergeEither(toDbImage)
    .mergeMap(dal.siteImageRepository.updateImage)
);

const deleteImage = imageId => reader(
  ({ dal }) => Observable.from(dal.siteImageRepository.deleteImage(imageId))
);

const reorderImage = (siteId, currentOrder, nextOrder) => reader(
  ({ dal }) => Observable.from(dal.siteImageRepository.reorderImage(siteId, currentOrder, nextOrder))
);

const saveImage = image => reader(
  ({ dal }) => Observable.from(dal.siteImageRepository.saveImage(image))
);

const clearAllDeviceSiteRelations = () => reader(
  ({ dal }) => Observable.from(dal.deviceSiteRepository.clearAllRelations())
);

const normalizeSitesImportDataRow = when(pathEq(['type'], 'client'), assoc('type', SiteTypeEnum.Endpoint));

const canImport = (sitesAndEndpointsMap, row) =>
  isSite(row) || has([`${SiteTypeEnum.Site}${toLower(row.parentSiteName)}`], sitesAndEndpointsMap);

/* eslint-disable no-param-reassign */
const saveImportedSiteOrEndpoint = row => reader(({ dal, sitesAndEndpointsMap }) => {
  if (!canImport(sitesAndEndpointsMap, row)) {
    return Observable.empty();
  }

  // translate parent site's name to parent site's id
  if (isEndpoint(row)) {
    row.parentSiteId = get(
      [`${SiteTypeEnum.Site}${toLower(row.parentSiteName)}`, 'identification', 'id'],
      sitesAndEndpointsMap
    );
  }

  const site$ = siteOrEndpointAlreadyExists(row, sitesAndEndpointsMap)
    ? Observable.of(get([`${row.type}${toLower(row.name)}`], sitesAndEndpointsMap))
    : Observable.from(dal.siteRepository.saveNewSite(createNewSite(row)))
        .mergeEither(fromDbSite({}));

  return site$
    .do((newSite) => {
      const { type, name } = newSite.identification;
      sitesAndEndpointsMap[`${type}${toLower(name)}`] = newSite;
    })
    .tapO(site => dal.deviceSiteRepository.addRelations(site.identification.id, row.ipAddresses, row.macAddresses));
});
/* eslint-enable no-param-reassign */

const handleSitesImport = source => reader(({ dal, DB, deviceEvents }) => {
  const sitesAndEndpointsMap$ = getSitesAndEndpointsMap().run({ dal });
  const [importSites$, importEndpoints$] = Observable.from(source).map(normalizeSitesImportDataRow).partition(isSite);

  return sitesAndEndpointsMap$
    .mergeMap(sitesAndEndpointsMap => Observable.concat(importSites$, importEndpoints$)
      .concatMap(weave(saveImportedSiteOrEndpoint, { sitesAndEndpointsMap, dal }))
    )
    .ignoreElements()
    .concat(Observable.of(true))
    .mergeMap(() => Observable.from(DB.device.list())
      .mergeEither(fromDbDeviceList({}))
      .mergeAll()
      .filter(isDeviceUnauthorized)
      .mergeMap(cmDevice => deviceEvents.deviceAuthorizationQueue.tryAuthorize(cmDevice))
    )
    .catch((error) => {
      log.error('Sites import failed', error);
      return Observable.of(false);
    })
    .toPromise();
});

const saveSiteStatistics = () => reader(
  ({ dal, statistics }) => Observable.from(dal.siteRepository.getSiteAndEndpointCountsByStatus())
    .mergeMap((result) => {
      // normalize result structure
      const siteStatistics = assign({
        [SiteTypeEnum.Site]: [{ count: 0, status: StatusEnum.Active }],
        [SiteTypeEnum.Endpoint]: [{ count: 0, status: StatusEnum.Active }],
      }, groupBy('type', result));

      const allSites = sumBy('count', siteStatistics[SiteTypeEnum.Site]
        .filter(pathNotEq(['status'], StatusEnum.Inactive)));
      const liveSites = sumBy('count', siteStatistics[SiteTypeEnum.Site]
        .filter(pathEq(['status'], StatusEnum.Active)));
      const allClients = sumBy('count', siteStatistics[SiteTypeEnum.Endpoint]
        .filter(pathNotEq(['status'], StatusEnum.Inactive)));
      const liveClients = sumBy('count', siteStatistics[SiteTypeEnum.Endpoint]
        .filter(pathEq(['status'], StatusEnum.Active)));

      const total = allSites + allClients;

      return statistics.collectForDevice('sites', {
        timestamp: Date.now(),
        weight: 1,
        stats: {
          networkHealth: total > 0 ? ((liveSites + liveClients) / (total)) : 0,
          allSites,
          liveSites,
          allClients,
          liveClients,
        },
      });
    })
);

module.exports = {
  createSite,
  updateSite,
  clearNotification,
  addSiteNotification,
  getSites,
  getSite,
  deleteSite,
  getImages,
  getImage,
  updateImage,
  deleteImage,
  reorderImage,
  getSiteEndpointsCount,
  getDevicesBySiteId,
  saveImage,
  increaseImageVersion,
  synchronizeSitesStatus,
  synchronizeSiteStatus,
  synchronizeSiteStatusByDevice,
  handleSitesImport,
  clearAllDeviceSiteRelations,
  getSiteAndEndpointCountsByStatus,
  saveSiteStatistics,
};
