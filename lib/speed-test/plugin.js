'use strict';

const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../util/hapi');
const { WsNotificationTypeEnum } = require('../enums');
const stateFactory = require('./state');

const { start, stop, getSpeedTestDetail, getSpeedTests, handleSubscription } = require('./service');

function register(server) {
  const { notificationHub, deviceStore, DB, socketApi } = server.plugins;
  const state = stateFactory();

  socketApi.registerHandler(
    WsNotificationTypeEnum.SpeedTestNotifications,
    weave(handleSubscription, { notificationHub })
  );

  const service = {
    start: weave(start, { notificationHub, deviceStore, state, DB }),
    stop: weave(stop, { state }),
    getSpeedTestDetail: weave(getSpeedTestDetail, { state }),
    getSpeedTests: weave(getSpeedTests, { state }),
  };

  server.expose(service);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'speedTest',
  version: '1.0.0',
  dependencies: ['notificationHub', 'socketApi', 'deviceStore', 'DB'],
};
