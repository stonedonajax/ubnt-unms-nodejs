'use strict';

const { Observable } = require('rxjs');
const { Reader: reader } = require('monet');
const {
  path, when, pipe, split, head, unary, cond, pathEq, converge,
  defaultTo, map, flatten, filter, pathSatisfies, lt,
} = require('ramda');
const { isNotNull, list, isNotNil, isNilOrEmpty } = require('ramda-adjunct');
const { noop } = require('lodash/fp');
const uuid = require('aguid');

const {
  EntityEnum, SpeedTestDirectionEnum, SpeedTestNotificationTypeEnum, WsNotificationActionEnum,
} = require('../enums');
const { entityExistsCheck, toMs } = require('../util');
const { UbntNotAcceptableError, UbntError } = require('../util/errors');
const {
  toWsSpeedTestStatsNotification, toWsSpeedTestStartNotification, toWsSpeedTestStopNotification,
  toWsSpeedTestErrorNotification,
} = require('./transformers/mappers');

const IPERF_KEEPALIVE = toMs('second', 10);
const MAX_SPEED_THRESHOLD = 11000000000;

const setupServer = (testId, deviceId) => reader(
  ({ deviceStore }) => Observable.of(deviceStore.get(deviceId))
    .do(entityExistsCheck(EntityEnum.Device))
    .mergeMap(commDevice => commDevice.startIperfServer(testId, IPERF_KEEPALIVE))
);

const getAddresses = pipe(path(['addresses']), filter(pathSatisfies(isNotNil, ['cidr'])));
const getIps = map(pipe(path(['cidr']), split('/'), head));

const setupClient = (testId, deviceId, serverId) => reader(
  ({ deviceStore, DB }) => Observable.of(deviceStore.get(deviceId))
    .do(entityExistsCheck(EntityEnum.Device))
    .mergeMap(
      commDevice => Observable.from(DB.device.findById(serverId))
        .do(entityExistsCheck(EntityEnum.Device))
        .map(unary(converge(defaultTo, [
          pipe(path(['identification', 'ipAddress']), when(isNotNil, list), defaultTo([])),
          pipe(path(['interfaces']), map(pipe(getAddresses, getIps)), flatten),
        ])))
        .map(when(isNilOrEmpty, () => { throw new UbntNotAcceptableError('Device has no IP address') }))
        .mergeMap(ipList => commDevice.startIperfClient(testId, IPERF_KEEPALIVE, ipList))
    )
);

const sendKeepalive = (testId, deviceId) => reader(
  ({ deviceStore }) => Observable.of(deviceStore.get(deviceId))
    .do(entityExistsCheck(EntityEnum.Device))
    .mergeMap(commDevice => commDevice.iperfKeepalive(testId))
);

const start = options => reader(
  ({ config, notificationHub, deviceStore, state, DB }) => {
    const { publish, messages: { speedTestNotificationMessage } } = notificationHub;
    let speedTest$;
    const testId = uuid();

    // setup server and client based on speed test type
    if (options.direction === SpeedTestDirectionEnum.Downlink) {
      speedTest$ = Observable.concat(
        setupServer(testId, options.source).run({ config, deviceStore }).ignoreElements(),
        setupClient(testId, options.target, options.source).run({ config, deviceStore, DB })
      );
    } else if (options.direction === SpeedTestDirectionEnum.Uplink) {
      speedTest$ = Observable.concat(
        setupServer(testId, options.target).run({ config, deviceStore }).ignoreElements(),
        setupClient(testId, options.source, options.target).run({ config, deviceStore, DB })
      );
    } else {
      speedTest$ = Observable.concat(
        Observable.merge(
          setupServer(testId, options.source).run({ config, deviceStore }).ignoreElements(),
          setupServer(testId, options.target).run({ config, deviceStore }).ignoreElements()
        ),
        Observable.merge(
          setupClient(testId, options.target, options.source).run({ config, deviceStore, DB }),
          setupClient(testId, options.source, options.target).run({ config, deviceStore, DB })
        )
      );
    }

    // setup keepalive messages
    speedTest$ = Observable.merge(
      speedTest$,
      Observable.interval(IPERF_KEEPALIVE / 2)
        .mergeMap(() => Observable.merge(
          sendKeepalive(testId, options.source).run({ deviceStore }),
          sendKeepalive(testId, options.target).run({ deviceStore })
        ))
        .ignoreElements()
    );

    // push results to notificationHub
    speedTest$ = speedTest$
      .map(unary(toWsSpeedTestStatsNotification(testId, options.source)))
      .map(unary(when(
        pathSatisfies(lt(MAX_SPEED_THRESHOLD), ['speed']),
        () => { throw new UbntError('Speedtest failed') }
      )))
      .do(notification => state.update(testId, notification))
      .do(notification =>
        publish(speedTestNotificationMessage(testId, SpeedTestNotificationTypeEnum.Stats, notification))
      );

    // push error message notification
    speedTest$ = speedTest$
      .catch((e) => {
        let error;

        if (e instanceof UbntError) {
          error = e;
        } else {
          error = new UbntError(e.message);
        }

        const notification = toWsSpeedTestErrorNotification({ testId, error });

        return Observable.from(
          publish(speedTestNotificationMessage(
            testId,
            SpeedTestNotificationTypeEnum.Error,
            notification
          ))
        ).mergeMapTo(Observable.throw(error));
      });

    // limit lifetime if duration is set
    if (isNotNull(options.duration)) {
      speedTest$ = speedTest$.takeUntil(Observable.timer(options.duration));
    }


    speedTest$ = speedTest$
      .finally(() => {
        // try to stop iperf
        const sourceCommDevice = deviceStore.get(options.source);
        if (sourceCommDevice !== null) {
          sourceCommDevice.stopIperf(testId).subscribe(noop, noop);
        }

        const targetCommDevice = deviceStore.get(options.target);
        if (targetCommDevice !== null) {
          targetCommDevice.stopIperf(testId).subscribe(noop, noop);
        }
      })
      .finally(
        // push stop to notificationHub when unsubscribed
        () => notificationHub.publish(speedTestNotificationMessage(
          testId,
          SpeedTestNotificationTypeEnum.Stop,
          toWsSpeedTestStopNotification(state.getTestDetail(testId))
        )
      ));

    // store in state and push start to notificationHub
    return Observable.of(state.start(testId, options, speedTest$))
      .do(id => notificationHub.publish(speedTestNotificationMessage(
        id,
        SpeedTestNotificationTypeEnum.Start,
        toWsSpeedTestStartNotification(state.getTestDetail(id))
      )));
  }
);

const stop = speedTestId => reader(
  ({ state }) => Observable.of(speedTestId)
    .map(unary(state.stop.bind(state)))
);

const getSpeedTests = () => reader(
  ({ state }) => Observable.of(state.getTests())
);

const getSpeedTestDetail = id => reader(
  ({ state }) => Observable.of(state.getTestDetail(id))
);

const handleSubscription = options => reader(
  ({ notificationHub }) => cond([
    [
      pathEq(['action'], WsNotificationActionEnum.Subscribe),
      () => notificationHub.subscribeToSpeedTests(options.connectionId),
    ],
    [
      pathEq(['action'], WsNotificationActionEnum.Unsubscribe),
      () => notificationHub.unsubscribeFromSpeedTests(options.connectionId),
    ],
  ])(options)
);

module.exports = {
  start,
  stop,
  getSpeedTests,
  getSpeedTestDetail,
  handleSubscription,
};
