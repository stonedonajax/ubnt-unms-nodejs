'use strict';

const { pipe, append, takeLast, map, fromPairs, over, lensIndex } = require('ramda');
const { noop } = require('ramda-adjunct');

const { UbntNotFoundError } = require('../util/errors');

class SpeedTest {
  constructor(id, meta, speedTest$) {
    this.id = id;
    this.data = [];
    this.timestamp = new Date();
    this.speedTest$ = speedTest$;
    this.meta = meta;
  }

  unsubscribe() {
    this.speedTest$.unsubscribe();
  }

  update(data) {
    this.data = pipe(append(data), takeLast(300))(this.data);
    return this;
  }

  toObject() {
    const { id, data, timestamp, meta } = this;
    return { id, data, timestamp, meta };
  }
}

class SpeedTestState {
  constructor() {
    this.tests = new Map();
  }

  stop(testId) {
    if (!this.tests.has(testId)) {
      throw new UbntNotFoundError('Speed test not found');
    }

    this.tests.get(testId).unsubscribe();
    this.tests.delete(testId);

    return true;
  }

  cleanUp(testId) {
    if (this.tests.has(testId)) {
      this.tests.get(testId).unsubscribe();
      this.tests.delete(testId);
    }
  }

  getTests() {
    return pipe(
      map(over(lensIndex(1), v => v.toObject())),
      fromPairs
    )([...this.tests]);
  }

  getTestDetail(id) {
    if (!this.tests.has(id)) {
      throw new UbntNotFoundError('Not found');
    }

    return this.tests.get(id).toObject();
  }

  update(id, data) {
    if (!this.tests.has(id)) {
      throw new UbntNotFoundError('Not found');
    }

    this.tests.get(id).update(data);
  }

  start(testId, metadata, speedTest$) {
    this.tests.set(testId, new SpeedTest(
      testId, metadata, speedTest$.subscribe(noop, () => this.cleanUp(testId), () => this.cleanUp(testId))
    ));

    return testId;
  }
}

const createSpeedTestState = () => new SpeedTestState();

module.exports = createSpeedTestState;
