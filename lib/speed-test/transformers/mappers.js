'use strict';

const { curry, ifElse, equals, always } = require('ramda');

const { SpeedTestDirectionEnum } = require('../../enums');

const toWsSpeedTestStatsNotification = curry((testId, sourceId, notification) => ({
  speedTestId: testId,
  timestamp: notification.timestamp,
  resultIndex: notification.index,
  speed: notification.bps,
  direction: ifElse(
    equals(sourceId),
    always(SpeedTestDirectionEnum.Uplink),
    always(SpeedTestDirectionEnum.Downlink)
  )(notification.deviceId),
}));

const toWsSpeedTestStartNotification = notification => ({
  speedTestId: notification.id,
  masterId: notification.meta.source,
  slaveId: notification.meta.target,
  timeLimit: notification.meta.duration,
  timestamp: notification.timestamp,
  direction: notification.meta.direction,
});

const toWsSpeedTestStopNotification = notification => ({
  speedTestId: notification.id,
  masterId: notification.meta.source,
  slaveId: notification.meta.target,
  timeLimit: notification.meta.duration,
  timestamp: notification.timestamp,
});

const toWsSpeedTestErrorNotification = notification => ({
  speedTestId: notification.testId,
  timestamp: Date.now(),
  error: notification.error.toWsError(),
});

module.exports = {
  toWsSpeedTestStatsNotification,
  toWsSpeedTestStartNotification,
  toWsSpeedTestStopNotification,
  toWsSpeedTestErrorNotification,
};
