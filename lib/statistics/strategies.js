'use strict';

const { isNil, keys, getOr, has } = require('lodash/fp');
const { assocPath } = require('ramda');

const { weightedAverage } = require('../util');


// strategy for merging device statistics
// mergeDeviceStats :: (Object, Object) -> Object
function mergeDeviceStats(cmOldStats, cmNewStats) {
  if (isNil(cmOldStats)) return cmNewStats;

  const result = Object.assign({}, cmOldStats, { weight: (cmOldStats.weight || 0) + cmNewStats.weight, stats: {} });

  Object
    .keys(cmNewStats.stats)
    .forEach((name) => {
      result.stats[name] = cmOldStats.stats && Object.hasOwnProperty.call(cmOldStats.stats, name)
        ? weightedAverage(cmOldStats.stats[name], cmOldStats.weight, cmNewStats.stats[name], cmNewStats.weight)
        : cmNewStats.stats[name];
    });

  return result;
}

// strategy for merging device sub statistics
// mergeStatsObject :: (Object, Object) -> Object
const mergeStatsObject = key => (cmOldStats, cmNewStats) => {
  const newNames = keys(cmNewStats[key]);
  const mergedStats = newNames.reduce((accumulator, name) => {
    const existingStats = getOr(null, [key, name], cmOldStats);
    const newStats = cmNewStats[key][name];
    return assocPath([name], mergeDeviceStats(existingStats, newStats), accumulator);
  }, cmOldStats[key]);
  return assocPath([key], mergedStats, cmOldStats);
};

const mergeInterfaceStats = mergeStatsObject('interfaces');

const mergePsuStats = mergeStatsObject('psu');

function mergeStats(cmOldStats, cmNewStats) {
  let stats = cmOldStats;
  if (has('interfaces', cmNewStats)) {
    stats = mergeInterfaceStats(stats, cmNewStats);
  }

  if (has('psu', cmNewStats)) {
    stats = mergePsuStats(stats, cmNewStats);
  }

  return mergeDeviceStats(stats, cmNewStats);
}


module.exports = {
  mergeDeviceStats,
  mergeInterfaceStats,
  mergePsuStats,
  mergeStats,
};
