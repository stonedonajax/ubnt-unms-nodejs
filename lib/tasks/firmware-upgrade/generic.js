'use strict';

const { Reader: reader } = require('monet');
const { Observable } = require('rxjs/Rx');
const { getOr, isError, isNull } = require('lodash/fp');
const { when } = require('ramda');
const { cata } = require('ramda-adjunct');

const { error: logError } = require('../../logging');
const { toMs } = require('../../util');
const { TaskStatusEnum, DeviceTypeEnum } = require('../../enums');
const { fromDb: fromDbDevice } = require('../../transformers/device');

const UPGRADE_TIMEOUT = toMs('minutes', 12);

const expectedUpgradeDuration = (device) => {
  const type = device.identification.type;
  switch (type) {
    case DeviceTypeEnum.AirMax:
    case DeviceTypeEnum.AirFiber:
      return toMs('minutes', 6);
    case DeviceTypeEnum.Eswitch:
      return toMs('minutes', 10);
    default:
      return toMs('minutes', 6);
  }
};

const generateFirmwareUrl = (settings, device, firmware) => {
  const type = device.identification.type;
  switch (type) {
    case DeviceTypeEnum.AirMax:
    case DeviceTypeEnum.AirFiber:
    case DeviceTypeEnum.ToughSwitch:
    case DeviceTypeEnum.Epower:
      return `${settings.firmwaresPublicUrl()}${firmware.secureUrl}`;
    case DeviceTypeEnum.Eswitch:
    default:
      return `${settings.firmwaresWsUrl()}${firmware.secureUrl}`;
  }
};

/**
 * @param {string} deviceId
 * @return {Reader.<waitForDeviceToConnect~callback>}
 */
const waitForDeviceToConnect = deviceId => reader(
  /**
   * @function waitForDeviceToConnect~callback
   * @param {MessageHub} messageHub
   * @param {DB} DB
   * @return {Observable}
   */
  ({ messageHub, DB }) => messageHub.stream(`device.${deviceId}.connected`)
    .take(1)
    .mergeMap(() => DB.device.findById(deviceId))
);

/**
 * @param {CorrespondenceDevice} device
 * @param {CorrespondenceFirmware} firmware
 * @return {!Reader.<upgrade~callback>}
 */
const upgrade = (device, firmware) => reader(
  /**
   * @function upgrade~callback
   * @param {DB} DB
   * @param {DeviceStore} deviceStore
   * @param {FirmwareDal} firmwareDal
   * @param {MessageHub} messageHub
   * @param {Settings} settings
   * @param {Tasks} taskManager
   * @param {string} taskId
   * @return {Observable}
   */
  ({ DB, deviceStore, firmwareDal, messageHub, settings, taskManager, taskId }) => {
    const firmwareUrl = generateFirmwareUrl(settings, device, firmware);
    const deviceId = device.identification.id;
    const expectedDuration = expectedUpgradeDuration(device);

    deviceStore.updateUpgradeStatus(deviceId, {
      status: TaskStatusEnum.InProgress,
      expectedDuration,
      firmware,
    });

    const taskStart = Date.now();
    return Observable.fromPromise(taskManager.startTask(taskId))
    // start and track upgrade progress
      .mergeMap(() => Observable.of(deviceStore.get(deviceId))
        .do(when(isNull, () => { throw new Error('Device not connected') }))
        .mergeMap(commDevice => commDevice.systemUpgrade(firmwareUrl))
        .switchMap(() => {
          const elapsedTime = Date.now() - taskStart;
          const progress = elapsedTime / expectedDuration;
          return Observable.fromPromise(taskManager.updateProgress(taskId, progress));
        })
        .last() // wait for all observables to complete
        .timeoutWith(UPGRADE_TIMEOUT, Observable.throw(new Error('Device upgrade timeout')))
      )
      .mergeMap(() => waitForDeviceToConnect(deviceId).run({ DB, messageHub }) // upgrade finished
        .map(fromDbDevice({ firmwareDal }))
        .mergeMap(cata(Observable.throw, Observable.of))
        .mergeMap((upgradedDevice) => { // check upgrade version
          const firmwareVersion = getOr(null, ['firmware', 'current'], upgradedDevice);
          const expectedFirmwareVersion = firmware.identification.version;
          if (firmwareVersion === expectedFirmwareVersion) {
            const upgradeStatus = { status: TaskStatusEnum.Success };

            deviceStore.updateUpgradeStatus(deviceId, upgradeStatus);
            return Observable.of(upgradeStatus);
          }

          // eslint-disable-next-line max-len
          const errorMessage = `Unexpected upgrade result, expected firmware version ${expectedFirmwareVersion}, but got ${firmwareVersion}`;
          logError(errorMessage);
          return Observable.throw(new Error(errorMessage));
        }))
      .catch((error) => {
        const upgradeStatus = { status: TaskStatusEnum.Failed, error: isError(error) ? error.message : String(error) };
        deviceStore.updateUpgradeStatus(deviceId, upgradeStatus);
        return Observable.of(upgradeStatus);
      });
  }
);

/**
 * @param {CorrespondenceDevice} device
 * @return {!Reader.<onEnqueue~callback>}
 */
const onEnqueue = device => reader(
  /**
   * @function onEnqueue~callback
   * @param {DeviceStore} deviceStore
   * @return {boolean}
   */
  ({ deviceStore }) => deviceStore.updateUpgradeStatus(
    device.identification.id, { status: TaskStatusEnum.Queued }
  )
);

/**
 * @param {CorrespondenceDevice} device
 * @return {!Reader.<cancel~callback>}
 */
const cancel = device => reader(
  /**
   * @function cancel~callback
   * @param {DeviceStore} deviceStore
   * @return {boolean}
   */
  ({ deviceStore }) => deviceStore.updateUpgradeStatus(
    device.identification.id, { status: TaskStatusEnum.Canceled }
  )
);

module.exports = {
  upgrade,
  onEnqueue,
  cancel,
};
