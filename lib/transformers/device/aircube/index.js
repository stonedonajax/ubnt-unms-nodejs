'use strict';

const { fromCorrespondence } = require('../../index');

const { safeToApiStationsList, safeToApiSsidList } = require('./mappers');

const toApiStationsList = fromCorrespondence(safeToApiStationsList);

const toApiSsidList = fromCorrespondence(safeToApiSsidList);


module.exports = {
  toApiStationsList,
  toApiSsidList,
};
