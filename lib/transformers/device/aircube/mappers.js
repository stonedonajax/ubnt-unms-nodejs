'use strict';

const { pick, map, join, pathOr, complement, pipe, converge, unapply, filter, when } = require('ramda');
const { flow, flatten, curry, compact, getOr } = require('lodash/fp');
const { isNilOrEmpty, stubNull } = require('ramda-adjunct');
const { liftMapper } = require('../../index');

const toApiAircubeAttributes = correspondenceData => ({
  ssid: pipe(
    converge(unapply(filter(complement(isNilOrEmpty))), [
      pathOr(null, ['aircube', 'wifi5Ghz', 'ssid']),
      pathOr(null, ['aircube', 'wifi2Ghz', 'ssid']),
    ]),
    join('/'),
    when(isNilOrEmpty, stubNull)
  )(correspondenceData),
});

const toApiStationsList = cmStationsList => map(pick([
  'timestamp',
  'ipAddress',
  'name',
  'vendor',
  'radio',
  'mac',
  'upTime',
  'latency',
  'distance',
  'rxBytes',
  'txBytes',
  'rxRate',
  'txRate',
  'rxSignal',
  'txSignal',
  'downlinkCapacity',
  'uplinkCapacity',
  'deviceIdentification',
]))(cmStationsList);

// toApiSsid :: WifiKey -> CmAirCube -> SsidData
//    WifiKey = String
//    CmAirCube = Object
//    SsidData = Object
const toApiSsid = curry((wifiKey, correspondenceData) => ({
  deviceId: correspondenceData.identification.id,
  ssid: getOr(null, ['aircube', wifiKey, 'ssid'], correspondenceData),
  key: getOr(null, ['aircube', wifiKey, 'key'], correspondenceData),
  mac: getOr(null, ['aircube', wifiKey, 'mac'], correspondenceData),
}));

const toApiSsidWifi2Ghz = toApiSsid('wifi2Ghz');
const toApiSsidWifi5Ghz = toApiSsid('wifi5Ghz');

// toApiSsid :: [CmAirCube] -> [SsidData]
//     CmAirCube = Object
//     SsidData = Object
const toApiSsidList = flow(
  map(cmAirCube => [
    cmAirCube.aircube.wifi2Ghz.available ? toApiSsidWifi2Ghz(cmAirCube) : null,
    cmAirCube.aircube.wifi5Ghz.available ? toApiSsidWifi5Ghz(cmAirCube) : null,
  ]),
  flatten,
  compact
);

const toApiFrequencyLists = cmFrequencyLists => ({
  radio2GhzFrequencyList: cmFrequencyLists.radio2GhzFrequencyList,
  radio5GhzFrequencyList: cmFrequencyLists.radio5GhzFrequencyList,
});

const toApiNetworkConfig = cmNetworkConfig => ({
  mode: cmNetworkConfig.mode,
  blockManagementAccess: cmNetworkConfig.blockManagementAccess,
  lan: {
    type: cmNetworkConfig.lan.type,
    interfaceNames: cmNetworkConfig.lan.interfaceNames,
    gateway: cmNetworkConfig.lan.gateway,
    cidr: cmNetworkConfig.lan.cidr,
    proto: cmNetworkConfig.lan.proto,
    dns: cmNetworkConfig.lan.dns,
    dhcp: {
      ignore: cmNetworkConfig.lan.dhcp.ignore,
      interface: cmNetworkConfig.lan.dhcp.interface,
      rangeStart: cmNetworkConfig.lan.dhcp.rangeStart,
      rangeEnd: cmNetworkConfig.lan.dhcp.rangeEnd,
      leaseTime: cmNetworkConfig.lan.dhcp.leaseTime,
    },
  },
  wan: {
    enabled: cmNetworkConfig.wan.enabled,
    interfaceNames: cmNetworkConfig.wan.interfaceNames,
    cidr: cmNetworkConfig.wan.cidr,
    gateway: cmNetworkConfig.wan.gateway,
    proto: cmNetworkConfig.wan.proto,
    dns: cmNetworkConfig.wan.dns,
    service: cmNetworkConfig.wan.service,
    username: cmNetworkConfig.wan.username,
    password: cmNetworkConfig.wan.password,
  },
});

const toApiSystemConfig = cmSystemConfig => ({
  deviceName: cmSystemConfig.deviceName,
  timezone: cmSystemConfig.timezone,
  zonename: cmSystemConfig.zonename,
  username: cmSystemConfig.username,
  ledNightMode: {
    enable: cmSystemConfig.ledNightMode.enable,
    start: cmSystemConfig.ledNightMode.start,
    end: cmSystemConfig.ledNightMode.end,
  },
  poePassthrough: cmSystemConfig.poePassthrough,
});


module.exports = {
  toApiAircubeAttributes,
  toApiStationsList,
  toApiSsidList,
  toApiFrequencyLists,
  toApiNetworkConfig,
  toApiSystemConfig,

  safeToAircubeAttributes: liftMapper(toApiAircubeAttributes),
  safeToApiStationsList: liftMapper(toApiStationsList),
  safeToApiSsidList: liftMapper(toApiSsidList),
};
