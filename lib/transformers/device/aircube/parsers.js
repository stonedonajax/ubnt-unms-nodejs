'use strict';

const { constant } = require('lodash/fp');
const { pathOr } = require('ramda');

const { FirmwarePlatformIdEnum, WifiAuthenticationEnum, WifiSecurityEnum } = require('../../../enums');


// parsePlatformId :: FullFirmwareVersion -> PlatformId
//     FullFirmwareVersion = String
//     PlatformId = String
const parsePlatformId = constant(FirmwarePlatformIdEnum.ACB);

const parseApiWirelessConfig = apiWifiConfig => ({
  wifi2Ghz: {
    authentication: apiWifiConfig.wifi2Ghz.isWPA2PSKEnabled ? WifiAuthenticationEnum.PSK2 : WifiAuthenticationEnum.None,
    available: apiWifiConfig.wifi2Ghz.available,
    channel: apiWifiConfig.wifi2Ghz.channel,
    country: pathOr(null, ['wifi2Ghz', 'country'], apiWifiConfig),
    encryption: apiWifiConfig.wifi2Ghz.isWPA2PSKEnabled ? WifiSecurityEnum.WPA2 : WifiSecurityEnum.None,
    frequency: apiWifiConfig.wifi2Ghz.frequency,
    key: apiWifiConfig.wifi2Ghz.key,
    mac: pathOr(null, ['wifi2Ghz', 'mac'], apiWifiConfig),
    mode: apiWifiConfig.wifi2Ghz.mode,
    ssid: apiWifiConfig.wifi2Ghz.ssid,
    txPower: apiWifiConfig.wifi2Ghz.txPower,
  },
  wifi5Ghz: {
    authentication: apiWifiConfig.wifi5Ghz.isWPA2PSKEnabled ? WifiAuthenticationEnum.PSK2 : WifiAuthenticationEnum.None,
    available: apiWifiConfig.wifi5Ghz.available,
    channel: apiWifiConfig.wifi5Ghz.channel,
    country: pathOr(null, ['wifi5Ghz', 'country'], apiWifiConfig),
    encryption: apiWifiConfig.wifi5Ghz.isWPA2PSKEnabled ? WifiSecurityEnum.WPA2 : WifiSecurityEnum.None,
    frequency: apiWifiConfig.wifi5Ghz.frequency,
    key: apiWifiConfig.wifi5Ghz.key,
    mac: pathOr(null, ['wifi5Ghz', 'mac'], apiWifiConfig),
    mode: apiWifiConfig.wifi5Ghz.mode,
    ssid: apiWifiConfig.wifi5Ghz.ssid,
    txPower: apiWifiConfig.wifi5Ghz.txPower,
  },
});

const parseApiNetworkConfig = apiNetworkConfig => ({
  mode: apiNetworkConfig.mode,
  blockManagementAccess: apiNetworkConfig.blockManagementAccess,
  lan: {
    type: apiNetworkConfig.lan.type,
    interfaceNames: apiNetworkConfig.lan.interfaceNames,
    gateway: apiNetworkConfig.lan.gateway,
    cidr: apiNetworkConfig.lan.cidr,
    proto: apiNetworkConfig.lan.proto,
    dns: apiNetworkConfig.lan.dns,
    dhcp: {
      ignore: apiNetworkConfig.lan.dhcp.ignore,
      interface: apiNetworkConfig.lan.dhcp.interface,
      rangeStart: apiNetworkConfig.lan.dhcp.rangeStart,
      rangeEnd: apiNetworkConfig.lan.dhcp.rangeEnd,
      leaseTime: apiNetworkConfig.lan.dhcp.leaseTime,
    },
  },
  wan: {
    enabled: apiNetworkConfig.wan.enabled,
    interfaceNames: apiNetworkConfig.wan.interfaceNames,
    cidr: apiNetworkConfig.wan.cidr,
    gateway: apiNetworkConfig.wan.gateway,
    proto: apiNetworkConfig.wan.proto,
    dns: apiNetworkConfig.wan.dns,
    service: apiNetworkConfig.wan.service,
    username: apiNetworkConfig.wan.username,
    password: apiNetworkConfig.wan.password,
  },
});

const parseApiSystemConfig = apiSystemConfig => ({
  deviceName: apiSystemConfig.deviceName,
  timezone: apiSystemConfig.timezone,
  zonename: apiSystemConfig.zonename,
  username: apiSystemConfig.username,
  newPassword: pathOr(null, ['newPassword'], apiSystemConfig),
  ledNightMode: {
    enable: apiSystemConfig.ledNightMode.enable,
    start: apiSystemConfig.ledNightMode.start,
    end: apiSystemConfig.ledNightMode.end,
  },
  poePassthrough: apiSystemConfig.poePassthrough,
});


module.exports = {
  parsePlatformId,
  parseApiWirelessConfig,
  parseApiNetworkConfig,
  parseApiSystemConfig,
};
