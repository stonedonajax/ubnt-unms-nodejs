'use strict';

const { fromCorrespondence } = require('../../index');

const { safeToApiApsProfileList } = require('./mappers');

const toApiApsProfileList = fromCorrespondence(safeToApiApsProfileList);


module.exports = {
  toApiApsProfileList,
};
