'use strict';

const { map, view, assoc, dissoc, applySpec } = require('ramda');
const { isNotUndefined } = require('ramda-adjunct');
const { has, cloneDeep } = require('lodash/fp');

const { liftMapper } = require('../../index');
const { siteLatitudeLens, siteLongitudeLens, deviceLatitudeLens, deviceLongitudeLens } = require('../util');

// parseLatitude :: CmData -> Number
//     CmData = Object
const parseLatitude = (cmData) => {
  if (isNotUndefined(view(deviceLatitudeLens, cmData))) { return view(deviceLatitudeLens, cmData) }
  if (isNotUndefined(view(siteLatitudeLens, cmData))) { return view(siteLatitudeLens, cmData) }

  return null;
};

// parseLongitude :: CmData -> Number
//     CmData = Object
const parseLongitude = (cmData) => {
  if (isNotUndefined(view(deviceLongitudeLens, cmData))) { return view(deviceLongitudeLens, cmData) }
  if (isNotUndefined(view(siteLongitudeLens, cmData))) { return view(siteLongitudeLens, cmData) }

  return null;
};

// parseLocation :: CmData -> Location
//     CmData = Object
//     Location = Object
const parseLocation = applySpec({
  latitude: parseLatitude,
  longitude: parseLongitude,
});

// parseDeviceIdentification :: CmDeviceIdentification -> Number
//     CmDeviceIdentification = Object
const parseDeviceIdentification = (cmDeviceIdentification) => {
  let cmDevice = cloneDeep(cmDeviceIdentification);
  if (has(['site'], cmDevice)) {
    cmDevice = dissoc('siteId', cmDevice);
  }
  if (has(['site', 'identification'], cmDevice)) {
    cmDevice = assoc('site', cmDevice.site.identification, cmDevice);
  }
  return cmDevice;
};

// toApiApsProfile :: CmAirFiber -> ApsProfile
//     CmAirFiber = Object
//     ApsProfile = Object
const toApiApsProfile = correspondenceData => ({
  device: parseDeviceIdentification(correspondenceData.identification),
  ssid: correspondenceData.airfiber.ssid,
  key: correspondenceData.airfiber.key,
  mac: correspondenceData.airfiber.mac,
  countryCode: correspondenceData.airfiber.countryCode,
  channelWidth: correspondenceData.airfiber.channelWidth,
  frequency: correspondenceData.airfiber.frequency,
  security: correspondenceData.airfiber.security,
  authentication: correspondenceData.airfiber.authentication,
  location: parseLocation(correspondenceData),
  airfiber: {
    mode: correspondenceData.airfiber.wirelessMode,
    frameLength: correspondenceData.airfiber.frameLength,
  },
});

// toApiApsProfileList :: [CmAirFiber] -> [ApsProfile]
//     CmAirFiber = Object
//     ApsProfile = Object
const toApiApsProfileList = map(toApiApsProfile);

module.exports = {
  toApiApsProfileList,

  safeToApiApsProfileList: liftMapper(toApiApsProfileList),
};
