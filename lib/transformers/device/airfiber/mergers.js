'use strict';

const { StatusEnum } = require('../../../enums');
const { mergeDeviceUpdate } = require('../mergers');


// mergeDbWithHw :: (Object, Object) -> DeviceCorrespondenceData
//     DeviceCorrespondenceData = Object
const mergeDbWithHw = (dbDeviceCorrespondenceData, hwDeviceCorrespondenceData) => {
  const newStatus = dbDeviceCorrespondenceData.identification.authorized ? StatusEnum.Active : StatusEnum.Unauthorized;

  return mergeDeviceUpdate(dbDeviceCorrespondenceData, {
    identification: {
      mac: hwDeviceCorrespondenceData.identification.mac,
      name: hwDeviceCorrespondenceData.identification.name,
      model: hwDeviceCorrespondenceData.identification.model,
      type: hwDeviceCorrespondenceData.identification.type,
      category: hwDeviceCorrespondenceData.identification.category,
      firmwareVersion: hwDeviceCorrespondenceData.identification.firmwareVersion,
      platformId: hwDeviceCorrespondenceData.identification.platformId,
      updated: hwDeviceCorrespondenceData.identification.updated,
      ipAddress: hwDeviceCorrespondenceData.identification.ipAddress,
    },
    overview: {
      lastSeen: hwDeviceCorrespondenceData.overview.lastSeen,
      location: hwDeviceCorrespondenceData.overview.location,
      signal: hwDeviceCorrespondenceData.overview.signal,
      status: newStatus,
      uptime: hwDeviceCorrespondenceData.overview.uptime,
      cpu: hwDeviceCorrespondenceData.overview.cpu,
      ram: hwDeviceCorrespondenceData.overview.ram,
      distance: hwDeviceCorrespondenceData.overview.distance,
      transmitPower: hwDeviceCorrespondenceData.overview.transmitPower,
    },
    mode: hwDeviceCorrespondenceData.mode,
    airfiber: {
      ssid: hwDeviceCorrespondenceData.airfiber.ssid,
      wirelessMode: hwDeviceCorrespondenceData.airfiber.wirelessMode,
      frequency: hwDeviceCorrespondenceData.airfiber.frequency,
      antenna: hwDeviceCorrespondenceData.airfiber.antenna,
      channelWidth: hwDeviceCorrespondenceData.airfiber.channelWidth,
      countryCode: hwDeviceCorrespondenceData.airfiber.countryCode,
      security: hwDeviceCorrespondenceData.airfiber.security,
      authentication: hwDeviceCorrespondenceData.airfiber.authentication,
      key: hwDeviceCorrespondenceData.airfiber.key,
      mac: hwDeviceCorrespondenceData.airfiber.mac,
    },
    interfaces: hwDeviceCorrespondenceData.interfaces,
  });
};


module.exports = {
  mergeDbWithHw,
};
