'use strict';

const { match } = require('ramda');
const { flow, nth, getOr, __, toLower } = require('lodash/fp');

const { FirmwarePlatformIdEnum } = require('../../../enums');


// parsePlatformId :: FullFirmwareVersion -> PlatformId
//     FullFirmwareVersion = String
//     PlatformId = String
const parsePlatformId = flow(match(/^(\w+)\./), nth(1), toLower, getOr(null, __, {
  af5xhd: FirmwarePlatformIdEnum.AF5XHD,
  afltu: FirmwarePlatformIdEnum.AFLTU,
  af: FirmwarePlatformIdEnum.AF,
  af02: FirmwarePlatformIdEnum.AF02,
  af06: FirmwarePlatformIdEnum.AF06,
  af07: FirmwarePlatformIdEnum.AF07,
  af08: FirmwarePlatformIdEnum.AF08,
  af09: FirmwarePlatformIdEnum.AF09,
}));

module.exports = {
  parsePlatformId,
};
