'use strict';

const { fromCorrespondence } = require('../../index');

const { safeToApiSsidList, safeToApiApsProfileList } = require('./mappers');

const toApiSsidList = fromCorrespondence(safeToApiSsidList);

const toApiApsProfileList = fromCorrespondence(safeToApiApsProfileList);


module.exports = {
  toApiSsidList,
  toApiApsProfileList,
};
