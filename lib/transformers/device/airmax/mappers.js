'use strict';

const { map, pick, view, assoc, dissoc, applySpec } = require('ramda');
const { isNotUndefined } = require('ramda-adjunct');
const { has, cloneDeep } = require('lodash/fp');

const { liftMapper } = require('../../index');
const { deviceModelToSeries } = require('../../../feature-detection/airmax');
const { siteLatitudeLens, siteLongitudeLens, deviceLatitudeLens, deviceLongitudeLens } = require('../util');


// toApiAirmaxAttributes :: DeviceCorrespondence -> ApiAirmaxAttributes
//     DeviceCorrespondence = Object
//     ApiAirmaxAttributes = Object
const toApiAirmaxAttributes = correspondenceData => ({
  series: deviceModelToSeries(correspondenceData.identification.model),
  ssid: correspondenceData.airmax.ssid,
});


// toApiStationsList :: Array.<StationCorrespondence> -> Array.<StationApi>
//     StationCorrespondence = Object
//     StationApi = Object
const toApiStationsList = cmStationsList => map(pick([
  'timestamp',
  'ipAddress',
  'name',
  'vendor',
  'radio',
  'mac',
  'upTime',
  'latency',
  'distance',
  'rxBytes',
  'txBytes',
  'rxRate',
  'txRate',
  'rxSignal',
  'txSignal',
  'downlinkCapacity',
  'uplinkCapacity',
  'deviceIdentification',
]))(cmStationsList);

// toApiSsid :: CmAirMax -> SsidData
//     CmAirMax = Object
//     SsidData = Object
const toApiSsid = correspondenceData => ({
  deviceId: correspondenceData.identification.id,
  ssid: correspondenceData.airmax.ssid,
  key: correspondenceData.airmax.key,
  mac: correspondenceData.airmax.mac,
});

// toApiSsidList :: [CmAirMax] -> [SsidData]
//     CmAirMax = Object
//     SsidData = Object
const toApiSsidList = map(toApiSsid);

// parseLatitude :: CmData -> Number
//     CmData = Object
const parseLatitude = (cmData) => {
  if (isNotUndefined(view(deviceLatitudeLens, cmData))) { return view(deviceLatitudeLens, cmData) }
  if (isNotUndefined(view(siteLatitudeLens, cmData))) { return view(siteLatitudeLens, cmData) }

  return null;
};

// parseLongitude :: CmData -> Number
//     CmData = Object
const parseLongitude = (cmData) => {
  if (isNotUndefined(view(deviceLongitudeLens, cmData))) { return view(deviceLongitudeLens, cmData) }
  if (isNotUndefined(view(siteLongitudeLens, cmData))) { return view(siteLongitudeLens, cmData) }

  return null;
};

// parseLocation :: CmData -> Location
//     CmData = Object
//     Location = Object
const parseLocation = applySpec({
  latitude: parseLatitude,
  longitude: parseLongitude,
});

// parseDeviceIdentification :: CmDeviceIdentification -> Number
//     CmDeviceIdentification = Object
const parseDeviceIdentification = (cmDeviceIdentification) => {
  let cmDevice = cloneDeep(cmDeviceIdentification);
  if (has(['site'], cmDevice)) {
    cmDevice = dissoc('siteId', cmDevice);
  }
  if (has(['site', 'identification'], cmDevice)) {
    cmDevice = assoc('site', cmDevice.site.identification, cmDevice);
  }
  return cmDevice;
};

// toApiApsProfile :: CmAirMax -> ApsProfile
//     CmAirMax = Object
//     ApsProfile = Object
const toApiApsProfile = correspondenceData => ({
  device: parseDeviceIdentification(correspondenceData.identification),
  ssid: correspondenceData.airmax.ssid,
  key: correspondenceData.airmax.key,
  mac: correspondenceData.airmax.mac,
  countryCode: correspondenceData.airmax.countryCode,
  channelWidth: correspondenceData.airmax.channelWidth,
  frequency: correspondenceData.airmax.frequency,
  security: correspondenceData.airmax.security,
  authentication: correspondenceData.airmax.authentication,
  location: parseLocation(correspondenceData),
  airmax: {
    mode: correspondenceData.airmax.wirelessMode,
    wds: correspondenceData.airmax.wds,
  },
});

// toApiApsProfileList :: [CmAirMax] -> [ApsProfile]
//     CmAirMax = Object
//     ApsProfile = Object
const toApiApsProfileList = map(toApiApsProfile);


module.exports = {
  toApiAirmaxAttributes,
  toApiStationsList,
  toApiSsidList,
  toApiApsProfileList,

  safeToApiAirmaxAttributes: liftMapper(toApiAirmaxAttributes),
  safeToApiStationsList: liftMapper(toApiStationsList),
  safeToApiSsidList: liftMapper(toApiSsidList),
  safeToApiApsProfileList: liftMapper(toApiApsProfileList),
};
