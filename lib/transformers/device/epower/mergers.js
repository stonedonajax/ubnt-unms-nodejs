'use strict';

const { StatusEnum } = require('../../../enums');
const { mergeDeviceUpdate } = require('../mergers');


// mergeDbWithHw :: (Object, Object) -> DeviceCorrespondenceData
//     DeviceCorrespondenceData = Object
const mergeDbWithHw = (dbDeviceCorrespondenceData, hwDeviceCorrespondenceData) => {
  const newStatus = dbDeviceCorrespondenceData.identification.authorized ? StatusEnum.Active : StatusEnum.Unauthorized;

  return mergeDeviceUpdate(dbDeviceCorrespondenceData, {
    identification: {
      mac: hwDeviceCorrespondenceData.identification.mac,
      name: hwDeviceCorrespondenceData.identification.name,
      model: hwDeviceCorrespondenceData.identification.model,
      type: hwDeviceCorrespondenceData.identification.type,
      category: hwDeviceCorrespondenceData.identification.category,
      firmwareVersion: hwDeviceCorrespondenceData.identification.firmwareVersion,
      platformId: hwDeviceCorrespondenceData.identification.platformId,
      updated: hwDeviceCorrespondenceData.identification.updated,
      ipAddress: hwDeviceCorrespondenceData.identification.ipAddress,
    },
    overview: {
      lastSeen: hwDeviceCorrespondenceData.overview.lastSeen,
      signal: hwDeviceCorrespondenceData.overview.signal,
      status: newStatus,
      uptime: hwDeviceCorrespondenceData.overview.uptime,
      cpu: hwDeviceCorrespondenceData.overview.cpu,
      ram: hwDeviceCorrespondenceData.overview.ram,
      distance: hwDeviceCorrespondenceData.overview.distance,
      transmitPower: hwDeviceCorrespondenceData.overview.transmitPower,
    },
    interfaces: hwDeviceCorrespondenceData.interfaces,
  });
};


module.exports = {
  mergeDbWithHw,
};
