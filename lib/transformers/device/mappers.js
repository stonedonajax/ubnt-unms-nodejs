'use strict';

const { map, isNull, getOr, pick, merge, flow, get, isEmpty } = require('lodash/fp');
const { when, pathOr, isNil, ifElse, is, identity } = require('ramda');

const { StatusEnum, DeviceTypeEnum, DeviceProductNameEnum } = require('../../enums');
const { isNotNull } = require('../../util');
const { liftMapper } = require('../index');
const { toApiSemver } = require('../semver/mappers');
const { toApiFirmware } = require('../../api/v2.1/firmwares/transformers/mappers');
const { toApiDeviceMetadata: toApiDeviceMetadataMap } = require('./metadata/mappers');
const { toApiAirmaxAttributes } = require('./airmax/mappers');
const { toApiAircubeAttributes } = require('./aircube/mappers');


/**
 * Helpers.
 */

/**
 * Parser implementations.
 */

const parseSiteParent = ifElse(
  is(String),
  identity,
  pathOr(null, ['id'])
);

// toApiSiteIdentification :: Object -> ApiSiteIdentification
//     ApiSiteIdentification :: Object
const toApiSiteIdentification = correspondenceData => ({
  id: correspondenceData.identification.id,
  status: correspondenceData.identification.status,
  name: correspondenceData.identification.name,
  parent: parseSiteParent(correspondenceData.identification.parent),
  type: correspondenceData.identification.type,
});

// toApiDeviceDisplayName :: CorrespondenceDataIdentification -> String
//     CorrespondenceDataIdentification = Object
const toApiDeviceDisplayName = correspondenceData => flow(
  getOr(null, ['meta', 'alias']),
  when(isEmpty, () => get(['identification', 'name'], correspondenceData))
)(correspondenceData);

// toApiDeviceIdentification :: Object -> ApiDeviceIdentification
//     ApiDeviceIdentification = Object
const toApiDeviceIdentification = (correspondenceData) => {
  const model = correspondenceData.identification.model;
  return {
    id: correspondenceData.identification.id,
    site: when(isNotNull, toApiSiteIdentification, correspondenceData.identification.site),
    mac: correspondenceData.identification.mac,
    name: correspondenceData.identification.name,
    serialNumber: correspondenceData.identification.serialNumber,
    firmwareVersion: correspondenceData.identification.firmwareVersion,
    model,
    modelName: getOr(model, model, DeviceProductNameEnum),
    platformId: correspondenceData.identification.platformId,
    type: correspondenceData.identification.type,
    category: correspondenceData.identification.category,
    authorized: correspondenceData.identification.authorized,
    updated: correspondenceData.identification.updated,
    displayName: toApiDeviceDisplayName(correspondenceData),
  };
};

// toApiDeviceOverviewByStatus :: Object -> ApiDeviceOverviewByStatus
//     ApiDeviceOverviewByStatus = Object
const toApiDeviceOverviewByStatus = (correspondenceData) => {
  const { status, cpu, ram, signal, receiveRate, transmitRate, uptime } = correspondenceData.overview;
  const isActive = status === StatusEnum.Active;

  return {
    cpu: isActive ? cpu : null,
    ram: isActive ? ram : null,
    signal: isActive ? signal : null,
    rxRate: isActive ? receiveRate : null,
    txRate: isActive ? transmitRate : null,
    uptime: isActive ? uptime : null,
  };
};

// toApiDeviceOverview :: Object -> ApiDeviceOverview
//     ApiDeviceOverview = Object
const toApiDeviceOverview = correspondenceData => merge(
  toApiDeviceOverviewByStatus(correspondenceData),
  {
    biasCurrent: correspondenceData.overview.biasCurrent,
    canUpgrade: correspondenceData.overview.canUpgrade,
    location: correspondenceData.overview.location,
    distance: correspondenceData.overview.distance,
    isLocating: correspondenceData.overview.isLocating,
    lastSeen: correspondenceData.overview.lastSeen,
    receivePower: correspondenceData.overview.receivePower,
    rxDropped: correspondenceData.overview.receiveDropped,
    rxErrors: correspondenceData.overview.receiveErrors,
    status: correspondenceData.overview.status,
    temps: correspondenceData.overview.temps,
    transmitPower: correspondenceData.overview.transmitPower,
    txDropped: correspondenceData.overview.transmitDropped,
    txErrors: correspondenceData.overview.transmitErrors,
    voltage: correspondenceData.overview.voltage,
    rxBytes: correspondenceData.overview.receiveBytes,
    txBytes: correspondenceData.overview.transmitBytes,
  }
);

// toApiDeviceUpgradeStatus :: Object -> ApiDeviceUpgradeStatus
//     ApiDeviceUpgradeStatus = {
//       status: String,
//       error: String,
//       changeAt: Number,
//       expectedDuration: Number,
//       firmware: Object|Null
//     }
const toApiDeviceUpgradeStatus = (correspondenceData) => {
  if (isNull(correspondenceData.upgrade)) { return null }

  return {
    status: correspondenceData.upgrade.status,
    error: correspondenceData.upgrade.error,
    changedAt: correspondenceData.upgrade.changedAt,
    expectedDuration: correspondenceData.upgrade.expectedDuration,
    firmware: when(isNotNull, toApiFirmware, correspondenceData.upgrade.firmware),
  };
};

// toApiDeviceFirmware :: Object -> ApiDeviceFirmware
//     ApiDeviceFirmware = Object
const toApiDeviceFirmware = (correspondenceData) => {
  if (isNull(correspondenceData.firmware)) { return null }

  return {
    current: correspondenceData.firmware.current,
    latest: correspondenceData.firmware.latest,
    semver: {
      current: toApiSemver(correspondenceData.firmware.semver.current),
      latest: toApiSemver(correspondenceData.firmware.semver.latest),
    },
  };
};

// toApiDeviceMetadata :: Object -> ApiDeviceMetadata
//     ApiDeviceMetadata = Object
const toApiDeviceMetadata = (correspondenceData) => {
  if (correspondenceData.meta === null) { return null }

  return toApiDeviceMetadataMap(correspondenceData.meta);
};

// toApiDeviceOlt :: Object -> Olt
//     Olt = Object
const toApiDeviceOlt = (correspondenceData) => {
  if (correspondenceData.olt === null) { return null }

  return {
    hasUnsupportedOnu: getOr(false, ['olt', 'hasUnsupportedOnu'], correspondenceData),
    layer2: getOr(null, ['olt', 'layer2'], correspondenceData),
  };
};

// toApiDeviceCanDisplayStatistics :: Object -> Boolean
const toApiDeviceCanDisplayStatistics = (correspondenceData) => {
  const isDisconnected = correspondenceData.overview.status === StatusEnum.Disconnected;

  return !isDisconnected;
};

// toApiDeviceAttributes :: DeviceCorrespondence -> ApiDeviceAttributes|Null
//     DeviceCorrespondence = Object
//     ApiDeviceAttributes = Object
const toApiDeviceAttributes = (correspondenceData) => {
  if (correspondenceData.identification.type === DeviceTypeEnum.AirMax) {
    return toApiAirmaxAttributes(correspondenceData);
  } else if (correspondenceData.identification.type === DeviceTypeEnum.AirCube) {
    return toApiAircubeAttributes(correspondenceData);
  }
  return null;
};

// toApiDeviceStatusOverview :: Object -> ApiDeviceStatusOverview
//     ApiDeviceStatusOverview = Object
const toApiDeviceStatusOverview = correspondenceData => ({
  identification: toApiDeviceIdentification(correspondenceData),
  overview: toApiDeviceOverview(correspondenceData),
  firmware: toApiDeviceFirmware(correspondenceData),
  upgrade: toApiDeviceUpgradeStatus(correspondenceData),
  meta: toApiDeviceMetadata(correspondenceData),
  attributes: toApiDeviceAttributes(correspondenceData),
  ipAddress: getOr(null, ['identification', 'ipAddress'], correspondenceData),
  latestBackup: correspondenceData.latestBackup,
});

// toApiDeviceStatusOverviewList :: CorrespondenceList -> ApiDeviceStatusOverviewList
//     ApiDeviceStatusOverviewList = Array.<ApiDeviceStatusOverview>
const toApiDeviceStatusOverviewList = map(toApiDeviceStatusOverview);

// toApiONUStatusOverview :: Object -> ApiONUStatusOverview
//     ApiONUStatusOverview = Object
const toApiONUStatusOverview = correspondenceData => ({
  identification: toApiDeviceIdentification(correspondenceData),
  enabled: getOr(true, ['enabled'], correspondenceData),
  overview: toApiDeviceOverview(correspondenceData),
  firmware: toApiDeviceFirmware(correspondenceData),
  upgrade: toApiDeviceUpgradeStatus(correspondenceData),
  parentId: correspondenceData.onu.id,
  canDisplayStatistics: toApiDeviceCanDisplayStatistics(correspondenceData),
  onu: {
    id: correspondenceData.onu.id,
    port: correspondenceData.onu.port,
    profile: correspondenceData.onu.profile,
    profileName: pathOr(null, ['onu', 'profileName'], correspondenceData),
  },
});

// toApiONUStatusOverviewList :: CorrespondenceList -> ApiONUStatusOverviewList
//     ApiONUStatusOverviewList = Array.<ApiONUStatusOverview>
const toApiONUStatusOverviewList = map(toApiONUStatusOverview);

// toApiErouterStatusDetail :: Object -> ApiErouterStatusDetail
//     ApiErouterStatusDetail = Object
const toApiErouterStatusDetail = correspondenceData => ({
  identification: toApiDeviceIdentification(correspondenceData),
  overview: toApiDeviceOverview(correspondenceData),
  gateway: correspondenceData.overview.gateway,
  ipAddress: correspondenceData.identification.ipAddress,
  firmware: toApiDeviceFirmware(correspondenceData),
  upgrade: toApiDeviceUpgradeStatus(correspondenceData),
  meta: toApiDeviceMetadata(correspondenceData),
});

// toApiAirMaxStatusDetail :: Object -> ApiAirMaxStatusDetail
//     ApiAirMaxStatusDetail = Object
const toApiAirMaxStatusDetail = correspondenceData => ({
  identification: toApiDeviceIdentification(correspondenceData),
  overview: toApiDeviceOverview(correspondenceData),
  gateway: correspondenceData.overview.gateway,
  ipAddress: correspondenceData.identification.ipAddress,
  firmware: toApiDeviceFirmware(correspondenceData),
  upgrade: toApiDeviceUpgradeStatus(correspondenceData),
  meta: toApiDeviceMetadata(correspondenceData),
  latestBackup: correspondenceData.latestBackup,
  mode: correspondenceData.mode,
  airmax: {
    series: correspondenceData.airmax.series,
    ssid: correspondenceData.airmax.ssid,
    frequency: correspondenceData.airmax.frequency,
    frequencyBands: correspondenceData.airmax.frequencyBands,
    frequencyCenter: correspondenceData.airmax.frequencyCenter,
    security: correspondenceData.airmax.security,
    channelWidth: correspondenceData.airmax.channelWidth,
    antenna: correspondenceData.airmax.antenna,
    noiseFloor: correspondenceData.airmax.noiseFloor,
    transmitChains: correspondenceData.airmax.transmitChains,
    receiveChains: correspondenceData.airmax.receiveChains,
    apMac: correspondenceData.airmax.apMac,
    wlanMac: correspondenceData.airmax.wlanMac,
    ccq: correspondenceData.airmax.ccq,
    wds: correspondenceData.airmax.wds,
    stationsCount: correspondenceData.airmax.stationsCount,
    wirelessMode: correspondenceData.airmax.wirelessMode,
    remoteSignal: correspondenceData.airmax.remoteSignal,
    lanStatus: {
      eth0: (() => {
        if (isNull(correspondenceData.airmax.lanStatus.eth0)) { return null }

        return {
          description: correspondenceData.airmax.lanStatus.eth0.description,
          plugged: correspondenceData.airmax.lanStatus.eth0.plugged,
          speed: correspondenceData.airmax.lanStatus.eth0.speed,
          duplex: correspondenceData.airmax.lanStatus.eth0.duplex,
        };
      })(),
      eth1: (() => {
        if (isNull(correspondenceData.airmax.lanStatus.eth1)) { return null }

        return {
          description: correspondenceData.airmax.lanStatus.eth1.description,
          plugged: correspondenceData.airmax.lanStatus.eth1.plugged,
          speed: correspondenceData.airmax.lanStatus.eth1.speed,
          duplex: correspondenceData.airmax.lanStatus.eth1.duplex,
        };
      })(),
    },
    polling: {
      enabled: correspondenceData.airmax.polling.enabled,
    },
  },
});

// toApiDeviceAirFiber :: Object -> ApiAirFiberStatusDetail
//     ApiAirFiberStatusDetail = Object
const toApiDeviceAirFiber = correspondenceData => ({
  ssid: correspondenceData.airfiber.ssid,
  wirelessMode: correspondenceData.airfiber.wirelessMode,
  frequency: correspondenceData.airfiber.frequency,
  antenna: correspondenceData.airfiber.antenna,
  frequencyBands: correspondenceData.airfiber.frequencyBands,
});

// toApiAirCubeStatusDetail :: Object -> ApiAirCubeStatusDetail
//     ApiAirCubeStatusDetail = Object
const toApiAirCubeStatusDetail = correspondenceData => ({
  identification: toApiDeviceIdentification(correspondenceData),
  overview: toApiDeviceOverview(correspondenceData),
  gateway: correspondenceData.overview.gateway,
  ipAddress: correspondenceData.identification.ipAddress,
  mode: correspondenceData.mode,
  firmware: toApiDeviceFirmware(correspondenceData),
  upgrade: toApiDeviceUpgradeStatus(correspondenceData),
  meta: toApiDeviceMetadata(correspondenceData),
  latestBackup: correspondenceData.latestBackup,
  aircube: correspondenceData.aircube,
});

// toApiOLTStatusDetail :: Object -> ApiOLTStatusDetail
//     ApiOLTStatusDetail = Object
const toApiOLTStatusDetail = correspondenceData => ({
  identification: toApiDeviceIdentification(correspondenceData),
  overview: toApiDeviceOverview(correspondenceData),
  gateway: correspondenceData.overview.gateway,
  ipAddress: correspondenceData.identification.ipAddress,
  firmware: toApiDeviceFirmware(correspondenceData),
  upgrade: toApiDeviceUpgradeStatus(correspondenceData),
  meta: toApiDeviceMetadata(correspondenceData),
  latestBackup: correspondenceData.latestBackup,
  olt: toApiDeviceOlt(correspondenceData),
});

// toApiEswitchStatusDetail :: Object -> ApiEswitchStatusDetail
//     ApiEswitchStatusDetail = Object
const toApiEswitchStatusDetail = correspondenceData => ({
  identification: toApiDeviceIdentification(correspondenceData),
  overview: toApiDeviceOverview(correspondenceData),
  gateway: correspondenceData.overview.gateway,
  ipAddress: correspondenceData.identification.ipAddress,
  firmware: toApiDeviceFirmware(correspondenceData),
  upgrade: toApiDeviceUpgradeStatus(correspondenceData),
  meta: toApiDeviceMetadata(correspondenceData),
  latestBackup: correspondenceData.latestBackup,
});

// toApiEpowerStatusDetail :: Object -> ApiEswitchStatusDetail
//     ApiEswitchStatusDetail = Object
const toApiEpowerStatusDetail = correspondenceData => ({
  identification: toApiDeviceIdentification(correspondenceData),
  overview: toApiDeviceOverview(correspondenceData),
  gateway: correspondenceData.overview.gateway,
  ipAddress: correspondenceData.identification.ipAddress,
  firmware: toApiDeviceFirmware(correspondenceData),
  upgrade: toApiDeviceUpgradeStatus(correspondenceData),
  meta: toApiDeviceMetadata(correspondenceData),
});

// toApiToughSwitchStatusDetail :: Object -> ApiToughSwitchStatusDetail
//     ApiToughSwitchStatusDetail = Object
const toApiToughSwitchStatusDetail = correspondenceData => ({
  identification: toApiDeviceIdentification(correspondenceData),
  overview: toApiDeviceOverview(correspondenceData),
  gateway: correspondenceData.overview.gateway,
  ipAddress: correspondenceData.identification.ipAddress,
  firmware: toApiDeviceFirmware(correspondenceData),
  upgrade: toApiDeviceUpgradeStatus(correspondenceData),
  meta: toApiDeviceMetadata(correspondenceData),
});

// toApiEswitchStatusDetail :: Object -> ApiEswitchStatusDetail
//     ApiEswitchStatusDetail = Object
const toApiAirFiberStatusDetail = correspondenceData => ({
  identification: toApiDeviceIdentification(correspondenceData),
  overview: toApiDeviceOverview(correspondenceData),
  gateway: correspondenceData.overview.gateway,
  ipAddress: correspondenceData.identification.ipAddress,
  firmware: toApiDeviceFirmware(correspondenceData),
  upgrade: toApiDeviceUpgradeStatus(correspondenceData),
  meta: toApiDeviceMetadata(correspondenceData),
  airfiber: toApiDeviceAirFiber(correspondenceData),
});

// toDbDevice :: Object -> DbDevice
//     DbDevice = Object
const toDbDevice = correspondenceData => ({
  id: correspondenceData.identification.id,
  enabled: getOr(true, ['enabled'], correspondenceData),
  identification: {
    id: correspondenceData.identification.id,
    site: {
      id: correspondenceData.identification.siteId,
    },
    mac: correspondenceData.identification.mac,
    name: correspondenceData.identification.name,
    serialNumber: correspondenceData.identification.serialNumber,
    firmwareVersion: correspondenceData.identification.firmwareVersion,
    platformId: correspondenceData.identification.platformId,
    model: correspondenceData.identification.model,
    timestamp: correspondenceData.identification.updated,
    authorized: correspondenceData.identification.authorized,
    type: correspondenceData.identification.type,
    category: correspondenceData.identification.category,
    ipAddress: getOr(null, ['identification', 'ipAddress'], correspondenceData),
  },
  overview: {
    status: correspondenceData.overview.status,
    canUpgrade: correspondenceData.overview.canUpgrade,
    location: correspondenceData.overview.location,
    locating: correspondenceData.overview.isLocating,
    cpu: correspondenceData.overview.cpu,
    ram: correspondenceData.overview.ram,
    voltage: correspondenceData.overview.voltage,
    temps: map(pick(['value', 'type', 'name']), correspondenceData.overview.temperature),
    signal: correspondenceData.overview.signal,
    distance: correspondenceData.overview.distance,
    biasCurrent: correspondenceData.overview.biasCurrent,
    receivePower: correspondenceData.overview.receivePower,
    rxRate: correspondenceData.overview.receiveRate,
    rxBytes: correspondenceData.overview.receiveBytes,
    rxErrors: correspondenceData.overview.receiveErrors,
    rxDropped: correspondenceData.overview.receiveDropped,
    transmitPower: correspondenceData.overview.transmitPower,
    txRate: correspondenceData.overview.transmitRate,
    txBytes: correspondenceData.overview.transmitBytes,
    txErrors: correspondenceData.overview.transmitErrors,
    txDropped: correspondenceData.overview.transmitDropped,
    lastSeen: correspondenceData.overview.lastSeen,
    uptime: correspondenceData.overview.uptime,
    gateway: getOr(null, ['overview', 'gateway'], correspondenceData),
  },
  upgrade: (() => {
    if (isNull(correspondenceData.upgrade)) { return null }

    return {
      status: correspondenceData.upgrade.status,
      error: correspondenceData.upgrade.error,
      changedAt: correspondenceData.upgrade.changedAt,
      expectedDuration: correspondenceData.upgrade.expectedDuration,
      firmware: correspondenceData.upgrade.firmware,
    };
  })(),
  mode: correspondenceData.mode,
  olt: (() => {
    if (isNull(correspondenceData.olt)) { return null }

    return {
      hasUnsupportedOnu: correspondenceData.olt.hasUnsupportedOnu,
      layer2: getOr(null, ['olt', 'layer2'], correspondenceData),
    };
  })(),
  onu: (() => {
    if (isNull(correspondenceData.onu)) { return null }

    return {
      id: correspondenceData.onu.id,
      onuId: correspondenceData.onu.onuId,
      port: correspondenceData.onu.port,
      profile: correspondenceData.onu.profile,
    };
  })(),
  aircube: (() => {
    if (isNil(correspondenceData.aircube)) { return null }

    return {
      wifiMode: correspondenceData.aircube.wifiMode,
      poe: correspondenceData.aircube.poe,
      wifi2Ghz: {
        available: correspondenceData.aircube.wifi2Ghz.available,
        mode: correspondenceData.aircube.wifi2Ghz.mode,
        mac: correspondenceData.aircube.wifi2Ghz.mac,
        ssid: correspondenceData.aircube.wifi2Ghz.ssid,
        country: correspondenceData.aircube.wifi2Ghz.country,
        channel: correspondenceData.aircube.wifi2Ghz.channel,
        isChannelAuto: correspondenceData.aircube.wifi2Ghz.isChannelAuto,
        frequency: correspondenceData.aircube.wifi2Ghz.frequency,
        encryption: correspondenceData.aircube.wifi2Ghz.encryption,
        authentication: correspondenceData.aircube.wifi2Ghz.authentication,
        txPower: correspondenceData.aircube.wifi2Ghz.txPower,
        key: correspondenceData.aircube.wifi2Ghz.key,
      },
      wifi5Ghz: {
        available: correspondenceData.aircube.wifi5Ghz.available,
        mode: correspondenceData.aircube.wifi5Ghz.mode,
        mac: correspondenceData.aircube.wifi5Ghz.mac,
        ssid: correspondenceData.aircube.wifi5Ghz.ssid,
        country: correspondenceData.aircube.wifi5Ghz.country,
        channel: correspondenceData.aircube.wifi5Ghz.channel,
        isChannelAuto: correspondenceData.aircube.wifi5Ghz.isChannelAuto,
        frequency: correspondenceData.aircube.wifi5Ghz.frequency,
        encryption: correspondenceData.aircube.wifi5Ghz.encryption,
        authentication: correspondenceData.aircube.wifi5Ghz.authentication,
        txPower: correspondenceData.aircube.wifi5Ghz.txPower,
        key: correspondenceData.aircube.wifi5Ghz.key,
      },
    };
  })(),
  airmax: (() => {
    if (isNull(correspondenceData.airmax)) { return null }

    return {
      series: correspondenceData.airmax.series,
      ssid: correspondenceData.airmax.ssid,
      frequency: correspondenceData.airmax.frequency,
      frequencyBands: correspondenceData.airmax.frequencyBands,
      frequencyCenter: correspondenceData.airmax.frequencyCenter,
      security: correspondenceData.airmax.security,
      authentication: correspondenceData.airmax.authentication,
      channelWidth: correspondenceData.airmax.channelWidth,
      countryCode: correspondenceData.airmax.countryCode,
      antenna: correspondenceData.airmax.antenna,
      noiseFloor: correspondenceData.airmax.noiseFloor,
      transmitChains: correspondenceData.airmax.transmitChains,
      receiveChains: correspondenceData.airmax.receiveChains,
      apMac: correspondenceData.airmax.apMac,
      wlanMac: correspondenceData.airmax.wlanMac,
      ccq: correspondenceData.airmax.ccq,
      wds: correspondenceData.airmax.wds,
      stationsCount: correspondenceData.airmax.stationsCount,
      wirelessMode: correspondenceData.airmax.wirelessMode,
      remoteSignal: correspondenceData.airmax.remoteSignal,
      lanStatus: {
        eth0: (() => {
          if (isNull(correspondenceData.airmax.lanStatus.eth0)) { return null }

          return {
            description: correspondenceData.airmax.lanStatus.eth0.description,
            plugged: correspondenceData.airmax.lanStatus.eth0.plugged,
            speed: correspondenceData.airmax.lanStatus.eth0.speed,
            duplex: correspondenceData.airmax.lanStatus.eth0.duplex,
          };
        })(),
        eth1: (() => {
          if (isNull(correspondenceData.airmax.lanStatus.eth1)) { return null }

          return {
            description: correspondenceData.airmax.lanStatus.eth1.description,
            plugged: correspondenceData.airmax.lanStatus.eth1.plugged,
            speed: correspondenceData.airmax.lanStatus.eth1.speed,
            duplex: correspondenceData.airmax.lanStatus.eth1.duplex,
          };
        })(),
      },
      polling: {
        enabled: correspondenceData.airmax.polling.enabled,
      },
      key: correspondenceData.airmax.key,
      mac: correspondenceData.airmax.mac,
    };
  })(),
  airfiber: (() => {
    if (isNull(correspondenceData.airfiber)) { return null }

    return {
      ssid: correspondenceData.airfiber.ssid,
      wirelessMode: correspondenceData.airfiber.wirelessMode,
      frequency: correspondenceData.airfiber.frequency,
      frequencyBands: correspondenceData.airfiber.frequencyBands,
      frameLength: correspondenceData.airfiber.frameLength,
      antenna: correspondenceData.airfiber.antenna,
      channelWidth: correspondenceData.airfiber.channelWidth,
      countryCode: correspondenceData.airfiber.countryCode,
      security: correspondenceData.airfiber.security,
      authentication: correspondenceData.airfiber.authentication,
      key: correspondenceData.airfiber.key,
      mac: correspondenceData.airfiber.mac,
    };
  })(),
  interfaces: getOr([], 'interfaces', correspondenceData), // just re-map the passed interfaces
  unmsSettings: (() => {
    const cmUnmsSettings = getOr(null, ['unmsSettings'], correspondenceData);
    if (isNull(cmUnmsSettings)) { return null }

    return {
      overrideGlobal: cmUnmsSettings.overrideGlobal,
      devicePingAddress: cmUnmsSettings.devicePingAddress,
      devicePingIntervalNormal: cmUnmsSettings.devicePingIntervalNormal,
      devicePingIntervalOutage: cmUnmsSettings.devicePingIntervalOutage,
      deviceTransmissionProfile: cmUnmsSettings.deviceTransmissionProfile,
    };
  })(),
});

// toDbDeviceList :: Array.<CorrespondenceData> -> Array.<DbDevice>
//     CorrespondenceData = Object
//     DbDevice = Object
const toDbDeviceList = map(toDbDevice);

module.exports = {
  toApiDeviceIdentification,
  toApiDeviceOverview,
  toApiDeviceFirmware,
  toApiDeviceCanDisplayStatistics,
  toApiDeviceStatusOverview,
  toApiDeviceStatusOverviewList,
  toApiONUStatusOverview,
  toApiONUStatusOverviewList,
  toApiErouterStatusDetail,
  toApiOLTStatusDetail,
  toApiAirMaxStatusDetail,
  toApiAirCubeStatusDetail,
  toApiEswitchStatusDetail,
  toApiEpowerStatusDetail,
  toApiAirFiberStatusDetail,
  toApiToughSwitchStatusDetail,
  toApiDeviceDisplayName,
  toDbDevice,
  toDbDeviceList,

  safeToApiDeviceStatusOverview: liftMapper(toApiDeviceStatusOverview),
  safeToApiDeviceStatusOverviewList: liftMapper(toApiDeviceStatusOverviewList),
  safeToApiONUStatusOverview: liftMapper(toApiONUStatusOverview),
  safeToApiONUStatusOverviewList: liftMapper(toApiONUStatusOverviewList),
  safeToApiErouterStatusDetail: liftMapper(toApiErouterStatusDetail),
  safeToApiOLTStatusDetail: liftMapper(toApiOLTStatusDetail),
  safeToApiAirMaxStatusDetail: liftMapper(toApiAirMaxStatusDetail),
  safeToApiAirCubeStatusDetail: liftMapper(toApiAirCubeStatusDetail),
  safeToApiEswitchStatusDetail: liftMapper(toApiEswitchStatusDetail),
  safeToApiAirFiberStatusDetail: liftMapper(toApiAirFiberStatusDetail),
  safeToApiEpowerStatusDetail: liftMapper(toApiEpowerStatusDetail),
  safeToApiToughSwitchStatusDetail: liftMapper(toApiToughSwitchStatusDetail),
  safeToDbDevice: liftMapper(toDbDevice),
  safeToDbDeviceList: liftMapper(toDbDeviceList),
};
