'use strict';

const { liftMapper } = require('../../index');
const { pick, map, getOr } = require('lodash/fp');

// toApiOnuProfile :: CmOnuProfile -> Object
//    CmOnuProfilesList = Object
const toApiOnuProfile = cmOnuProfile => ({
  id: getOr(null, ['id'], cmOnuProfile),
  name: getOr(null, ['name'], cmOnuProfile),
  mode: getOr(null, ['mode'], cmOnuProfile),
  adminPassword: getOr(null, ['adminPassword'], cmOnuProfile),
  bridge: getOr(null, ['bridge'], cmOnuProfile),
  router: getOr(null, ['router'], cmOnuProfile),
  lanAddress: getOr(null, ['lanAddress'], cmOnuProfile),
  lanProvisioned: getOr(null, ['lanProvisioned'], cmOnuProfile),
  linkSpeed: getOr(null, ['linkSpeed'], cmOnuProfile),
  onuCount: getOr(null, ['onuCount'], cmOnuProfile),
});

// toApiOnuProfilesList :: cmOnuProfilesList -> Object
//    cmOnuProfilesList = Object
const toApiOnuProfileList = map(toApiOnuProfile);

// toApiOnuPolicies :: CmOnuPolicies -> Object
//    CmOnuPolicies = Object
const toApiOnuPolicies = pick(['defaultState']);

module.exports = {
  toApiOnuProfileList,
  toApiOnuPolicies,

  safeToApiOnuProfileList: liftMapper(toApiOnuProfileList),
  safeToApiOnuPolicies: liftMapper(toApiOnuPolicies),
};
