'use strict';

const { curry, assocPath, pathEq, map } = require('ramda');

const { mergeDeviceUpdate } = require('../mergers');
const { StatusEnum } = require('../../../enums');


/**
* Merge erouter from DB with erouter just received from configuration
*
* @param {CorrespondenceDevice} dbDeviceCorrespondenceData
* @param {CorrespondenceDevice} hwDeviceCorrespondenceData
* @return {!CorrespondenceDevice}
*/
const mergeDbWithHw = (dbDeviceCorrespondenceData, hwDeviceCorrespondenceData) => {
  const newStatus = dbDeviceCorrespondenceData.identification.authorized ? StatusEnum.Active : StatusEnum.Unauthorized;

  return mergeDeviceUpdate(dbDeviceCorrespondenceData, {
    identification: {
      mac: hwDeviceCorrespondenceData.identification.mac,
      name: hwDeviceCorrespondenceData.identification.name,
      model: hwDeviceCorrespondenceData.identification.model,
      type: hwDeviceCorrespondenceData.identification.type,
      category: hwDeviceCorrespondenceData.identification.category,
      firmwareVersion: hwDeviceCorrespondenceData.identification.firmwareVersion,
      platformId: hwDeviceCorrespondenceData.identification.platformId,
      updated: Date.now(),
      ipAddress: hwDeviceCorrespondenceData.identification.ipAddress,
    },
    overview: {
      lastSeen: hwDeviceCorrespondenceData.overview.lastSeen,
      status: newStatus,
      gateway: hwDeviceCorrespondenceData.overview.gateway,
    },
    interfaces: hwDeviceCorrespondenceData.interfaces,
    olt: hwDeviceCorrespondenceData.olt,
  });
};

const mergeOnuCount = curry((onus, profiles) => map(
  (profile) => {
    const onusInProfile = onus.filter(pathEq(['onu', 'profile'], profile.id));

    return assocPath(['onuCount'], onusInProfile.length, profile);
  }
)(profiles));

module.exports = {
  mergeOnuCount,
  mergeDbWithHw,
};
