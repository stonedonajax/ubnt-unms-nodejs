'use strict';

const { getOr, flow, defaultTo, nth, toPairs, pick, invokeArgs, spread, isNull, split, curryN } = require('lodash/fp');
const { match, pathOr, pathEq, path, equals, curry, map, when, isEmpty, always, ifElse } = require('ramda');
const { isNotNull } = require('ramda-adjunct');
const { Maybe } = require('monet');

const { liftParser } = require('../../index');
const { defaultToWhen } = require('../../../util');
const { parseInterfaceListIpAddressCidr } = require('../../interfaces/parsers');

// parsePlatformId :: FullFirmwareVersion -> PlatformId
//     FullFirmwareVersion = String
//     PlatformId = String
const parsePlatformId = flow(match(/ER-(e\d+)\./), nth(1), defaultTo(null));

/**
 * @typedef {Object} OnuProfileBridge
 * @prop {?number} nativeVlan
 * @prop {!Array.<Number>} includeVlans
 */

/**
 * @typedef {Object} OnuProfileRouter
 * @prop {?number} wanVlan
 * @prop {string} gateway
 * @prop {string} dnsResolver
 * @prop {string} lanAddress
 * @prop {string} dhcpPool
 * @prop {?string} unmsConnString
 */

/**
 * @typedef {Object} CorrespondenceOnuProfile
 * @prop {?string} id
 * @prop {string} name
 * @prop {string} mode
 * @prop {string} adminPassword
 * @prop {OnuProfileBridge} bridge
 * @prop {OnuProfileRouter} router
 */

// parsePathValueArray :: ([Key], HwObject) -> [Number]|[]
const parsePathValueArray = (parsePath, hwObject) =>
  flow(getOr('', parsePath), when(isEmpty, always([])))(hwObject);

// parsePathValueArrayNumber :: ([Key], HwObject) -> [Number]|[]
const parsePathValueArrayNumber = (parsePath, hwObject) =>
  flow(getOr('', parsePath), when(isEmpty, always([])), map(Number))(hwObject);

// parseNativeVlan :: HwBridge -> Number|Null
const parseNativeVlan = flow(
  path(['port', '1', 'native-vlan']),
  defaultToWhen(equals(''), null),
  Maybe.fromNull,
  map(Number),
  invokeArgs('orSome', [null])
);

// parseHwOnuProfileBridge :: Object -> Object
const parseHwOnuProfileBridge = hwBridge => ({
  includeVlans: parsePathValueArrayNumber(['port', '1', 'include-vlan'], hwBridge),
  nativeVlan: parseNativeVlan(hwBridge),
});

// parsePathValueOrNull :: ([Key], Object) -> Number|Null
const parsePathValueOrNull = curryN(2, flow(getOr(null), when(isEmpty, always(null))));

// parsePathNumberOrNull :: ([Key], Object) -> Number|Null
const parsePathNumberOrNull = (parsePath, hwRouter) =>
  flow(parsePathValueOrNull(parsePath), when(isNotNull, Number))(hwRouter);

// parseHwOnuProfileRouter :: Object -> Object
const parseHwOnuProfileRouter = (hwRouter) => {
  const [dhcpPoolStart, dhcpPoolEnd] = flow(
    parsePathValueOrNull(['dhcp-pool']),
    ifElse(isNull, always([null, null]), split('-'))
  )(hwRouter);
  const dnsResolver = parsePathValueArray(['dns-resolver'], hwRouter);

  return {
    dhcpPoolStart,
    dhcpPoolEnd,
    wanVlan: parsePathNumberOrNull(['wan-vlan'], hwRouter),
    wanMode: parsePathValueOrNull(['wan-mode'], hwRouter),
    gateway: parsePathValueOrNull(['gateway'], hwRouter),
    primaryDns: getOr(null, [0], dnsResolver),
    secondaryDns: getOr(null, [1], dnsResolver),
    dhcpServer: parsePathValueOrNull(['dhcp-server'], hwRouter),
    dhcpRelay: parsePathValueOrNull(['dhcp-relay'], hwRouter),
    dhcpLeaseTime: parsePathNumberOrNull(['dhcp-lease-time'], hwRouter),
    dnsProxyEnable: pathEq(['dns-proxy-enable'], 'true', hwRouter),
  };
};

// parseHwOnuProfile :: [ String, Object ] -> CorrespondenceOnuProfile
//    CorrespondenceOnuProfile = Object
const parseHwOnuProfile = (profileId, hwOnuValue) => ({
  id: profileId,
  name: pathOr(null, ['name'], hwOnuValue),
  mode: pathOr(null, ['mode'], hwOnuValue),
  adminPassword: pathOr(null, ['admin-password'], hwOnuValue),
  bridge: parseHwOnuProfileBridge(pathOr(null, ['bridge-mode'], hwOnuValue)),
  router: parseHwOnuProfileRouter(pathOr(null, ['router-mode'], hwOnuValue)),
  lanAddress: parsePathValueOrNull(['lan-address'], hwOnuValue),
  lanProvisioned: pathEq(['lan-provisioned'], 'true', hwOnuValue),
  linkSpeed: parsePathValueOrNull(['port', '1', 'link-speed'], hwOnuValue),
});


// parseHwOnuProfileList :: (Auxiliaries, hwOnuProfiles) -> Array.<Object>
//    auxiliaries   = Object
//    hwOnuProfiles = Object
const parseHwOnuProfileList = (auxiliaries, hwOnuProfiles) => flow(
  toPairs,
  map(spread(parseHwOnuProfile))
)(hwOnuProfiles);

// parseHwOnu :: hwOnu -> Object
//    hwOnu = Object
const parseHwOnu = hwOnu => ({
  name: pathOr(null, ['1', 'name'], hwOnu),
  profile: pathOr(null, ['1', 'profile'], hwOnu),
  enabled: pathEq(['1', 'disable'], 'false', hwOnu),
});

// parseHwOnuList :: (Auxiliaries, HwOnuList) -> Array.<Object>
//    Auxiliaries = Object
//    HwOnuList   = Object
const parseHwOnuList = (auxiliaries, hwOnuList) => flow(
  toPairs,
  map(parseHwOnu)
)(hwOnuList);

// parseApiOnuProfile :: (Auxiliaries, ApiOnuProfile) -> Object
//    Auxiliaries = Object
//    ApiOnuProfile = Object
const parseApiOnuProfile = (auxiliaries, apiOnuProfile) => ({
  id: getOr(null, 'id', apiOnuProfile),
  name: apiOnuProfile.name,
  mode: apiOnuProfile.mode,
  adminPassword: apiOnuProfile.adminPassword,
  bridge: {
    nativeVlan: getOr(null, ['bridge', 'nativeVlan'], apiOnuProfile),
    includeVlans: getOr(null, ['bridge', 'includeVlans'], apiOnuProfile),
  },
  router: {
    wanMode: getOr(null, ['router', 'wanMode'], apiOnuProfile),
    wanVlan: getOr(null, ['router', 'wanVlan'], apiOnuProfile),
    gateway: getOr(null, ['router', 'gateway'], apiOnuProfile),
    primaryDns: getOr(null, ['router', 'primaryDns'], apiOnuProfile),
    secondaryDns: getOr(null, ['router', 'secondaryDns'], apiOnuProfile),
    dhcpServer: getOr(null, ['router', 'dhcpServer'], apiOnuProfile),
    relayAddress: getOr(null, ['router', 'relayAddress'], apiOnuProfile),
    dhcpPoolStart: getOr(null, ['router', 'dhcpPoolStart'], apiOnuProfile),
    dhcpPoolEnd: getOr(null, ['router', 'dhcpPoolEnd'], apiOnuProfile),
    dnsProxyEnable: getOr(null, ['router', 'dnsProxyEnable'], apiOnuProfile),
    dhcpLeaseTime: getOr(null, ['router', 'dhcpLeaseTime'], apiOnuProfile),
  },
  lanAddress: getOr(null, ['lanAddress'], apiOnuProfile),
  lanProvisioned: getOr(null, ['lanProvisioned'], apiOnuProfile),
  linkSpeed: getOr(null, ['linkSpeed'], apiOnuProfile),
});

// parseHwOnuPolicies :: (Auxiliaries, HwOnuPolicies) => Object
//    Auxiliaries   = Object
//    HwOnuPolicies = Object
const parseHwOnuPolicies = (auxiliaries, hwOnuPolicies) => ({
  defaultState: pathOr(null, ['default-state'], hwOnuPolicies),
});

// parseApiOnuPolicies :: (Auxiliaries, ApiOnuPolicies) => Object
//    Auxiliaries   = Object
//    ApiOnuPolicies = Object
const parseApiOnuPolicies = (auxiliaries, apiOnuPolicies) => pick(['defaultState'], apiOnuPolicies);

// parseOltIpAddress :: Auxiliaries -> CorrespondenceData -> IpAddress
//    Auxiliaries        = Object
//    CorrespondenceData = Object
//    IpAddress          = String
const parseOltIpAddress = curry((auxiliaries, olt) =>
  parseInterfaceListIpAddressCidr({ gateway: getOr(null, ['overview', 'gateway'], olt) }, olt.interfaces)
);


module.exports = {
  parsePlatformId,
  parseOltIpAddress,

  safeParseOltIpAddress: liftParser(parseOltIpAddress),

  parseHwOnuProfileList,
  parseApiOnuProfile,
  parseHwOnuList,

  safeParseHwOnuProfileList: liftParser(parseHwOnuProfileList),
  safeParseApiOnuProfile: liftParser(parseApiOnuProfile),
  safeParseHwOnuList: liftParser(parseHwOnuList),

  parseHwOnuPolicies,
  parseApiOnuPolicies,

  safeParseHwOnuPolicies: liftParser(parseHwOnuPolicies),
  safeParseApiOnuPolicies: liftParser(parseApiOnuPolicies),
};
