'use strict';

const { lensPath, compose } = require('ramda');

const latitudeLens = lensPath(['latitude']);
const longitudeLens = lensPath(['longitude']);
const deviceLocationLens = lensPath(['overview', 'location']);
const siteLocationLens = lensPath(['identification', 'site', 'description', 'location']);
const deviceLatitudeLens = compose(deviceLocationLens, latitudeLens);
const deviceLongitudeLens = compose(deviceLocationLens, longitudeLens);
const siteLatitudeLens = compose(siteLocationLens, latitudeLens);
const siteLongitudeLens = compose(siteLocationLens, longitudeLens);


module.exports = {
  siteLatitudeLens,
  siteLongitudeLens,
  deviceLatitudeLens,
  deviceLongitudeLens,
};
