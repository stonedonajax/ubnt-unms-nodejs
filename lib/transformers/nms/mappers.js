'use strict';

const { comparator } = require('ramda');

const { liftMapper } = require('../index');

// toApiNmsSummary :: CmNmsSummary -> ApiNmsSummary
//     CmNmsSummary = Object
//     NmsSummary = Object
const toApiNmsSummary = correspondenceData => ({
  logsUnreadCount: correspondenceData.logsUnreadCount,
  outagesUnreadCount: correspondenceData.outagesUnreadCount,
  devicesUnauthorizedCount: correspondenceData.devicesUnauthorizedCount,
  firmwaresUnreadCount: correspondenceData.firmwaresUnreadCount,
  outagesActiveCount: correspondenceData.outagesActiveCount,
  allSitesCount: correspondenceData.allSitesCount,
  activeSitesCount: correspondenceData.activeSitesCount,
  allClientsCount: correspondenceData.allClientsCount,
  activeClientsCount: correspondenceData.activeClientsCount,
});

// toApiSmtp :: NmsCorrespondenceData -> ApiSmtp
//     NmsCorrespondenceData = Object
//     ApiSmtp = Object
const toApiSmtp = cmNms => ({
  customSmtpAuthEnabled: cmNms.smtp.customSmtpAuthEnabled,
  customSmtpHostname: cmNms.smtp.customSmtpHostname,
  customSmtpPassword: cmNms.smtp.customSmtpPassword,
  customSmtpPort: cmNms.smtp.customSmtpPort,
  customSmtpSecurityMode: cmNms.smtp.customSmtpSecurityMode,
  customSmtpSender: cmNms.smtp.customSmtpSender,
  customSmtpUsername: cmNms.smtp.customSmtpUsername,
  gmailPassword: cmNms.smtp.gmailPassword,
  gmailUsername: cmNms.smtp.gmailUsername,
  tlsAllowUnauthorized: cmNms.smtp.tlsAllowUnauthorized,
  type: cmNms.smtp.type,
});

// toApiNmsSettings :: NmsSettingsCorrespondenceData -> ApiNmsSettings
//     NmsSettingsCorrespondenceData = Object
//     ApiNmsSettings = Object
const toApiNmsSettings = cmNmsSettings => ({
  timezone: cmNmsSettings.timezone,
  hostname: cmNmsSettings.hostname,
  useLetsEncrypt: cmNmsSettings.useLetsEncrypt,
  letsEncryptError: cmNmsSettings.letsEncryptError,
  letsEncryptTimestamp: cmNmsSettings.letsEncryptTimestamp,
  autoBackups: cmNmsSettings.autoBackups,
  mapsProvider: cmNmsSettings.maps.provider,
  googleMapsApiKey: cmNmsSettings.maps.googleMapsApiKey,
  devicePingAddress: cmNmsSettings.devicePingAddress,
  devicePingIntervalNormal: cmNmsSettings.devicePingIntervalNormal,
  devicePingIntervalOutage: cmNmsSettings.devicePingIntervalOutage,
  allowLoggingToSentry: cmNmsSettings.allowLoggingToSentry,
  allowLoggingToLogentries: cmNmsSettings.allowLoggingToLogentries,
  deviceTransmissionProfile: cmNmsSettings.deviceTransmissionProfile,
  allowSelfSignedCertificate: cmNmsSettings.allowSelfSignedCertificate,
  defaultGracePeriod: cmNmsSettings.outages.defaultGracePeriod,
  restartGracePeriod: cmNmsSettings.outages.restartGracePeriod,
  upgradeGracePeriod: cmNmsSettings.outages.upgradeGracePeriod,
  dateFormat: cmNmsSettings.locale.longDateFormat.LL,
  timeFormat: cmNmsSettings.locale.longDateFormat.LT,
  allowAutoUpdateUbntFirmwares: cmNmsSettings.firmwares.allowAutoUpdateUbntFirmwares,
  allowBetaFirmwares: cmNmsSettings.firmwares.allowBetaFirmwares,
});

// toDbNms :: NmsCorrespondenceData -> DbNms
//    NmsCorrespondenceData = Object
//    DbNms = Object
const toDbNms = cmNms => ({
  id: cmNms.id,
  isConfigured: cmNms.isConfigured,
  instanceId: cmNms.instanceId,
  aesKey: cmNms.aesKey,
  autoBackups: cmNms.autoBackups,
  smtp: {
    type: cmNms.smtp.type,
    tlsAllowUnauthorized: cmNms.smtp.tlsAllowUnauthorized,
    customSmtpAuthEnabled: cmNms.smtp.customSmtpAuthEnabled,
    customSmtpHostname: cmNms.smtp.customSmtpHostname,
    customSmtpPort: cmNms.smtp.customSmtpPort,
    customSmtpUsername: cmNms.smtp.customSmtpUsername,
    customSmtpPassword: cmNms.smtp.customSmtpPassword,
    customSmtpSender: cmNms.smtp.customSmtpSender,
    gmailPassword: cmNms.smtp.gmailPassword,
    gmailUsername: cmNms.smtp.gmailUsername,
    customSmtpSecurityMode: cmNms.smtp.customSmtpSecurityMode,
  },
  deviceLog: cmNms.deviceLog,
  devicePingAddress: cmNms.devicePingAddress,
  devicePingIntervalNormal: cmNms.devicePingIntervalNormal,
  devicePingIntervalOutage: cmNms.devicePingIntervalOutage,
  deviceTransmissionProfile: cmNms.deviceTransmissionProfile,
  allowLoggingToSentry: cmNms.allowLoggingToSentry,
  allowLoggingToLogentries: cmNms.allowLoggingToLogentries,
  allowSelfSignedCertificate: cmNms.allowSelfSignedCertificate,
  maps: cmNms.maps,
  hostname: cmNms.hostname,
  useLetsEncrypt: cmNms.useLetsEncrypt,
  letsEncryptError: cmNms.letsEncryptError,
  letsEncryptTimestamp: cmNms.letsEncryptTimestamp,
  timezone: cmNms.timezone,
  outages: cmNms.outages,
  locale: cmNms.locale,
  firmwares: cmNms.firmwares,
  eula: {
    email: cmNms.eula.email,
    timestamp: cmNms.eula.timestamp,
  },
});

const sortStatistics = comparator((a, b) => a.x < b.x);

const toApiStatisticsList = ({ statistics, limits }) => {
  const apiStatistics = {
    networkHealth: [],
    allSites: [],
    liveSites: [],
    allClients: [],
    liveClients: [],
    period: limits.period,
    interval: limits.interval,
  };

  const timestamps = new Set();
  statistics.forEach((statistic) => {
    const timestamp = statistic.timestamp;
    if (timestamps.has(timestamp) || timestamp < limits.interval.start || timestamp > limits.interval.end) {
      return;
    }
    timestamps.add(timestamp);
    apiStatistics.allSites.push({ x: timestamp, y: statistic.stats.allSites });
    apiStatistics.liveSites.push({ x: timestamp, y: statistic.stats.liveSites });
    apiStatistics.allClients.push({ x: timestamp, y: statistic.stats.allClients });
    apiStatistics.liveClients.push({ x: timestamp, y: statistic.stats.liveClients });
    apiStatistics.networkHealth.push({ x: timestamp, y: statistic.stats.networkHealth });
  });

  apiStatistics.allSites.sort(sortStatistics);
  apiStatistics.liveSites.sort(sortStatistics);
  apiStatistics.allClients.sort(sortStatistics);
  apiStatistics.liveClients.sort(sortStatistics);
  apiStatistics.networkHealth.sort(sortStatistics);

  return apiStatistics;
};

module.exports = {
  toApiNmsSummary,
  toApiSmtp,
  toApiNmsSettings,
  toApiStatisticsList,

  toDbNms,

  safeToApiNmsSummary: liftMapper(toApiNmsSummary),
  safeToApiSmtp: liftMapper(toApiSmtp),
  safeToApiNmsSettings: liftMapper(toApiNmsSettings),

  safeToDbNms: liftMapper(toDbNms),

  safeToApiStatisticsList: liftMapper(toApiStatisticsList),
};
