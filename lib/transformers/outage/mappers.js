'use strict';

const { transduce, merge, when, map } = require('ramda');
const { isPlainObject, getOr, defaultTo } = require('lodash/fp');
const { isNotPlainObj } = require('ramda-adjunct');

const { liftMapper } = require('../index');
const { parseAllCount } = require('./parsers');
const { DeviceProductNameEnum } = require('../../enums');

// toApiOutageSiteIdentification :: Object -> ApiOutageSiteIdentification
//     ApiOutageSiteIdentification = Object
const toApiOutageSiteIdentification = correspondenceData => ({
  id: correspondenceData.id,
  name: correspondenceData.name,
  type: correspondenceData.type,
  parent: correspondenceData.parent,
  status: correspondenceData.status,
});

// toApiOutageDevice :: Object -> ApiOutageDeviceIdentification
//     ApiOutageDeviceIdentification = Object
const toApiOutageDevice = (correspondenceData) => {
  const { device: cmDevice, deviceMetadata } = correspondenceData;
  if (isNotPlainObj(cmDevice)) { return null }

  const model = cmDevice.model;
  return {
    id: cmDevice.id,
    site: when(isPlainObject, toApiOutageSiteIdentification, cmDevice.site),
    siteId: cmDevice.siteId,
    mac: cmDevice.mac,
    name: cmDevice.name,
    serialNumber: cmDevice.serialNumber,
    firmwareVersion: cmDevice.firmwareVersion,
    model,
    modelName: getOr(model, model, DeviceProductNameEnum),
    platformId: cmDevice.platformId,
    type: cmDevice.type,
    category: cmDevice.category,
    authorized: cmDevice.authorized,
    updated: cmDevice.updated,
    displayName: defaultTo(cmDevice.name, deviceMetadata.alias),
  };
};

// toApiOutageItem :: Object -> ApiOutageItem
//     ApiOutageItem = Object
const toApiOutageItem = correspondenceData => ({
  id: correspondenceData.id,
  startTimestamp: correspondenceData.startTimestamp,
  endTimestamp: correspondenceData.endTimestamp,
  aggregatedTime: correspondenceData.aggregatedTime,
  type: correspondenceData.type,
  inProgress: correspondenceData.inProgress,
  device: toApiOutageDevice(correspondenceData),
  deviceMetadata: correspondenceData.deviceMetadata,
  site: correspondenceData.site,
});

// toApiOutageItemList :: Array.<OutageItemCorrespondenceData> -> Array.<ApiOutageItem>
//     OutageItemCorrespondenceData = Object
const toApiOutageItemList = map(toApiOutageItem);

// toApiOutageAggregationItem :: OutageAggregationItemCorrespondenceData -> ApiOutageAggregationItem
//     OutageAggregationItemCorrespondenceData = Object
//     ApiOutageAggregationItem = Object
const toApiOutageAggregationItem = cmOutageAggregationItem => ({
  [`${cmOutageAggregationItem.type}Count`]: cmOutageAggregationItem.count,
});

// toApiOutageAggregation :: Array.<OutageAggregationItemCorrespondenceData> -> toApiOutageAggregation
//     OutageAggregationItemCorrespondenceData = Object
//     toApiOutageAggregation = Object
const toApiOutageAggregation = (cmOutageAggregationList) => {
  const allCount = parseAllCount(cmOutageAggregationList);
  const aggregationCounts = transduce(map(toApiOutageAggregationItem), merge, {}, cmOutageAggregationList);

  return merge({
    allCount,
    outageCount: 0,
    qualityCount: 0,
  }, aggregationCounts);
};

// toApiOutagePagination :: OutagePaginationCorrespondenceData -> ApiOutagePagination
//     OutagePaginationCorrespondenceData = Object
//     ApiOutagePagination = Object
const toApiOutagePagination = cmPagination => ({
  total: cmPagination.total,
  count: cmPagination.count,
  page: cmPagination.page,
  pages: cmPagination.pages,
});

// toApiOutageView :: OutageViewCorrespondenceData -> ApiOutageView
//     OutageViewCorrespondenceData = Object
//     ApiOutageView = Object
const toApiOutageView = cmOutageView => ({
  items: toApiOutageItemList(cmOutageView.itemList),
  aggregation: toApiOutageAggregation(cmOutageView.aggregationItemList),
  pagination: toApiOutagePagination(cmOutageView.pagination),
});


module.exports = {
  toApiOutageItem,
  toApiOutageItemList,
  toApiOutageAggregationItem,
  toApiOutageAggregation,
  toApiOutagePagination,
  toApiOutageView,

  safeToApiOutageItem: liftMapper(toApiOutageItem),
  safeToApiOutageItemList: liftMapper(toApiOutageItemList),
  safeToApiOutageAggregationItem: liftMapper(toApiOutageAggregationItem),
  safeToApiOutageAggregation: liftMapper(toApiOutageAggregation),
  safeToApiOutagePagination: liftMapper(toApiOutagePagination),
  safeToApiOutageView: liftMapper(toApiOutageView),
};
