'use strict';

const { Maybe } = require('monet');
const { replace, when } = require('ramda');
const { isString, pullAt } = require('lodash/fp');
const semver = require('semver');
const {
  getOr, flow, has, trimChars, memoize, split, compact, take, join, defaultTo, toLower,
} = require('lodash/fp');

/**
 * @typedef {SemVer} CorrespondenceSemver
 * @property {string} order
 */

// prerelease string -> number (order) or zero (equal to stable)
const SPECIAL_PRERELEASES = {
  '-1': 1, // 1.9.1-1
  '-1unms': 2, // 1.9.1-1unms
  '-hotfix': 1, // 1.9.7-hotfix
  '-cs': 0, // airmax firmwares with custom scripts, single
  cs: 0, // airmax firmwares with custom scripts, combined with alpha/beta/rc
};

const stripCustomScriptSuffix = when(isString, replace(/-cs$/, ''));

const parseSpecial = (prerelease) => {
  if (prerelease.length === 0) {
    return { value: 0, prerelease };
  }

  for (let i = 0; i < prerelease.length; i += 1) {
    const part = i === 0 ? `-${prerelease[i]}` : prerelease[i];
    if (has(part, SPECIAL_PRERELEASES)) {
      return {
        value: getOr(0, part, SPECIAL_PRERELEASES),
        prerelease: pullAt(i, prerelease),
      };
    }
  }

  return {
    value: 0,
    prerelease: prerelease.map(stripCustomScriptSuffix),
  };
};

/**
 * Sortable semver
 *
 * @param {SemVer} v
 * @return {string}
 */
const parseSemverOrder = (v) => {
  const { value: special, prerelease } = parseSpecial(v.prerelease);
  const isStable = prerelease.length === 0;

  // compress major and minor together
  const majorMinor = (v.major << 16) | v.minor; // eslint-disable-line no-bitwise

  if (!isStable) {
    return `${majorMinor}.${v.patch}.${special}-${prerelease.join('.')}`;
  }

  return `${majorMinor}.${v.patch}.${special}`;
};

/**
 * @param {string} rawSemver
 * @return {CorrespondenceSemver}
 */
const parseSemver = (rawSemver) => {
  const parsedVersion = semver.parse(rawSemver);
  if (parsedVersion !== null) {
    parsedVersion.order = parseSemverOrder(parsedVersion);
  }

  return parsedVersion;
};

/**
 * @param {SemVer} semverVersion
 * @return {boolean}
 */
const parseStableVersion = semverVersion => (
  semverVersion.prerelease.length === 0 ||
  has(`-${semverVersion.prerelease[0]}`, SPECIAL_PRERELEASES)
);

/**
 * @function sanitizePreRelease
 * @param {string} preRelease suffix
 * @return {string}
 */
const sanitizePreRelease = flow(
  toLower,
  // normalize prerelease, split number from name, strip leading zeroes
  replace(
    /^(alpha|beta|rc)(\d+)([a-z-.]*).*/,
    (match, prerelease, number, suffix) => `${prerelease}.${Number(number)}.${trimChars('-', suffix)}`
  ),
  trimChars('.'),
  replace(/[^a-z0-9-.]+/, '-'),
  split('-'),
  compact,
  take(2),
  join('-')
);

/**
 * @function sanitizePreRelease
 * @param {string} preRelease suffix
 * @return {string}
 */
const sanitizeVersionString = flow(
  replace(/\.[\w\d]+\.\d{6}.\d{4}$/, ''), // strip compile date
  replace(/^(EdgeRouter|Fiber)\.ER-e\d+/, ''), // strip EdgeOs prefix
  replace(
    /.*?(\d+)\.(\d+)(\.(\d+))?[^A-Za-z0-9]*(.*)/, // patch version can be optional
    (match, major, minor, _, patch, prerelease) =>
      `${major}.${minor}.${defaultTo(0, patch)}-${sanitizePreRelease(prerelease)}`
  ),
  trimChars('-_')
);

/**
 * @param {string} commFirmwareVersion
 * @return {?string}
 */
const parseCommFirmwareVersion = commFirmwareVersion => Maybe
  .fromNull(commFirmwareVersion)
  .map(sanitizeVersionString)
  .filter(semver.valid)
  .orSome(null);

module.exports = {
  parseCommFirmwareVersion: memoize(parseCommFirmwareVersion),

  parseSemver,
  parseStableVersion,
};
