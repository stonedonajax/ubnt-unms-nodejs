'use strict';

const mappers = require('./mappers');
const parsers = require('./parsers');


const { toCorrespondence, fromCorrespondence } = require('../index');

// fromDb :: Auxiliaries -> DbSite -> Either.<Object>
//     Auxiliaries = Object
//     DbSite = Object
const fromDb = toCorrespondence(parsers.safeParseDbSite);

// fromDbList :: Auxiliaries -> Array.<DbSite> -> Either.<Object>
//     Auxiliaries = Object
//     DbSite = Object
const fromDbList = toCorrespondence(parsers.safeParseDbSiteList);

// fromDbImage :: Auxiliaries -> DbImage -> Either.<Object>
//     Auxiliaries = Object
//     DbImage = Object
const fromDbImage = toCorrespondence(parsers.safeParseDbImage);

// fromDbImageList :: Auxiliaries -> Array.<DbImage> -> Either.<Object>
//     Auxiliaries = Object
//     DbImage = Object
const fromDbImageList = toCorrespondence(parsers.safeParseDbImageList);

// toApiOverview :: Correspondence -> Either.<Object>
//     Correspondence = Object
const toApiOverview = fromCorrespondence(mappers.safeToApiSiteOverview);

// toApiOverviewList :: CorrespondenceList -> Either.<Object>
//     CorrespondenceList = Array.<Object>
const toApiOverviewList = fromCorrespondence(mappers.safeToApiSiteOverviewList);

// toApiImage :: Correspondence -> Either.<Object>
//     Correspondence = Object
const toApiImage = fromCorrespondence(mappers.safeToApiImage);

// toApiImageList :: CorrespondenceList -> Either.<Object>
//     CorrespondenceList = Array.<Object>
const toApiImageList = fromCorrespondence(mappers.safeToApiImageList);

// toDb :: SiteCorrenspondenceData -> Either.<DbSite>
//     SiteCorrenspondenceData = Object
//     DbNms = DbSite
const toDb = fromCorrespondence(mappers.safeToDbSite);

// toDbImage :: ImageCorrenspondenceData -> Either.<DbImage>
//     ImageCorrenspondenceData = Object
const toDbImage = fromCorrespondence(mappers.safeToDbImage);

// fromApiSite :: ApiSiteData -> Either.<SiteCorrespondenceData>
//    ApiSiteData = Object
//    SiteCorrespondenceData = Object
const fromApiSite = toCorrespondence(parsers.safeParseApiSite);


module.exports = {
  fromApiSite,

  fromDb,
  fromDbList,
  fromDbImage,
  fromDbImageList,

  toDb,
  toDbImage,

  toApiOverview,
  toApiOverviewList,
  toApiImage,
  toApiImageList,
};
