'use strict';

const { get, isNull, isNil, map, defaultTo, getOr } = require('lodash/fp');
const { isNotUndefined } = require('ramda-adjunct');

const { liftMapper } = require('../index');

// TODO(vladimir.gorej@gmail.com): mapping of users and endpoints must be implemented.

// parseCmSiteLocation :: CorrespondenceData -> Object
//     CorrespondenceData = Object
const parseCmSiteLocation = (cmSite) => {
  if (isNull(cmSite.description.location)) { return null }

  return {
    longitude: cmSite.description.location.longitude,
    latitude: cmSite.description.location.latitude,
  };
};

// toApiSiteIdentification :: Object -> ApiSiteIdentification
//     ApiSiteIdentification :: Object
const toApiSiteIdentification = correspondenceData => ({
  id: correspondenceData.identification.id,
  status: correspondenceData.identification.status,
  name: correspondenceData.identification.name,
  parent: correspondenceData.identification.parent,
  type: correspondenceData.identification.type,
});

// toApiSiteOverview :: Object -> ApiSiteOverview
//     ApiSiteOverview = Object
const toApiSiteOverview = correspondenceData => ({
  id: correspondenceData.identification.id,
  identification: toApiSiteIdentification(correspondenceData),
  description: {
    address: correspondenceData.description.address,
    note: correspondenceData.description.note,
    contact: {
      name: correspondenceData.description.contact.name,
      phone: correspondenceData.description.contact.phone,
      email: correspondenceData.description.contact.email,
    },
    location: parseCmSiteLocation(correspondenceData),
    height: correspondenceData.description.height,
    elevation: correspondenceData.description.elevation,
    endpoints: correspondenceData.description.endpoints,
  },
  notifications: {
    type: correspondenceData.notifications.type,
    users: correspondenceData.notifications.users,
  },
});

// toApiSiteOverviewList :: CorrespondenceList -> ApiSiteOverviewList
//     CorrespondenceList = Array
//     ApiSiteOverviewList = Array.<ApiSiteOverview>
const toApiSiteOverviewList = map(toApiSiteOverview);

// toApiImage :: Object -> ApiImage
//     ApiImage = Object
const toApiImage = correspondenceData => ({
  id: correspondenceData.id,
  identification: correspondenceData.identification,
  name: defaultTo('', getOr(null, ['name'], correspondenceData)),
  fileName: correspondenceData.fileName,
  fileType: correspondenceData.fileType,
  height: correspondenceData.height,
  width: correspondenceData.width,
  order: correspondenceData.order,
  description: defaultTo('', getOr(null, ['description'], correspondenceData)),
  date: correspondenceData.date,
  size: correspondenceData.size,
  fullUrl: correspondenceData.fullUrl,
  thumbUrl: correspondenceData.thumbUrl,
});

// toApiImageList :: CorrespondenceList -> ApiImageList
//     CorrespondenceList = Array
//     ApiImageList = Array.<ApiImage>
const toApiImageList = map(toApiImage);


// parseParent :: CorrespondenceData -> Object
//     CorrespondenceData = Object
const parseParent = (cmSite) => {
  if (isNil(cmSite.identification.parent)) { return null }
  if (isNotUndefined(get(['identification', 'parent', 'id'], cmSite))) { return cmSite.identification.parent }

  return { id: cmSite.identification.parent };
};

// toDbSite :: SiteCorrespondenceData -> DbSite
//    SiteCorrespondenceData = Object
//    DbSite = Object
const toDbSite = cmSite => ({
  id: cmSite.identification.id,
  identification: {
    id: cmSite.identification.id,
    name: cmSite.identification.name,
    type: cmSite.identification.type,
    status: cmSite.identification.status,
    parent: parseParent(cmSite),
  },
  description: {
    address: cmSite.description.address,
    note: cmSite.description.note,
    contact: {
      name: cmSite.description.contact.name,
      phone: cmSite.description.contact.phone,
      email: cmSite.description.contact.email,
    },
    location: parseCmSiteLocation(cmSite),
    endpoints: cmSite.description.endpointIds,
    height: cmSite.description.height,
    elevation: cmSite.description.elevation,
  },
  notifications: {
    type: cmSite.notifications.type,
    users: cmSite.notifications.users,
  },
});

// toDbImage :: ImageCorrespondenceData -> DbImage
//    ImageCorrespondenceData = Object
//    DbImage = Object
const toDbImage = cmImage => ({
  id: cmImage.id,
  name: cmImage.name,
  height: cmImage.height,
  width: cmImage.width,
  fileName: cmImage.fileName,
  fileType: cmImage.fileType,
  order: cmImage.order,
  description: cmImage.description,
  date: cmImage.date,
  size: cmImage.size,
  siteId: cmImage.siteId,
  version: cmImage.version,
});

module.exports = {
  toApiSiteOverview,
  toApiSiteOverviewList,
  toApiImage,
  toApiImageList,
  toApiSiteIdentification,

  toDbSite,
  toDbImage,

  safeToDbSite: liftMapper(toDbSite),
  safeToDbImage: liftMapper(toDbImage),

  safeToApiSiteOverview: liftMapper(toApiSiteOverview),
  safeToApiSiteOverviewList: liftMapper(toApiSiteOverviewList),
  safeToApiImage: liftMapper(toApiImage),
  safeToApiImageList: liftMapper(toApiImageList),
};
