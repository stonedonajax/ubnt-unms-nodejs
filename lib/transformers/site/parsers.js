'use strict';

const { pathEq } = require('ramda');
const { isUndefined, curry, map, getOr, isNil, isNull } = require('lodash/fp');

const { liftParser } = require('../index');
const { AlertTypeEnum } = require('../../enums');
const { isValidLocation } = require('../../util/gps');
const { getSiteImagesFileUrl } = require('../../util/siteImage');

// TODO(vladimir.gorej@gmail.com): parsing auxiliaries of dbUser and dbEndpoint must be implemented.

// parseDbSiteLocation :: Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseDbSiteLocation = (dbSite) => {
  const location = {
    longitude: Number(dbSite.longitude),
    latitude: Number(dbSite.latitude),
  };

  return isValidLocation(location) ? location : null;
};

// parseDbSiteEndpoints :: Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseDbSiteEndpoints = (dbSite) => {
  if (isUndefined(dbSite.endpoints)) { return [] }

  return dbSite.endpoints.map(endpoint => ({
    id: endpoint.id,
    name: endpoint.name,
    parent: endpoint.parentId,
    status: endpoint.status,
    type: endpoint.type,
  }));
};

// parseDbSiteUsers :: Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseDbSiteUsers = (dbSite) => {
  if (isNil(dbSite.users)) { return [] }

  return dbSite.users.map(user => ({
    id: user.id,
    username: user.username,
    email: user.email,
    totpAuthEnabled: user.totpAuthAnabled,
  }));
};

// parseDbSiteParent :: Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseDbSiteParent = (dbSite) => {
  if (isNil(dbSite.parentId)) { return null }
  if (isNil(dbSite.parent)) { return dbSite.parentId }

  return {
    id: dbSite.parent.id,
    name: dbSite.parent.name,
    parent: null,
    status: dbSite.parent.status,
    type: dbSite.parent.type,
  };
};

// parseDbSite :: Object -> Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseDbSite = curry((auxiliaries, dbSite) => {
  if (pathEq(['options', 'allowNull'], true, auxiliaries) && dbSite === null) { return null }

  return {
    id: dbSite.id,
    identification: {
      id: dbSite.id,
      name: getOr(null, ['name'], dbSite),
      type: getOr(null, ['type'], dbSite),
      status: getOr(null, ['status'], dbSite),
      parent: parseDbSiteParent(dbSite),
    },
    description: {
      address: getOr(null, ['address'], dbSite),
      note: getOr(null, ['note'], dbSite),
      contact: {
        name: getOr(null, ['contactName'], dbSite),
        phone: getOr(null, ['contactPhone'], dbSite),
        email: getOr(null, ['contactEmail'], dbSite),
      },
      location: parseDbSiteLocation(dbSite),
      endpoints: parseDbSiteEndpoints(dbSite),
      height: getOr(null, ['height'], dbSite),
      elevation: getOr(null, ['elevation'], dbSite),
    },
    notifications: {
      type: getOr(null, ['notificationType'], dbSite),
      users: parseDbSiteUsers(dbSite),
    },
  };
});

// parseDbSiteList :: (Object, Array.<DbSite>) -> Array.<Correspondence>
//     DbSite = Object
//     Correspondence = Object
const parseDbSiteList = (auxiliaries, dbSites) => map(parseDbSite(auxiliaries), dbSites);

// parseDbImage :: Object -> Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseDbImage = curry((auxiliaries, dbImage) => ({
  id: dbImage.id,
  identification: { id: dbImage.id },
  siteId: dbImage.siteId,
  name: dbImage.name,
  fileName: dbImage.fileName,
  fileType: dbImage.fileType,
  height: dbImage.height,
  width: dbImage.width,
  order: dbImage.order,
  description: dbImage.description,
  date: dbImage.date,
  size: dbImage.size,
  fullUrl: `${auxiliaries.hostname}${getSiteImagesFileUrl(dbImage.siteId, dbImage.id)}?v=${dbImage.version}`,
  thumbUrl: `${auxiliaries.hostname}${getSiteImagesFileUrl(dbImage.siteId, dbImage.id, true)}?v=${dbImage.version}`,
  version: dbImage.version,
}));

// parseDbImageList :: (Object, Array.<DbImage>) -> Array.<Correspondence>
//     DbImage = Object
//     Correspondence = Object
const parseDbImageList = (auxiliaries, dbImages) => map(parseDbImage(auxiliaries), dbImages);

// parseApiSiteParent :: Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseApiSiteParent = (apiSite) => {
  if (isNull(getOr(null, ['identification', 'parent', 'id'], apiSite))) { return null }

  return {
    id: apiSite.identification.parent.id,
    name: apiSite.identification.parent.name,
    parent: null,
    status: apiSite.identification.parent.status,
    type: apiSite.identification.parent.type,
  };
};

// parseApiSiteLocation :: Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseApiSiteLocation = (apiSite) => {
  if (isNull(apiSite.description.location)) { return null }

  return {
    longitude: apiSite.description.location.longitude,
    latitude: apiSite.description.location.latitude,
  };
};

// parseApiSite :: Object -> Object -> CorrespondenceData
//     CorrespondenceData = Object
const parseApiSite = curry((auxiliaries, apiSite) => ({
  id: apiSite.id,
  identification: {
    id: apiSite.id,
    name: getOr(null, ['identification', 'name'], apiSite),
    type: getOr(null, ['identification', 'type'], apiSite),
    status: getOr(null, ['identification', 'status'], apiSite),
    parent: parseApiSiteParent(apiSite),
  },
  description: {
    address: getOr(null, ['description', 'address'], apiSite),
    note: getOr(null, ['description', 'note'], apiSite),
    contact: {
      name: getOr(null, ['description', 'contact', 'name'], apiSite),
      phone: getOr(null, ['description', 'contact', 'phone'], apiSite),
      email: getOr(null, ['description', 'contact', 'email'], apiSite),
    },
    location: parseApiSiteLocation(apiSite),
    endpoints: apiSite.description.endpoints,
    height: getOr(null, ['description', 'height'], apiSite),
    elevation: getOr(null, ['description', 'elevation'], apiSite),
  },
  notifications: {
    type: getOr(AlertTypeEnum.None, ['notifications', 'type'], apiSite),
    users: getOr([], ['notifications', 'users'], apiSite),
  },
}));


module.exports = {
  parseApiSite,

  parseDbSite,
  parseDbSiteList,
  parseDbImage,
  parseDbImageList,

  safeParseApiSite: liftParser(parseApiSite),

  safeParseDbSite: liftParser(parseDbSite),
  safeParseDbSiteList: liftParser(parseDbSiteList),
  safeParseDbImage: liftParser(parseDbImage),
  safeParseDbImageList: liftParser(parseDbImageList),
};
