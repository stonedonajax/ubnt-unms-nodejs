'use strict';

const MessageNameEnum = Object.freeze({
  Connect: 'connect',
  GetSysInfo: 'getSysInfo',
  SubscribeCmd: 'subscribeCmd',
  UnsubscribeCmd: 'unsubscribeCmd',
  Ping: 'ping',
  Cmd: 'cmd',
  Unknown: 'unknown',
  ShellConnect: 'shellConnect',
  ShellDisconnect: 'shellDisconnect',
  Shell: 'shell',
});

const MessageTypeEnum = Object.freeze({
  Event: 'event',
  Rpc: 'rpc',
  Cmd: 'cmd',
  RpcError: 'rpc-error',
});

module.exports = {
  MessageNameEnum,
  MessageTypeEnum,
};
