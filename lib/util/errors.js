'use strict';

const Boom = require('boom');

class UbntError extends Error {
  constructor(message, code = 500) {
    super(message);
    this.code = code;
  }

  toBoom() {
    return Boom.create(this.code, this.message);
  }

  toWsError() {
    const { code, message } = this;
    return { code, message };
  }
}

class UbntInvalidParamsError extends UbntError {
  constructor(message) {
    super(message, 400);
  }
}

class UbntUnauthorizedError extends UbntError {
  constructor(message) {
    super(message, 401);
  }
}

class UbntForbiddenError extends UbntError {
  constructor(message) {
    super(message, 403);
  }
}

class UbntNotFoundError extends UbntError {
  constructor(message) {
    super(message, 404);
  }
}

class UbntMethodNotAllowedError extends UbntError {
  constructor(message) {
    super(message, 405);
  }
}

class UbntNotAcceptableError extends UbntError {
  constructor(message) {
    super(message, 406);
  }
}

class UbntConflictError extends UbntError {
  constructor(message) {
    super(message, 409);
  }
}

class UbntNoLongerAvailableError extends UbntError {
  constructor(message) {
    super(message, 410);
  }
}

class UbntNotImplementedError extends UbntError {
  constructor(message) {
    super(message, 501);
  }
}

class UbntServiceUnavailableError extends UbntError {
  constructor(message) {
    super(message, 503);
  }
}

module.exports = {
  UbntError,
  UbntInvalidParamsError,
  UbntUnauthorizedError,
  UbntForbiddenError,
  UbntNotFoundError,
  UbntMethodNotAllowedError,
  UbntNotAcceptableError,
  UbntConflictError,
  UbntNoLongerAvailableError,
  UbntNotImplementedError,
  UbntServiceUnavailableError,
};
