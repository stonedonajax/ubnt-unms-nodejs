'use strict';

const { isPlainObj } = require('ramda-adjunct');

const isValidLatitude = latitude => Number.isFinite(latitude) && latitude >= -90 && latitude <= 90;

const isValidLongitude = longitude => Number.isFinite(longitude) && longitude >= -180 && longitude <= 180;

const isValidLocation = location => (
  isPlainObj(location) &&
  isValidLatitude(location.latitude) &&
  isValidLongitude(location.longitude) &&
  (location.latitude !== 0 || location.longitude !== 0) // both can't be zero
);

module.exports = {
  isValidLocation,
};
