/* eslint-disable */
module.exports = /*
 * Generated by PEG.js 0.10.0.
 *
 * http://pegjs.org/
 */
  (function() {
    "use strict";

    function peg$subclass(child, parent) {
      function ctor() { this.constructor = child; }
      ctor.prototype = parent.prototype;
      child.prototype = new ctor();
    }

    function peg$SyntaxError(message, expected, found, location) {
      this.message  = message;
      this.expected = expected;
      this.found    = found;
      this.location = location;
      this.name     = "SyntaxError";

      if (typeof Error.captureStackTrace === "function") {
        Error.captureStackTrace(this, peg$SyntaxError);
      }
    }

    peg$subclass(peg$SyntaxError, Error);

    peg$SyntaxError.buildMessage = function(expected, found) {
      var DESCRIBE_EXPECTATION_FNS = {
        literal: function(expectation) {
          return "\"" + literalEscape(expectation.text) + "\"";
        },

        "class": function(expectation) {
          var escapedParts = "",
            i;

          for (i = 0; i < expectation.parts.length; i++) {
            escapedParts += expectation.parts[i] instanceof Array
              ? classEscape(expectation.parts[i][0]) + "-" + classEscape(expectation.parts[i][1])
              : classEscape(expectation.parts[i]);
          }

          return "[" + (expectation.inverted ? "^" : "") + escapedParts + "]";
        },

        any: function(expectation) {
          return "any character";
        },

        end: function(expectation) {
          return "end of input";
        },

        other: function(expectation) {
          return expectation.description;
        }
      };

      function hex(ch) {
        return ch.charCodeAt(0).toString(16).toUpperCase();
      }

      function literalEscape(s) {
        return s
          .replace(/\\/g, '\\\\')
          .replace(/"/g,  '\\"')
          .replace(/\0/g, '\\0')
          .replace(/\t/g, '\\t')
          .replace(/\n/g, '\\n')
          .replace(/\r/g, '\\r')
          .replace(/[\x00-\x0F]/g,          function(ch) { return '\\x0' + hex(ch); })
          .replace(/[\x10-\x1F\x7F-\x9F]/g, function(ch) { return '\\x'  + hex(ch); });
      }

      function classEscape(s) {
        return s
          .replace(/\\/g, '\\\\')
          .replace(/\]/g, '\\]')
          .replace(/\^/g, '\\^')
          .replace(/-/g,  '\\-')
          .replace(/\0/g, '\\0')
          .replace(/\t/g, '\\t')
          .replace(/\n/g, '\\n')
          .replace(/\r/g, '\\r')
          .replace(/[\x00-\x0F]/g,          function(ch) { return '\\x0' + hex(ch); })
          .replace(/[\x10-\x1F\x7F-\x9F]/g, function(ch) { return '\\x'  + hex(ch); });
      }

      function describeExpectation(expectation) {
        return DESCRIBE_EXPECTATION_FNS[expectation.type](expectation);
      }

      function describeExpected(expected) {
        var descriptions = new Array(expected.length),
          i, j;

        for (i = 0; i < expected.length; i++) {
          descriptions[i] = describeExpectation(expected[i]);
        }

        descriptions.sort();

        if (descriptions.length > 0) {
          for (i = 1, j = 1; i < descriptions.length; i++) {
            if (descriptions[i - 1] !== descriptions[i]) {
              descriptions[j] = descriptions[i];
              j++;
            }
          }
          descriptions.length = j;
        }

        switch (descriptions.length) {
          case 1:
            return descriptions[0];

          case 2:
            return descriptions[0] + " or " + descriptions[1];

          default:
            return descriptions.slice(0, -1).join(", ")
              + ", or "
              + descriptions[descriptions.length - 1];
        }
      }

      function describeFound(found) {
        return found ? "\"" + literalEscape(found) + "\"" : "end of input";
      }

      return "Expected " + describeExpected(expected) + " but " + describeFound(found) + " found.";
    };

    function peg$parse(input, options) {
      options = options !== void 0 ? options : {};

      var peg$FAILED = {},

        peg$startRuleFunctions = { Sections: peg$parseSections },
        peg$startRuleFunction  = peg$parseSections,

        peg$c0 = function(section, sections) { return Object.assign(sections, section) },
        peg$c1 = function(section) { return section },
        peg$c2 = function(section, values) { return { [section]:values } },
        peg$c3 = function(section) { return { [section]:{} } },
        peg$c4 = "[",
        peg$c5 = peg$literalExpectation("[", false),
        peg$c6 = "]",
        peg$c7 = peg$literalExpectation("]", false),
        peg$c8 = function(name) { return name },
        peg$c9 = function(kw, values) { return merge(values, kw) },
        peg$c10 = function(kw) { return kw },
        peg$c11 = "=",
        peg$c12 = peg$literalExpectation("=", false),
        peg$c13 = function(key, value) {
          if (key[0][key[1]] !== null) {
            key[0][key[1]].push(value);
          } else {
            key[0][key[1]] = value;
          }
          return key[0];
        },
        peg$c14 = /^[^\n\r]/,
        peg$c15 = peg$classExpectation(["\n", "\r"], true, false),
        peg$c16 = function(key) { return key[0] },
        peg$c17 = /^[0-9]/,
        peg$c18 = peg$classExpectation([["0", "9"]], false, false),
        peg$c19 = function() { return parseInt(text(), 10) },
        peg$c20 = function() { return text() },
        peg$c21 = "[]",
        peg$c22 = peg$literalExpectation("[]", false),
        peg$c23 = function(part) { return [{ [part]: [] }, part] },
        peg$c24 = function(part) { return [{ [part]: null }, part] },
        peg$c25 = /^[^= \t\n\r[\]]/,
        peg$c26 = peg$classExpectation(["=", " ", "\t", "\n", "\r", "[", "]"], true, false),
        peg$c27 = /^[^\]\n\r]/,
        peg$c28 = peg$classExpectation(["]", "\n", "\r"], true, false),
        peg$c29 = /^[\n\r]/,
        peg$c30 = peg$classExpectation(["\n", "\r"], false, false),
        peg$c31 = peg$otherExpectation("whitespace"),
        peg$c32 = /^[ \t\n\r]/,
        peg$c33 = peg$classExpectation([" ", "\t", "\n", "\r"], false, false),
        peg$c34 = peg$anyExpectation(),

        peg$currPos          = 0,
        peg$savedPos         = 0,
        peg$posDetailsCache  = [{ line: 1, column: 1 }],
        peg$maxFailPos       = 0,
        peg$maxFailExpected  = [],
        peg$silentFails      = 0,

        peg$resultsCache = {},

        peg$result;

      if ("startRule" in options) {
        if (!(options.startRule in peg$startRuleFunctions)) {
          throw new Error("Can't start parsing from rule \"" + options.startRule + "\".");
        }

        peg$startRuleFunction = peg$startRuleFunctions[options.startRule];
      }

      function text() {
        return input.substring(peg$savedPos, peg$currPos);
      }

      function location() {
        return peg$computeLocation(peg$savedPos, peg$currPos);
      }

      function expected(description, location) {
        location = location !== void 0 ? location : peg$computeLocation(peg$savedPos, peg$currPos)

        throw peg$buildStructuredError(
          [peg$otherExpectation(description)],
          input.substring(peg$savedPos, peg$currPos),
          location
        );
      }

      function error(message, location) {
        location = location !== void 0 ? location : peg$computeLocation(peg$savedPos, peg$currPos)

        throw peg$buildSimpleError(message, location);
      }

      function peg$literalExpectation(text, ignoreCase) {
        return { type: "literal", text: text, ignoreCase: ignoreCase };
      }

      function peg$classExpectation(parts, inverted, ignoreCase) {
        return { type: "class", parts: parts, inverted: inverted, ignoreCase: ignoreCase };
      }

      function peg$anyExpectation() {
        return { type: "any" };
      }

      function peg$endExpectation() {
        return { type: "end" };
      }

      function peg$otherExpectation(description) {
        return { type: "other", description: description };
      }

      function peg$computePosDetails(pos) {
        var details = peg$posDetailsCache[pos], p;

        if (details) {
          return details;
        } else {
          p = pos - 1;
          while (!peg$posDetailsCache[p]) {
            p--;
          }

          details = peg$posDetailsCache[p];
          details = {
            line:   details.line,
            column: details.column
          };

          while (p < pos) {
            if (input.charCodeAt(p) === 10) {
              details.line++;
              details.column = 1;
            } else {
              details.column++;
            }

            p++;
          }

          peg$posDetailsCache[pos] = details;
          return details;
        }
      }

      function peg$computeLocation(startPos, endPos) {
        var startPosDetails = peg$computePosDetails(startPos),
          endPosDetails   = peg$computePosDetails(endPos);

        return {
          start: {
            offset: startPos,
            line:   startPosDetails.line,
            column: startPosDetails.column
          },
          end: {
            offset: endPos,
            line:   endPosDetails.line,
            column: endPosDetails.column
          }
        };
      }

      function peg$fail(expected) {
        if (peg$currPos < peg$maxFailPos) { return; }

        if (peg$currPos > peg$maxFailPos) {
          peg$maxFailPos = peg$currPos;
          peg$maxFailExpected = [];
        }

        peg$maxFailExpected.push(expected);
      }

      function peg$buildSimpleError(message, location) {
        return new peg$SyntaxError(message, null, null, location);
      }

      function peg$buildStructuredError(expected, found, location) {
        return new peg$SyntaxError(
          peg$SyntaxError.buildMessage(expected, found),
          expected,
          found,
          location
        );
      }

      function peg$parseSections() {
        var s0, s1, s2;

        var key    = peg$currPos * 12 + 0,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        s0 = peg$currPos;
        s1 = peg$parseSection();
        if (s1 !== peg$FAILED) {
          s2 = peg$parseSections();
          if (s2 !== peg$FAILED) {
            peg$savedPos = s0;
            s1 = peg$c0(s1, s2);
            s0 = s1;
          } else {
            peg$currPos = s0;
            s0 = peg$FAILED;
          }
        } else {
          peg$currPos = s0;
          s0 = peg$FAILED;
        }
        if (s0 === peg$FAILED) {
          s0 = peg$currPos;
          s1 = peg$parseSection();
          if (s1 !== peg$FAILED) {
            peg$savedPos = s0;
            s1 = peg$c1(s1);
          }
          s0 = s1;
        }

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parseSection() {
        var s0, s1, s2;

        var key    = peg$currPos * 12 + 1,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        s0 = peg$currPos;
        s1 = peg$parseSectionHeader();
        if (s1 !== peg$FAILED) {
          s2 = peg$parseKeyValues();
          if (s2 !== peg$FAILED) {
            peg$savedPos = s0;
            s1 = peg$c2(s1, s2);
            s0 = s1;
          } else {
            peg$currPos = s0;
            s0 = peg$FAILED;
          }
        } else {
          peg$currPos = s0;
          s0 = peg$FAILED;
        }
        if (s0 === peg$FAILED) {
          s0 = peg$currPos;
          s1 = peg$parseSectionHeader();
          if (s1 !== peg$FAILED) {
            peg$savedPos = s0;
            s1 = peg$c3(s1);
          }
          s0 = s1;
        }

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parseSectionHeader() {
        var s0, s1, s2, s3, s4, s5;

        var key    = peg$currPos * 12 + 2,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        s0 = peg$currPos;
        s1 = peg$parse_();
        if (s1 !== peg$FAILED) {
          if (input.charCodeAt(peg$currPos) === 91) {
            s2 = peg$c4;
            peg$currPos++;
          } else {
            s2 = peg$FAILED;
            if (peg$silentFails === 0) { peg$fail(peg$c5); }
          }
          if (s2 !== peg$FAILED) {
            s3 = peg$parseheader();
            if (s3 !== peg$FAILED) {
              if (input.charCodeAt(peg$currPos) === 93) {
                s4 = peg$c6;
                peg$currPos++;
              } else {
                s4 = peg$FAILED;
                if (peg$silentFails === 0) { peg$fail(peg$c7); }
              }
              if (s4 !== peg$FAILED) {
                s5 = peg$parse_();
                if (s5 !== peg$FAILED) {
                  peg$savedPos = s0;
                  s1 = peg$c8(s3);
                  s0 = s1;
                } else {
                  peg$currPos = s0;
                  s0 = peg$FAILED;
                }
              } else {
                peg$currPos = s0;
                s0 = peg$FAILED;
              }
            } else {
              peg$currPos = s0;
              s0 = peg$FAILED;
            }
          } else {
            peg$currPos = s0;
            s0 = peg$FAILED;
          }
        } else {
          peg$currPos = s0;
          s0 = peg$FAILED;
        }

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parseKeyValues() {
        var s0, s1, s2, s3, s4;

        var key    = peg$currPos * 12 + 3,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        s0 = peg$currPos;
        s1 = peg$parseKeyValue();
        if (s1 !== peg$FAILED) {
          s2 = peg$parsenewline();
          if (s2 !== peg$FAILED) {
            s3 = peg$parse_();
            if (s3 !== peg$FAILED) {
              s4 = peg$parseKeyValues();
              if (s4 !== peg$FAILED) {
                peg$savedPos = s0;
                s1 = peg$c9(s1, s4);
                s0 = s1;
              } else {
                peg$currPos = s0;
                s0 = peg$FAILED;
              }
            } else {
              peg$currPos = s0;
              s0 = peg$FAILED;
            }
          } else {
            peg$currPos = s0;
            s0 = peg$FAILED;
          }
        } else {
          peg$currPos = s0;
          s0 = peg$FAILED;
        }
        if (s0 === peg$FAILED) {
          s0 = peg$currPos;
          s1 = peg$parseKeyValue();
          if (s1 !== peg$FAILED) {
            s2 = peg$parsenewline();
            if (s2 === peg$FAILED) {
              s2 = null;
            }
            if (s2 !== peg$FAILED) {
              s3 = peg$parse_();
              if (s3 !== peg$FAILED) {
                peg$savedPos = s0;
                s1 = peg$c10(s1);
                s0 = s1;
              } else {
                peg$currPos = s0;
                s0 = peg$FAILED;
              }
            } else {
              peg$currPos = s0;
              s0 = peg$FAILED;
            }
          } else {
            peg$currPos = s0;
            s0 = peg$FAILED;
          }
        }

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parseKeyValue() {
        var s0, s1, s2, s3, s4, s5;

        var key    = peg$currPos * 12 + 4,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        s0 = peg$currPos;
        s1 = peg$parseKey();
        if (s1 !== peg$FAILED) {
          if (input.charCodeAt(peg$currPos) === 61) {
            s2 = peg$c11;
            peg$currPos++;
          } else {
            s2 = peg$FAILED;
            if (peg$silentFails === 0) { peg$fail(peg$c12); }
          }
          if (s2 !== peg$FAILED) {
            s3 = peg$parseValue();
            if (s3 !== peg$FAILED) {
              peg$savedPos = s0;
              s1 = peg$c13(s1, s3);
              s0 = s1;
            } else {
              peg$currPos = s0;
              s0 = peg$FAILED;
            }
          } else {
            peg$currPos = s0;
            s0 = peg$FAILED;
          }
        } else {
          peg$currPos = s0;
          s0 = peg$FAILED;
        }
        if (s0 === peg$FAILED) {
          s0 = peg$currPos;
          s1 = peg$parseKey();
          if (s1 !== peg$FAILED) {
            if (input.charCodeAt(peg$currPos) === 61) {
              s2 = peg$c11;
              peg$currPos++;
            } else {
              s2 = peg$FAILED;
              if (peg$silentFails === 0) { peg$fail(peg$c12); }
            }
            if (s2 === peg$FAILED) {
              s2 = null;
            }
            if (s2 !== peg$FAILED) {
              s3 = [];
              if (peg$c14.test(input.charAt(peg$currPos))) {
                s4 = input.charAt(peg$currPos);
                peg$currPos++;
              } else {
                s4 = peg$FAILED;
                if (peg$silentFails === 0) { peg$fail(peg$c15); }
              }
              while (s4 !== peg$FAILED) {
                s3.push(s4);
                if (peg$c14.test(input.charAt(peg$currPos))) {
                  s4 = input.charAt(peg$currPos);
                  peg$currPos++;
                } else {
                  s4 = peg$FAILED;
                  if (peg$silentFails === 0) { peg$fail(peg$c15); }
                }
              }
              if (s3 !== peg$FAILED) {
                s4 = peg$currPos;
                peg$silentFails++;
                s5 = peg$parsenewline();
                peg$silentFails--;
                if (s5 !== peg$FAILED) {
                  peg$currPos = s4;
                  s4 = void 0;
                } else {
                  s4 = peg$FAILED;
                }
                if (s4 !== peg$FAILED) {
                  peg$savedPos = s0;
                  s1 = peg$c16(s1);
                  s0 = s1;
                } else {
                  peg$currPos = s0;
                  s0 = peg$FAILED;
                }
              } else {
                peg$currPos = s0;
                s0 = peg$FAILED;
              }
            } else {
              peg$currPos = s0;
              s0 = peg$FAILED;
            }
          } else {
            peg$currPos = s0;
            s0 = peg$FAILED;
          }
        }

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parseValue() {
        var s0, s1, s2, s3;

        var key    = peg$currPos * 12 + 5,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        s0 = peg$currPos;
        s1 = [];
        if (peg$c17.test(input.charAt(peg$currPos))) {
          s2 = input.charAt(peg$currPos);
          peg$currPos++;
        } else {
          s2 = peg$FAILED;
          if (peg$silentFails === 0) { peg$fail(peg$c18); }
        }
        if (s2 !== peg$FAILED) {
          while (s2 !== peg$FAILED) {
            s1.push(s2);
            if (peg$c17.test(input.charAt(peg$currPos))) {
              s2 = input.charAt(peg$currPos);
              peg$currPos++;
            } else {
              s2 = peg$FAILED;
              if (peg$silentFails === 0) { peg$fail(peg$c18); }
            }
          }
        } else {
          s1 = peg$FAILED;
        }
        if (s1 !== peg$FAILED) {
          s2 = peg$currPos;
          peg$silentFails++;
          s3 = peg$parsenewline();
          peg$silentFails--;
          if (s3 !== peg$FAILED) {
            peg$currPos = s2;
            s2 = void 0;
          } else {
            s2 = peg$FAILED;
          }
          if (s2 !== peg$FAILED) {
            peg$savedPos = s0;
            s1 = peg$c19();
            s0 = s1;
          } else {
            peg$currPos = s0;
            s0 = peg$FAILED;
          }
        } else {
          peg$currPos = s0;
          s0 = peg$FAILED;
        }
        if (s0 === peg$FAILED) {
          s0 = peg$currPos;
          s1 = [];
          if (peg$c14.test(input.charAt(peg$currPos))) {
            s2 = input.charAt(peg$currPos);
            peg$currPos++;
          } else {
            s2 = peg$FAILED;
            if (peg$silentFails === 0) { peg$fail(peg$c15); }
          }
          if (s2 !== peg$FAILED) {
            while (s2 !== peg$FAILED) {
              s1.push(s2);
              if (peg$c14.test(input.charAt(peg$currPos))) {
                s2 = input.charAt(peg$currPos);
                peg$currPos++;
              } else {
                s2 = peg$FAILED;
                if (peg$silentFails === 0) { peg$fail(peg$c15); }
              }
            }
          } else {
            s1 = peg$FAILED;
          }
          if (s1 !== peg$FAILED) {
            peg$savedPos = s0;
            s1 = peg$c20();
          }
          s0 = s1;
        }

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parseKey() {
        var s0, s1, s2;

        var key    = peg$currPos * 12 + 6,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        s0 = peg$currPos;
        s1 = peg$parsepart();
        if (s1 !== peg$FAILED) {
          if (input.substr(peg$currPos, 2) === peg$c21) {
            s2 = peg$c21;
            peg$currPos += 2;
          } else {
            s2 = peg$FAILED;
            if (peg$silentFails === 0) { peg$fail(peg$c22); }
          }
          if (s2 !== peg$FAILED) {
            peg$savedPos = s0;
            s1 = peg$c23(s1);
            s0 = s1;
          } else {
            peg$currPos = s0;
            s0 = peg$FAILED;
          }
        } else {
          peg$currPos = s0;
          s0 = peg$FAILED;
        }
        if (s0 === peg$FAILED) {
          s0 = peg$currPos;
          s1 = peg$parsepart();
          if (s1 !== peg$FAILED) {
            peg$savedPos = s0;
            s1 = peg$c24(s1);
          }
          s0 = s1;
        }

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parsepart() {
        var s0, s1, s2;

        var key    = peg$currPos * 12 + 7,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        s0 = peg$currPos;
        s1 = [];
        if (peg$c25.test(input.charAt(peg$currPos))) {
          s2 = input.charAt(peg$currPos);
          peg$currPos++;
        } else {
          s2 = peg$FAILED;
          if (peg$silentFails === 0) { peg$fail(peg$c26); }
        }
        if (s2 !== peg$FAILED) {
          while (s2 !== peg$FAILED) {
            s1.push(s2);
            if (peg$c25.test(input.charAt(peg$currPos))) {
              s2 = input.charAt(peg$currPos);
              peg$currPos++;
            } else {
              s2 = peg$FAILED;
              if (peg$silentFails === 0) { peg$fail(peg$c26); }
            }
          }
        } else {
          s1 = peg$FAILED;
        }
        if (s1 !== peg$FAILED) {
          peg$savedPos = s0;
          s1 = peg$c20();
        }
        s0 = s1;

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parseheader() {
        var s0, s1, s2;

        var key    = peg$currPos * 12 + 8,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        s0 = peg$currPos;
        s1 = [];
        if (peg$c27.test(input.charAt(peg$currPos))) {
          s2 = input.charAt(peg$currPos);
          peg$currPos++;
        } else {
          s2 = peg$FAILED;
          if (peg$silentFails === 0) { peg$fail(peg$c28); }
        }
        if (s2 !== peg$FAILED) {
          while (s2 !== peg$FAILED) {
            s1.push(s2);
            if (peg$c27.test(input.charAt(peg$currPos))) {
              s2 = input.charAt(peg$currPos);
              peg$currPos++;
            } else {
              s2 = peg$FAILED;
              if (peg$silentFails === 0) { peg$fail(peg$c28); }
            }
          }
        } else {
          s1 = peg$FAILED;
        }
        if (s1 !== peg$FAILED) {
          peg$savedPos = s0;
          s1 = peg$c20();
        }
        s0 = s1;

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parsenewline() {
        var s0;

        var key    = peg$currPos * 12 + 9,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        if (peg$c29.test(input.charAt(peg$currPos))) {
          s0 = input.charAt(peg$currPos);
          peg$currPos++;
        } else {
          s0 = peg$FAILED;
          if (peg$silentFails === 0) { peg$fail(peg$c30); }
        }
        if (s0 === peg$FAILED) {
          s0 = peg$parseEOF();
        }

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parse_() {
        var s0, s1;

        var key    = peg$currPos * 12 + 10,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        peg$silentFails++;
        s0 = [];
        if (peg$c32.test(input.charAt(peg$currPos))) {
          s1 = input.charAt(peg$currPos);
          peg$currPos++;
        } else {
          s1 = peg$FAILED;
          if (peg$silentFails === 0) { peg$fail(peg$c33); }
        }
        while (s1 !== peg$FAILED) {
          s0.push(s1);
          if (peg$c32.test(input.charAt(peg$currPos))) {
            s1 = input.charAt(peg$currPos);
            peg$currPos++;
          } else {
            s1 = peg$FAILED;
            if (peg$silentFails === 0) { peg$fail(peg$c33); }
          }
        }
        peg$silentFails--;
        if (s0 === peg$FAILED) {
          s1 = peg$FAILED;
          if (peg$silentFails === 0) { peg$fail(peg$c31); }
        }

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }

      function peg$parseEOF() {
        var s0, s1;

        var key    = peg$currPos * 12 + 11,
          cached = peg$resultsCache[key];

        if (cached) {
          peg$currPos = cached.nextPos;

          return cached.result;
        }

        s0 = peg$currPos;
        peg$silentFails++;
        if (input.length > peg$currPos) {
          s1 = input.charAt(peg$currPos);
          peg$currPos++;
        } else {
          s1 = peg$FAILED;
          if (peg$silentFails === 0) { peg$fail(peg$c34); }
        }
        peg$silentFails--;
        if (s1 === peg$FAILED) {
          s0 = void 0;
        } else {
          peg$currPos = s0;
          s0 = peg$FAILED;
        }

        peg$resultsCache[key] = { nextPos: peg$currPos, result: s0 };

        return s0;
      }


      function merge(base, extend) {
        if (typeof(base) !== 'object')
          return extend;
        if (Array.isArray(base) && Array.isArray(extend)) {
          base.push(...extend);
          return base;
        }
        for (var key in extend) {
          if (typeof(base[key]) === 'object' && typeof(extend[key]) === 'object') {
            base[key] = merge(base[key], extend[key]);
          } else {
            base[key] = extend[key];
          }
        }

        return base;
      }


      peg$result = peg$startRuleFunction();

      if (peg$result !== peg$FAILED && peg$currPos === input.length) {
        return peg$result;
      } else {
        if (peg$result !== peg$FAILED && peg$currPos < input.length) {
          peg$fail(peg$endExpectation());
        }

        throw peg$buildStructuredError(
          peg$maxFailExpected,
          peg$maxFailPos < input.length ? input.charAt(peg$maxFailPos) : null,
          peg$maxFailPos < input.length
            ? peg$computeLocation(peg$maxFailPos, peg$maxFailPos + 1)
            : peg$computeLocation(peg$maxFailPos, peg$maxFailPos)
        );
      }
    }

    return {
      SyntaxError: peg$SyntaxError,
      parse:       peg$parse
    };
  })();
