'use strict';

const { isIP, isInt } = require('validator');
const { partial, __, parseInt } = require('lodash/fp');

const isPositiveInt = partial(isInt, [__, { min: 0 }]);

const isCidr = (mayBeCidr) => {
  const cidrParts = mayBeCidr.split('/');

  if (cidrParts.length !== 2 || !isPositiveInt(cidrParts[1])) {
    return false;
  }

  const address = cidrParts[0];
  const suffix = parseInt(cidrParts[1], 10);

  return (isIP(address, 4) && suffix <= 32) || (isIP(address, 6) && suffix <= 128);
};

module.exports = {
  isCidr,
};
