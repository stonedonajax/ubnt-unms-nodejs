'use strict';

const { registerPlugin } = require('../util/hapi');
const { registerRoutes } = require('./routes');
const { UbntError } = require('../util/errors');

function register(server, options) {
  const serverContext = {
    updateVersionOverride: null,
  };
  server.bind(serverContext);

  const service = {
    getUpdateVersionOverride: () => serverContext.updateVersionOverride,
  };
  server.expose(service);

  server.ext('onPreResponse', (request, reply) => {
    const response = request.response;
    if (response instanceof UbntError) { // if not error then continue
      return reply(response.toBoom());
    }
    return reply.continue();
  });

  registerRoutes(server, options);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'web',
  version: '1.0.0',
  dependencies: ['views'],
};

