# Notifications WS API

## Message Structure

- `type`: deviceNotification/unmsNotification
- `entity`: ID for deviceNotification or entity name for UNMS notification
- `action`: subscribe/unsubscribe
- `payload`: arbitrary data passed to WS_API adapter