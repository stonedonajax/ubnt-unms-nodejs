'use strict';

const { Server: WebSocketServer } = require('uws');
const { Observable } = require('rxjs/Rx');
const { weave } = require('ramda-adjunct');

const { registerPlugin } = require('../util/hapi');
const { verifyClient } = require('./verify');
const { wsApiPort } = require('../../config');
const { toMs } = require('../util');
const state = require('./ws-state')();
const { parseTokenFromReq } = require('./transformers/parsers');

const {
  claimConnectionToken, clearToken, connectionTokenExists,
  createWsApiConnection, handleSubscription, registerHandler,
} = require('./service');


/**
 * Special event handler, because ws is not an EventEmitter
 * @param {WebSocket} ws
 * @param {string} eventName
 * @return {Observable.<*>}
 */
const fromWebSocketEvent = (ws, eventName) => Observable.fromEventPattern(
  (handler) => { ws.on(eventName, handler) },
  (handler) => { ws.removeListener(eventName, handler) }
);

const startServer = (server, wsServer) => {
  const { logging, notificationHub } = server.plugins;

  return fromWebSocketEvent(wsServer, 'connection')
    .mergeMap((connection) => {
      const connectionId = parseTokenFromReq(connection.upgradeReq);

      logging.log(
        ['info', 'ws-api', 'connection'],
        `Websocket notification connection: ${connectionId}`
      );

      return notificationHub.createExclusiveQueue(connectionId)
        .map(JSON.stringify)
        .do(msg => connection.send(msg))
        .takeUntil(fromWebSocketEvent(connection, 'close'));
    });
};

/*
 * Hapi plugin definition
 */
function register(server) {
  const { logging } = server.plugins;

  const wsServer = new WebSocketServer({
    port: wsApiPort,
    verifyClient: (options, callback) => verifyClient(state, options, logging, callback),
    noDelay: true,
  });

  Observable.fromEvent(server, 'start')
    .mergeMap(() => startServer(server, wsServer))
    .takeUntil(Observable.fromEvent(server, 'stop'))
    .subscribe();

  wsServer.startAutoPing(toMs('seconds', 30));

  const service = {
    claimConnectionToken: weave(claimConnectionToken, { state }),
    clearToken: weave(clearToken, { state }),
    connectionTokenExists: weave(connectionTokenExists, { state }),
    createWsApiConnection: weave(createWsApiConnection, { state }),
    handleSubscription: weave(handleSubscription, { state }),
    registerHandler: weave(registerHandler, { state }),
  };

  server.expose(service);
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'socketApi',
  version: '1.0.0',
  dependencies: ['settings', 'logging', 'notificationHub'],
};
