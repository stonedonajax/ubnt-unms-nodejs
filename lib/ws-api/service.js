'use strict';

const { Reader: reader } = require('monet');
const uuid = require('aguid');

const createWsApiConnection = userId => reader(
  ({ state }) => {
    const connectionToken = uuid();

    state.storeToken(userId, connectionToken);

    return connectionToken;
  }
);

const claimConnectionToken = token => reader(
  ({ state }) => state.claimToken(token)
);

const connectionTokenExists = token => reader(
  ({ state }) => state.hasToken(token)
);

const clearToken = token => reader(
  ({ state }) => state.clearToken(token)
);

const registerHandler = (type, handler) => reader(
  ({ state }) => state.registerHandler(type, handler)
);

const handleSubscription = options => reader(
  ({ state }) => state.notifyHandlers(options)
);

module.exports = {
  createWsApiConnection,
  claimConnectionToken,
  connectionTokenExists,
  clearToken,
  registerHandler,
  handleSubscription,
};
