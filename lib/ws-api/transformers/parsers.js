'use strict';

const { nth, pipe, path } = require('ramda');
const parseurl = require('parseurl');
const { parse: qsParse } = require('querystring');


/**
 * @param {Request} request
 * @returns {String}
 */
const parseTokenFromReq = pipe(
  parseurl,
  path(['query']),
  qsParse,
  path(['connectionId'])
);

  /**
   * @param {Request} request
   * @returns {String}
   */
const parseUrlFromReq = pipe(
  parseurl,
  parsedUrl => parsedUrl.pathname.split('/')
);

const parseDeviceId = pipe(
  parseUrlFromReq,
  nth(2)
);


module.exports = {
  parseTokenFromReq,
  parseUrlFromReq,
  parseDeviceId,
};
