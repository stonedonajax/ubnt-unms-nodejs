'use strict';

const { parseTokenFromReq } = require('./transformers/parsers');


const verifyClient = (state, options, logging, callback) => {
  const token = parseTokenFromReq(options.req);

  try {
    state.claimToken(token);
    callback(true);
  } catch (e) {
    logging.log(
      ['info', 'ws-shell', 'internal'],
      `Connection to websocket client failed: [401] ${e.message}`
    );
    callback(false, 401, e.message);
  }
};


module.exports = {
  verifyClient,
};
