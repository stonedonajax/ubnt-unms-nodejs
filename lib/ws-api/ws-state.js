'use strict';

const { Observable } = require('rxjs/Rx');

const { has, not, pathEq, append, T, cond } = require('ramda');
const { UbntNotAcceptableError, UbntNotFoundError, UbntNoLongerAvailableError } = require('../util/errors');

class WsState {
  constructor() {
    this.tokens = {};
    this.handlers = [];
  }

  storeToken(userId, token) {
    this.tokens[token] = { userId, claimed: false };
    return this;
  }

  claimToken(token) {
    if (not(has(token, this.tokens))) {
      throw new UbntNotFoundError('Token not found');
    } else if (this.tokens[token].claimed) {
      throw new UbntNoLongerAvailableError('Token has been used');
    }

    this.tokens[token].claimed = true;
    return this;
  }

  hasToken(token) {
    return has(token, this.tokens);
  }

  clearToken(token) {
    delete this.tokens[token];
    return this;
  }

  registerHandler(type, handler) {
    this.handlers.push([pathEq(['type'], type), handler]);
    return this;
  }

  notifyHandlers(options) {
    const conditions = append(
      [T, () => Observable.throw(new UbntNotAcceptableError())],
      this.handlers
    );

    return cond(conditions)(options);
  }
}

const createWsState = () => new WsState();

module.exports = createWsState;
