'use strict';

const { Observable } = require('rxjs/Rx');
const { curry } = require('ramda');
const WebSocketShellConnection = require('./connection');


const createConnection = (ws, deviceId, connections) => () => new WebSocketShellConnection(ws, deviceId, connections);

const setupConnection = curry((connections, connection) => {
  connections.set(connection.deviceId, connection);
  connection.close$.connect();
  return Observable.merge(Observable.of(connection), connection.error$)
    .takeUntil(connection.close$);
});


module.exports = {
  createConnection,
  setupConnection,
};
