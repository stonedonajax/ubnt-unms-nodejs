'use strict';

const { Observable } = require('rxjs/Rx');


/**
 * Special event handler, because ws is not an EventEmitter
 * @param {WebSocket} ws
 * @param {string} eventName
 * @return {Observable.<*>}
 */
const fromWebSocketEvent = (ws, eventName) => Observable.fromEventPattern(
  (handler) => { ws.on(eventName, handler) },
  (handler) => { ws.removeListener(eventName, handler) }
);

class WebSocketShellConnection {
  /**
   * @param {WebSocket} ws
   * @param {string} deviceId
   * @param {Map} connections
   */
  constructor(ws, deviceId, connections) {
    this.deviceId = deviceId;
    this.ws = ws;

    this.messages$ = fromWebSocketEvent(ws, 'message').map(msg => msg.replace(/\r/g, '\n')).share();
    this.close$ = fromWebSocketEvent(ws, 'close').take(1)
        .do(() => connections.delete(this.deviceId))
        .publishLast();
    this.error$ = fromWebSocketEvent(ws, 'error').mergeMap(Observable.throw);

    this.sendToSocket = Observable.bindNodeCallback(ws.send.bind(ws));
  }

  close() {
    this.ws.close();
    return this.close$;
  }

  unsubscribe() {
    this.ws.close();
  }
}


module.exports = WebSocketShellConnection;
