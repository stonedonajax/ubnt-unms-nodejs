'use strict';

const { Observable } = require('rxjs');
const { Reader: reader } = require('monet');

require('../util/observable');

/**
 * @param {WebSocketShellConnection} connection
 * @return {Observable}
 */
const attachToEvents = connection => reader(
  ({ deviceStore }) => {
    const commDevice = deviceStore.get(connection.deviceId);

    if (commDevice === null) {
      return Observable.empty();
    }

    const connect$ = commDevice.shellConnect();

    const disconnect$ = connection.close$.mergeMap(() => commDevice.shellDisconnect()).publish();

    const input$ = connection.messages$.mergeMap(m => commDevice.shellCmd(m));

    const output$ = commDevice.shellResponse()
      .map(data => data.replace(/\n/g, '\r\n'))
      .mergeMap(deviceMessage => connection.sendToSocket(deviceMessage));

    disconnect$.connect();
    return Observable.merge(connect$, disconnect$, input$, output$)
      .takeUntil(commDevice.connection.close$);
  });


module.exports = {
  attachToEvents,
};
