'use strict';

const { Server: WebSocketServer } = require('uws');
const { Observable } = require('rxjs/Rx');
const { weave, isNotUndefined } = require('ramda-adjunct');

const { registerPlugin } = require('../util/hapi');
const { attachToEvents } = require('./events');
const { verifyClient } = require('./verify');
const { wsShellPort } = require('../../config');
const { toMs } = require('../util');
const { createConnection, setupConnection } = require('./connection-factory');


const startServer = (server, wsServer, connections) => {
  const { logging, deviceStore } = server.plugins;

  const attachToEventsBound = weave(attachToEvents, { logging, deviceStore });

  return Observable.fromEvent(wsServer, 'connection')
    .groupBy(ws => ws.upgradeReq.deviceId)
    .mergeMap(group => group
      .mergeMap((ws) => {
        const deviceId = ws.upgradeReq.deviceId;
        const connection = connections.get(deviceId);

        return isNotUndefined(connection) ?
        connection.close().mapTo({ ws, deviceId }) :
        Observable.of({ ws, deviceId });
      })
      .switchMap(({ ws, deviceId }) =>
        Observable.using(createConnection(ws, deviceId, connections), setupConnection(connections))
          .mergeMap(connection => attachToEventsBound(connection))
          .catch((error) => {
            if (error) {
              logging.error('Unexpected connection error', error);
            }
            return Observable.empty(); // don't stop server on error
          })
    ));
};

/*
 * Hapi plugin definition
 */
function register(server) {
  const { logging } = server.plugins;
  const connections = new Map();

  const wsServer = new WebSocketServer({
    port: wsShellPort,
    verifyClient: (options, callback) => verifyClient(server, connections, options, logging, callback),
    noDelay: true,
  });

  Observable.fromEvent(server, 'start')
    .mergeMap(() => startServer(server, wsServer, connections))
    .takeUntil(Observable.fromEvent(server, 'stop'))
    .subscribe();

  wsServer.startAutoPing(toMs('seconds', 30));
}

exports.register = registerPlugin(register);
exports.register.attributes = {
  name: 'socketShell',
  version: '1.0.0',
  dependencies: ['settings', 'deviceStore', 'logging'],
};
