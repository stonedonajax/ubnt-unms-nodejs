'use strict';

const { nth, pipe, equals, path } = require('ramda');
const parseurl = require('parseurl');
const { parse: qsParse } = require('querystring');


const parseForceConnectFromReq = pipe(
  parseurl,
  path(['query']),
  qsParse,
  path(['forceConnect']),
  equals('true')
);

/**
 * @param {Request} request
 * @returns {String}
 */
const parseTokenFromReq = pipe(
  parseurl,
  path(['query']),
  qsParse,
  path(['token'])
);

  /**
   * @param {Request} request
   * @returns {String}
   */
const parseUrlFromReq = pipe(
  parseurl,
  parsedUrl => parsedUrl.pathname.split('/')
);

const parseDeviceId = pipe(
  parseUrlFromReq,
  nth(2)
);


module.exports = {
  parseForceConnectFromReq,
  parseTokenFromReq,
  parseUrlFromReq,
  parseDeviceId,
};
