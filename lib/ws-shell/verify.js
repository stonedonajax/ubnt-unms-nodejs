'use strict';

const { OK, CONFLICT } = require('http-status');
const httpMessages = require('http-status');
const { ifElse, equals, pipe, pathOr, tap } = require('ramda');

const { parseTokenFromReq, parseForceConnectFromReq, parseDeviceId } = require('./transformers/parsers');


const verifyClient = (server, connections, options, logging, callback) => server.inject({
  method: 'POST',
  url: '/v2.1/ws/restricted',
  allowInternals: true,
  headers: { 'x-auth-token': parseTokenFromReq(options.req) },
})
  .then(ifElse(
    pipe(pathOr(null, ['statusCode']), equals(OK)),
    () => {
      const force = parseForceConnectFromReq(options.req);
      const deviceId = parseDeviceId(options.req);
      options.req.deviceId = deviceId; // eslint-disable-line no-param-reassign

      if (connections.has(deviceId) && !force) {
        callback(false, CONFLICT, httpMessages[CONFLICT]);
        return;
      }
      callback(true);
    },
    pipe(
      tap(res => logging.log(
        ['info', 'ws-shell', 'internal'],
        `Connection to websocket client failed: [${res.statusCode}] ${res.statusMessage}`)
      ),
      res => callback(false, res.statusCode, res.statusMessage)
    )
  ));


module.exports = {
  verifyClient,
};
