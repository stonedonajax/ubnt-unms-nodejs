'use strict';

const { defaultTo } = require('lodash/fp');
const semver = require('semver');

const { MessageNameEnum } = require('../../transformers/socket/enums');

const ZERO_VERSION = semver.parse('0.0.0');

/**
 * Inject udapi-bridge version to all messages
 */
class VersionMiddleware {
  constructor() {
    this.version = ZERO_VERSION;
  }

  handleIncoming(incomingMessage) {
    switch (incomingMessage.name) {
      case MessageNameEnum.Connect:
        this.version = defaultTo(this.version, incomingMessage.data.udapi);
        break;
      default:
        incomingMessage.udapi = this.version; // eslint-disable-line no-param-reassign
    }

    return incomingMessage;
  }
}

const createMiddleware = () => new VersionMiddleware();

module.exports = createMiddleware;
